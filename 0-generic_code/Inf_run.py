#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 28 15:56:26 2021

@author: user
"""


#%%
import time 
import random as rd
import numpy as np
import numpy.random as npr
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import scipy.sparse as sp


#%%
#Propa propagation of infection at each step

#constant parameters :
#KA, KB, nuA, nuB

#parameters evolving during the iterations
#tC : current time of the iteration
#T : provides the memory of the infectious times for each individual
#i : array of indices among the cities of the one entering lockdown at the iteration
#IA_o : boolean array indicating among the cities the ones infected but still not under lockdown at time t_0
#R : cities now resistant (including the infecting one)
#Sh : "remaining exponential shield" providing a protection without memory against infection
    
#R = (Sh == 0) (( or (Sh =<0) ))

#%%
def Propa(tC, T, i, IA_o, R, Sh, KA, KB, nuA, nuB):
    Sh[R] = 0
    #erosion of the shields of any susceptible city
    KAi = np.sum(KA[i])
    KBi = np.sum(KB[i])
    ER = KAi* nuA[~R] + KBi* nuB[~R]
    Sh[~R] = Sh[~R] - ER 
    #new infected are those for which Sh < 0 -->
    IA = (Sh < 0)
    T[IA] = tC
    R[IA] = True
    Sh[IA] = 0
    IA_n = IA_o
    IA_n[i] = False
    IA_n[IA] = True
    return T, IA_n, R, Sh

    # IA_n = np.delete(IA_o, IA_o == i)
    # IA_n = np.append(IA_n, IA)
    #return T, IA_n, R, Sh
#%% 
# Ife_U is designed to pursue existing iterations until a final time t_f,
#starting at time t_0 with current state T, IA, R, Sh

#constant parameters :
#KA, KB, nuA, nuB
#TL : period between infection to lockdown 

#parameters evolving during the iterations
#T : provides the memory of the infectious times for each individual
#IA : boolean array indicating among the cities the ones infected but still not under lockdown at time t_0
#R : cities now resistant (including the infecting one)
#Sh : "remaining exponential shield" providing a protection without memory against infection
    
#R = (Sh == 0) (( or (Sh =<0) ))

#%%

def Ife_U(t_f, t_0, T, IA, R, Sh, KA, KB, nuA, nuB, TL):
    t_C = t_0
    while t_C < t_f and np.sum(IA)>0 :
        #next event ?
        t_C = np.min(T[IA] + TL[IA])
        idx = (T[IA] + TL[IA] == t_C)
        i = np.zeros(IA.size, dtype = bool)
        i[IA] = idx
        T, IA, R, Sh = Propa(t_C, T, i, IA, R, Sh, KA, KB, nuA, nuB)
    return T, IA, R, Sh

    #while t_C < t_f and np.size(IA)>0:
        #next event ?
        #t_C, idx = min((val, idx) for (idx, val) in enumerate(T[IA] + TL[IA]))
        #i = IA[idx]
#%%
#preceding function in a starting case
#with a simplified description of the initial condition
#A0 as a sparse matrix N x 1
#I chose the starting day to be 1 to avoid sparsity issues

def InfectionI(t_f, IA, KA, KB, nuA, nuB, TL):
    #initialisation of time
    T = np.zeros((np.size(KA)))
    #starting day at one
    T[IA] = 1
    #
    Sh = npr.exponential(1, size = np.size(KA))
    Sh[IA] = 0
    #
    R = np.zeros(np.size(KA), dtype = bool)
    R[IA] = 1
    return Ife_U(t_f, 1, T, IA, R, Sh, KA, KB, nuA, nuB, TL)

#%%
#adaptation of the preceding function to the case 
#where IA consists of a uniform sample of a city according to nuA
#where restart is automatic
#as long as the size threshold NI is not reached while IA is empty

def InfectionR(t_f, NI, KA, KB, nuA, nuB, TL):
    NC = np.size(KA)
    IAf = np.zeros(NC)
    Rf = np.zeros(NC, dtype = bool) 
    N_it = 0 
    while np.sum(IAf) == 0 and np.sum(Rf) < NI :
        #choice of the infected cities
        IA = np.zeros((NC), dtype = bool)
        IA[npr.choice(np.arange(NC), size = 1, p = nuA)] = True
        #initialisation of time
        T = np.zeros(NC, dtype = float)
        #starting day at one
        T[IA] = 1
        #
        R = np.zeros(NC, dtype = bool) 
        R[IA] = 1
        #
        Sh = npr.exponential(1, size = NC)
        Sh[IA] = 0
        # 
        N_it = N_it + 1
        Tf, IAf, Rf, Shf = Ife_U(t_f, 1., T, IA, R, Sh, KA, KB, nuA, nuB, TL)
    return Tf, IAf, Rf, Shf, N_it

#%%
#in the following functions, we simply replaced the dependency 
#on nu_A, nu_B, K_A and K_B by the one on the matrix->MFAAs

#%%
def PropaM(tC, T, i, IA_o, R, Sh, Mat):
    Sh[R] = 0
    #erosion of the shields of any susceptible city
    ER = Mat[i][:, ~R].sum(axis = 0)
    #ER = np.reshape(np.asarray(ER), ER.size)
    Sh[~R] = Sh[~R] - ER 
    #new infected are those for which Sh < 0 -->
    IA = (Sh < 0)
    T[IA] = tC
    R[IA] = True
    Sh[IA] = 0
    IA_n = IA_o
    IA_n[i] = False
    IA_n[IA] = True
    return T, IA_n, R, Sh

    # IA_n = np.delete(IA_o, IA_o == i)
    # IA_n = np.append(IA_n, IA)
    #return T, IA_n, R, Sh
    
#%%

def Ife_UM(t_f, t_0, T, IA, R, Sh, Mat, TL):
    t_C = t_0
    while t_C < t_f and np.sum(IA)>0 :
        #next event ?
        t_C = np.min(T[IA] + TL[IA])
        idx = (T[IA] + TL[IA] == t_C)
        i = np.zeros(IA.size, dtype = bool)
        i[IA] = idx
        T, IA, R, Sh = PropaM(t_C, T, i, IA, R, Sh, Mat)
    return T, IA, R, Sh

    #while t_C < t_f and np.size(IA)>0:
        #next event ?
        #t_C, idx = min((val, idx) for (idx, val) in enumerate(T[IA] + TL[IA]))
        #i = IA[idx]

#%%
#preceding function in a starting case
#with a simplified description of the initial condition
#A0 as a sparse matrix N x 1
#I chose the starting day to be 1 to avoid sparsity issues

def InfectionIM(t_f, IA, Mat, TL):
    #initialisation of time
    T = np.zeros((Mat.shape[0]))
    #starting day at one
    T[IA] = 1
    #
    Sh = npr.exponential(1, size = Mat.shape[0])
    Sh[IA] = 0
    #
    R = np.zeros(Mat.shape[0], dtype = bool)
    R[IA] = 1
    return Ife_UM(t_f, 1, T, IA, R, Sh, Mat, TL)

#%%
#adaptation of the preceding function to the case 
#where IA consists of a uniform sample of a city according to nu0
#where restart is automatic
#as long as the size threshold NI is not reached while IA is empty

def InfectionRM(t_f, NI, Mat, nu0, TL):
    NC = Mat.shape[0]
    IAf = np.zeros(NC)
    Rf = np.zeros(NC, dtype = bool) 
    N_it = 0 
    while np.sum(IAf) == 0 and np.sum(Rf) < NI :
        #choice of the infected cities
        IA = np.zeros((NC), dtype = bool)
        IA[npr.choice(np.arange(NC), size = 1, p = nu0)] = True
        #initialisation of time
        T = np.zeros(NC, dtype = float)
        #starting day at one
        T[IA] = 1
        #
        R = np.zeros(NC, dtype = bool) 
        R[IA] = 1
        #
        Sh = npr.exponential(1, size = NC)
        Sh[IA] = 0
        # 
        N_it = N_it + 1
        Tf, IAf, Rf, Shf = Ife_UM(t_f, 1., T, IA, R, Sh, Mat, TL)
    return Tf, IAf, Rf, Shf, N_it
