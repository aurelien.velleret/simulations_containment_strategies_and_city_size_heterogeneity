# -*- coding: utf-8 -*-
"""
Created on Tue Feb 15 10:50:24 2022

@author: alter
"""

#%%
#Analysis for variant U of the model
from Lockdown_infections import *

#from threshold_through_pOutbreak, deduce the values of R0 for the reference infection
from threshold_through_pOutbreak import *

#%%
#For strategy (U)

beta1 = beta.reshape((NC, 1))
beta2 = beta.reshape((1, NC))
#sum of contributions to the kernel, the diagonal being removed
Sb = np.sum(beta1**(1+b)*beta2**a)-np.sum(beta**(1+b+a))
KM = RU_ref/(RU * NC) * Sb / MCom.sum() 
# KV = RU_ref/RU
# KM = KV/MCom.sum() * Sb / NC
print("k_M.K_vee~{:.3e}".format(KM))
print("equivalent R_0~{:.3e}".format(RU_ref))

MERi = MCom
MERe = MCom.transpose()
MER = KM*(MERi + MERe)
for j in np.arange(NC):
    MER[j] = MER[j]/beta[j]
MEr = MER.toarray()

Tf2, IAf2, Rf2, Shf2, N_it2 = InfectionRM(10000, 30, MEr, nuA, TL_U)
print("Number of failed attempts =", N_it2-1)

#%%
#For strategy (P)

beta1 = beta.reshape((NC, 1))
beta2 = beta.reshape((1, NC))
pM = RP_ref/(RP * NC) * Sb / MCom.sum() 
# KV = RU_ref/RU
# KM = KV/MCom.sum() * Sb / NC
print("p_M.K_vee~{:.3e}".format(pM))
print("equivalent R_0~{:.3e}".format(RP_ref))
        

MERi = MCom
MERe = MCom.transpose()
MER = pM*(MERi + MERe)
MErP = MER.toarray()

Tf2, IAf2, Rf2, Shf2, N_it2 = InfectionRM(10000, 30, MErP, nuA, TL_P)
print("Number of failed attempts =", N_it2-1)

#%%
#t_o = TL_U[0]*float(input("Up to how many reference generations does the epidemic spread?  > t_o/TL_U = "))
t_o = TL_U[0]*100

#reference time adjusted just a bit higher than a fraction of the generation time for the "U" model
#N_d = int(input("Which precision in the time-discretisation (Dt = TL_U/N_d) for aggregation?  > N_d = "))
N_d = 5
t_R = TL_U[0]*(1/N_d + 1e-9)

#N_p = int(input("How many runs do you wish to perform?  > N_p = "))
N_p = 1000  #200 suggested
summary_parameters["pO_Nbr_runs"] = N_p
#N_I = int(input("What is the size threshold under which simulations are restarted?  > N_I = "))
N_I = 5
summary_parameters["pO_min_Size_restart"] = N_I

Gt = np.arange(1+1e-8, 1+t_o, t_R)
Nt = Gt.size
Gtb = Gt.reshape((Nt, 1))
Rt = np.zeros((N_p, Nt))
It = np.zeros((N_p, Nt))


Prop_I = np.zeros((N_p, Counts_ref.size))
Is_I = np.zeros((N_p, beta.size), dtype = bool)
#%%
N_fl = 0
for k in np.arange(N_p):
    Tf, IAf, Rf, Shf, N_it2 = InfectionRM(1+t_o, N_I, MEr, nuA, TL_U)
    #failure update
    N_fl = N_fl + N_it2/N_p
    #Is_I : who has been infected eventually?
    Is_I[k] = (Tf!= 0)
    #update for the total number of infected 
    T1 = Tf[Rf]
    T1 = T1.reshape((1, np.size(T1)))
    Rt[k] = np.sum(T1 < Gtb, axis = 1)
    #update for the number of infectious
    T2 = Tf[Rf] + TL_U[Rf]
    T2 = np.reshape(T2, (1, np.size(T2)))
    It[k] = np.sum((T1 < Gtb)*(Gtb < T2), axis = 1)
    #proportion of infected cities as a function of their size
    counts, bins, bars = plt.hist(np.log10(beta[Tf==0]), bins = Sep)
    plt.clf()
    Prop_I[k] = 1- counts/Counts_ref
    
    
print("Runs of simulations completed.")
print("Averaged number of failures observed : N_fl~{:.2e}".format(N_fl-1) )  
summary_parameters['pO_strU_N_fl'] = N_fl-1
#%%
#Now for the case of the P strategy
RtP = np.zeros((N_p, Nt))
ItP = np.zeros((N_p, Nt))
Prop_IP = np.zeros((N_p, Counts_ref.size))
Is_IP = np.zeros((N_p, beta.size), dtype = bool)
N_flP = 0
for k in np.arange(N_p):
    TfP, IAfP, RfP, ShfP, N_itP2 = InfectionRM(1+t_o, N_I, MErP, nuA, TL_P)
    #failure update
    N_flP = N_flP + N_itP2/N_p
    #Is_IP : who has been infected eventually?
    Is_IP[k] = (TfP!= 0)
    #update for the total number of infected 
    T1P = TfP[RfP]
    T1P = T1P.reshape((1, np.size(T1P)))
    RtP[k] = np.sum(T1P < Gtb, axis = 1)
    #update for the number of infectious
    T2P = TfP[RfP] + TL_P[RfP]
    T2P = np.reshape(T2P, (1, np.size(T2P)))
    ItP[k] = np.sum((T1P < Gtb)*(Gtb < T2P), axis = 1)
    #proportion of infected cities as a function of their size
    counts, bins, bars = plt.hist(np.log10(beta[TfP==0]), bins = Sep)
    plt.clf()
    Prop_IP[k] = 1- counts/Counts_ref
    
    
print("Runs of simulations completed.")
print("Averaged number of failures observed : N_fl~{:.2e}".format(N_flP-1) )  
summary_parameters['pO_strP_N_fl'] = N_flP-1

#%%
plt.clf()
plt.hist(Rt[:, -1], bins = 50, range = (0, max(Rt[:, -1])))
plt.title("Distribution of final incidence")
plt.legend(title = "{:s}, pO, strU, TG".format(abbrev))
plt.savefig("total_nbr_infected_v{:d}_pO_strU_TG.png".format(version))

#%%
plt.clf()
plt.hist(RtP[:, -1], bins = 50, range = (0, max(RtP[:, -1])))
plt.title("Distribution of final incidence")
plt.legend(title = "{:s}, pO, strP, TG".format(abbrev))
plt.savefig("total_nbr_infected_v{:d}_pO_strP_TG.png".format(version))

#%%
print("Results from the last run")
t_f = np.max(Tf[Rf])
Gt2 = Gt[Gt < t_f]
Rtf = Rt[-1, Gt < t_f]

plt.figure(1)
plt.clf()
plt.plot(Gt2[1:], np.log10(Rtf[1:]))
rhoE = np.log10(RU_ref)/TL_U[0]
print("Expected growth rate~{:.2e} for power 10".format(rhoE))
summary_parameters['pO_strU_exp_growth_rate'] = rhoE

t_I = np.sum((Rtf < 20))
Lin = np.log10(Rtf[t_I]) + rhoE*(Gt2-Gt2[t_I])
t_M = np.sum((Lin < np.log10(Rtf[-1])))
t_m = np.sum(Lin < 0)
plt.plot(Gt2[t_m:t_M], Lin[t_m:t_M], c ="purple")
plt.title("Progression of the number of infected, str (U), TG")
plt.xlabel("Time since the start of infection")
plt.ylabel("Log10 of the total number of infected")
plt.savefig("slope_adjustment_{:d}_v{:d}_TG.png".format(20, version))

plt.figure(2)
plt.clf()
plt.plot(Gt2[1:]/TL_U[0], Rtf[1:])
plt.plot(Gt2[t_m:t_M]/TL_U[0], 10**(Lin[t_m:t_M]), c ="purple")
plt.title("Progression of the number of infected, str (U), TG")
plt.xlabel("Number of generations since the start of infection")
plt.ylabel("Total number of infected")
plt.savefig("number_infected_perG_{:d}_v{:d}_pO_strU_TG.png".format(20, version))

#%%
print("Results from the last run")
t_f = np.max(TfP[RfP])
Gt2 = Gt[Gt < t_f]
RtfP = RtP[-1, Gt < t_f]

plt.figure(1)
plt.clf()
plt.plot(Gt2[1:], np.log10(RtfP[1:]))
plt.title("Progression of the number of infected, str (P), TG")
plt.xlabel("Time since the start of infection")
plt.ylabel("Log10 of the total number of infected")
plt.savefig("slope_adjustment_{:d}_v{:d}_TG_pO_strP.png".format(20, version))

plt.figure(2)
plt.clf()
plt.plot(Gt2[1:]/TL_P[0], RtfP[1:])
plt.title("Progression of the number of infected, str (P), TG")
plt.xlabel("Number of generations since the start of infection")
plt.ylabel("Total number of infected")
plt.savefig("number_infected_perG_{:d}_v{:d}_pO_strP_TG.png".format(20, version))

#%%
err = 1
n_it = 0
pi = 1- np.exp(- bKA_U - bKB_U)
while (err > 5E-5) * (n_it < 500):
    pi2 = 1-np.exp(- bKA_U * np.sum(bnuA*pi) - bKB_U* np.sum(bnuB*pi))
    err = np.max(np.abs(1-pi/pi2))
    n_it = n_it +1
    pi = pi2
    
Sort = np.argsort(beta)
RefV = Sort[Bins]
piRef_U = (pi[RefV[1:]] + pi[RefV[:-1]])/2

plt.figure(5)
plt.clf()
plt.scatter(np.log10(beta), pi, c="black", marker="+", label = "analytical relation")
plt.title("Theoretical infection probability, \n \
from {:s} data, strU, TG".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pO, strP, TG".format(abbrev))
plt.ylim(0, 1)

plt.savefig("theoretical_Infected_vs_size_v{:d}_pO_strU_TG.png".format(version))
summary_per_city["pO_strU_thr_pi"] = pi
summary_per_bin["pO_strU_thr_pi"] = piRef_U

#%%

n_it = 0
pi = 1- np.exp(- bKA_U - bKB_U)
pi_AU = np.ones(15)
pi_BU = np.ones(15)
for i in np.arange(15):
    pi_AU[i] = np.sum(bnuA*pi)
    pi_BU[i] = np.sum(bnuB*pi)
    pi = 1-np.exp(- bKA_U * pi_AU[i]  - bKB_U* pi_BU[i])
    n_it = n_it +1

plt.plot(np.arange(14), np.log10(np.abs(pi_AU[:-1]/pi_AU[-1] -1)), c="blue", marker="*", label = "pi_C")
plt.plot(np.arange(14), np.log10(np.abs(pi_BU[:-1]/pi_BU[-1] -1)), c="red", marker="+", label = "pi_D")


plt.xlabel("Iteration")
plt.ylabel("Error ({:s} scale)".format(r'$log_{10}$'))
plt.title("Error in the estimation of the infection probability\n \
relative error for the two summary parameters, str U, K")
plt.legend(title = "{:s}, pO, strU, KG".format(abbrev))
plt.savefig("Error_I_vs_size_v{:d}_U_K.png".format(version))

#%%

Prop_I_avg = np.sum(Prop_I, axis =0)/N_p
summary_per_bin["pO_strU_emp_pi"] = Prop_I_avg

Prop_I_var = np.sum(Prop_I**2, axis =0)/N_p - Prop_I_avg**2
summary_per_bin["pO_strU_std_emp_pi"] = np.sqrt(Prop_I_var)

Prop_I_min = np.min(Prop_I, axis =0)
summary_per_bin["pO_strU_min_pi"] = Prop_I_min

Prop_I_max = np.max(Prop_I, axis =0)
summary_per_bin["pO_strU_max_pi"] = Prop_I_max


Prop_I_sort = np.sort(Prop_I, axis =0)

Prop_I_s = Prop_I_avg + np.sqrt(Prop_I_var)
Prop_I_ms = Prop_I_avg - np.sqrt(Prop_I_var)

plt.figure(3)
plt.clf()
plt.plot(MidBins, piRef_U, "ks--", label = "analytical")
plt.plot(MidBins, Prop_I_avg, "bD", label="empirical")
plt.plot(MidBins, Prop_I_s, "^r:", label="std")
plt.plot(MidBins, Prop_I_ms, "vr:")
plt.vlines(MidBins, \
          ymin = Prop_I_min,  ymax = Prop_I_max,\
         color= "purple", linestyle =":")
plt.plot(MidBins, Prop_I_min, "mv:")
plt.plot(MidBins, Prop_I_max, "m^:", label = "range")
plt.title("Empirical and theoretical infection probability,\n \
fluctuations between the runs, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pO, strU, TG".format(abbrev))
plt.ylim(-0.02, 1.02)

plt.savefig("Infected_vs_size_v{:d}_pO_strU_TG.png".format(version))

#%%

Is_I_avg = np.sum(Is_I, axis =0)/N_p
summary_per_city["pO_strU_emp_pi"] = Is_I_avg

Is_I_var = np.sum(Is_I**2, axis =0)/N_p - Is_I_avg**2
summary_per_city["pO_strU_std_emp_pi"] = np.sqrt(Is_I_var)

Prop2I_avg = np.zeros(MidBins.size)
Prop2I_var = np.zeros(MidBins.size)
Prop2I_min = np.zeros(MidBins.size)
Prop2I_max = np.zeros(MidBins.size)
for i in np.arange(MidBins.size):
    Is_bin = (np.log10(beta)>=Bins_ref[i])& (np.log10(beta)<Bins_ref[i+1])
    Prop2I_avg[i] = np.mean(Is_I_avg[Is_bin])
    Prop2I_var[i] = np.var(Is_I_avg[Is_bin])
    Prop2I_min[i] = np.min(Is_I_avg[Is_bin])
    Prop2I_max[i] = np.max(Is_I_avg[Is_bin])
summary_per_bin["pO_strU_empB_pi"] = Prop2I_avg
summary_per_bin["pO_strU_stdB_emp_pi"] = np.sqrt(Prop2I_var)
summary_per_bin["pO_strU_minB_pi"] = Prop2I_min
summary_per_bin["pO_strU_maxB_pi"] = Prop2I_max


Prop2I_s = Prop2I_avg + np.sqrt(Prop2I_var)
Prop2I_ms = Prop2I_avg - np.sqrt(Prop2I_var)


R2 = 1-np.var(Is_I_avg - pi)/np.var(Is_I_avg)
summary_parameters["pO_strU_infP_R2"] = R2

plt.figure(4)
plt.clf()
plt.plot(MidBins, piRef_U, "ks--", label = "analytical")
plt.plot(MidBins, Prop2I_avg, "bD", label="empirical")
plt.plot(MidBins, Prop2I_s, "^r:", label="std")
plt.plot(MidBins, Prop2I_ms, "vr:")
plt.vlines(MidBins, \
          ymin = Prop2I_min,  ymax = Prop2I_max,\
         color= "purple", linestyle =":")
plt.plot(MidBins, Prop2I_min, "mv:")
plt.plot(MidBins, Prop2I_max, "m^:", label = "range")
plt.title("Empirical and theoretical infection probability,\n \
fluctuations within the bin, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pO, strU, TG".format(abbrev))
plt.ylim(-0.02, 1.02)


plt.savefig("variation_infected_vs_size_v{:d}_pO_strU_TG.png".format(version))

#%%
err = 1
n_it = 0
pi = 1- np.exp(- bKA_U - bKB_U)
while (err > 5E-5) * (n_it < 500):
    pi2 = 1-np.exp(- bKA_P * np.sum(bnuA_P*pi) - bKB_P* np.sum(bnuB_P*pi))
    err = np.max(np.abs(1-pi/pi2))
    n_it = n_it +1
    pi = pi2
    
Sort = np.argsort(beta)
RefV = Sort[Bins]
piRef_P = (pi[RefV[1:]] + pi[RefV[:-1]])/2
summary_per_city["pO_strP_thr_pi"] = pi
summary_per_bin["pO_strP_thr_pi"] = piRef_P

#%%

Prop_IP_avg = np.sum(Prop_IP, axis =0)/N_p
summary_per_bin["pO_strP_emp_pi"] = Prop_IP_avg

Prop_IP_var = np.sum(Prop_IP**2, axis =0)/N_p - Prop_IP_avg**2
summary_per_bin["pO_strP_std_emp_pi"] = np.sqrt(Prop_IP_var)

Prop_IP_min = np.min(Prop_IP, axis =0)
summary_per_bin["pO_strP_min_pi"] = Prop_IP_min

Prop_IP_max = np.max(Prop_IP, axis =0)
summary_per_bin["pO_strP_max_pi"] = Prop_IP_max


Prop_IP_sort = np.sort(Prop_IP, axis =0)

Prop_IP_2s = Prop_IP_avg + np.sqrt(Prop_IP_var)
Prop_IP_m2s = Prop_IP_avg - np.sqrt(Prop_IP_var)

plt.figure(3)
plt.clf()
plt.plot(MidBins, piRef_P, "ks--", label = "analytical")
plt.plot(MidBins, Prop_IP_avg, "bD", label="empirical")
plt.plot(MidBins, Prop_IP_2s, "^r:", label="std")
plt.plot(MidBins, Prop_IP_m2s, "vr:")
plt.vlines(MidBins, \
          ymin = Prop_IP_min,  ymax = Prop_IP_max,\
         color= "purple", linestyle =":")
plt.plot(MidBins, Prop_IP_min, "mv:")
plt.plot(MidBins, Prop_IP_max, "m^:", label = "range")
plt.title("Empirical and theoretical infection probability,\n \
fluctuations between the runs, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pO, strP, TG".format(abbrev))
plt.ylim(0, 1.02)

plt.savefig("Infected_vs_size_v{:d}_pO_strP_TG.png".format(version))

#%%

Is_IP_avg = np.sum(Is_IP, axis =0)/N_p
summary_per_city["pO_strP_emp_pi"] = Is_IP_avg

Is_IP_var = np.sum(Is_IP**2, axis =0)/N_p - Is_IP_avg**2
summary_per_city["pO_strP_std_emp_pi"] = np.sqrt(Is_IP_var)

Prop2IP_avg = np.zeros(MidBins.size)
Prop2IP_var = np.zeros(MidBins.size)
Prop2IP_min = np.zeros(MidBins.size)
Prop2IP_max = np.zeros(MidBins.size)
for i in np.arange(MidBins.size):
    Is_bin = (np.log10(beta)>=Bins_ref[i])& (np.log10(beta)<Bins_ref[i+1])
    Prop2IP_avg[i] = np.mean(Is_IP_avg[Is_bin])
    Prop2IP_var[i] = np.var(Is_IP_avg[Is_bin])
    Prop2IP_min[i] = np.min(Is_IP_avg[Is_bin])
    Prop2IP_max[i] = np.max(Is_IP_avg[Is_bin])
summary_per_bin["pO_strP_empB_pi"] = Prop2IP_avg
summary_per_bin["pO_strP_stdB_emp_pi"] = np.sqrt(Prop2IP_var)
summary_per_bin["pO_strP_minB_pi"] = Prop2IP_min
summary_per_bin["pO_strP_maxB_pi"] = Prop2IP_max


Prop2IP_s = Prop2IP_avg + np.sqrt(Prop2IP_var)
Prop2IP_ms = Prop2IP_avg - np.sqrt(Prop2IP_var)


R2 = 1-np.var(Is_IP_avg - pi)/np.var(Is_IP_avg)
summary_parameters["pO_strP_infP_R2"] = R2

plt.figure(4)
plt.clf()
plt.plot(MidBins, piRef_P, "ks--", label = "analytical")
plt.plot(MidBins, Prop2IP_avg, "bD", label="empirical")
plt.plot(MidBins, Prop2IP_s, "^r:", label="std")
plt.plot(MidBins, Prop2IP_ms, "vr:")
plt.vlines(MidBins, \
          ymin = Prop2IP_min,  ymax = Prop2IP_max,\
         color= "purple", linestyle =":")
plt.plot(MidBins, Prop2IP_min, "mv:")
plt.plot(MidBins, Prop2IP_max, "m^:", label = "range")
plt.title("Empirical and theoretical infection probability,\n \
fluctuations within the bin, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pO, strP, TG".format(abbrev))
plt.ylim(0, 1.02)


plt.savefig("variation_infected_vs_size_v{:d}_pO_strP_TG.png".format(version))



#%%
err = 1
n_it = 0
eta_U = 1- np.exp(- KA_U - KB_U)
while (err > 5E-5) * (n_it < 500):
    eta_U2 = 1-np.exp(- KA_U * np.sum(nuA*eta_U) - KB_U* np.sum(nuB*eta_U))
    err = np.max(np.abs(1-eta_U/eta_U2))
    n_it = n_it +1
    eta_U = eta_U2
    
Sort = np.argsort(beta)
RefV = Sort[Bins]
etaRef_U = (eta_U[RefV[1:]] + eta_U[RefV[:-1]])/2
summary_per_city["pO_strU_thr_eta"] = eta_U
summary_per_bin["pO_strU_thr_eta"] = etaRef_U


plt.figure(5)
plt.clf()
plt.plot(np.log10(beta), eta_U, "ks", markersize = 5, label = "analytical relation")
plt.title("Theoretical outbreak probability, \n \
data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pO, strU, KB".format(abbrev))
plt.savefig("theoretical_pOutbreak_vs_size_v{:d}_TG.png".format(version))


#%%

Err = 1
n_it = 0
eta = 1- np.exp(- KA_U - KB_U)
eta_AU = np.ones(15)
eta_BU = np.ones(15)
for i in np.arange(15):
    eta_AU[i] = np.sum(nuA*eta)
    eta_BU[i] = np.sum(nuB*eta)
    eta = 1-np.exp(- KA_U * eta_AU[i]  - KB_U* eta_BU[i])
    n_it = n_it +1

plt.plot(np.arange(14), np.log10(np.abs(eta_AU[:-1]/eta_AU[-1] -1)), c="blue", marker="*", label = "eta_A")
plt.plot(np.arange(14), np.log10(np.abs(eta_BU[:-1]/eta_BU[-1] -1)), c="red", marker="+", label = "eta_B")


plt.xlabel("Iteration")
plt.ylabel("Error ({:s} scale)".format(r'$log_{10}$'))
plt.title("Error in the estimation of the probability of Outbreak\n \
relative error for the two parameters")
plt.legend(title = "{:s}, pO, strU, KB".format(abbrev))
plt.savefig("Error_probO_v{:d}_U_K.png".format(version))

#%%

Isort = np.argsort(beta)
Prop_Out = np.zeros(Bins.size-1)
#S_O = int(input("What is the size threshold for a successful outbreak?  > S_O = "))
S_O = 60.
#R_c = int(input("How many runs for each initial city?  > Rc = "))
Rc = 1000
for ir in np.arange(Bins.size -1):
    iA = Isort[np.arange(Bins[ir], Bins[ir+1])]
    Success = np.zeros((Rc), dtype = bool)
    for k in np.arange(Rc):
        IA = np.zeros((NC), dtype = float)
        IA[iA] = 1
        IA = IA/np.sum(IA)
        Tf, IAf, Rf, Shf, Ni = InfectionRM(1+15*TL_U[0], 1, MEr, IA, TL_U)
        Success[k] = ( np.sum(Rf) > S_O)
    #Prop_Out[ir] = N_O/Rc
    Prop_Out[ir] = np.sum(Success)/Rc
    
summary_per_bin["pO_strU_emp1_eta"] = Prop_Out

plt.figure(4)
plt.clf()
plt.scatter(MidBins, etaRef_U, c="black", marker="+", label = "analytical relation")
plt.scatter(np.log10(beta[Isort[Bins]][:-1]), Prop_Out, c="red", marker="+", label = "from simulation runs of epidemic")
plt.title("Empirical and theoretical outbreak probability, \n \
data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.ylim(0,1.02)
plt.legend(title = "{:s}, pO, strU, TG".format(abbrev))
plt.savefig("empirical_pOutbreak_vs_size_v{:d}_pO_strU_TG.png".format(version))

#It appears that the sizes of outbreaks are so small 
#that the threshold on the size makes a large impact on the observed probabilities


#%%
#
Isort = np.argsort(beta)
Prop_Out = np.zeros(beta.size)
#S_O = int(input("What is the size threshold for a successful outbreak?  > S_O = "))
S_O = 60.
summary_parameters["pO_size_successful_outbreak"] = S_O
#R_c = int(input("How many runs for each initial city?  > Rc = "))
Rc = 1000
summary_parameters["pO_strU_eta_Nbr_runs"] = Rc
for i in np.arange(beta.size):
    Success = np.zeros((Rc), dtype = bool)
    for k in np.arange(Rc):
        IA = np.zeros((NC), dtype = float)
        IA[i] = 1
        Tf, IAf, Rf, Shf, Ni = InfectionRM(1+20*TL_U[0], 1, MEr, IA, TL_U)
        Success[k] = ( np.sum(Rf) > S_O)
    Prop_Out[i] = np.sum(Success)/Rc

summary_per_city["pO_strU_emp_eta"] = Prop_Out

Mean_pOut = np.zeros(MidBins.size)
Std_pOut = np.zeros(MidBins.size)
Min_pOut = np.zeros(MidBins.size)
Max_pOut = np.zeros(MidBins.size)
for ir in np.arange(MidBins.size):
    Is_bin = (np.log10(beta)>=Bins_ref[ir])& (np.log10(beta)<Bins_ref[ir+1])
    Mean_pOut[ir] = np.mean(Prop_Out[Is_bin])
    Std_pOut[ir] = np.std(Prop_Out[Is_bin])
    Min_pOut[ir] = np.min(Prop_Out[Is_bin])
    Max_pOut[ir] = np.max(Prop_Out[Is_bin])
#GX = summary_per_bin['Central_value_log_sc']
summary_per_bin["pO_strU_empB_eta"] = Mean_pOut
summary_per_bin["pO_strU_stdB_emp_eta"] = Std_pOut
summary_per_bin["pO_strU_minB_eta"] = Min_pOut
summary_per_bin["pO_strU_maxB_eta"] = Max_pOut

plt.figure(4)
plt.clf()
plt.plot(MidBins, etaRef_U, "ks-", label = "analytical")
plt.plot(MidBins, Mean_pOut, "bD--", label = "empirical")
plt.plot(MidBins, Mean_pOut + Std_pOut, "^r:", label="std")
plt.plot(MidBins, Mean_pOut - Std_pOut, "vr:")
plt.vlines(MidBins, \
          ymin = Min_pOut,  ymax = Max_pOut,\
         color= "purple", linestyle =":")
plt.plot(MidBins, Min_pOut, "mv:")
plt.plot(MidBins, Max_pOut, "m^:", label = "range")
plt.title("Empirical and theoretical outbreak probability, \n \
Fluctuations within the bin, data :{:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.ylim(0,1.02)
plt.legend(title = "{:s}, pO, strU, TG".format(abbrev))
plt.savefig("variation_empirical_pOutbreak_vs_size_v{:d}_pO_strU_TG.png".format(version))

#It appears that the sizes of outbreaks are so small 
#that the threshold on the size makes a large impact on the observed probabilities

#%%

Isort = np.argsort(beta)
Prop_Out = np.zeros(Bins.size-1)
#S_O = int(input("What is the size threshold for a successful outbreak?  > S_O = "))
S_O = 60.
#R_c = int(input("How many runs for each initial city?  > Rc = "))
Rc = 1000 
for ir in np.arange(Bins.size-1):
    iA = Isort[Bins[ir]]
    Success = np.zeros((Rc), dtype = bool)
    for k in np.arange(Rc):
        IA = np.zeros((NC), dtype = bool)
        IA[iA] = True
        Tf, IAf, Rf, Shf = InfectionI(1+30*TL_U[0], IA, KA_U, KB_U, nuA, nuB, TL_U)
        Success[k] = ( np.sum(Rf) > S_O)
    #Prop_Out[ir] = N_O/Rc
    Prop_Out[ir] = np.sum(Success)/Rc
summary_per_bin["pO_strU_empK_eta"] = Prop_Out    
    
plt.figure(4)
plt.clf()
plt.scatter(MidBins, etaRef_U, c="black", marker="+", label = "analytical relation")
plt.scatter(np.log10(beta[Isort[Bins[:-1]]]), Prop_Out, c="red", marker="+", label = "from simulation runs of epidemic")
plt.title("Empirical probability of triggering an outbreak as a function , \n \
of the size of first infected, from {:s} data, str. U, K".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion of outbreaks")
plt.ylim(0,1)
plt.legend(title = "{:s}, pO, strU, KG".format(abbrev))
plt.savefig("empirical_pOutbreak_vs_size_v{:d}_pO_strU_K.png".format(version))


#%%
err = 1
n_it = 0
eta_P = 1- np.exp(- KA_P - KB_P)
while (err > 5E-5) * (n_it < 500):
    eta_P2 = 1-np.exp(- KA_P * np.sum(nuA*eta_P) - KB_P* np.sum(nuB*eta_P))
    err = np.max(np.abs(1-eta_P/eta_P2))
    n_it = n_it +1
    eta_P = eta_P2
    
Sort = np.argsort(beta)
RefV = Sort[Bins]
etaRef_P = (eta_P[RefV[1:]] + eta_P[RefV[:-1]])/2
summary_per_city["pO_strP_thr_eta"] = eta_P
summary_per_bin["pO_strP_thr_eta"] = etaRef_P

plt.figure(5)
plt.clf()
plt.plot(np.log10(beta), eta_P, "ks", markersize = 5, label = "analytical relation")
plt.title("Theoretical probability of triggering an outbreak as a function , \n \
of the size of first infected, from {:s} data, str (P), TG".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pO, strP, TG".format(abbrev))
plt.savefig("theoretical_pOutbreak_vs_size_v{:d}_pO_strP_TG.png".format(version))




#%%
#
Isort = np.argsort(beta)
Prop_Out = np.zeros(beta.size)
#S_O = int(input("What is the size threshold for a successful outbreak?  > S_O = "))
S_O = 60.
#R_c = int(input("How many runs for each initial city?  > Rc = "))
Rc = 1000
for i in np.arange(beta.size):
    Success = np.zeros((Rc), dtype = bool)
    for k in np.arange(Rc):
        IA = np.zeros((NC), dtype = float)
        IA[i] = 1
        Tf, IAf, Rf, Shf, Ni = InfectionRM(1+20*TL_U[0], 1, MErP, IA, TL_U)
        Success[k] = ( np.sum(Rf) > S_O)
    Prop_Out[i] = np.sum(Success)/Rc

summary_per_city["pO_strP_emp_eta"] = Prop_Out

Mean_pOut = np.zeros(MidBins.size)
Std_pOut = np.zeros(MidBins.size)
Min_pOut = np.zeros(MidBins.size)
Max_pOut = np.zeros(MidBins.size)
for ir in np.arange(MidBins.size):
    Is_bin = (np.log10(beta)>=Bins_ref[ir])& (np.log10(beta)<Bins_ref[ir+1])
    Mean_pOut[ir] = np.mean(Prop_Out[Is_bin])
    Std_pOut[ir] = np.std(Prop_Out[Is_bin])
    Min_pOut[ir] = np.min(Prop_Out[Is_bin])
    Max_pOut[ir] = np.max(Prop_Out[Is_bin])
#GX = summary_per_bin['Central_value_log_sc']
summary_per_bin["pO_strP_empB_eta"] = Mean_pOut
summary_per_bin["pO_strP_stdB_emp_eta"] = Std_pOut
summary_per_bin["pO_strP_minB_eta"] = Min_pOut
summary_per_bin["pO_strP_maxB_eta"] = Max_pOut

plt.figure(4)
plt.clf()
plt.plot(MidBins, etaRef_P, "ks-", label = "analytical")
plt.plot(MidBins, Mean_pOut, "bD:", label = "empirical")
plt.plot(MidBins, Mean_pOut + Std_pOut, "^r:", label="std")
plt.plot(MidBins, Mean_pOut - Std_pOut, "vr:")
plt.vlines(MidBins, \
          ymin = Min_pOut,  ymax = Max_pOut,\
         color= "purple", linestyle =":")
plt.plot(MidBins, Min_pOut, "mv:")
plt.plot(MidBins, Max_pOut, "m^:", label = "range")
plt.title("Empirical and theoretical outbreak probability, \n \
Fluctuations within the bin, data :{:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.ylim(0,1.02)
plt.legend(title = "{:s}, pO, strP, TG".format(abbrev))
plt.savefig("variation_empirical_pOutbreak_vs_size_v{:d}_pO_strP_TG.png".format(version))

#%%
summary_per_bin.to_csv("summary_per_bin_v{:d}.csv".format(version))
summary_per_city.to_csv("summary_per_city_v{:d}.csv".format(version))
summary_parameters.to_csv("summary_parameters_v{:d}.csv".format(version))
########################################################################
#%%
#In this new version, least square estimation is exploited \
#instead of a simple interpolation between extremities

#PoT = float(input("Which threshold for the infected population size at quasi-equilibrium? \n >PoR = "))
PoR = 30
summary_parameters["pO_strU_TI_init_thres"] = PoR

#Recall "N_o : Number of generations for the measurement"


SK = (Rt[:, -1]>PoR)
NpK = np.sum(SK)

t_R=np.zeros(NpK, dtype = int)
TotE = np.zeros((NpK, N_o-2))
for k in np.arange(NpK):
    t_R[k]= np.where(Rt[SK][k] > PoR)[0][0]
    for i in np.arange(N_o-2):
        X = np.arange(i+2)
        Y = np.log10(Rt[SK][k, t_R[k] + N_d * X])
        TotE[k, i] = (np.sum(X*Y)-np.sum(X)*np.sum(Y)/np.size(X))/ (np.sum(X**2)-np.sum(X)**2/np.size(X))


TotE_avg = np.sum(TotE, axis =0)/NpK
TotE_var = np.sum(TotE**2, axis =0)/NpK - TotE_avg**2

summary_per_gen["pO_strU_TI_R0_emp"] = TotE_avg
summary_per_gen["pO_strU_TI_R0_std"] = np.sqrt(TotE_var)

TotE_sort = np.sort(TotE, axis = 0)
summary_per_gen["pO_strU_TI_R0_q5"] = TotE_sort[np.int(NpK/20)]
summary_per_gen["pO_strU_TI_R0_q95"] = TotE_sort[np.int(NpK*19/20)]

    
TotE_s = TotE_avg +np.sqrt(TotE_var)
TotE_ms = TotE_avg - np.sqrt(TotE_var)



rhoE = np.log10(RU_ref)
print("Expected growth rate~{:.2e}".format(rhoE))
summary_parameters["pO_strU_log10-R0_per_gen"] = rhoE

plt.figure(6)
plt.clf()
plt.plot(np.arange(2, N_o), TotE_avg, "bD--", label="empirical")
plt.plot(np.arange(2, N_o), rhoE*np.ones(N_o-2), c="black", label="expected")
plt.plot(np.arange(2, N_o), TotE_s, "r^:", label="+std")
plt.plot(np.arange(2, N_o), TotE_ms, "rv:", label="-std")
plt.vlines(np.arange(2, N_o), \
          ymin = summary_per_gen["pO_strU_TI_R0_q5"],\
          ymax = summary_per_gen["pO_strU_TI_R0_q95"],\
         color= "purple", linestyle =":")
plt.plot(np.arange(2, N_o), summary_per_gen["pO_strU_TI_R0_q95"], "m^:")
plt.plot(np.arange(2, N_o), summary_per_gen["pO_strU_TI_R0_q5"], "mv:", label = "5%-95%")
plt.title("Growth rate estimation, \n\
from the total number of infected, reference at {:.0f}, data: {:s} ".format(PoR, D_Name))
plt.xlabel("Number of generations in the time-interval")
plt.ylabel("Estimated growth rate")
plt.legend(title = "{:s}, pO, strU, TG".format(abbrev))
plt.ylim(-0.02*rhoE, 1.1*rhoE)
plt.savefig("growth_rate_R0_LSestimation_{:d}_v{:d}_pO_strU_TG.png".format(PoR, version))

print("initial threshold value~{:.0f}".format(PoR))
print("Number of simulations kept~{:.0f}".format(NpK))


#%%
#rescaled graph
plt.figure(7)
plt.clf()
plt.plot(np.arange(2, N_o), TotE_avg/rhoE, "bD--", label="empirical")
plt.plot(np.arange(2, N_o), np.ones(N_o-2), c="black", label="expected")
plt.plot(np.arange(2, N_o), TotE_s/rhoE, "r^:", label="+std")
plt.plot(np.arange(2, N_o), TotE_ms/rhoE, "rv:", label="-std")
plt.vlines(np.arange(2, N_o), \
          ymin = summary_per_gen["pO_strU_TI_R0_q5"]/rhoE,\
          ymax = summary_per_gen["pO_strU_TI_R0_q95"]/rhoE,\
         color= "purple", linestyle =":")
plt.plot(np.arange(2, N_o), summary_per_gen["pO_strU_TI_R0_q95"]/rhoE, "m^:")
plt.plot(np.arange(2, N_o), summary_per_gen["pO_strU_TI_R0_q5"]/rhoE, "mv:", label = "5%-95%")
plt.title("Growth rate estimation, \n\
from the total number of infected, reference at {:.0f}, data: {:s} ".format(PoR, D_Name))
plt.xlabel("Number of generations in the time-interval")
plt.ylabel("Estimated growth rate")
plt.legend(title = "{:s}, pO, strU, TG".format(abbrev))
plt.ylim(-0.02, 1.1)
plt.savefig("RS_growth_rate_TotE_estimation_{:d}_v{:d}_pO_strU_TG.png".format(PoR, version))
#%%
k_dec=1
plt.figure(8)
plt.clf()
Xv = np.array([])
Yv = np.array([])
for k in np.arange(NpK):
    Xv = np.append(Xv, Gt[N_d*np.arange(k_dec+N_o)])
    Yv = np.append(Yv, np.log10(Rt[SK][k, t_R[k] + N_d*np.arange(k_dec+N_o)]) - np.log10(Rt[SK][k, t_R[k]]))
plt.hist2d(Xv, Yv, bins = (k_dec+N_o, 30), cmap=plt.cm.jet)    
plt.title("Logarithmic increase of the total number of infected\n\
reference at {:.0f}, from French data, str (U), TG".format(PoR, D_Name))
plt.xlabel("Duration of the threshold time-interval")
plt.ylabel("Log10 increase of Rt")
plt.savefig("growth_observation_Rr_hist_{:d}_v{:d}_pO_strU_TG.png".format(PoR, version))

#%%
plt.figure(9)
plt.clf()
for k in np.arange(20):
    plt.plot(Gt[N_d*np.arange(k_dec+N_o)], \
             np.log10(Rt[SK][k, t_R[k] + N_d*np.arange(k_dec+N_o)]) - np.log10(Rt[SK][k, t_R[k]]))
plt.title("Logarithmic increase of the total number of infected\n\
reference at {:.0f}, from {:s} data, TG".format(PoR, D_Name))
plt.xlabel("Duration of the threshold time-interval")
plt.ylabel("Log10 increase of Rt")
plt.savefig("growth_observation_Rr_traj_{:d}_v{:d}_TG.png".format(PoR, version))

#%%

#PoI = float(input("Which threshold for the infectious population size at equilibrium? \n >PoI = "))
PoI = 10

#t_I = np.sum((Rt < 50), axis = 1)
# np.max(t_I)
#Number of generations for the measurement
N_o = 20
#computation initiated after k_dec %ge 2 generation 
k_dec = 4
#we shall only look at multiples of the generation time

#rescaled by this initial condition to get the slope
#simulations discarded if the threshold PoR is not reached
SK = (np.max(It, axis = 1)>PoI)&(np.min(It[:, 10:130], axis = 1)>0)
NpK = np.sum(SK)
t_I=np.zeros(NpK, dtype = int)
InfE = np.zeros((NpK, N_o-k_dec))
for k in np.arange(NpK):
    t_I[k]= np.where(It[SK][k] > PoI)[0][0]
    for i in np.arange(N_o-k_dec):
        X = np.arange(i+k_dec)
        Y = np.log(It[SK][k, t_I[k] + N_d * X])
        InfE[k, i] = (np.sum(X*Y)-np.sum(X)*np.sum(Y)/np.size(X))/ (np.sum(X**2)-np.sum(X)**2/np.size(X))
    
InfE_avg = np.sum(InfE, axis =0)/NpK
InfE_var = np.sum(InfE**2, axis =0)/NpK - InfE_avg**2

InfE_sort = np.sort(InfE, axis = 0)
    
InfE_2s = InfE_avg + 2*np.sqrt(InfE_var)
InfE_m2s = InfE_avg - 2*np.sqrt(InfE_var)

rhoE = np.log(RU_ref)
print("Expected growth rate~{:.2e}".format(rhoE))
print("initial threshold value~{:.0f}".format(PoI))
print("Number of simulations kept~{:.0f}".format(NpK))

plt.figure(10)
plt.clf()
plt.plot(np.arange(k_dec, N_o), InfE_avg, c="blue", label="average")
plt.plot(np.arange(k_dec, N_o), rhoE*np.ones(N_o-k_dec), c="black", label="expected")
plt.plot(np.arange(k_dec, N_o), InfE_2s, c="red", label="mean + 2 sigma")
plt.plot(np.arange(k_dec, N_o), InfE_m2s, c="red", label="mean - 2 sigma")
plt.plot(np.arange(k_dec, N_o), InfE_sort[np.int(NpK/20)], c="purple", label= "lower 5%")
plt.plot(np.arange(k_dec, N_o), InfE_sort[np.int(NpK*19/20)], c="purple", label= "upper 5%")
plt.title("Growth rate obtained from the number of infectious cities\n\
reference at {:.0f}, from {:s} data, TG".format(PoI, D_Name))
plt.xlabel("Duration of the threshold time-interval")
plt.ylabel("Estimated growth rate")
plt.legend(title = "{:s}, pO, strU, TG".format(abbrev))
plt.ylim(0, np.max(InfE_2s))
plt.savefig("growth_rate_InfE_estimation_{:d}_v{:d}_TG.png".format(PoI, version))



#The fact that the red curve is much higher than the purple 
#a priori indicates a non-Gaussian distribution of values
#with heavy-tail for large values of the slope


#%%
#rescaled graph
plt.figure(11)
plt.clf()
plt.plot(np.arange(k_dec, N_o), InfE_avg/rhoE, c="blue", label="average")
plt.plot(np.arange(k_dec, N_o), np.ones(N_o-k_dec), c="black", label="expected")
plt.plot(np.arange(k_dec, N_o), InfE_2s/rhoE, c="red", label="mean + 2 sigma")
plt.plot(np.arange(k_dec, N_o), InfE_m2s/rhoE, c="red", label="mean - 2 sigma")
plt.plot(np.arange(k_dec, N_o), InfE_sort[np.int(NpK/20)]/rhoE, c="purple", label= "lower 5%")
plt.plot(np.arange(k_dec, N_o), InfE_sort[np.int(NpK*19/20)]/rhoE, c="purple", label= "upper 5%")
plt.title("Growth rate obtained from the number of infectious cities\n\
reference at {:.0f}, from {:s} data, TG".format(PoI, D_Name))
plt.xlabel("Number of generations for the threshold time-interval")
plt.ylabel("Ratio of the estimated growth rate over the expected one")
plt.legend(title = "{:s}, pO, strU, TG".format(abbrev))
plt.ylim(0, 1.05)
plt.savefig("RS_growth_rate_InfE_estimation_{:d}_v{:d}_TG.png".format(PoI, version))
#%%
plt.figure(12)
plt.clf()
Xv = np.array([])
Yv = np.array([])
for k in np.arange(NpK):
    Xv = np.append(Xv, Gt[N_d*np.arange(k_dec+N_o)])
    Yv = np.append(Yv, np.log10(It[SK][k, t_I[k] + N_d*np.arange(k_dec+N_o)]) - np.log10(It[SK][k, t_I[k]]))
plt.hist2d(Xv, Yv, bins = (k_dec+N_o, 20), cmap=plt.cm.jet)    
plt.title("Logarithmic increase of the number of infectious cities\n\
reference at {:.0f}, from {:s} data, TG".format(PoI, D_Name))
plt.xlabel("Duration of the threshold time-interval")
plt.ylabel("Log10 increase of It")
plt.savefig("growth_observation_InfE_hist_{:d}_v{:d}_TG.png".format(PoI, version))

#%%
plt.figure(13)
plt.clf()
for k in np.arange(20):
    plt.plot(Gt[N_d*np.arange(k_dec+N_o)], \
             np.log10(It[SK][k, t_I[k] + N_d*np.arange(k_dec+N_o)]) - np.log10(It[SK][k, t_I[k]]))
plt.title("Logarithmic increase of the number of infectious cities\n\
reference at {:.0f}, from {:s} data, TG".format(PoI, D_Name))
plt.xlabel("Duration of the threshold time-interval")
plt.ylabel("Log10 increase of It")
plt.savefig("growth_observation_InfE_traj_{:d}_v{:d}_TG.png".format(PoI, version))


#%%
summary_parameters.to_csv("summary_parameters_v{:d}.csv".format(version))
summary_per_gen.to_csv("summary_per_gen_v{:d}.csv".format(version))
#%%
Nr = 200
r_0test= np.linspace(1.01, 6, Nr)

KV0 = 1/RU

#for the backward dynamics
bKA_U0 = KV0 * beta**(1+b)*np.sum(beta**(a-1))/NC
bKB_U0 = KV0 * beta**(a)*np.sum(beta**(b))/NC

bnuA = beta**(a-1)/np.sum(beta**(a-1))
bnuB = beta**(b)/np.sum(beta**(b))

#%%
#Nr = 200
pi_AU= np.ones(Nr)
pi_BU= np.ones(Nr)
Inc_cit_U = np.zeros(Nr)
Inc_pop_U = np.zeros(Nr)
for i in np.arange(Nr):
    err = 1
    n_it = 0
    pi = 1- np.exp(- r_0test[i] *(bKA_U0 + bKB_U0) )
    while (err > 5E-5) * (n_it < 500):
        pi_AU[i]= np.sum(bnuA*pi)
        pi_BU[i]= np.sum(bnuB*pi)
        pi2 = 1-np.exp(- r_0test[i] *( bKA_U0 *pi_AU[i] +bKB_U0* pi_BU[i]))
        err = np.max(np.abs(1-pi/pi2))
        n_it = n_it +1
        pi = pi2
    Inc_cit_U[i] = 1/NC * np.sum(pi)
    Inc_pop_U[i] = 1/np.sum(beta) * np.sum(beta*pi)
    
#%%

Tr = 2/NC*np.sum(beta**(1+a+b))
Det = (np.sum(beta**(1+a+b))/NC)**2 - np.sum(beta**(2*a))/NC * np.sum(beta**(2+2*b))/NC
Delta = Tr**2 - 4*Det
RP = (Tr + np.sqrt(Delta))/2
pV0 = 1 / RP

bKA_P0 = pV0 * beta**(1+b)*np.sum(beta**a)/NC
bKB_P0 = pV0 * beta**(a)*np.sum(beta**(1+b))/NC

bnuA_P = beta**(a)/np.sum(beta**(a))
bnuB_P = beta**(1+b)/np.sum(beta**(1+b))
#%%
pi_AP= np.ones(Nr)
pi_BP= np.ones(Nr)
Inc_cit_P = np.zeros(Nr)
Inc_pop_P = np.zeros(Nr)
for i in np.arange(Nr):
    err = 1
    n_it = 0
    pi = 1- np.exp(- r_0test[i] *(bKA_P0 + bKB_P0) )
    while (err > 5E-5) * (n_it < 500):
        pi_AP[i]= np.sum(bnuA_P*pi)
        pi_BP[i]= np.sum(bnuB_P*pi)
        pi2 = 1-np.exp(- r_0test[i] *( bKA_P0 *pi_AP[i] +bKB_P0* pi_BP[i]))
        err = np.max(np.abs(1-pi/pi2))
        n_it = n_it +1
        pi = pi2
    Inc_cit_P[i] = 1/NC * np.sum(pi)
    Inc_pop_P[i] = 1/np.sum(beta) * np.sum(beta*pi)


#%%
plt.plot(r_0test, Inc_cit_U, 'r^', label = "str (U)")
plt.plot(r_0test, Inc_cit_P, 'b*', label = "str (P)")
plt.title("Final incidence as a function \n \
of the initial reproduction number")
plt.xlabel("Initial reproduction number")
plt.ylabel("Proportion of infected cities")
plt.legend(title = "{:s}, pO, KB".format(abbrev))
plt.ylim(0, 1)
plt.savefig("final_inc_cit_ro_b_v{:d}_a{:.2f}_b{:.2f}_K.png".format(version, a, b))
#%%
plt.plot(r_0test, Inc_pop_U, 'r^', label = "str (U)")
plt.plot(r_0test, Inc_pop_P, 'b*', label = "str (P)")
plt.title("Final incidence of population as a function \n\
 of the initial reproduction number")
plt.xlabel("Initial reproduction number")
plt.ylabel("Proportion of infected people")
plt.legend(title = "{:s}, pO, KB".format(abbrev))
plt.ylim(0, 1)
plt.savefig("final_inc_pop_ro_b_v{:d}_a{:.2f}_b{:.2f}_K.png".format(version, a, b))

#%%
plt.plot(Inc_pop_U, r_0test*KV0*Inc_cit_U*NC/(Inc_pop_U*np.sum(beta)), 'r^', label = "str (U)")
plt.plot(Inc_pop_P, r_0test*pV0, 'b*', label = "str (P)")
plt.title("Scaled proportion of infected among quarantined \n \
as a function of the quarantined")
plt.xlabel("Proportion of quarantined people")
plt.ylabel("Scaled proportion of infected among quarantined")
plt.legend(title = "{:s}, pO, KB".format(abbrev))
plt.xlim(0, np.max(Inc_pop_P))
plt.ylim(0, np.max(r_0test*pV0))
plt.savefig("final_inc_ratio_b_v{:d}_a{:.2f}_b{:.2f}_K.png".format(version, a, b))

#%%
plt.plot(r_0test*KV0*Inc_cit_U*NC, r_0test*KV0*Inc_cit_U*NC/(Inc_pop_U*np.sum(beta)), 'r^', label = "str (U)")
plt.plot(r_0test*pV0*Inc_pop_P*np.sum(beta), r_0test*pV0, 'b*', label = "str (P)")
plt.title("Scaled proportion of infected among quarantined \n \
as a function of the infected population")
plt.xlabel("Scaled proportion of infected")
plt.ylabel("Scaled proportion of infected among quarantined")
plt.legend(title = "{:s}, pO, KB".format(abbrev))
plt.xlim(0, np.max(r_0test*pV0*Inc_pop_P*np.sum(beta)))
plt.ylim(0, np.max(r_0test*pV0))
plt.savefig("final_inc_ratio2_b_v{:d}_a{:.2f}_b{:.2f}_K.png".format(version, a, b))

#%%
plt.plot(r_0test*KV0*Inc_cit_U*NC, Inc_pop_U, 'r^', label = "str (U)")
plt.plot(r_0test*pV0*Inc_pop_P*np.sum(beta), Inc_pop_P, 'b*', label = "str (P)")
plt.title("Scaled proportion of infected among quarantined \n \
as a function of the infected population")
plt.xlabel("Scaled proportion of infected, a={:.2f}, b= {:.2f}".format(a, b))
plt.ylabel("Scaled proportion of people quarantined")
plt.legend(title = "{:s}, pO, KB".format(abbrev))
Im= np.sum(Inc_pop_U < np.max(Inc_pop_P))
plt.xlim(0, r_0test[Im]*KV0*Inc_cit_U[Im]*NC)
plt.ylim(0, np.max(Inc_pop_P))
plt.savefig("final_quar_vs_infected_v{:d}_a{:.2f}_b{:.2f}_K.png".format(version, a, b))
#%%
#Nr = 200
pi_AU= np.ones(Nr)
pi_BU= np.ones(Nr)
Inc_cit_U = np.zeros(Nr)
Inc_pop_U = np.zeros(Nr)
for i in np.arange(Nr):
    err = 1
    n_it = 0
    pi = 1- np.exp(- r_0test[i] *bKA_U0)
    while (err > 5E-5) * (n_it < 500):
        pi_AU[i]= np.sum(bnuA*pi)
        pi_BU[i]= np.sum(bnuB*pi)
        pi2 = 1-np.exp(- r_0test[i] *( bKA_U0 *pi_AU[i] +bKB_U0* pi_BU[i]))
        err = np.max(np.abs(1-pi/pi2))
        n_it = n_it +1
        pi = pi2
    Inc_cit_U[i] = 1/NC * np.sum(pi)
    Inc_pop_U[i] = 1/np.sum(beta) * np.sum(beta*pi)
    
#%%

Tr = 2/NC*np.sum(beta**(1+a+b))
Det = (np.sum(beta**(1+a+b))/NC)**2 - np.sum(beta**(2*a))/NC * np.sum(beta**(2+2*b))/NC
Delta = Tr**2 - 4*Det
RP = (Tr + np.sqrt(Delta))/2
pV0 = 1 / RP

bKA_P0 = pV0 * beta**(1+b)*np.sum(beta**a)/NC
bKB_P0 = pV0 * beta**(a)*np.sum(beta**(1+b))/NC

bnuA_P = beta**(a)/np.sum(beta**(a))
bnuB_P = beta**(1+b)/np.sum(beta**(1+b))
#%%
pi_AP= np.ones(Nr)
pi_BP= np.ones(Nr)
Inc_cit_P = np.zeros(Nr)
Inc_pop_P = np.zeros(Nr)
for i in np.arange(Nr):
    err = 1
    n_it = 0
    pi = 1- np.exp(- r_0test[i] *(bKA_P0 + bKB_P0) )
    while (err > 5E-5) * (n_it < 500):
        pi_AP[i]= np.sum(bnuA_P*pi)
        pi_BP[i]= np.sum(bnuB_P*pi)
        pi2 = 1-np.exp(- r_0test[i] *( bKA_P0 *pi_AP[i] +bKB_P0* pi_BP[i]))
        err = np.max(np.abs(1-pi/pi2))
        n_it = n_it +1
        pi = pi2
    Inc_cit_P[i] = 1/NC * np.sum(pi)
    Inc_pop_P[i] = 1/np.sum(beta) * np.sum(beta*pi)


#%%

KA_U0 = KV0 * beta**(b)*np.sum(beta**a)/NC
KB_U0 = KV0 * beta**(a-1)*np.sum(beta**(1+b))/NC

KA_P0 = pV0 * beta**(1+b)*np.sum(beta**a)/NC
KB_P0 = pV0 * beta**(a)*np.sum(beta**(1+b))/NC

nuA = beta**(a)/np.sum(beta**(a))
nuB = beta**(1+b)/np.sum(beta**(1+b))

#%%
pi_AU= np.ones(Nr)
pi_BU= np.ones(Nr)
Poutbreak_cit_U = np.zeros(Nr)
Poutbreak_pop_U = np.zeros(Nr)
for i in np.arange(Nr):
    err = 1
    n_it = 0
    pi = 1- np.exp(- r_0test[i] *(KA_U0 + KB_U0) )
    while (err > 5E-5) * (n_it < 500):
        pi_AU[i]= np.sum(nuA*pi)
        pi_BU[i]= np.sum(nuB*pi)
        pi2 = 1-np.exp(- r_0test[i] *( KA_U0 *pi_AU[i] +KB_U0* pi_BU[i]))
        err = np.max(np.abs(1-pi/pi2))
        n_it = n_it +1
        pi = pi2
    Poutbreak_cit_U[i] = 1/NC * np.sum(pi)
    Poutbreak_pop_U[i] = 1/np.sum(beta) * np.sum(beta*pi)

#%%
pi_AP= np.ones(Nr)
pi_BP= np.ones(Nr)
Poutbreak_cit_P = np.zeros(Nr)
Poutbreak_pop_P = np.zeros(Nr)
for i in np.arange(Nr):
    err = 1
    n_it = 0
    pi = 1- np.exp(- r_0test[i] *(KA_P0 + KB_P0))
    while (err > 5E-5) * (n_it < 500):
        pi_AP[i]= np.sum(nuA*pi)
        pi_BP[i]= np.sum(nuB*pi)
        pi2 = 1-np.exp(- r_0test[i] *( KA_P0 *pi_AP[i] +KB_P0* pi_BP[i]))
        err = np.max(np.abs(1-pi/pi2))
        n_it = n_it +1
        pi = pi2
    Poutbreak_cit_P[i] = 1/NC * np.sum(pi)
    Poutbreak_pop_P[i] = 1/np.sum(beta) * np.sum(beta*pi)
    

#%%
plt.plot(r_0test, Poutbreak_cit_U, 'r^', label = "str (U)")
plt.plot(r_0test, Poutbreak_cit_P, 'b*', label = "str (P)")
plt.title("Theoretical probability of outbreak as a function \n \
of the initial reproduction number")
plt.xlabel("Initial reproduction number")
plt.ylabel("Theoretical probability of outbreak \n from a uniformly chosen city")
plt.legend(title = "{:s}, pO, KB".format(abbrev))
plt.ylim(0, 1)
plt.savefig("final_Poutbreak_cit_ro_v{:d}_K.png".format(version))
#%%
plt.plot(r_0test, Poutbreak_pop_U, 'r^', label = "str (U)")
plt.plot(r_0test, Poutbreak_pop_P, 'b*', label = "str (P)")
plt.title("Theoretical probability of outbreak \n \
as a function of the initial reproduction number")
plt.xlabel("Initial reproduction number")
plt.ylabel("Theoretical probability of outbreak \n from a uniformly chosen individual")
plt.legend(title = "{:s}, pO, KB".format(abbrev))
plt.ylim(0, 1)
plt.savefig("final_Poutbreak_pop_ro_v{:d}_K.png".format(version))

#%%

Inf_cit = np.zeros((bR, NR))
Inf_ppl = np.zeros((bR, NR))
NP = np.sum(beta)
for iR in np.arange(bR):
    for j in np.arange(NR):
        Tf2, IAf2, Rf2, Shf2, N_it2 = InfectionRM(100, 1, (R0_var[iR]/RP_ref) * MErP, nuA, np.ones(NC))
        Inf_cit[iR, j] = np.sum(Rf2)/NC
        Inf_ppl[iR, j] = np.sum(beta[Rf2])/NP
summary_per_R0['pO_strP_Inf_cit'] = np.mean(Inf_cit, axis = 1)
summary_per_R0['pO_strP_Inf_ppl'] = np.mean(Inf_ppl, axis = 1)


plt.plot(R0_var, summary_per_R0['pO_strP_Inf_cit'], 'b*', label = "Infected Cities")
plt.plot(R0_var, summary_per_R0['pO_strP_Inf_ppl'], 'r*', label = "People in Infected Cities")
plt.title("Proportion of infected cities and people,\n \
From {:s} data, str (P), TG".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("Proportion of infected entities")
plt.legend(title = "{:s}, strP, TG".format(abbrev))
plt.ylim(0, 1)
plt.savefig("Final_incidence_ro_v{:d}_strP_TG.png".format(version))

#%%
pi_AP= np.ones(Nr)
pi_BP= np.ones(Nr)
Inc_cit_P = np.zeros(bR)
Inc_pop_P = np.zeros(bR)
for i in np.arange(bR):
    err = 1
    n_it = 0
    pi = 1- np.exp(- R0_var[i] *(bKA_P0 + bKB_P0) )
    while (err > 5E-5) * (n_it < 500):
        pi_AP[i]= np.sum(bnuA_P*pi)
        pi_BP[i]= np.sum(bnuB_P*pi)
        pi2 = 1-np.exp(- R0_var[i] *( bKA_P0 *pi_AP[i] +bKB_P0* pi_BP[i]))
        err = np.max(np.abs(1-pi/pi2))
        n_it = n_it +1
        pi = pi2
    Inc_cit_P[i] = 1/NC * np.sum(pi)
    Inc_pop_P[i] = 1/np.sum(beta) * np.sum(beta*pi)




plt.plot(R0_var, Inc_cit_P, 'b*', label = "Infected Cities")
plt.plot(R0_var, Inc_pop_P, 'r*', label = "People in Infected Cities")
plt.title("Expected proportion of infected cities and people,\n \
From {:s} data, str (P), K".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("Proportion of infected entities")
plt.legend(title = "{:s}, strP, KG".format(abbrev))
plt.ylim(0, 1)
plt.savefig("Final_incidence_thr_ro_v{:d}_strP_K.png".format(version))
#%%
#%%
eta_AP= np.ones(Nr)
eta_BP= np.ones(Nr)
Prob_Out = np.zeros(bR)
for i in np.arange(bR):
    err = 1
    n_it = 0
    eta = 1- np.exp(- R0_var[i] *(KA_P0 + KB_P0) )
    while (err > 5E-5) * (n_it < 500):
        eta_AP[i]= np.sum(nuA*eta)
        eta_BP[i]= np.sum(nuB*eta)
        eta2 = 1-np.exp(- R0_var[i] *( KA_P0 *eta_AP[i] + KB_P0* eta_BP[i]))
        err = np.max(np.abs(1-eta/eta2))
        n_it = n_it +1
        eta = eta2
    Prob_Out[i] = np.sum(eta*nuA)



plt.plot(R0_var, summary_per_R0['pO_strP_Inf_cit'], 'b*', label = "TG Infected Cities")
plt.plot(R0_var, summary_per_R0['pO_strP_Inf_ppl'], 'r*', label = "TG People in Infected Cities")
plt.plot(R0_var, Prob_Out * Inc_cit_P, 'bo:', label = "Exp Infected Cities")
plt.plot(R0_var, Prob_Out * Inc_pop_P, 'ro:', label = "Exp People in Infected Cities")

plt.title("Proportion of infected cities and people,\n \
From {:s} data, str (P)".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("Proportion of infected entities")
plt.legend(loc='upper left')
plt.ylim(0, 1)
plt.savefig("Final_incidence_ro_v{:d}_strP_TG.png".format(version))

#%%
R0_var_2 = R0_var[R0_var > 2]
bR2 = R0_var_2.size
Inf_cit = np.zeros((bR2, NR))
Inf_ppl = np.zeros((bR2, NR))
NP = np.sum(beta)
S_O = 60
for iR in np.arange(bR2):
    for j in np.arange(NR):
        Tf2, IAf2, Rf2, Shf2, N_it2 = InfectionRM(100, S_O, (R0_var_2[iR]/RP_ref) * MErP, nuA, np.ones(NC))
        Inf_cit[iR, j] = np.sum(Rf2)/NC
        Inf_ppl[iR, j] = np.sum(beta[Rf2])/NP
summary_per_R0['pO_strP_cO_Inf_cit'] = -1 
summary_per_R0.loc[R0_var > 2, 'pO_strP_cO_Inf_cit'] = np.mean(Inf_cit, axis = 1)
summary_per_R0['pO_strP_cO_Inf_ppl'] = -1
summary_per_R0.loc[R0_var > 2, 'pO_strP_cO_Inf_ppl'] = np.mean(Inf_ppl, axis = 1)


plt.plot(R0_var_2, np.mean(Inf_cit, axis = 1), 'b*', label = "Infected Cities")
plt.plot(R0_var_2, np.mean(Inf_ppl, axis = 1), 'r*', label = "People in Infected Cities")
plt.title("Proportion of infected cities and people,\n \
From {:s} data, str (P), TG".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("Proportion of infected entities")
plt.legend()
plt.ylim(0, 1)
plt.savefig("Final_incidence_ro_v{:d}_pO_strP_TG.png".format(version))

#%%
rho_AP= np.ones(Nr)
rho_BP= np.ones(Nr)
Inc_cit_P = np.zeros(bR2)
Inc_pop_P = np.zeros(bR2)
for i in np.arange(bR2):
    err = 1
    n_it = 0
    rho = 1- np.exp(- R0_var_2[i] *(bKA_P0 + bKB_P0) )
    while (err > 5E-5) * (n_it < 500):
        rho_AP[i]= np.sum(bnuA_P*rho)
        rho_BP[i]= np.sum(bnuB_P*rho)
        rho2 = 1-np.exp(- R0_var_2[i] *( bKA_P0 *rho_AP[i] +bKB_P0* rho_BP[i]))
        err = np.max(np.abs(1-rho/rho2))
        n_it = n_it +1
        rho = rho2
    Inc_cit_P[i] = 1/NC * np.sum(rho)
    Inc_pop_P[i] = 1/np.sum(beta) * np.sum(beta*rho)




plt.plot(R0_var_2, Inc_cit_P, 'b*', label = "Infected Cities")
plt.plot(R0_var_2, Inc_pop_P, 'r*', label = "People in Infected Cities")
plt.title("Expected proportion of infected cities and people,\n \
From {:s} data, str (P), K".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("Proportion of infected entities")
plt.legend()
plt.ylim(0, 1)
plt.savefig("Final_incidence_thr_ro_v{:d}_pO_strP_K.png".format(version))
#%%

plt.plot(R0_var_2, np.mean(Inf_cit, axis = 1), 'b*', label = "TG Infected Cities")
plt.plot(R0_var_2, np.mean(Inf_ppl, axis = 1), 'r*', label = "TG People in Infected Cities")
plt.plot(R0_var_2, Inc_cit_P, 'bo:', label = "Exp Infected Cities")
plt.plot(R0_var_2, Inc_pop_P, 'ro:', label = "Exp People in Infected Cities")

plt.title("Proportion of infected cities and people,\n \
From {:s} data, str (P)".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("Proportion of infected entities")
plt.legend(loc='upper left')
plt.ylim(0, 1)
plt.savefig("Final_incidence_ro_v{:d}_pO_strP_TG.png".format(version))

#%%
summary_parameters.to_csv("summary_parameters_v{:d}.csv".format(version))
summary_per_R0.to_csv("summary_per_R0_v{:d}.csv".format(version))
#%%
