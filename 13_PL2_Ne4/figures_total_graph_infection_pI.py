# -*- coding: utf-8 -*-
"""
Created on Tue Feb 15 10:50:24 2022

@author: alter
"""

#%% Import and description

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

print("The simulation data is assumed to have already been collected under this version number\n \
under the stipulated version name. Proceed only if correct!")
version = int(input("What is the number of the version?  > version = "))
#version = 86
D_Name = input("How the data should be described in the titles?  > D_Name = ")
#D_Name = "Polish (D0+)"
abbrev = input("How the data should be abbreviated in the legends?  > abbrev = ")
#abbrev = "PC, D0+"

print("The simulation data is assumed to be available for the (pI) choice of R0\n\
where a reference for the theoretical probability for a city to be infected is taken.\n\
cf threshold_through_pInfected.py for more details.")

#%% Value extractions

summary_parameters = pd.read_csv("summary_parameters_v{:d}.csv".format(version))
summary_per_city= pd.read_csv("summary_per_city_v{:d}.csv".format(version))
summary_per_bin= pd.read_csv("summary_per_bin_v{:d}.csv".format(version))
summary_per_gen = pd.read_csv("summary_per_gen_v{:d}.csv".format(version))
summary_per_R0 = pd.read_csv("summary_per_R0_v{:d}.csv".format(version))

beta = np.array(summary_per_city["City_size"])
NC = beta.size
a = summary_parameters.loc[0, 'a']
b = summary_parameters.loc[0, 'b']

MidBins = summary_per_bin['Central_value_log_sc']

RU = 1/NC*(np.sum(beta**(a+b)) + np.sqrt(np.sum(beta**(2*a-1)) \
* np.sum(beta**(1+2*b))))
RU_ref = summary_parameters.loc[0, "pI_strU_thr_R0"] 

RP_ref = summary_parameters.loc[0, "pI_strP_thr_R0"]
RP = 1/NC*(np.sum(beta**(a+b+1)) + np.sqrt(np.sum(beta**(2*a)) \
* np.sum(beta**(2+2*b))))
    
rhoE = summary_parameters.loc[0, 'pI_strU_exp_growth_rate']
print("Expected growth rate~{:.2e} for power 10".format(rhoE))

    
# #%%
# #For strategy (U)

# beta1 = beta.reshape((NC, 1))
# beta2 = beta.reshape((1, NC))
# #sum of contributions to the kernel, the diagonal being removed
# Sb = np.sum(beta1**(1+b)*beta2**a)-np.sum(beta**(1+b+a))

# KM = RU_ref/(RU * NC) * Sb / MCom.sum() 
# # KV = RU_ref/RU
# # KM = KV/MCom.sum() * Sb / NC
# print("k_M.K_vee~{:.3e}".format(KM))
# print("equivalent R_0~{:.3e}".format(RU_ref))

# MERi = MCom
# MERe = MCom.transpose()
# MER = KM*(MERi + MERe)
# for j in np.arange(NC):
#     MER[j] = MER[j]/beta[j]
# MEr = MER.toarray()


# #%%
# #For strategy (P)

# beta1 = beta.reshape((NC, 1))
# beta2 = beta.reshape((1, NC))


# pM = RP_ref/(RP * NC) * Sb / MCom.sum() 
# # KV = RU_ref/RU
# # KM = KV/MCom.sum() * Sb / NC
# print("p_M.K_vee~{:.3e}".format(pM))
# print("equivalent R_0~{:.3e}".format(RP_ref))
        

# MERi = MCom
# MERe = MCom.transpose()
# MER = pM*(MERi + MERe)
# MErP = MER.toarray()




#%% Plot "theoretical_Infected_vs_size_v{:d}_pI_strU_TG"

pi_U = summary_per_city["pI_strU_thr_pi"] 
piRef_U = summary_per_bin["pI_strU_thr_pi"] 

plt.figure(5)
plt.clf()
plt.scatter(np.log10(beta), pi_U, c="black", marker="+", label = "analytical relation")
plt.title("Theoretical infection probability, \n \
data: {:s}, strU, TG".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pI, strP, TG".format(abbrev))
plt.ylim(0, 1)
plt.savefig("theoretical_Infected_vs_size_v{:d}_pI_strU_TG.png".format(version))

#%% Plot "Infected_vs_size_v{:d}_pI_strU_TG.png"

PropI_avg = summary_per_bin["pI_strU_emp_pi"] 

PropI_std = summary_per_bin["pI_strU_std_emp_pi"]

PropI_min = summary_per_bin["pI_strU_min_pi"] 

PropI_max = summary_per_bin["pI_strU_max_pi"]

PropI_s = PropI_avg + PropI_std
PropI_ms = PropI_avg - PropI_std

plt.figure(3)
plt.clf()
plt.plot(MidBins, piRef_U, "ks--", label = "analytical")
plt.plot(MidBins, PropI_avg, "bD", label="empirical")
plt.plot(MidBins, PropI_s, "^r:", label="std")
plt.plot(MidBins, PropI_ms, "vr:")
plt.vlines(MidBins, \
          ymin = PropI_min,  ymax = PropI_max,\
         color= "purple", linestyle =":")
plt.plot(MidBins, PropI_min, "mv:")
plt.plot(MidBins, PropI_max, "m^:", label = "range")
plt.title("Empirical and theoretical infection probability,\n \
fluctuations between the runs, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pI, strU, TG".format(abbrev))
plt.ylim(-0.02, 1.02)

plt.savefig("Infected_vs_size_v{:d}_pI_strU_TG.png".format(version))

#%% Plot "variation_infected_vs_size_v{:d}_pI_strU_TG.png"

Is_I_avg = summary_per_city["pI_strU_emp_pi"] 

#Is_I_var = np.sum(Is_I**2, axis =0)/N_p - Is_I_avg**2
Is_I_std = summary_per_city["pI_strU_std_emp_pi"]

Prop2I_avg = summary_per_bin["pI_strU_empB_pi"]
Prop2I_std = summary_per_bin["pI_strU_stdB_emp_pi"]
Prop2I_min = summary_per_bin["pI_strU_minB_pi"]
Prop2I_max = summary_per_bin["pI_strU_maxB_pi"]

Prop2I_s = Prop2I_avg + Prop2I_std
Prop2I_ms = Prop2I_avg - Prop2I_std

R2 = summary_parameters.loc[0, "pI_strU_infP_R2"]

plt.figure(4)
plt.clf()
plt.plot(MidBins, piRef_U, "ks--", label = "analytical")
plt.plot(MidBins, Prop2I_avg, "bD", label="empirical")
plt.plot(MidBins, Prop2I_s, "^r:", label="std")
plt.plot(MidBins, Prop2I_ms, "vr:")
plt.vlines(MidBins, \
          ymin = Prop2I_min,  ymax = Prop2I_max,\
         color= "purple", linestyle =":")
plt.plot(MidBins, Prop2I_min, "mv:")
plt.plot(MidBins, Prop2I_max, "m^:", label = "range")
plt.title("Empirical and theoretical infection probability,\n \
fluctuations within the bin, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pI, strU, TG\n\
R^2 = {:.2f}".format(abbrev, summary_parameters.loc[0,"pI_strU_infP_R2"]))
plt.ylim(-0.02, 1.02)


plt.savefig("variation_infected_vs_size_v{:d}_pI_strU_TG.png".format(version))

#%%Plot "Infected_vs_size_v{:d}_pI_strP_TG.png"

pi_P = summary_per_city["pI_strP_thr_pi"]
piRef_P = summary_per_bin["pI_strP_thr_pi"]

PropIP_avg = summary_per_bin["pI_strP_emp_pi"]
PropIP_std = summary_per_bin["pI_strP_std_emp_pi"]
PropIP_min = summary_per_bin["pI_strP_min_pi"]
PropIP_max = summary_per_bin["pI_strP_max_pi"]

PropIP_s = PropIP_avg + PropIP_std
PropIP_ms = PropIP_avg - PropIP_std

plt.figure(3)
plt.clf()
plt.plot(MidBins, piRef_P, "ks--", label = "analytical")
plt.plot(MidBins, PropIP_avg, "bD", label="empirical")
plt.plot(MidBins, PropIP_s, "^r:", label="std")
plt.plot(MidBins, PropIP_ms, "vr:")
plt.vlines(MidBins, \
          ymin = PropIP_min,  ymax = PropIP_max,\
         color= "purple", linestyle =":")
plt.plot(MidBins, PropIP_min, "mv:")
plt.plot(MidBins, PropIP_max, "m^:", label = "range")
plt.title("Empirical and theoretical infection probability,\n \
fluctuations between the runs, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pI, strP, TG".format(abbrev))
plt.ylim(0, 1.02)

plt.savefig("Infected_vs_size_v{:d}_pI_strP_TG.png".format(version))

#%% Plot "variation_infected_vs_size_v{:d}_pI_strP_TG.png"

Is_IP_avg = summary_per_city["pI_strP_emp_pi"]
Is_IP_std = summary_per_city["pI_strP_std_emp_pi"]

Prop2IP_avg = summary_per_bin["pI_strP_empB_pi"] 
Prop2IP_std = summary_per_bin["pI_strP_stdB_emp_pi"]
Prop2IP_min = summary_per_bin["pI_strP_minB_pi"]
Prop2IP_max = summary_per_bin["pI_strP_maxB_pi"]

Prop2IP_s = Prop2IP_avg + Prop2IP_std
Prop2IP_ms = Prop2IP_avg - Prop2IP_std

R2 = summary_parameters.loc[0, "pI_strP_infP_R2"]

plt.figure(4)
plt.clf()
plt.plot(MidBins, piRef_P, "ks--", label = "analytical")
plt.plot(MidBins, Prop2IP_avg, "bD", label="empirical")
plt.plot(MidBins, Prop2IP_s, "^r:", label="std")
plt.plot(MidBins, Prop2IP_ms, "vr:")
plt.vlines(MidBins, \
          ymin = Prop2IP_min,  ymax = Prop2IP_max,\
         color= "purple", linestyle =":")
plt.plot(MidBins, Prop2IP_min, "mv:")
plt.plot(MidBins, Prop2IP_max, "m^:", label = "range")
plt.title("Empirical and theoretical infection probability,\n \
fluctuations within the bin, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pI, strP, TG\n\
R^2 = {:.2f}".format(abbrev, summary_parameters.loc[0,"pI_strP_infP_R2"]))
plt.ylim(0, 1.02)


plt.savefig("variation_infected_vs_size_v{:d}_pI_strP_TG.png".format(version))



#%% Plot "theoretical_pOutbreak_vs_size_v{:d}_KB.png"

eta_U = summary_per_city["pI_strU_thr_eta"]
etaRef_U = summary_per_bin["pI_strU_thr_eta"] 


plt.figure(5)
plt.clf()
plt.plot(np.log10(beta), eta_U, "ks", markersize = 5, label = "analytical relation")
plt.title("Theoretical outbreak probability, \n \
data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pI, strU, KB".format(abbrev))
plt.savefig("theoretical_pOutbreak_vs_size_v{:d}_KB.png".format(version))


#%% Plot "empirical_pOutbreak_vs_size_v{:d}_pI_strU_TG.png"

PropOut_TGU = summary_per_bin["pI_strU_emp1_eta"]

plt.figure(4)
plt.clf()
plt.scatter(MidBins, etaRef_U, c="black", marker="+", label = "analytical relation")
plt.scatter(np.log10(summary_per_bin['Lower_size']), PropOut_TGU, c="red", marker="+", label = "from simulation runs of epidemic")
plt.title("Empirical and theoretical outbreak probability, \n \
data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.ylim(0,1.02)
plt.legend(title = "{:s}, pI, strU, TG".format(abbrev))
plt.savefig("empirical_pOutbreak_vs_size_v{:d}_pI_strU_TG.png".format(version))

#It appears that the sizes of outbreaks are so small 
#that the threshold on the size makes a large impact on the observed probabilities


#%% Plot "variation_empirical_pOutbreak_vs_size_v{:d}_pI_strU_TG.png"

#S_O = int(input("What is the size threshold for a successful outbreak?  > S_O = "))
#S_O = 30.
S_O = summary_parameters.loc[0, "pI_size_successful_outbreak"]
#R_c = int(input("How many runs for each initial city?  > Rc = "))
#Rc = 1000
Rc = summary_parameters.loc[0, "pI_strU_eta_Nbr_runs"]

PropOut_TGU = summary_per_city["pI_strU_emp_eta"]

Mean_pOut_TGU = summary_per_bin["pI_strU_empB_eta"]
Std_pOut_TGU = summary_per_bin["pI_strU_stdB_emp_eta"] 
Min_pOut_TGU = summary_per_bin["pI_strU_minB_eta"]
Max_pOut_TGU = summary_per_bin["pI_strU_maxB_eta"]

plt.figure(4)
plt.clf()
plt.plot(MidBins, etaRef_U, "ks-", label = "analytical")
plt.plot(MidBins, Mean_pOut_TGU, "bD--", label = "empirical")
plt.plot(MidBins, Mean_pOut_TGU + Std_pOut_TGU, "^r:", label="std")
plt.plot(MidBins, Mean_pOut_TGU - Std_pOut_TGU, "vr:")
plt.vlines(MidBins, \
          ymin = Min_pOut_TGU,  ymax = Max_pOut_TGU,\
         color= "purple", linestyle =":")
plt.plot(MidBins, Min_pOut_TGU, "mv:")
plt.plot(MidBins, Max_pOut_TGU, "m^:", label = "range")
plt.title("Empirical and theoretical outbreak probability, \n \
Fluctuations within the bin, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.ylim(0,1.02)
plt.legend(title = "{:s}, pI, strU, TG".format(abbrev))
plt.savefig("variation_empirical_pOutbreak_vs_size_v{:d}_pI_strU_TG.png".format(version))

#It appears that the sizes of outbreaks are so small 
#that the threshold on the size makes a large impact on the observed probabilities

#%% Plot "empirical_pOutbreak_vs_size_v{:d}_pI_strU_KG.png"

PropOut_KGU = summary_per_bin["pI_strU_empK_eta"]
    
plt.figure(4)
plt.clf()
plt.scatter(MidBins, etaRef_U, c="black", marker="+", label = "analytical relation")
plt.scatter(np.log10(summary_per_bin['Lower_size']), PropOut_KGU, c="red", marker="+", label = "from simulation runs of epidemic")
plt.title("Empirical probability of triggering an outbreak as a function , \n \
of the size of first infected, data: {:s}, str. U, KG".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion of outbreaks")
plt.ylim(0,1)
plt.legend(title = "{:s}, pI, strU, KG".format(abbrev))
plt.savefig("empirical_pOutbreak_vs_size_v{:d}_pI_strU_KG.png".format(version))


#%% Plot "theoretical_pOutbreak_vs_size_v{:d}_pI_strP_TG.png"

eta_P = summary_per_city["pI_strP_thr_eta"]
etaRef_P = summary_per_bin["pI_strP_thr_eta"]

plt.figure(5)
plt.clf()
plt.plot(np.log10(beta), eta_P, "ks", markersize = 5, label = "analytical relation")
plt.title("Theoretical probability of triggering an outbreak as a function , \n \
of the size of first infected, data: {:s}, strP, TG".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pI, strP, TG".format(abbrev))
plt.savefig("theoretical_pOutbreak_vs_size_v{:d}_pI_strP_TG.png".format(version))




#%% Plot "variation_empirical_pOutbreak_vs_size_v{:d}_pI_strP_TG.png"

PropOut_TGP = summary_per_city["pI_strP_emp_eta"]

Mean_pOut_TGP = summary_per_bin["pI_strP_empB_eta"] 
Std_pOut_TGP = summary_per_bin["pI_strP_stdB_emp_eta"]
Min_pOut_TGP = summary_per_bin["pI_strP_minB_eta"]
Max_pOut_TGP = summary_per_bin["pI_strP_maxB_eta"]

plt.figure(4)
plt.clf()
plt.plot(MidBins, etaRef_P, "ks-", label = "analytical")
plt.plot(MidBins, Mean_pOut_TGP, "bD:", label = "empirical")
plt.plot(MidBins, Mean_pOut_TGP + Std_pOut_TGP, "^r:", label="std")
plt.plot(MidBins, Mean_pOut_TGP - Std_pOut_TGP, "vr:")
plt.vlines(MidBins, \
          ymin = Min_pOut_TGP,  ymax = Max_pOut_TGP,\
         color= "purple", linestyle =":")
plt.plot(MidBins, Min_pOut_TGP, "mv:")
plt.plot(MidBins, Max_pOut_TGP, "m^:", label = "range")
plt.title("Empirical and theoretical outbreak probability, \n \
Fluctuations within the bin, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.ylim(0,1.02)
plt.legend(title = "{:s}, pI, strP, TG".format(abbrev))
plt.savefig("variation_empirical_pOutbreak_vs_size_v{:d}_pI_strP_TG.png".format(version))


#%% R0 estimation
#######################################################
#%%  Plot "growth_rate_R0_LSestimation_{:d}_v{:d}_pI_strU_TG.png"

#In this new version, least square estimation is exploited \
#instead of a simple interpolation between extremities

#PoT = float(input("Which threshold for the infected population size at quasi-equilibrium? \n >PoR = "))
#PoR = 30
PoR = summary_parameters.loc[0, "pI_strU_TI_init_thres"]

TotE_avg = summary_per_gen["pI_strU_TI_R0_emp"] 
TotE_std = summary_per_gen["pI_strU_TI_R0_std"] 
N_o = TotE_avg-1
    
TotE_s = TotE_avg +TotE_std
TotE_ms = TotE_avg - TotE_std

rhoE = summary_parameters.loc[0, "pI_strU_log10-R0_per_gen"]
print("Expected growth rate~{:.2e}".format(rhoE))

plt.figure(6)
plt.clf()
plt.plot(summary_per_gen['Gen_after_thr'], TotE_avg, "bD--", label="empirical")
plt.plot(summary_per_gen['Gen_after_thr'], \
   rhoE*np.ones(summary_per_gen['Gen_after_thr'].size), c="black", label="expected")
plt.plot(summary_per_gen['Gen_after_thr'], TotE_s, "r^:", label="+std")
plt.plot(summary_per_gen['Gen_after_thr'], TotE_ms, "rv:", label="-std")
plt.vlines(summary_per_gen['Gen_after_thr'], \
          ymin = summary_per_gen["pI_strU_TI_R0_q5"],\
          ymax = summary_per_gen["pI_strU_TI_R0_q95"],\
         color= "purple", linestyle =":")
plt.plot(summary_per_gen['Gen_after_thr'], summary_per_gen["pI_strU_TI_R0_q95"], "m^:")
plt.plot(summary_per_gen['Gen_after_thr'], summary_per_gen["pI_strU_TI_R0_q5"], "mv:", label = "5%-95%")
plt.title("Growth rate estimation, \n\
from the total number of infected, reference at {:.0f}, data: {:s} ".format(PoR, D_Name))
plt.xlabel("Number of generations in the time-interval")
plt.ylabel("Estimated growth rate")
plt.legend(title = "{:s}, pI, strU, TG".format(abbrev))
plt.ylim(-0.02*rhoE, 1.1*rhoE)
plt.savefig("growth_rate_R0_LSestimation_{:d}_v{:d}_pI_strU_TG.png".format(PoR, version))

#%% Plot "RS_growth_rate_TotE_estimation_{:d}_v{:d}_pI_strU_TG.png"
#rescaled graph
plt.figure(7)
plt.clf()
plt.plot(summary_per_gen['Gen_after_thr'], TotE_avg/rhoE, "bD--", label="empirical")
plt.plot(summary_per_gen['Gen_after_thr'], \
         np.ones(summary_per_gen['Gen_after_thr'].size), c="black", label="expected")
plt.plot(summary_per_gen['Gen_after_thr'], TotE_s/rhoE, "r^:", label="+std")
plt.plot(summary_per_gen['Gen_after_thr'], TotE_ms/rhoE, "rv:", label="-std")
plt.vlines(summary_per_gen['Gen_after_thr'], \
          ymin = summary_per_gen["pI_strU_TI_R0_q5"]/rhoE,\
          ymax = summary_per_gen["pI_strU_TI_R0_q95"]/rhoE,\
         color= "purple", linestyle =":")
plt.plot(summary_per_gen['Gen_after_thr'], summary_per_gen["pI_strU_TI_R0_q95"]/rhoE, "m^:")
plt.plot(summary_per_gen['Gen_after_thr'], summary_per_gen["pI_strU_TI_R0_q5"]/rhoE, "mv:", label = "5%-95%")
plt.title("Growth rate estimation, \n\
from the total number of infected, reference at {:.0f}, data: {:s} ".format(PoR, D_Name))
plt.xlabel("Number of generations in the time-interval")
plt.ylabel("Estimated growth rate")
plt.legend(title = "{:s}, pI, strU, TG".format(abbrev))
plt.ylim(-0.02, 1.1)
plt.savefig("RS_growth_rate_TotE_estimation_{:d}_v{:d}_pI_strU_TG.png".format(PoR, version))


#%% Variations in R0
############################################################
#%% Setting for varying R0
Nr = 200
r_0test= np.linspace(1.01, 6, Nr)

KV0 = 1/RU

#for the backward dynamics
bKA_U0 = KV0 * beta**(1+b)*np.sum(beta**(a-1))/NC
bKB_U0 = KV0 * beta**(a)*np.sum(beta**(b))/NC

bnuA_U = beta**(a-1)/np.sum(beta**(a-1))
bnuB_U = beta**(b)/np.sum(beta**(b))

#%% Inc_cit_U and Inc_pop_U computed

#Nr = 200
pi_AU= np.ones(Nr)
pi_BU= np.ones(Nr)
Inc_cit_U = np.zeros(Nr)
Inc_pop_U = np.zeros(Nr)
for i in np.arange(Nr):
    err = 1
    n_it = 0
    pi = 1- np.exp(- r_0test[i] *(bKA_U0 + bKB_U0) )
    while (err > 5E-5) * (n_it < 500):
        pi_AU[i]= np.sum(bnuA_U*pi)
        pi_BU[i]= np.sum(bnuB_U*pi)
        pi2 = 1-np.exp(- r_0test[i] *( bKA_U0 *pi_AU[i] +bKB_U0* pi_BU[i]))
        err = np.max(np.abs(1-pi/pi2))
        n_it = n_it +1
        pi = pi2
    Inc_cit_U[i] = 1/NC * np.sum(pi)
    Inc_pop_U[i] = 1/np.sum(beta) * np.sum(beta*pi)
    
#%% Neutral kernels computed

Tr = 2/NC*np.sum(beta**(1+a+b))
Det = (np.sum(beta**(1+a+b))/NC)**2 - np.sum(beta**(2*a))/NC * np.sum(beta**(2+2*b))/NC
Delta = Tr**2 - 4*Det
RP = (Tr + np.sqrt(Delta))/2
pV0 = 1 / RP

bKA_P0 = pV0 * beta**(1+b)*np.sum(beta**a)/NC
bKB_P0 = pV0 * beta**(a)*np.sum(beta**(1+b))/NC

bnuA_P = beta**(a)/np.sum(beta**(a))
bnuB_P = beta**(1+b)/np.sum(beta**(1+b))

#%% Inc_cit_P and Inc_pop_P computed
pi_AP= np.ones(Nr)
pi_BP= np.ones(Nr)
Inc_cit_P = np.zeros(Nr)
Inc_pop_P = np.zeros(Nr)
for i in np.arange(Nr):
    err = 1
    n_it = 0
    pi = 1- np.exp(- r_0test[i] *(bKA_P0 + bKB_P0) )
    while (err > 5E-5) * (n_it < 500):
        pi_AP[i]= np.sum(bnuA_P*pi)
        pi_BP[i]= np.sum(bnuB_P*pi)
        pi2 = 1-np.exp(- r_0test[i] *( bKA_P0 *pi_AP[i] +bKB_P0* pi_BP[i]))
        err = np.max(np.abs(1-pi/pi2))
        n_it = n_it +1
        pi = pi2
    Inc_cit_P[i] = 1/NC * np.sum(pi)
    Inc_pop_P[i] = 1/np.sum(beta) * np.sum(beta*pi)


#%% Plot "final_inc_cit_cO_b_v{:d}_a{:.2f}_b{:.2f}_KB.png"

plt.plot(r_0test, Inc_cit_U, 'r^', label = "strU")
plt.plot(r_0test, Inc_cit_P, 'b*', label = "strP")
plt.title("Final incidence as a function \n \
of the initial reproduction number, KB, conditional on outbreak")
plt.xlabel("Initial reproduction number")
plt.ylabel("Mean proportion of infected cities")
plt.legend(title = "{:s}, pI, KB".format(abbrev))
plt.ylim(0, 1)
plt.savefig("final_inc_cit_cO_b_v{:d}_a{:.2f}_b{:.2f}_KB.png".format(version, a, b))

#%% Plot "final_inc_pop_cO_b_v{:d}_a{:.2f}_b{:.2f}_KB.png"

plt.plot(r_0test, Inc_pop_U, 'r^', label = "strU")
plt.plot(r_0test, Inc_pop_P, 'b*', label = "strP")
plt.title("Final incidence of population as a function \n\
 of the initial reproduction number, KB, conditional on outbreak")
plt.xlabel("Initial reproduction number")
plt.ylabel("Mean proportion of infected people")
plt.legend(title = "{:s}, pI, KB".format(abbrev))
plt.ylim(0, 1)
plt.savefig("final_inc_pop_cO_b_v{:d}_a{:.2f}_b{:.2f}_KB.png".format(version, a, b))

#%%Plot "final_inc_ratio_b_v{:d}_a{:.2f}_b{:.2f}_KB.png"

plt.plot(Inc_pop_U, r_0test*KV0*Inc_cit_U*NC/(Inc_pop_U*np.sum(beta)), 'r^', label = "strU")
plt.plot(Inc_pop_P, r_0test*pV0, 'b*', label = "strP")
plt.title("Scaled proportion of infected among quarantined \n \
as a function of the quarantined, KB")
plt.xlabel("Proportion of quarantined people")
plt.ylabel("Scaled proportion of infected among quarantined")
plt.legend(title = "{:s}, pI, KB".format(abbrev))
plt.xlim(0, np.max(Inc_pop_P))
plt.ylim(0, np.max(r_0test*pV0))
plt.savefig("final_inc_ratio_b_v{:d}_a{:.2f}_b{:.2f}_KB.png".format(version, a, b))

#%% Plot "final_inc_ratio2_b_v{:d}_a{:.2f}_b{:.2f}_KB.png"

plt.plot(r_0test*KV0*Inc_cit_U*NC, r_0test*KV0*Inc_cit_U*NC/(Inc_pop_U*np.sum(beta)), 'r^', label = "strU")
plt.plot(r_0test*pV0*Inc_pop_P*np.sum(beta), r_0test*pV0, 'b*', label = "strP")
plt.title("Scaled proportion of infected among quarantined \n \
as a function of the infected population, KB")
plt.xlabel("Scaled proportion of infected")
plt.ylabel("Scaled proportion of infected among quarantined")
plt.legend(title = "{:s}, pI, KB".format(abbrev))
plt.xlim(0, np.max(r_0test*pV0*Inc_pop_P*np.sum(beta)))
plt.ylim(0, np.max(r_0test*pV0))
plt.savefig("final_inc_ratio2_b_v{:d}_a{:.2f}_b{:.2f}_KB.png".format(version, a, b))

#%% Plot "final_quar_vs_infected_v{:d}_a{:.2f}_b{:.2f}_KB.png"

plt.plot(r_0test*KV0*Inc_cit_U*NC, Inc_pop_U, 'r^', label = "strU")
plt.plot(r_0test*pV0*Inc_pop_P*np.sum(beta), Inc_pop_P, 'b*', label = "strP")
plt.title("Scaled proportion of infected among quarantined \n \
as a function of the infected population, KB")
plt.xlabel("Scaled proportion of infected, a={:.2f}, b= {:.2f}".format(a, b))
plt.ylabel("Scaled proportion of people quarantined")
plt.legend(title = "{:s}, pI, KB".format(abbrev))
Im= np.sum(Inc_pop_U < np.max(Inc_pop_P))
plt.xlim(0, r_0test[Im]*KV0*Inc_cit_U[Im]*NC)
plt.ylim(0, np.max(Inc_pop_P))
plt.savefig("final_quar_vs_infected_v{:d}_a{:.2f}_b{:.2f}_KB.png".format(version, a, b))

#%% Extended version with outbreak probab

#%% Inc_cit_U and Inc_pop_U computed

#Nr = 200
pi_AU= np.ones(Nr)
pi_BU= np.ones(Nr)
Inc_cit_U = np.zeros(Nr)
Inc_pop_U = np.zeros(Nr)
for i in np.arange(Nr):
    err = 1
    n_it = 0
    pi = 1- np.exp(- r_0test[i] *bKA_U0)
    while (err > 5E-5) * (n_it < 500):
        pi_AU[i]= np.sum(bnuA_U*pi)
        pi_BU[i]= np.sum(bnuB_U*pi)
        pi2 = 1-np.exp(- r_0test[i] *( bKA_U0 *pi_AU[i] +bKB_U0* pi_BU[i]))
        err = np.max(np.abs(1-pi/pi2))
        n_it = n_it +1
        pi = pi2
    Inc_cit_U[i] = 1/NC * np.sum(pi)
    Inc_pop_U[i] = 1/np.sum(beta) * np.sum(beta*pi)
    
#%% Backward strP

Tr = 2/NC*np.sum(beta**(1+a+b))
Det = (np.sum(beta**(1+a+b))/NC)**2 - np.sum(beta**(2*a))/NC * np.sum(beta**(2+2*b))/NC
Delta = Tr**2 - 4*Det
RP = (Tr + np.sqrt(Delta))/2
pV0 = 1 / RP

bKA_P0 = pV0 * beta**(1+b)*np.sum(beta**a)/NC
bKB_P0 = pV0 * beta**(a)*np.sum(beta**(1+b))/NC

bnuA_P = beta**(a)/np.sum(beta**(a))
bnuB_P = beta**(1+b)/np.sum(beta**(1+b))
#%% Inc_cit_P and Inc_pop_P computed
pi_AP= np.ones(Nr)
pi_BP= np.ones(Nr)
Inc_cit_P = np.zeros(Nr)
Inc_pop_P = np.zeros(Nr)
for i in np.arange(Nr):
    err = 1
    n_it = 0
    pi = 1- np.exp(- r_0test[i] *(bKA_P0 + bKB_P0) )
    while (err > 5E-5) * (n_it < 500):
        pi_AP[i]= np.sum(bnuA_P*pi)
        pi_BP[i]= np.sum(bnuB_P*pi)
        pi2 = 1-np.exp(- r_0test[i] *( bKA_P0 *pi_AP[i] +bKB_P0* pi_BP[i]))
        err = np.max(np.abs(1-pi/pi2))
        n_it = n_it +1
        pi = pi2
    Inc_cit_P[i] = 1/NC * np.sum(pi)
    Inc_pop_P[i] = 1/np.sum(beta) * np.sum(beta*pi)


#%% Forward neutral kernels

KA_U0 = KV0 * beta**(b)*np.sum(beta**a)/NC
KB_U0 = KV0 * beta**(a-1)*np.sum(beta**(1+b))/NC

KA_P0 = pV0 * beta**(1+b)*np.sum(beta**a)/NC
KB_P0 = pV0 * beta**(a)*np.sum(beta**(1+b))/NC

nuA = beta**(a)/np.sum(beta**(a))
nuB = beta**(1+b)/np.sum(beta**(1+b))

#%% Poutbreak_cit_U and Poutbreak_pop_U computed

pi_AU= np.ones(Nr)
pi_BU= np.ones(Nr)
Poutbreak_cit_U = np.zeros(Nr)
Poutbreak_pop_U = np.zeros(Nr)
for i in np.arange(Nr):
    err = 1
    n_it = 0
    pi = 1- np.exp(- r_0test[i] *(KA_U0 + KB_U0) )
    while (err > 5E-5) * (n_it < 500):
        pi_AU[i]= np.sum(nuA*pi)
        pi_BU[i]= np.sum(nuB*pi)
        pi2 = 1-np.exp(- r_0test[i] *( KA_U0 *pi_AU[i] +KB_U0* pi_BU[i]))
        err = np.max(np.abs(1-pi/pi2))
        n_it = n_it +1
        pi = pi2
    Poutbreak_cit_U[i] = 1/NC * np.sum(pi)
    Poutbreak_pop_U[i] = 1/np.sum(beta) * np.sum(beta*pi)

#%% Poutbreak_cit_P

pi_AP= np.ones(Nr)
pi_BP= np.ones(Nr)
Poutbreak_cit_P = np.zeros(Nr)
Poutbreak_pop_P = np.zeros(Nr)
for i in np.arange(Nr):
    err = 1
    n_it = 0
    pi = 1- np.exp(- r_0test[i] *(KA_P0 + KB_P0))
    while (err > 5E-5) * (n_it < 500):
        pi_AP[i]= np.sum(nuA*pi)
        pi_BP[i]= np.sum(nuB*pi)
        pi2 = 1-np.exp(- r_0test[i] *( KA_P0 *pi_AP[i] +KB_P0* pi_BP[i]))
        err = np.max(np.abs(1-pi/pi2))
        n_it = n_it +1
        pi = pi2
    Poutbreak_cit_P[i] = 1/NC * np.sum(pi)
    Poutbreak_pop_P[i] = 1/np.sum(beta) * np.sum(beta*pi)
    

#%% Plot "final_Poutbreak_cit_ro_v{:d}_KB.png"

plt.plot(r_0test, Poutbreak_cit_U, 'r^', label = "strU")
plt.plot(r_0test, Poutbreak_cit_P, 'b*', label = "strP")
plt.title("Theoretical outbreak probability\n \
from a uniformly chosen city, data: {:s}, pI, KB".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("outbreak probability")
plt.legend(title = "{:s}, pI, KB".format(abbrev))
plt.ylim(0, 1)
plt.savefig("final_Poutbreak_cit_ro_v{:d}_KB.png".format(version))

#%% Plot "final_Poutbreak_pop_ro_v{:d}_KB.png"

plt.plot(r_0test, Poutbreak_pop_U, 'r^', label = "strU")
plt.plot(r_0test, Poutbreak_pop_P, 'b*', label = "strP")
plt.title("Theoretical outbreak probability\n \
from a uniformly chosen citizen, data: {:s}, pI, KB".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("outbreak probability")
plt.legend(title = "{:s}, pI, KB".format(abbrev))
plt.ylim(0, 1)
plt.savefig("final_Poutbreak_pop_ro_v{:d}_KB.png".format(version))

#%% Plot "Final_incidence_ro_v{:d}_strP_TG.png"

R0_var = summary_per_R0['R0']
bR = R0_var.size

plt.plot(R0_var, summary_per_R0['pI_strP_Inf_cit'], 'b*', label = "Cities under isolation")
plt.plot(R0_var, summary_per_R0['pI_strP_Inf_ppl'], 'r^', label = "People under isolation")
plt.title("Mean proportion of cities and people under isolation,\n \
data: {:s}, strP, TG".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("Mean proportion")
plt.legend(title = "{:s}, strP, TG".format(abbrev))
plt.ylim(0, 1)
plt.savefig("Final_incidence_ro_v{:d}_strP_TG.png".format(version))

#%% Plot "Final_incidence_thr_ro_v{:d}_strP_KB.png"

pi_AP= np.ones(Nr)
pi_BP= np.ones(Nr)
Inc_cit_P = np.zeros(bR)
Inc_pop_P = np.zeros(bR)
for i in np.arange(bR):
    err = 1
    n_it = 0
    pi = 1- np.exp(- R0_var[i] *(bKA_P0 + bKB_P0) )
    while (err > 5E-4) * (n_it < 500):
        pi_AP[i]= np.sum(bnuA_P*pi)
        pi_BP[i]= np.sum(bnuB_P*pi)
        pi2 = 1-np.exp(- R0_var[i] *( bKA_P0 *pi_AP[i] +bKB_P0* pi_BP[i]))
        err = np.max(np.abs(1-pi/pi2))
        n_it = n_it +1
        pi = pi2
    Inc_cit_P[i] = 1/NC * np.sum(pi)
    Inc_pop_P[i] = 1/np.sum(beta) * np.sum(beta*pi)




plt.plot(R0_var, Inc_cit_P, 'b*', label = "Cities under isolation")
plt.plot(R0_var, Inc_pop_P, 'r*', label = "People under isolation")
plt.title("Expected proportion of cities and people under isolation,\n \
data: {:s}, strP, KB".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("Mean proportion")
plt.legend(title = "{:s}, strP, KB".format(abbrev))
plt.ylim(0, 1)
plt.savefig("Final_incidence_thr_ro_v{:d}_strP_KB.png".format(version))
#%% Plot "Final_incidence_ro_v{:d}_strP_TG.png"

eta_AP= np.ones(Nr)
eta_BP= np.ones(Nr)
Prob_Out = np.zeros(bR)
for i in np.arange(bR):
    err = 1
    n_it = 0
    eta = 1- np.exp(- R0_var[i] *(KA_P0 + KB_P0) )
    while (err > 5E-5) * (n_it < 500):
        eta_AP[i]= np.sum(nuA*eta)
        eta_BP[i]= np.sum(nuB*eta)
        eta2 = 1-np.exp(- R0_var[i] *( KA_P0 *eta_AP[i] + KB_P0* eta_BP[i]))
        err = np.max(np.abs(1-eta/eta2))
        n_it = n_it +1
        eta = eta2
    Prob_Out[i] = np.sum(eta*nuA)



plt.plot(R0_var, summary_per_R0['pI_strP_Inf_cit'], 'b*', label = "Cities under isolation, TG")
plt.plot(R0_var, summary_per_R0['pI_strP_Inf_ppl'], 'r*', label = "People under isolation, TG")
plt.plot(R0_var, Prob_Out * Inc_cit_P, 'bo:', label = "Cities under isolation, KB")
plt.plot(R0_var, Prob_Out * Inc_pop_P, 'ro:', label = "People under isolation, KB")

plt.title("Mean proportion of cities and people under isolation,\n \
data: {:s}, strP".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("Mean proportion")
plt.legend(loc='upper left')
plt.ylim(0, 1)
plt.savefig("Final_incidence_ro_v{:d}_strP_TG.png".format(version))

#%% Plot "Final_incidence_ro_v{:d}_pI_strP_TG.png"

R0_var_2 = np.array(R0_var[R0_var > 2])
bR2 = R0_var_2.size

plt.plot(R0_var_2, summary_per_R0.loc[R0_var > 2, 'pI_strP_Inf_cit'], 'b*', label = "Cities under isolation")
plt.plot(R0_var_2, summary_per_R0.loc[R0_var > 2, 'pI_strP_Inf_ppl'], 'r*', label = "People under isolation")
plt.title("Mean proportion of cities and people under isolation,\n \
data: {:s}, strP, TG, no condition".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("Mean proportion")
plt.legend()
plt.ylim(0, 1)
plt.savefig("Final_incidence_ro_v{:d}_pI_strP_TG.png".format(version))

#%% Plot "Final_incidence_thr_ro_v{:d}_pI_strP_KB.png"

rho_AP= np.ones(Nr)
rho_BP= np.ones(Nr)
Inc_cit_P = np.zeros(bR2)
Inc_pop_P = np.zeros(bR2)
for i in np.arange(bR2):
    err = 1
    n_it = 0
    rho = 1- np.exp(- R0_var_2[i] *(bKA_P0 + bKB_P0) )
    while (err > 5E-5) * (n_it < 500):
        rho_AP[i]= np.sum(bnuA_P*rho)
        rho_BP[i]= np.sum(bnuB_P*rho)
        rho2 = 1-np.exp(- R0_var_2[i] *( bKA_P0 *rho_AP[i] +bKB_P0* rho_BP[i]))
        err = np.max(np.abs(1-rho/rho2))
        n_it = n_it +1
        rho = rho2
    Inc_cit_P[i] = 1/NC * np.sum(rho)
    Inc_pop_P[i] = 1/np.sum(beta) * np.sum(beta*rho)




plt.plot(R0_var_2, Inc_cit_P, 'b*', label = "Cities under isolation")
plt.plot(R0_var_2, Inc_pop_P, 'r*', label = "People under isolation")
plt.title("Expected proportion of cities and people under isolation,\n \
data: {:s}, strP, KB, conditional on outbreak".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("Mean proportion")
plt.legend()
plt.ylim(0, 1)
plt.savefig("Final_incidence_thr_cO_v{:d}_pI_strP_KB.png".format(version))
#%% Plot "Final_incidence_cO_v{:d}_pI_strP_TG.png"

plt.plot(R0_var_2, summary_per_R0.loc[R0_var > 2, 'pI_strP_cO_Inf_cit'], 'b*', label = "Cities under isolation, TG")
plt.plot(R0_var_2, summary_per_R0.loc[R0_var > 2, 'pI_strP_cO_Inf_ppl'], 'r*', label = "People under isolation, TG")
plt.plot(R0_var_2, Inc_cit_P, 'bo:', label = "Cities under isolation, KB")
plt.plot(R0_var_2, Inc_pop_P, 'ro:', label = "People under isolation, KB")

plt.title("Mean proportion of cities and people under isolation,\n \
data: {:s}, strP, conditional on outbreak".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("Mean proportion")
plt.legend(loc='upper left')
plt.ylim(0, 1)
plt.savefig("Final_incidence_cO_v{:d}_pI_strP_TG.png".format(version))

#%%
###########################################

#%% Infection probability with KG data
#Once variantU_pI and variantP_pI have been runned

#%% Plot "variation_infected_vs_size_v{:d}_pI_strU_TKG.png"

Is_I_avg = summary_per_city["pI_strU_emp_pi"] 

#Is_I_var = np.sum(Is_I**2, axis =0)/N_p - Is_I_avg**2
Is_I_std = summary_per_city["pI_strU_std_emp_pi"]

Prop2I_avg = summary_per_bin["pI_strU_empB_pi"]
Prop2I_std = summary_per_bin["pI_strU_stdB_emp_pi"]
Prop2I_min = summary_per_bin["pI_strU_minB_pi"]
Prop2I_max = summary_per_bin["pI_strU_maxB_pi"]

Prop2I_s = Prop2I_avg + Prop2I_std
Prop2I_ms = Prop2I_avg - Prop2I_std

PropKI_avg = summary_per_bin["pI_strU_K_emp_pi"]
PropKI_std = summary_per_bin["pI_strU_K_std_emp_pi"]
PropKI_s = PropKI_avg + PropKI_std
PropKI_ms = PropKI_avg - PropKI_std

R2 = 1-np.var(Prop2I_avg - piRef_U)/np.var(Prop2I_avg)

plt.figure(4)
plt.clf()
plt.vlines(MidBins, \
          ymin = Prop2I_min,  ymax = Prop2I_max,\
         color= "red", linestyle =":")
plt.plot(MidBins, Prop2I_min, "rv:")
rangeTG, = plt.plot(MidBins, Prop2I_max, "r^:", label = "range on TG")
stdKG, = plt.plot(MidBins, PropKI_s, "^y:",  label="std on KG")
plt.plot(MidBins, PropKI_ms, "vy:")
stdTG, = plt.plot(MidBins, Prop2I_s, "^m-.", label="std on TG")
plt.plot(MidBins, Prop2I_ms, "vm-.")
simKG, = plt.plot(MidBins, PropKI_avg, "gD--",  label="simulated on KG")
simTG, = plt.plot(MidBins, Prop2I_avg, "bD--", label="simulated on TG")
predKB, = plt.plot(MidBins, piRef_U, "ks-", label = "analytical")


plt.title("Simulated and theoretical infection probability,\n \
fluctuations within the bin, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(handles = [predKB, simTG,  stdTG, rangeTG, simKG, stdKG], title = "{:s}, pI, strU, TG\n\
R^2 = {:.2f}".format(abbrev, R2))
plt.ylim(-0.02, 1.02)


plt.savefig("variation_infected_vs_size_v{:d}_pI_strU_TKG.png".format(version))


#%% Plot "Infected_vs_size_v{:d}_pI_strU_KG.png"

PropI_avg = summary_per_bin["pI_strU_K_emp_pi"]
PropI_std = summary_per_bin["pI_strU_K_std_emp_pi"]
PropI_min = summary_per_bin["pI_strU_K_min_pi"]
PropI_max = summary_per_bin["pI_strU_K_max_pi"] 


PropI_s = PropI_avg + PropI_std
PropI_ms = PropI_avg - PropI_std

plt.figure(3)
plt.clf()
plt.plot(MidBins, piRef_U, "ks--", label = "analytical")
plt.plot(MidBins, PropI_avg, "bD", label="empirical")
plt.plot(MidBins, PropI_s, "^r:", label="std")
plt.plot(MidBins, PropI_ms, "vr:")
plt.vlines(MidBins, \
          ymin = PropI_min,  ymax = PropI_max,\
         color= "purple", linestyle =":", label = "range")
plt.plot(MidBins, PropI_min, "mv:")
plt.plot(MidBins, PropI_max, "m^:")
plt.title("Empirical and theoretical infection probability,\n \
fluctuations between the runs")
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion of infections")
plt.legend(title = "{:s}, pI, strU, KG".format(abbrev))
plt.ylim(-0.02, 1.02)

plt.savefig("Infected_vs_size_v{:d}_pI_strU_KG.png".format(version))

#%% Plot "variation_infected_vs_size_v{:d}_pI_strP_TKG.png"

Is_I_avg = summary_per_city["pI_strP_emp_pi"] 

#Is_I_var = np.sum(Is_I**2, axis =0)/N_p - Is_I_avg**2
Is_I_std = summary_per_city["pI_strP_std_emp_pi"]

Prop2I_avg = summary_per_bin["pI_strP_empB_pi"]
Prop2I_std = summary_per_bin["pI_strP_stdB_emp_pi"]
Prop2I_min = summary_per_bin["pI_strP_minB_pi"]
Prop2I_max = summary_per_bin["pI_strP_maxB_pi"]

Prop2I_s = Prop2I_avg + Prop2I_std
Prop2I_ms = Prop2I_avg - Prop2I_std

PropKI_avg = summary_per_bin["pI_strP_K_emp_pi"]
PropKI_std = summary_per_bin["pI_strP_K_std_emp_pi"]
PropKI_s = PropKI_avg + PropKI_std
PropKI_ms = PropKI_avg - PropKI_std

R2 = 1-np.var(Prop2I_avg - piRef_P)/np.var(Prop2I_avg)

plt.figure(4)
plt.clf()
plt.vlines(MidBins, \
          ymin = Prop2I_min,  ymax = Prop2I_max,\
         color= "red", linestyle =":")
plt.plot(MidBins, Prop2I_min, "rv:")
rangeTG, = plt.plot(MidBins, Prop2I_max, "r^:", label = "range on TG")
stdKG, = plt.plot(MidBins, PropKI_s, "^y:",  label="std on KG")
plt.plot(MidBins, PropKI_ms, "vy:")
stdTG, = plt.plot(MidBins, Prop2I_s, "^m-.", label="std on TG")
plt.plot(MidBins, Prop2I_ms, "vm-.")
simKG, = plt.plot(MidBins, PropKI_avg, "gD--",  label="simulated on KG")
simTG, = plt.plot(MidBins, Prop2I_avg, "bD--", label="simulated on TG")
predKB, = plt.plot(MidBins, piRef_P, "ks-", label = "analytical")


plt.title("Simulated and theoretical infection probability,\n \
fluctuations within the bin, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(handles = [predKB, simTG,  stdTG, rangeTG, simKG, stdKG], title = "{:s}, pI, strP, TG\n\
R^2 = {:.2f}".format(abbrev, R2))
plt.ylim(-0.02, 1.02)


plt.savefig("variation_infected_vs_size_v{:d}_pI_strP_TKG.png".format(version))


#%% Plot "Infected_vs_size_v{:d}_pI_strP_KG.png"

PropI_avg = summary_per_bin["pI_strP_K_emp_pi"]
PropI_std = summary_per_bin["pI_strP_K_std_emp_pi"]
PropI_min = summary_per_bin["pI_strP_K_min_pi"]
PropI_max = summary_per_bin["pI_strP_K_max_pi"] 


PropI_s = PropI_avg + PropI_std
PropI_ms = PropI_avg - PropI_std

plt.figure(3)
plt.clf()
plt.plot(MidBins, piRef_U, "ks--", label = "analytical")
plt.plot(MidBins, PropI_avg, "bD", label="empirical")
plt.plot(MidBins, PropI_s, "^r:", label="std")
plt.plot(MidBins, PropI_ms, "vr:")
plt.vlines(MidBins, \
          ymin = PropI_min,  ymax = PropI_max,\
         color= "purple", linestyle =":", label = "range")
plt.plot(MidBins, PropI_min, "mv:")
plt.plot(MidBins, PropI_max, "m^:")
plt.title("Empirical and theoretical infection probability,\n \
fluctuations between the runs")
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion of infections")
plt.legend(title = "{:s}, pI, strP, KG".format(abbrev))
plt.ylim(-0.02, 1.02)

plt.savefig("Infected_vs_size_v{:d}_pI_strP_KG.png".format(version))


#%% End


