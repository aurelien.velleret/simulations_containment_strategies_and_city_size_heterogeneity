# -*- coding: utf-8 -*-
"""
Created on Tue Feb 15 10:50:24 2022

@author: alter
"""

#%% *** Initialisation and naming

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

print("The simulation data is assumed to have already been collected under this version number\n \
under the stipulated version name. Proceed only if correct!")
version = int(input("What is the number of the version?  > version = "))
#version = 86

print("The simulation data is assumed to be available for the (pO) choice of R0\n\
where a reference for the theoretical probability for a city to be infected is taken.\n\
cf threshold_through_pOutbreak.py for more details.")

#%% *** Data extraction

summary_parameters = pd.read_csv("summary_parameters_v{:d}.csv".format(version), index_col = 0)
summary_per_city= pd.read_csv("summary_per_city_v{:d}.csv".format(version), index_col = 0)
summary_per_bin= pd.read_csv("summary_per_bin_v{:d}.csv".format(version), index_col = 0)
summary_per_gen = pd.read_csv("summary_per_gen_v{:d}.csv".format(version), index_col = 0)
summary_per_R0 = pd.read_csv("summary_per_R0_v{:d}.csv".format(version), index_col = 0)
D_Name = summary_parameters.loc[0, "Data_Name"]
#D_Name = input("How the data should be described in the titles?  > D_Name = ")
#D_Name = "Polish (D0+)"
abbrev = summary_parameters.loc[0, "abbrev"]
#abbrev = input("How the data should be abbreviated in the legends?  > abbrev = ")
#abbrev = "PC, D0+"


beta = np.array(summary_per_city["City_size"])
NC = beta.size
a = summary_parameters.loc[0, 'a']
b = summary_parameters.loc[0, 'b']

MidBins = summary_per_bin['Central_value_log_sc']

RU = 1/NC*(np.sum(beta**(a+b)) + np.sqrt(np.sum(beta**(2*a-1)) \
* np.sum(beta**(1+2*b))))
RU_ref = summary_parameters.loc[0, "pO_strU_thr_R0"] 

RP_ref = summary_parameters.loc[0, "pO_strP_thr_R0"]
RP = 1/NC*(np.sum(beta**(a+b+1)) + np.sqrt(np.sum(beta**(2*a)) \
* np.sum(beta**(2+2*b))))
    
    
rhoE = summary_parameters.loc[0, 'pO_strU_exp_growth_rate']
print("Expected growth rate~{:.2e} for power 10".format(rhoE))

# #%%
# #For strategy (U)

# beta1 = beta.reshape((NC, 1))
# beta2 = beta.reshape((1, NC))
# #sum of contributions to the kernel, the diagonal being removed
# Sb = np.sum(beta1**(1+b)*beta2**a)-np.sum(beta**(1+b+a))

# KM = RU_ref/(RU * NC) * Sb / MCom.sum() 
# # KV = RU_ref/RU
# # KM = KV/MCom.sum() * Sb / NC
# print("k_M.K_vee~{:.3e}".format(KM))
# print("equivalent R_0~{:.3e}".format(RU_ref))

# MERi = MCom
# MERe = MCom.transpose()
# MER = KM*(MERi + MERe)
# for j in np.arange(NC):
#     MER[j] = MER[j]/beta[j]
# MEr = MER.toarray()


# #%%
# #For strategy (P)

# beta1 = beta.reshape((NC, 1))
# beta2 = beta.reshape((1, NC))


# pM = RP_ref/(RP * NC) * Sb / MCom.sum() 
# # KV = RU_ref/RU
# # KM = KV/MCom.sum() * Sb / NC
# print("p_M.K_vee~{:.3e}".format(pM))
# print("equivalent R_0~{:.3e}".format(RP_ref))
        

# MERi = MCom
# MERe = MCom.transpose()
# MER = pM*(MERi + MERe)
# MErP = MER.toarray()




#%% * Plot theoretical_Infected_vs_size_v{:d}_pO_strU_TG

pi_U = summary_per_city["pO_strU_thr_pi"] 
piRef_U = summary_per_bin["pO_strU_thr_pi"] 

plt.figure(5)
plt.clf()
plt.scatter(np.log10(beta), pi_U, c="black", marker="+", label = "analytical relation")
plt.title("Theoretical infection probability")
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pO, strP, TG".format(abbrev))
plt.ylim(0, 1)
plt.savefig("theoretical_Infected_vs_size_v{:d}_pO_strU_TG.png".format(version))

#%% ** Plot Infected_vs_size_v{:d}_pO_strU_TG

PropTI_avg = summary_per_bin["pO_strU_emp_pi"] 
PropTI_std = summary_per_bin["pO_strU_std_emp_pi"]
PropTI_min = summary_per_bin["pO_strU_min_pi"] 
PropTI_max = summary_per_bin["pO_strU_max_pi"]
PropTI_s = PropTI_avg + PropTI_std
PropTI_ms = PropTI_avg - PropTI_std

plt.figure(3)
plt.clf()
plt.plot(MidBins, piRef_U, "ks--", label = "analytical")
plt.plot(MidBins, PropTI_avg, "bD", label="simulated")
plt.plot(MidBins, PropTI_s, "^r:", label="std")
plt.plot(MidBins, PropTI_ms, "vr:")
plt.vlines(MidBins, \
          ymin = PropTI_min,  ymax = PropTI_max,\
         color= "purple", linestyle =":")
plt.plot(MidBins, PropTI_min, "mv:")
plt.plot(MidBins, PropTI_max, "m^:", label = "range")
plt.title("Empirical and theoretical infection probability,\n \
fluctuations between the runs, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pO, strU, TG".format(abbrev))
plt.ylim(-0.02, 1.02)

plt.savefig("Infected_vs_size_v{:d}_pO_strU_TG.png".format(version))

#%% ** Plot variation_infected_vs_size_v{:d}_pO_strU_TG

Is_I_avg = summary_per_city["pO_strU_emp_pi"] 

#Is_I_var = np.sum(Is_I**2, axis =0)/N_p - Is_I_avg**2
Is_I_std = summary_per_city["pO_strU_std_emp_pi"]

Prop2I_avg = summary_per_bin["pO_strU_empB_pi"]
Prop2I_std = summary_per_bin["pO_strU_stdB_emp_pi"]
Prop2I_min = summary_per_bin["pO_strU_minB_pi"]
Prop2I_max = summary_per_bin["pO_strU_maxB_pi"]

Prop2I_s = Prop2I_avg + Prop2I_std
Prop2I_ms = Prop2I_avg - Prop2I_std

R2 = summary_parameters.loc[0, "pO_strU_infP_R2"]

plt.figure(4)
plt.clf()
plt.plot(MidBins, piRef_U, "ks--", label = "analytical")
plt.plot(MidBins, Prop2I_avg, "bD", label="simulated")
plt.plot(MidBins, Prop2I_s, "^r:", label="std")
plt.plot(MidBins, Prop2I_ms, "vr:")
plt.vlines(MidBins, \
          ymin = Prop2I_min,  ymax = Prop2I_max,\
         color= "purple", linestyle =":")
plt.plot(MidBins, Prop2I_min, "mv:")
plt.plot(MidBins, Prop2I_max, "m^:", label = "range")
plt.title("Empirical and theoretical infection probability,\n \
fluctuations within the bin, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pO, strU, TG\n\
R^2 = {:.2f}".format(abbrev, summary_parameters.loc[0, "pO_strU_infP_R2"]))
plt.ylim(-0.02, 1.02)


plt.savefig("variation_infected_vs_size_v{:d}_pO_strU_TG.png".format(version))

pi_P = summary_per_city["pO_strP_thr_pi"]
piRef_P = summary_per_bin["pO_strP_thr_pi"]

#%% ** Plot Infected_vs_size_v{:d}_pO_strP_TG

PropTIP_avg = summary_per_bin["pO_strP_emp_pi"]
PropTIP_std = summary_per_bin["pO_strP_std_emp_pi"]
PropTIP_min = summary_per_bin["pO_strP_min_pi"]
PropTIP_max = summary_per_bin["pO_strP_max_pi"]

PropTIP_s = PropTIP_avg + PropTIP_std
PropTIP_ms = PropTIP_avg - PropTIP_std

plt.figure(3)
plt.clf()
plt.plot(MidBins, piRef_P, "ks--", label = "analytical")
plt.plot(MidBins, PropTIP_avg, "bD", label="simulated")
plt.plot(MidBins, PropTIP_s, "^r:", label="std")
plt.plot(MidBins, PropTIP_ms, "vr:")
plt.vlines(MidBins, \
          ymin = PropTIP_min,  ymax = PropTIP_max,\
         color= "purple", linestyle =":")
plt.plot(MidBins, PropTIP_min, "mv:")
plt.plot(MidBins, PropTIP_max, "m^:", label = "range")
plt.title("Empirical and theoretical infection probability,\n \
fluctuations between the runs, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pO, strP, TG\n\
R^2 = {:.2f}".format(abbrev, summary_parameters.loc[0,"pO_strP_infP_R2"]))
plt.ylim(0, 1.02)

plt.savefig("Infected_vs_size_v{:d}_pO_strP_TG.png".format(version))

#%% ** Plot variation_infected_vs_size_v{:d}_pO_strP_TG

Is_IP_avg = summary_per_city["pO_strP_emp_pi"]
Is_IP_std = summary_per_city["pO_strP_std_emp_pi"]

Prop2IP_avg = summary_per_bin["pO_strP_empB_pi"] 
Prop2IP_std = summary_per_bin["pO_strP_stdB_emp_pi"]
Prop2IP_min = summary_per_bin["pO_strP_minB_pi"]
Prop2IP_max = summary_per_bin["pO_strP_maxB_pi"]

Prop2IP_s = Prop2IP_avg + Prop2IP_std
Prop2IP_ms = Prop2IP_avg - Prop2IP_std

R2 = summary_parameters.loc[0, "pO_strP_infP_R2"]

plt.figure(4)
plt.clf()
plt.plot(MidBins, piRef_P, "ks--", label = "analytical")
plt.plot(MidBins, Prop2IP_avg, "bD", label="simulated")
plt.plot(MidBins, Prop2IP_s, "^r:", label="std")
plt.plot(MidBins, Prop2IP_ms, "vr:")
plt.vlines(MidBins, \
          ymin = Prop2IP_min,  ymax = Prop2IP_max,\
         color= "purple", linestyle =":")
plt.plot(MidBins, Prop2IP_min, "mv:")
plt.plot(MidBins, Prop2IP_max, "m^:", label = "range")
plt.title("Empirical and theoretical infection probability,\n \
fluctuations within the bin, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pO, strP, TG".format(abbrev))
plt.ylim(0, 1.02)


plt.savefig("variation_infected_vs_size_v{:d}_pO_strP_TG.png".format(version))



#%% * Plot theoretical_Prop_outbreak_vs_size_v{:d}_TG

eta_U = summary_per_city["pO_strU_thr_eta"]
etaRef_U = summary_per_bin["pO_strU_thr_eta"] 


plt.figure(5)
plt.clf()
plt.plot(np.log10(beta), eta_U, "ks", markersize = 5, label = "analytical relation")
plt.title("Theoretical outbreak probability")
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pO, strU, KB".format(abbrev))
plt.savefig("theoretical_Prop_outbreak_vs_size_v{:d}_TG.png".format(version))


#%% ** Plot empirical_Prop_outbreak_vs_size_v{:d}_pO_strU_TG

Prop_outb_TGU = summary_per_bin["pO_strU_emp1_eta"]

plt.figure(4)
plt.clf()
plt.scatter(MidBins, etaRef_U, c="black", marker="+", label = "analytical relation")
plt.scatter(np.log10(summary_per_bin['Lower_size']), Prop_outb_TGU, c="red", marker="+", label = "from simulation runs of epidemic")
plt.title("Empirical and theoretical outbreak probability")
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.ylim(0,1.02)
plt.legend(title = "{:s}, pO, strU, TG".format(abbrev))
plt.savefig("empirical_Prop_outbreak_vs_size_v{:d}_pO_strU_TG.png".format(version))

#It appears that the sizes of outbreaks are so small 
#that the threshold on the size makes a large impact on the observed probabilities


#%% * Plot variation_empirical_Prop_outbreak_vs_size_v{:d}_pO_strU_TG

#S_O = int(input("What is the size threshold for a successful outbreak?  > S_O = "))
#S_O = 30.
S_O = summary_parameters.loc[0, "pO_size_successful_outbreak"]
#R_c = int(input("How many runs for each initial city?  > Rc = "))
#Rc = 1000
Rc = summary_parameters.loc[0, "pO_strU_eta_Nbr_runs"]

Prop_outb_TGU = summary_per_city["pO_strU_emp_eta"]

Mean_Prop_outb_TGU = summary_per_bin["pO_strU_empB_eta"]
Std_Prop_outb_TGU = summary_per_bin["pO_strU_stdB_emp_eta"] 
Min_Prop_outb_TGU = summary_per_bin["pO_strU_minB_eta"]
Max_Prop_outb_TGU = summary_per_bin["pO_strU_maxB_eta"]

plt.figure(4)
plt.clf()
plt.plot(MidBins, etaRef_U, "ks-", label = "analytical")
plt.plot(MidBins, Mean_Prop_outb_TGU, "bD--", label = "simulated")
plt.plot(MidBins, Mean_Prop_outb_TGU + Std_Prop_outb_TGU, "^r:", label="std")
plt.plot(MidBins, Mean_Prop_outb_TGU - Std_Prop_outb_TGU, "vr:")
plt.vlines(MidBins, \
          ymin = Min_Prop_outb_TGU,  ymax = Max_Prop_outb_TGU,\
         color= "purple", linestyle =":")
plt.plot(MidBins, Min_Prop_outb_TGU, "mv:")
plt.plot(MidBins, Max_Prop_outb_TGU, "m^:", label = "range")
plt.title("Empirical and theoretical outbreak probability, \n \
Fluctuations within the bin")
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.ylim(0,1.02)
plt.legend(title = "{:s}, pO, strU, TG".format(abbrev))
plt.savefig("variation_empirical_Prop_outbreak_vs_size_v{:d}_pO_strU_TG.png".format(version))

#It appears that the sizes of outbreaks are so small 
#that the threshold on the size makes a large impact on the observed probabilities

#%% ** Plot empirical_Prop_outbreak_vs_size_v{:d}_pO_strU_K

Prop_outb_KGU = summary_per_bin["pO_strU_empK_eta"]
    
plt.figure(4)
plt.clf()
plt.scatter(MidBins, etaRef_U, c="black", marker="+", label = "analytical relation")
plt.scatter(np.log10(summary_per_bin['Lower_size']), Prop_outb_KGU, c="red", marker="+", label = "from simulation runs of epidemic")
plt.title("Empirical probability of triggering an outbreak as a function , \n \
of the size of first infected")
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion of outbreaks")
plt.ylim(0,1)
plt.legend(title = "{:s}, pO, strU, KG".format(abbrev))
plt.savefig("empirical_Prop_outbreak_vs_size_v{:d}_pO_strU_K.png".format(version))


#%% * Plot theoretical_Prop_outbreak_vs_size_v{:d}_pO_strP_TG

eta_P = summary_per_city["pO_strP_thr_eta"]
etaRef_P = summary_per_bin["pO_strP_thr_eta"]

plt.figure(5)
plt.clf()
plt.plot(np.log10(beta), eta_P, "ks", markersize = 5, label = "analytical relation")
plt.title("Theoretical outbreak probability")
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pO, strP, KB".format(abbrev))
plt.savefig("theoretical_Prop_outbreak_vs_size_v{:d}_pO_strP_TG.png".format(version))




#%% ** Plot variation_empirical_Prop_outbreak_vs_size_v{:d}_pO_strP_TG

Prop_outb_TGP = summary_per_city["pO_strP_emp_eta"]

Mean_Prop_outb_TGP = summary_per_bin["pO_strP_empB_eta"] 
Std_Prop_outb_TGP = summary_per_bin["pO_strP_stdB_emp_eta"]
Min_Prop_outb_TGP = summary_per_bin["pO_strP_minB_eta"]
Max_Prop_outb_TGP = summary_per_bin["pO_strP_maxB_eta"]

plt.figure(4)
plt.clf()
plt.plot(MidBins, etaRef_P, "ks-", label = "analytical")
plt.plot(MidBins, Mean_Prop_outb_TGP, "bD:", label = "simulated")
plt.plot(MidBins, Mean_Prop_outb_TGP + Std_Prop_outb_TGP, "^r:", label="std")
plt.plot(MidBins, Mean_Prop_outb_TGP - Std_Prop_outb_TGP, "vr:")
plt.vlines(MidBins, \
          ymin = Min_Prop_outb_TGP,  ymax = Max_Prop_outb_TGP,\
         color= "purple", linestyle =":")
plt.plot(MidBins, Min_Prop_outb_TGP, "mv:")
plt.plot(MidBins, Max_Prop_outb_TGP, "m^:", label = "range")
plt.title("Empirical and theoretical outbreak probability, \n \
Fluctuations within the bins")
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.ylim(0,1.02)
plt.legend(title = "{:s}, pO, strP, TG".format(abbrev))
plt.savefig("variation_empirical_Prop_outbreak_vs_size_v{:d}_pO_strP_TG.png".format(version))


#%%  #######################################################
#%% * Plot growth_rate_R0_LSestimation_{:d}_v{:d}_pO_strU_TG

#In this new version, least square estimation is exploited \
#instead of a simple interpolation between extremities

#PoT = float(input("Which threshold for the infected population size at quasi-equilibrium? \n >PoR = "))
#PoR = 30
PoR = summary_parameters.loc[0, "pO_strU_TI_init_thres"]

TotE_avg = summary_per_gen["pO_strU_TI_R0_emp"] 
TotE_std = summary_per_gen["pO_strU_TI_R0_std"] 
N_o = TotE_avg-1
    
TotE_s = TotE_avg +TotE_std
TotE_ms = TotE_avg - TotE_std

rhoE = summary_parameters.loc[0, "pO_strU_exp_growth_rate"]
print("Expected growth rate~{:.2e}".format(rhoE))

plt.figure(6)
plt.clf()
plt.plot(summary_per_gen['Gen_after_thr'], TotE_avg, "bD--", label="simulated")
plt.plot(summary_per_gen['Gen_after_thr'], \
   rhoE*np.ones(summary_per_gen['Gen_after_thr'].size), c="black", label="expected")
plt.plot(summary_per_gen['Gen_after_thr'], TotE_s, "r^:", label="+std")
plt.plot(summary_per_gen['Gen_after_thr'], TotE_ms, "rv:", label="-std")
plt.vlines(summary_per_gen['Gen_after_thr'], \
          ymin = summary_per_gen["pO_strU_TI_R0_q5"],\
          ymax = summary_per_gen["pO_strU_TI_R0_q95"],\
         color= "purple", linestyle =":")
plt.plot(summary_per_gen['Gen_after_thr'], summary_per_gen["pO_strU_TI_R0_q95"], "m^:")
plt.plot(summary_per_gen['Gen_after_thr'], summary_per_gen["pO_strU_TI_R0_q5"], "mv:", label = "5%-95%")
plt.title("Growth rate estimation, \n\
from the total number of infected, reference at {:.0f}".format(PoR))
plt.xlabel("Number of generations in the time-interval")
plt.ylabel("Estimated growth rate")
plt.legend(title = "{:s}, pO, strU, TG".format(abbrev))
plt.ylim(-0.02*rhoE, 1.1*rhoE)
plt.savefig("growth_rate_R0_LSestimation_{:d}_v{:d}_pO_strU_TG.png".format(PoR, version))

#%% ** Plot RS_growth_rate_TotE_estimation_{:d}_v{:d}_pO_strU_TG

#rescaled graph
plt.figure(7)
plt.clf()
plt.plot(summary_per_gen['Gen_after_thr'], TotE_avg/rhoE, "bD--", label="simulated")
plt.plot(summary_per_gen['Gen_after_thr'], \
         np.ones(summary_per_gen['Gen_after_thr'].size), c="black", label="expected")
plt.plot(summary_per_gen['Gen_after_thr'], TotE_s/rhoE, "r^:", label="+std")
plt.plot(summary_per_gen['Gen_after_thr'], TotE_ms/rhoE, "rv:", label="-std")
plt.vlines(summary_per_gen['Gen_after_thr'], \
          ymin = summary_per_gen["pO_strU_TI_R0_q5"]/rhoE,\
          ymax = summary_per_gen["pO_strU_TI_R0_q95"]/rhoE,\
         color= "purple", linestyle =":")
plt.plot(summary_per_gen['Gen_after_thr'], summary_per_gen["pO_strU_TI_R0_q95"]/rhoE, "m^:")
plt.plot(summary_per_gen['Gen_after_thr'], summary_per_gen["pO_strU_TI_R0_q5"]/rhoE, "mv:", label = "5%-95%")
plt.title("Growth rate estimation, \n\
from the total number of infected, reference at {:.0f}".format(PoR))
plt.xlabel("Number of generations in the time-interval")
plt.ylabel("Estimated growth rate")
plt.legend(title = "{:s}, pO, strU, TG".format(abbrev))
plt.ylim(-0.02, 1.1)
plt.savefig("RS_growth_rate_TotE_estimation_{:d}_v{:d}_pO_strU_TG.png".format(PoR, version))


#%%  ############################################################
#%% *** Neutral kernel estimation, strU, bw
Nr = 200
r_0test= np.linspace(1.01, 6, Nr)

RU = 1/NC*(np.sum(beta**(a+b)) + np.sqrt(np.sum(beta**(2*a-1)) \
* np.sum(beta**(1+2*b))))
KV0 = 1/RU

#for the backward dynamics
bKA_U0 = KV0 * beta**(1+b)*np.sum(beta**(a-1))/NC
bKB_U0 = KV0 * beta**(a)*np.sum(beta**(b))/NC

bnuA_U = beta**(a-1)/np.sum(beta**(a-1))
bnuB_U = beta**(b)/np.sum(beta**(b))

#%% * Computation Inc_cit_U and Inc_pop_U

#Nr = 200
pi_AU= np.ones(Nr)
pi_BU= np.ones(Nr)
Inc_cit_U = np.zeros(Nr)
Inc_pop_U = np.zeros(Nr)
for i in np.arange(Nr):
    err = 1
    n_it = 0
    pi = 1- np.exp(- r_0test[i] *(bKA_U0 + bKB_U0) )
    while (err > 5E-5) * (n_it < 500):
        pi_AU[i]= np.sum(bnuA_U*pi)
        pi_BU[i]= np.sum(bnuB_U*pi)
        pi2 = 1-np.exp(- r_0test[i] *( bKA_U0 *pi_AU[i] +bKB_U0* pi_BU[i]))
        err = np.max(np.abs(1-pi/pi2))
        n_it = n_it +1
        pi = pi2
    Inc_cit_U[i] = 1/NC * np.sum(pi)
    Inc_pop_U[i] = 1/np.sum(beta) * np.sum(beta*pi)
    
#%% ** Neutral kernel estimation, strP, bw

RP = 1/NC*(np.sum(beta**(a+b+1)) + np.sqrt(np.sum(beta**(2*a)) \
* np.sum(beta**(2+2*b))))
pV0 = 1 / RP

bKA_P0 = pV0 * beta**(1+b)*np.sum(beta**a)/NC
bKB_P0 = pV0 * beta**(a)*np.sum(beta**(1+b))/NC

bnuA_P = beta**(a)/np.sum(beta**(a))
bnuB_P = beta**(1+b)/np.sum(beta**(1+b))

#%% * Computation Inc_cit_P and Inc_pop_P

pi_AP= np.ones(Nr)
pi_BP= np.ones(Nr)
Inc_cit_P = np.zeros(Nr)
Inc_pop_P = np.zeros(Nr)
for i in np.arange(Nr):
    err = 1
    n_it = 0
    pi = 1- np.exp(- r_0test[i] *(bKA_P0 + bKB_P0) )
    while (err > 5E-5) * (n_it < 500):
        pi_AP[i]= np.sum(bnuA_P*pi)
        pi_BP[i]= np.sum(bnuB_P*pi)
        pi2 = 1-np.exp(- r_0test[i] *( bKA_P0 *pi_AP[i] +bKB_P0* pi_BP[i]))
        err = np.max(np.abs(1-pi/pi2))
        n_it = n_it +1
        pi = pi2
    Inc_cit_P[i] = 1/NC * np.sum(pi)
    Inc_pop_P[i] = 1/np.sum(beta) * np.sum(beta*pi)


#%% * Plot final_inc_cit_ro_b_v{:d}_a{:.2f}_b{:.2f}_K

plt.plot(r_0test, Inc_cit_U, 'r^', label = "str (U)")
plt.plot(r_0test, Inc_cit_P, 'b*', label = "str (P)")
plt.title("Final incidence as a function \n \
of the initial reproduction number")
plt.xlabel("Initial reproduction number")
plt.ylabel("Mean proportion of infected cities")
plt.legend(title = "{:s}, pO, KB".format(abbrev))
plt.ylim(0, 1)
plt.savefig("final_inc_cit_ro_b_v{:d}_a{:.2f}_b{:.2f}_K.png".format(version, a, b))

#%% * Plot final_inc_pop_ro_b_v{:d}_a{:.2f}_b{:.2f}_K

plt.plot(r_0test, Inc_pop_U, 'r^', label = "str (U)")
plt.plot(r_0test, Inc_pop_P, 'b*', label = "str (P)")
plt.title("Final incidence of population as a function \n\
 of the initial reproduction number")
plt.xlabel("Initial reproduction number")
plt.ylabel("Mean proportion of infected people")
plt.legend(title = "{:s}, pO, KB".format(abbrev))
plt.ylim(0, 1)
plt.savefig("final_inc_pop_ro_b_v{:d}_a{:.2f}_b{:.2f}_K.png".format(version, a, b))

#%% * Plot final_inc_ratio_b_v{:d}_a{:.2f}_b{:.2f}_K

plt.plot(Inc_pop_U, r_0test*KV0*Inc_cit_U*NC/(Inc_pop_U*np.sum(beta)), 'r^', label = "str (U)")
plt.plot(Inc_pop_P, r_0test*pV0, 'b*', label = "str (P)")
plt.title("Scaled proportion of infected among quarantined \n \
as a function of the quarantined")
plt.xlabel("Proportion of quarantined people")
plt.ylabel("Scaled proportion of infected among quarantined")
plt.legend(title = "{:s}, pO, KB".format(abbrev))
plt.xlim(0, np.max(Inc_pop_P))
plt.ylim(0, np.max(r_0test*pV0))
plt.savefig("final_inc_ratio_b_v{:d}_a{:.2f}_b{:.2f}_K.png".format(version, a, b))

#%% * Plot final_inc_ratio2_b_v{:d}_a{:.2f}_b{:.2f}_K

plt.plot(r_0test*KV0*Inc_cit_U*NC, r_0test*KV0*Inc_cit_U*NC/(Inc_pop_U*np.sum(beta)), 'r^', label = "str (U)")
plt.plot(r_0test*pV0*Inc_pop_P*np.sum(beta), r_0test*pV0, 'b*', label = "str (P)")
plt.title("Scaled proportion of infected among quarantined \n \
as a function of the infected population")
plt.xlabel("Scaled proportion of infected")
plt.ylabel("Scaled proportion of infected among quarantined")
plt.legend(title = "{:s}, pO, KB".format(abbrev))
plt.xlim(0, np.max(r_0test*pV0*Inc_pop_P*np.sum(beta)))
plt.ylim(0, np.max(r_0test*pV0))
plt.savefig("final_inc_ratio2_b_v{:d}_a{:.2f}_b{:.2f}_K.png".format(version, a, b))

#%% * Plot final_quar_vs_infected_v{:d}_a{:.2f}_b{:.2f}_K

plt.plot(r_0test*KV0*Inc_cit_U*NC, Inc_pop_U, 'r^', label = "str (U)")
plt.plot(r_0test*pV0*Inc_pop_P*np.sum(beta), Inc_pop_P, 'b*', label = "str (P)")
plt.title("Scaled proportion of infected among quarantined \n \
as a function of the infected population")
plt.xlabel("Scaled proportion of infected, a={:.2f}, b= {:.2f}".format(a, b))
plt.ylabel("Scaled proportion of people quarantined")
plt.legend(title = "{:s}, pO, KB".format(abbrev))
Im= np.sum(Inc_pop_U < np.max(Inc_pop_P))
plt.xlim(0, r_0test[Im]*KV0*Inc_cit_U[Im]*NC)
plt.ylim(0, np.max(Inc_pop_P))
plt.savefig("final_quar_vs_infected_v{:d}_a{:.2f}_b{:.2f}_K.png".format(version, a, b))


#%% ** Neutral kernel estimation, strUP, fw

KA_U0 = KV0 * beta**(b)*np.sum(beta**a)/NC
KB_U0 = KV0 * beta**(a-1)*np.sum(beta**(1+b))/NC

KA_P0 = pV0 * beta**(1+b)*np.sum(beta**a)/NC
KB_P0 = pV0 * beta**(a)*np.sum(beta**(1+b))/NC

nuA = beta**(a)/np.sum(beta**(a))
nuB = beta**(1+b)/np.sum(beta**(1+b))

#%% * Computation  Poutbreak_cit_U and Poutbreak_pop_U

pi_AU= np.ones(Nr)
pi_BU= np.ones(Nr)
Poutbreak_cit_U = np.zeros(Nr)
Poutbreak_pop_U = np.zeros(Nr)
for i in np.arange(Nr):
    err = 1
    n_it = 0
    pi = 1- np.exp(- r_0test[i] *(KA_U0 + KB_U0) )
    while (err > 5E-5) * (n_it < 500):
        pi_AU[i]= np.sum(nuA*pi)
        pi_BU[i]= np.sum(nuB*pi)
        pi2 = 1-np.exp(- r_0test[i] *( KA_U0 *pi_AU[i] +KB_U0* pi_BU[i]))
        err = np.max(np.abs(1-pi/pi2))
        n_it = n_it +1
        pi = pi2
    Poutbreak_cit_U[i] = 1/NC * np.sum(pi)
    Poutbreak_pop_U[i] = 1/np.sum(beta) * np.sum(beta*pi)

#%% * Computation  Poutbreak_cit_P and Poutbreak_pop_P

pi_AP= np.ones(Nr)
pi_BP= np.ones(Nr)
Poutbreak_cit_P = np.zeros(Nr)
Poutbreak_pop_P = np.zeros(Nr)
for i in np.arange(Nr):
    err = 1
    n_it = 0
    pi = 1- np.exp(- r_0test[i] *(KA_P0 + KB_P0))
    while (err > 5E-5) * (n_it < 500):
        pi_AP[i]= np.sum(nuA*pi)
        pi_BP[i]= np.sum(nuB*pi)
        pi2 = 1-np.exp(- r_0test[i] *( KA_P0 *pi_AP[i] +KB_P0* pi_BP[i]))
        err = np.max(np.abs(1-pi/pi2))
        n_it = n_it +1
        pi = pi2
    Poutbreak_cit_P[i] = 1/NC * np.sum(pi)
    Poutbreak_pop_P[i] = 1/np.sum(beta) * np.sum(beta*pi)
    

#%% * Plot final_Poutbreak_cit_ro_v{:d}_K

plt.plot(r_0test, Poutbreak_cit_U, 'r^', label = "str (U)")
plt.plot(r_0test, Poutbreak_cit_P, 'b*', label = "str (P)")
plt.title("Theoretical outbreak probability\n \
from a uniformly chosen city")
plt.xlabel("Initial reproduction number")
plt.ylabel("outbreak probability")
plt.legend(title = "{:s}, pO, KB".format(abbrev))
plt.ylim(0, 1)
plt.savefig("final_Poutbreak_cit_ro_v{:d}_K.png".format(version))

#%% * Plot final_Poutbreak_pop_ro_v{:d}_K

plt.plot(r_0test, Poutbreak_pop_U, 'r^', label = "str (U)")
plt.plot(r_0test, Poutbreak_pop_P, 'b*', label = "str (P)")
plt.title("Theoretical outbreak probability\n \
from a uniformly chosen citizen")
plt.xlabel("Initial reproduction number")
plt.ylabel("outbreak probability")
plt.legend(title = "{:s}, pO, KB".format(abbrev))
plt.ylim(0, 1)
plt.savefig("final_Poutbreak_pop_ro_v{:d}_K.png".format(version))

#%% *** Plot Final_incidence_ro_v{:d}_strP_TG

R0_var = summary_per_R0['R0']
bR = R0_var.size

plt.plot(R0_var, summary_per_R0['pO_strP_Inf_cit'], 'b*', label = "Cities under isolation")
plt.plot(R0_var, summary_per_R0['pO_strP_Inf_ppl'], 'r^', label = "People under isolation")
plt.title("Mean proportion of cities and people under isolation,\n \
data: {:s}".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("Mean proportion")
plt.legend(title = "{:s}, strP, TG".format(abbrev))
plt.ylim(0, 1)
plt.savefig("Final_incidence_ro_v{:d}_strP_TG.png".format(version))

#%% * Plot Final_incidence_thr_ro_v{:d}_strP_K

pi_AP= np.ones(Nr)
pi_BP= np.ones(Nr)
Inc_cit_P = np.zeros(bR)
Inc_pop_P = np.zeros(bR)
for i in np.arange(bR):
    err = 1
    n_it = 0
    pi = 1- np.exp(- R0_var[i] *(bKA_P0 + bKB_P0) )
    while (err > 5E-5) * (n_it < 500):
        pi_AP[i]= np.sum(bnuA_P*pi)
        pi_BP[i]= np.sum(bnuB_P*pi)
        pi2 = 1-np.exp(- R0_var[i] *( bKA_P0 *pi_AP[i] +bKB_P0* pi_BP[i]))
        err = np.max(np.abs(1-pi/pi2))
        n_it = n_it +1
        pi = pi2
    Inc_cit_P[i] = 1/NC * np.sum(pi)
    Inc_pop_P[i] = 1/np.sum(beta) * np.sum(beta*pi)




plt.plot(R0_var, Inc_cit_P, 'b*', label = "Cities under isolation")
plt.plot(R0_var, Inc_pop_P, 'r*', label = "People under isolation")
plt.title("Expected proportion of cities and people under isolation,\n \
data: {:s}".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("Mean proportion")
plt.legend(title = "{:s}, strP, KB".format(abbrev))
plt.ylim(0, 1)
plt.savefig("Final_incidence_thr_ro_v{:d}_strP_K.png".format(version))

#%% ** Plot Final_incidence_ro_v{:d}_strP_TG


eta_AP= np.ones(Nr)
eta_BP= np.ones(Nr)
Prob_Out = np.zeros(bR)
for i in np.arange(bR):
    err = 1
    n_it = 0
    eta = 1- np.exp(- R0_var[i] *(KA_P0 + KB_P0) )
    while (err > 5E-5) * (n_it < 500):
        eta_AP[i]= np.sum(nuA*eta)
        eta_BP[i]= np.sum(nuB*eta)
        eta2 = 1-np.exp(- R0_var[i] *( KA_P0 *eta_AP[i] + KB_P0* eta_BP[i]))
        err = np.max(np.abs(1-eta/eta2))
        n_it = n_it +1
        eta = eta2
    Prob_Out[i] = np.sum(eta*nuA)



plt.plot(R0_var, summary_per_R0['pO_strP_Inf_cit'], 'b*', label = "Cities under isolation, TG")
plt.plot(R0_var, summary_per_R0['pO_strP_Inf_ppl'], 'r*', label = "People under isolation, TG")
plt.plot(R0_var, Prob_Out * Inc_cit_P, 'bo:', label = "Cities under isolation, KB")
plt.plot(R0_var, Prob_Out * Inc_pop_P, 'ro:', label = "People under isolation, KB")

plt.title("Mean proportion of cities and people under isolation,\n \
data: {:s}".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("Mean proportion")
plt.legend(title = "{:s}, pO, strP, TG".format(abbrev), loc='upper left')
plt.ylim(0, 1)
plt.savefig("Final_incidence_ro_v{:d}_strP_TG.png".format(version))

#%% *** Extraction summary_pO_per_R0

summary_pO_per_R0 = pd.read_csv("summary_pO_per_R0_v{:d}.csv".format(version), index_col = 0)
R0_var_2 = summary_pO_per_R0["R_0"]
bR2 = R0_var_2.size


#%% * Plot Final_incidence_thr_ro_v{:d}_pO_strP_K

rho_AP= np.ones(Nr)
rho_BP= np.ones(Nr)
Inc_cit_P = np.zeros(bR2)
Inc_pop_P = np.zeros(bR2)
for i in np.arange(bR2):
    err = 1
    n_it = 0
    rho = 1- np.exp(- R0_var_2[i] *(bKA_P0 + bKB_P0) )
    while (err > 5E-5) * (n_it < 500):
        rho_AP[i]= np.sum(bnuA_P*rho)
        rho_BP[i]= np.sum(bnuB_P*rho)
        rho2 = 1-np.exp(- R0_var_2[i] *( bKA_P0 *rho_AP[i] +bKB_P0* rho_BP[i]))
        err = np.max(np.abs(1-rho/rho2))
        n_it = n_it +1
        rho = rho2
    Inc_cit_P[i] = 1/NC * np.sum(rho)
    Inc_pop_P[i] = 1/np.sum(beta) * np.sum(beta*rho)




plt.plot(R0_var_2, Inc_cit_P, 'b*', label = "Infected Cities")
plt.plot(R0_var_2, Inc_pop_P, 'r*', label = "People in Infected Cities")
plt.title("Expected proportion of infected cities and people,\n \
data: {:s}".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("Proportion of infected entities")
plt.legend(title = "{:s}, pO, strP, KB".format(abbrev))
plt.ylim(0, 1)
plt.savefig("Final_incidence_thr_ro_v{:d}_pO_strP_KB.png".format(version))

#%% ** Plot Final_incidence_ro_v{:d}_pO_strP_TG

plt.plot(R0_var_2, summary_pO_per_R0["pO_strP_cO_Inf_cit"], 'b*', label = "TG Infected Cities")
plt.plot(R0_var_2, summary_pO_per_R0["pO_strP_cO_Inf_ppl"], 'r*', label = "TG People in Infected Cities")
plt.plot(R0_var_2, Inc_cit_P, 'bo:', label = "Exp Infected Cities")
plt.plot(R0_var_2, Inc_pop_P, 'ro:', label = "Exp People in Infected Cities")

plt.title("Proportion of infected cities and people,\n \
data: {:s} ".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("Proportion of infected entities")
plt.legend(loc='upper left', title = "{:s}, pO, strP, TG".format(abbrev))
plt.ylim(0, 1)
plt.savefig("Final_incidence_ro_v{:d}_pO_strP_TG.png".format(version))

###########################################

#%% Infection probability with KG data
#Once variantU_pO and variantP_pO have been runned

#%% Plot "variation_infected_vs_size_v{:d}_pO_strU_TKG.png"
piRef_U = summary_per_bin["pO_strU_thr_pi"] 

Prop2I_avg = summary_per_bin["pO_strU_empB_pi"]
Prop2I_std = summary_per_bin["pO_strU_stdB_emp_pi"]
Prop2I_min = summary_per_bin["pO_strU_minB_pi"]
Prop2I_max = summary_per_bin["pO_strU_maxB_pi"]

Prop2I_s = Prop2I_avg + Prop2I_std
Prop2I_ms = Prop2I_avg - Prop2I_std

PropKI_avg = summary_per_bin["pO_strU_K_emp_pi"]
PropKI_std = summary_per_bin["pO_strU_K_std_emp_pi"]
PropKI_s = PropKI_avg + PropKI_std
PropKI_ms = PropKI_avg - PropKI_std

R2 =  1-np.var(Prop2I_avg - piRef_U)/np.var(Prop2I_avg)

plt.figure(4)
plt.clf()
plt.vlines(MidBins, \
          ymin = Prop2I_min,  ymax = Prop2I_max,\
         color= "red", linestyle =":")
plt.plot(MidBins, Prop2I_min, "rv:")
rangeTG, = plt.plot(MidBins, Prop2I_max, "r^:", label = "range on TG")
stdKG, = plt.plot(MidBins, PropKI_s, "^y:",  label="std on KG")
plt.plot(MidBins, PropKI_ms, "vy:")
stdTG, = plt.plot(MidBins, Prop2I_s, "^m-.", label="std on TG")
plt.plot(MidBins, Prop2I_ms, "vm-.")
simKG, = plt.plot(MidBins, PropKI_avg, "gD--",  label="simulated on KG")
simTG, = plt.plot(MidBins, Prop2I_avg, "bD--", label="simulated on TG")
predKB, = plt.plot(MidBins, piRef_U, "ks-", label = "analytical")


plt.title("Simulated and theoretical infection probability,\n \
fluctuations within the bin, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(handles = [predKB, simTG,  stdTG, rangeTG, simKG, stdKG], title = "{:s}, pO, strU, TG\n\
R^2 = {:.2f}".format(abbrev, summary_parameters.loc[0,"pO_strU_infP_R2"]))
plt.ylim(-0.02, 1.02)


plt.savefig("variation_infected_vs_size_v{:d}_pO_strU_TKG.png".format(version))


#%% Plot "Infected_vs_size_v{:d}_pO_strU_KG.png"

PropKI_avg = summary_per_bin["pO_strU_K_emp_pi"]
PropKI_std = summary_per_bin["pO_strU_K_std_emp_pi"]
PropKI_min = summary_per_bin["pO_strU_K_min_pi"]
PropKI_max = summary_per_bin["pO_strU_K_max_pi"] 


PropKI_s = PropKI_avg + PropKI_std
PropKI_ms = PropKI_avg - PropKI_std

plt.figure(3)
plt.clf()
plt.plot(MidBins, piRef_U, "ks--", label = "analytical")
plt.plot(MidBins, PropKI_avg, "bD", label="empirical")
plt.plot(MidBins, PropKI_s, "^r:", label="std")
plt.plot(MidBins, PropKI_ms, "vr:")
plt.vlines(MidBins, \
          ymin = PropKI_min,  ymax = PropKI_max,\
         color= "purple", linestyle =":", label = "range")
plt.plot(MidBins, PropKI_min, "mv:")
plt.plot(MidBins, PropKI_max, "m^:")
plt.title("Empirical and theoretical infection probability,\n \
fluctuations between the runs")
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion of infections")
plt.legend(title = "{:s}, pO, strU, KG".format(abbrev))
plt.ylim(-0.02, 1.02)

plt.savefig("Infected_vs_size_v{:d}_pO_strU_KG.png".format(version))

#%% Plot "variation_infected_vs_size_v{:d}_pO_strP_TKG.png"


piRef_P = summary_per_bin["pO_strP_thr_pi"]

Prop2I_avg = summary_per_bin["pO_strP_empB_pi"]
Prop2I_std = summary_per_bin["pO_strP_stdB_emp_pi"]
Prop2I_min = summary_per_bin["pO_strP_minB_pi"]
Prop2I_max = summary_per_bin["pO_strP_maxB_pi"]

Prop2I_s = Prop2I_avg + Prop2I_std
Prop2I_ms = Prop2I_avg - Prop2I_std

PropKI_avg = summary_per_bin["pO_strP_K_emp_pi"]
PropKI_std = summary_per_bin["pO_strP_K_std_emp_pi"]
PropKI_s = PropKI_avg + PropKI_std
PropKI_ms = PropKI_avg - PropKI_std

R2 = 1-np.var(Prop2I_avg - piRef_P)/np.var(Prop2I_avg)

plt.figure(4)
plt.clf()
plt.vlines(MidBins, \
          ymin = Prop2I_min,  ymax = Prop2I_max,\
         color= "red", linestyle =":")
plt.plot(MidBins, Prop2I_min, "rv:")
rangeTG, = plt.plot(MidBins, Prop2I_max, "r^:", label = "range on TG")
stdKG, = plt.plot(MidBins, PropKI_s, "^y:",  label="std on KG")
plt.plot(MidBins, PropKI_ms, "vy:")
stdTG, = plt.plot(MidBins, Prop2I_s, "^m-.", label="std on TG")
plt.plot(MidBins, Prop2I_ms, "vm-.")
simKG, = plt.plot(MidBins, PropKI_avg, "gD--",  label="simulated on KG")
simTG, = plt.plot(MidBins, Prop2I_avg, "bD--", label="simulated on TG")
predKB, = plt.plot(MidBins, piRef_P, "ks-", label = "analytical")


plt.title("Simulated and theoretical infection probability,\n \
fluctuations within the bin, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(handles = [predKB, simTG,  stdTG, rangeTG, simKG, stdKG], title = "{:s}, pO, strP, TG\n\
R^2 = {:.2f}".format(abbrev, R2))
plt.ylim(-0.02, 1.02)


plt.savefig("variation_infected_vs_size_v{:d}_pO_strP_TKG.png".format(version))


#%% Plot "Infected_vs_size_v{:d}_pO_strP_KG.png"

PropKI_avg = summary_per_bin["pO_strP_K_emp_pi"]
PropKI_std = summary_per_bin["pO_strP_K_std_emp_pi"]
PropKI_min = summary_per_bin["pO_strP_K_min_pi"]
PropKI_max = summary_per_bin["pO_strP_K_max_pi"] 


PropKI_s = PropKI_avg + PropKI_std
PropKI_ms = PropKI_avg - PropKI_std

plt.figure(3)
plt.clf()
plt.plot(MidBins, piRef_P, "ks--", label = "analytical")
plt.plot(MidBins, PropKI_avg, "bD", label="empirical")
plt.plot(MidBins, PropKI_s, "^r:", label="std")
plt.plot(MidBins, PropKI_ms, "vr:")
plt.vlines(MidBins, \
          ymin = PropKI_min,  ymax = PropKI_max,\
         color= "purple", linestyle =":", label = "range")
plt.plot(MidBins, PropKI_min, "mv:")
plt.plot(MidBins, PropKI_max, "m^:")
plt.title("Empirical and theoretical infection probability,\n \
fluctuations between the runs")
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion of infections")
plt.legend(title = "{:s}, pO, strP, KG".format(abbrev))
plt.ylim(-0.02, 1.02)

plt.savefig("Infected_vs_size_v{:d}_pO_strP_KG.png".format(version))


#%% End
