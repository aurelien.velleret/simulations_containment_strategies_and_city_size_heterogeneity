# -*- coding: utf-8 -*-
"""
Created on Thu Aug 18 09:47:20 2022

@author: alter
"""


#%%
#Analysis for variant U of the model
from Lockdown_infections import *

#from threshold_through_pInfected, deduce the values of R0 for the reference infection
from threshold_through_pInfected import *

#%%
#For strategy (U)

beta1 = beta.reshape((NC, 1))
beta2 = beta.reshape((1, NC))
#sum of contributions to the kernel, the diagonal being removed
Sb = np.sum(beta1**(1+b)*beta2**a)-np.sum(beta**(1+b+a))
KM = RU_ref/(RU * NC) * Sb / MCom.sum() 
# KV = RU_ref/RU
# KM = KV/MCom.sum() * Sb / NC
print("k_M.K_vee~{:.3e}".format(KM))
print("equivalent R_0~{:.3e}".format(RU_ref))

MERi = MCom
MERe = MCom.transpose()
MER = KM*(MERi + MERe)
for j in np.arange(NC):
    MER[j] = MER[j]/beta[j]
MEr = MER.toarray()
#%%
#For strategy (P)

beta1 = beta.reshape((NC, 1))
beta2 = beta.reshape((1, NC))
pM = RP_ref/(RP * NC) * Sb / MCom.sum() 
# KV = RU_ref/RU
# KM = KV/MCom.sum() * Sb / NC
print("p_M.K_vee~{:.3e}".format(pM))
print("equivalent R_0~{:.3e}".format(RP_ref))
        

MERi = MCom
MERe = MCom.transpose()
MER = pM*(MERi + MERe)
MErP = MER.toarray()
#%%
#Bins decomposition
NL =20
beta_LS = np.sort(np.log10(beta))
Bins = np.linspace(0, beta.size-1, NL, dtype = int)
Bins = Bins[:-1]
#the last component are too variable for any inference, all cities then get infected
Sep = beta_LS[Bins]
Counts_ref, Bins_ref, bars = plt.hist(beta_LS, bins = Sep)
MidBins = (Bins_ref[:-1] + Bins_ref[1:])/2
#%%

MK_U = np.reshape(KA_U, (NC, 1))*np.reshape(nuA, (1, NC))\
    + np.reshape(KB_U, (NC, 1))*np.reshape(nuB, (1, NC))
MK_P = np.reshape(KA_P, (NC, 1))*np.reshape(nuA, (1, NC))\
    + np.reshape(KB_P, (NC, 1))*np.reshape(nuB, (1, NC))    
for j in np.arange(NC):
    MK_U[j, j] = 0
    MK_P[j, j] = 0


S_sum = pd.DataFrame(np.zeros((beta.size, 8)), \
     columns = ["OdKgUs", "OdTgUs", "OdKgPs", "OdTgPs", \
                "IdKgUs", "IdTgUs", "IdKgPs", "IdTgPs"])  
Desc_columns = np.array(["str (U), KG, out-degree", \
           "str (U), TG, out-degree",\
           "str (P), KG, out-degree", \
           "str (P), TG, out-degree",\
           "str (U), KG, in-degree", \
           "str (U), TG, in-degree",\
           "str (P), KG, in-degree", \
           "str (P), TG, in-degree"])    
S_sum["OdKgUs"] =  np.sum(1-np.exp(-MK_U), axis = 1)
S_sum["OdTgUs"] =  np.sum(1-np.exp(-MEr), axis = 1)
S_sum["OdKgPs"] =  np.sum(1-np.exp(-MK_P), axis = 1)
S_sum["OdTgPs"] =  np.sum(1-np.exp(-MErP), axis = 1)
S_sum["IdKgUs"] =  np.sum(1-np.exp(-MK_U), axis = 0)
S_sum["IdTgUs"] =  np.sum(1-np.exp(-MEr), axis = 0)
S_sum["IdKgPs"] =  np.sum(1-np.exp(-MK_P), axis = 0)
S_sum["IdTgPs"] =  np.sum(1-np.exp(-MErP), axis = 0)
#%%

SB_sum = pd.DataFrame(np.zeros((MidBins.size, 32)), \
    columns = ["OdKgUs_avg", "OdKgUs_std", "OdKgUs_min", "OdKgUs_max",\
               "OdTgUs_avg", "OdTgUs_std", "OdTgUs_min", "OdTgUs_max",\
               "OdKgPs_avg", "OdKgPs_std", "OdKgPs_min", "OdKgPs_max",\
               "OdTgPs_avg", "OdTgPs_std", "OdTgPs_min", "OdTgPs_max",\
               "IdKgUs_avg", "IdKgUs_std", "IdKgUs_min", "IdKgUs_max", \
               "IdTgUs_avg", "IdTgUs_std", "IdTgUs_min", "IdTgUs_max",\
               "IdKgPs_avg", "IdKgPs_std", "IdKgPs_min", "IdKgPs_max", \
               "IdTgPs_avg", "IdTgPs_std", "IdTgPs_min", "IdTgPs_max"])

for i in np.arange(MidBins.size):
    Is_bin = (np.log10(beta)>=Bins_ref[i])& (np.log10(beta)<Bins_ref[i+1])
    for j in np.arange(8):
        SB_sum.iloc[i, 4*j] =  np.mean(S_sum.iloc[Is_bin, j])
        SB_sum.iloc[i, 4*j+1] = np.std(S_sum.iloc[Is_bin, j])
        SB_sum.iloc[i, 4*j+2] = np.min(S_sum.iloc[Is_bin, j])
        SB_sum.iloc[i, 4*j+3] = np.max(S_sum.iloc[Is_bin, j])
for Col in S_sum.columns:
    SB_sum["{:s}_s".format(Col)] = SB_sum["{:s}_avg".format(Col)] \
        +SB_sum["{:s}_std".format(Col)]
    SB_sum["{:s}_ms".format(Col)] = SB_sum["{:s}_avg".format(Col)] \
        -SB_sum["{:s}_std".format(Col)]
        
#%%
#Maybe necessary to adjust ylim by cases...

for k in np.arange(8):
    plt.figure(k)
    plt.clf()
    plt.plot(MidBins, SB_sum["{:s}_avg".format(S_sum.columns[k])], \
             "bD", label="bin average")
    plt.plot(MidBins, SB_sum["{:s}_ms".format(S_sum.columns[k])],\
                 "vr:")
    plt.plot(MidBins, SB_sum["{:s}_s".format(S_sum.columns[k])], \
             "^r:", label="std")
    plt.plot(MidBins, SB_sum["{:s}_max".format(S_sum.columns[k])],\
             "vm:")  
    plt.plot(MidBins, SB_sum["{:s}_min".format(S_sum.columns[k])], \
             "^m:", label="range")
    plt.vlines(MidBins, \
        ymin = SB_sum["{:s}_min".format(S_sum.columns[k])], \
        ymax = SB_sum["{:s}_max".format(S_sum.columns[k])],\
        color= "purple", linestyle =":")    
    plt.title("Mean number of infectious links\n\
Data: {:s}, pI, {:s}".format(D_Name, Desc_columns[k]))
    plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
    plt.ylabel("Mean number")
    plt.legend()
    plt.savefig("variation_infectious_links_v{:d}_pI_{:s}.png".format(version, S_sum.columns[k]))
#%%
for k in np.arange(4):
    plt.figure(k)
    plt.clf()
    plt.plot(MidBins, SB_sum["{:s}_avg".format(S_sum.columns[int(2*k+1)])], \
             "bD", label="bin average, TG")
    plt.plot(MidBins, SB_sum["{:s}_ms".format(S_sum.columns[int(2*k+1)])],\
             "vr:")
    plt.plot(MidBins, SB_sum["{:s}_s".format(S_sum.columns[int(2*k+1)])], \
             "^r:", label="std, TG")
    plt.vlines(MidBins, \
        ymin = SB_sum["{:s}_ms".format(S_sum.columns[int(2*k+1)])], \
        ymax = SB_sum["{:s}_s".format(S_sum.columns[int(2*k+1)])],\
        color= "red", linestyle =":")           
    plt.plot(MidBins, SB_sum["{:s}_avg".format(S_sum.columns[int(2*k)])], \
             "gD", alpha = 0.8, label="bin average, KG")
    plt.plot(MidBins, SB_sum["{:s}_ms".format(S_sum.columns[int(2*k)])],\
             "vm:", alpha = 0.4) 
    plt.plot(MidBins, SB_sum["{:s}_s".format(S_sum.columns[int(2*k)])], \
             "^m:", alpha = 0.4, label="std, KG")
    plt.vlines(MidBins, \
        ymin = SB_sum["{:s}_ms".format(S_sum.columns[int(2*k)])], \
        ymax = SB_sum["{:s}_s".format(S_sum.columns[int(2*k)])],\
        color= "m", alpha = 0.4, linestyle =":")     
    plt.title("Mean number of infectious links\n\
Data: {:s}, pI, {:s}".format(D_Name, Desc_columns[int(2*k)]))
    plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
    plt.ylabel("Mean number")
    plt.ylim(0, 1.5*np.max(SB_sum["{:s}_avg".format(S_sum.columns[int(2*k)])]))
    plt.legend()
    plt.savefig("variationC_infectious_links_v{:d}_pI_{:s}.png".format(version, S_sum.columns[int(2*k+1)]))


#%%
plt.figure(11)
plt.clf()
plt.plot(S_sum["OdKgUs"], S_sum["IdKgUs"], \
         "b+")
plt.xlabel("Out-degree")
plt.ylabel("In_degree")
plt.title("From {:s} data, pI, {:s}".format(D_Name, "OdKgUs and IdKgUs"))
plt.ylim(0, 5)
plt.xlim(0, 6)
#%%
plt.figure(12)
plt.clf()
plt.plot(S_sum["OdTgUs"], S_sum["IdTgUs"], \
         "b+")
plt.xlabel("Out-degree")
plt.ylabel("In_degree")
plt.title("From {:s} data, pI, {:s}".format(D_Name, "OdTgUs and IdTgUs"))
plt.ylim(0, 5)
plt.xlim(0, 4)

#%%

