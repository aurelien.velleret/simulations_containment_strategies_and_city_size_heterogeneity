#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  5 22:12:30 2023

@author: avelleret
"""
 
from Lockdown_infections import *


#%% Study of the cumulative distribution for the population under restriction

City_sizes_log = np.linspace(np.min(np.log10(beta)), np.max(np.log10(beta))*1.02, 50)
City_sizes_log = City_sizes_log.reshape(1, len(City_sizes_log))
beta_0 = beta.reshape((beta.size, 1))
Cum_dist = np.sum(beta_0* (np.log10(beta_0) > City_sizes_log), axis = 0)/np.sum(beta)


plt.figure()
plt.clf()
plt.plot(City_sizes_log.reshape(City_sizes_log.size), Cum_dist, label="tail distribution")
dens = -np.diff(Cum_dist)
dens = dens/np.max(dens)
plt.plot(City_sizes_log.reshape(City_sizes_log.size)[1:], dens, label = "density")
plt.title("Proportion of citizen in large cities, \n \
data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion of citizen in cities larger than the x-value")
plt.legend(title = "{:s}".format(abbrev))
plt.ylim(0, 1)
plt.savefig("Proportion_citizen_vs_size_v{:d}.png".format(version))

#%% Estimation of Q_values for 5 values of pi_ref

#Different choices for Q_value depending on 5 choices of probability
#adapted from "threshold_through_pInfected", the estimates are not meant to be precise
Pi_choice = np.array([1/10, 1/3, 1/2, 2/3, 9/10])
N_pi = len(Pi_choice)

pI_ref_size = summary_parameters.loc[0, "pI_ref_size"] 
#log10 size of the cities taken as a reference
Upper_R_pI_strU = 20 #upper-value in the different values of R0 tested
Nr = 1000 #grid size in the different values of R0 tested

r_0test= np.linspace(1.01, Upper_R_pI_strU, Nr)
Lref = (beta>10**(pI_ref_size-0.1))&(beta<10**(pI_ref_size+0.1))
#on an interval to capture at least one city

pi_AU = np.ones(Nr)
pi_BU= np.ones(Nr)
pi_ref_U = np.zeros(Nr)

RU = 1/NC*(np.sum(beta**(a+b)) + np.sqrt(np.sum(beta**(2*a-1))* np.sum(beta**(1+2*b))))
KV0 = 1/RU
bKA_U0 = KV0 * beta**(1+b)*np.sum(beta**(a-1))/NC
bKB_U0 = KV0 * beta**(a)*np.sum(beta**(b))/NC

bnuA_U = beta**(a-1)/np.sum(beta**(a-1))
bnuB_U = beta**(b)/np.sum(beta**(b))

Q_values = np.zeros(N_pi)
RU_ref = np.zeros(N_pi)
Pi_effective = np.zeros(N_pi)

for i in np.arange(Nr):
    err = 1
    n_it = 0
    pi = 1- np.exp(- r_0test[i] *(bKA_U0 + bKB_U0) )
    while (err > 5E-5) * (n_it < 500):
        pi_AU[i]= np.sum(bnuA_U*pi)
        pi_BU[i]= np.sum(bnuB_U*pi)
        pi2 = 1-np.exp(- r_0test[i] *( bKA_U0 *pi_AU[i] +bKB_U0* pi_BU[i]))
        err = np.max(np.abs(1-pi/pi2))
        n_it = n_it +1
        pi = pi2
    pi_ref_U[i] = np.sum(pi[Lref])/np.sum(Lref)

for j in np.arange(N_pi):
    RU_ref[j] = r_0test[np.sum(pi_ref_U<Pi_choice[j])-1]
    Pi_effective[j] = pi_ref_U[np.sum(pi_ref_U<Pi_choice[j])-1]
    err = 1
    n_it = 0
    pi = 1- np.exp(- RU_ref[j] *(bKA_U0 + bKB_U0) )
    while (err > 5E-5) * (n_it < 500):
        pi_AU[i]= np.sum(bnuA_U*pi)
        pi_BU[i]= np.sum(bnuB_U*pi)
        pi2 = 1-np.exp(- RU_ref[j] *( bKA_U0 *RU_ref[j] +bKB_U0* RU_ref[j]))
        err = np.max(np.abs(1-pi/pi2))
        n_it = n_it +1
        pi = pi2
    Q_values[j] = int(np.sum(pi * beta)/NC)
print(Q_values) #values of Q to sample from 

print(Pi_effective) #corresponding value of the probablity for a city of size 10^5 to be infected
#%%