#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 28 16:02:08 2021

@author: user
"""

#%% Import
#Analysis for variant U of the model
from Lockdown_infections import *
#from threshold_through_pInfected, deduce the values of R0 for the reference infection
from threshold_through_pInfected_10KG import *

dir_n = '10KG_strP_PI'
if not os.path.isdir(dir_n): os.makedirs(dir_n)
dir_path = os.path.join(os.getcwd(),dir_n)
#%% Test run
Tf2, IAf2, Rf2, Shf2, N_it2 = InfectionR(10000, 30, KA_P, KB_P, nuA, nuB, TL_P_x10)
print("Number of failed attempts =", N_it2-1)

#%% Runs of simulations
#t_o = TL_U_x10[0]*float(input("Up to how many reference generations does the epidemic spread?  > t_o/TL_U_x10 = "))
t_o = TL_U_x10[0]*100

#reference time adjusted just a bit higher than a fraction of the generation time for the "U" model
#N_d = int(input("Which precision in the time-discretisation (Dt = TL_U_x10/N_d) for aggregation?  > N_d = "))
N_d = 5
t_R = TL_U_x10[0]*(1/N_d + 1e-9)

#N_p = int(input("How many runs do you wish to perform?  > N_p = "))
N_p = 200  #suggested
#N_I = int(input("What is the size threshold under which simulations are restarted?  > N_I = "))
N_I = 20

Gt = np.arange(1+1e-8, 1+t_o, t_R)
Nt = Gt.size
Gtb = Gt.reshape((Nt, 1))
Rt = np.zeros((N_p, Nt))
It = np.zeros((N_p, Nt))

PrI = np.zeros((N_p, Counts_ref.size))
Is_I = np.zeros((N_p, beta_x10.size), dtype = bool)

N_fl = 0
for k in np.arange(N_p):
    Tf, IAf, Rf, Shf, N_it2 = InfectionR(1+t_o, N_I, KA_P, KB_P, nuA, nuB, TL_P_x10)
    #Is_I : who has been infected eventually?
    Is_I[k] = (Tf!= 0)
    #failure update:
    N_fl = N_fl + N_it2/N_p
    #update for the total number of infected 
    T1 = Tf[Rf]
    T1 = T1.reshape((1, np.size(T1)))
    Rt[k] = np.sum(T1 < Gtb, axis = 1)
    #update for the number of infectious
    T2 = Tf[Rf] + TL_P_x10[Rf]
    T2 = np.reshape(T2, (1, np.size(T2)))
    It[k] = np.sum((T1 < Gtb)*(Gtb < T2), axis = 1)
    #proportion of infected cities as a function of their size
    counts, bins, bars = plt.hist(np.log10(beta_x10[Tf==0]), bins = Sep)
    plt.clf()
    PrI[k] = 1- counts/(10*Counts_ref)
    
    
print("Runs of simulations completed.")
print("Averaged number of failures observed : N_fl~{:.2e}".format(N_fl) )  
   
#%% Plot "total_nbr_infected_v{:d}_pI_strP_10KG.png"
plt.clf()
plt.hist(Rt[:, -1], bins = 50, range = (0, max(Rt[:, -1])))
plt.title("Distribution of final incidence")
plt.legend(title = "{:s}, pI, strP, 10KG".format(abbrev))
plt.savefig(dir_path+"/total_nbr_infected_v{:d}_pI_strP_10KG.png".format(version))
 
#%% Plot "number_infected_perG_{:d}_v{:d}_pI_strP_10KG.png"
print("Results from the last run")
t_f = np.max(Tf[Rf])
Gt2 = Gt[Gt < t_f]
Rtf = Rt[-1, Gt < t_f]

plt.figure(1)
plt.clf()
plt.plot(Gt2[1:], np.log10(Rtf[1:]))
# rhoE = np.log10(2)/TL_U_x10[0]
# print("Expected growth rate~{:.2e} for power 10".format(rhoE))
# t_I = np.sum((Rtf < 20))
# Lin = np.log10(Rtf[t_I]) + rhoE*(Gt2-Gt2[t_I])
# t_M = np.sum((Lin < np.log10(Rtf[-1])))
# t_m = np.sum(Lin < 0)
# plt.plot(Gt2[t_m:t_M], Lin[t_m:t_M], c ="purple")
plt.title("Progression of the number of infected, strP, 10KG")
plt.xlabel("Time since the start of infection")
plt.ylabel("Log10 of the total number of infected")
plt.savefig(dir_path+"/slope_adjustment_{:d}_v{:d}_pI_strP_10KG.png".format(20, version))

plt.figure(2)
plt.clf()
plt.plot(Gt2[1:]/TL_U_x10[0], Rtf[1:])
#plt.plot(Gt2[t_m:t_M]/TL_U_x10[0], 10**(Lin[t_m:t_M]), c ="purple")
plt.title("Progression of the number of infected, strP, 10KG")
plt.xlabel("Number of generations since the start of infection")
plt.ylabel("Total number of infected")
plt.savefig(dir_path+"/number_infected_perG_{:d}_v{:d}_pI_strP_10KG.png".format(20, version))

#%% Probab infection/outbreak

#%%% Plot "theoretical_Infected_vs_size_v{:d}_pI_strP_10KB.png"
err = 1
n_it = 0
pi = 1- np.exp(- bKA_P - bKB_P)
while (err > 5E-5) * (n_it < 500):
    pi2 = 1-np.exp(- bKA_P * np.sum(bnuA_P*pi) - bKB_P* np.sum(bnuB_P*pi))
    err = np.max(np.abs(1-pi/pi2))
    n_it = n_it +1
    pi = pi2
    
Sort = np.argsort(beta_x10)
RefV = Sort[10*Bins]
piRef_P = (pi[RefV[1:]] + pi[RefV[:-1]])/2

plt.figure(5)
plt.clf()
plt.scatter(MidBins, piRef_P, c="black", marker="+", label = "empirical relation")
plt.title("Theoretical proportion of infected cities as a function of their size, \n \
from {:s} data, str. P, 10KB".format(D_Name))
plt.xlabel("Log10 of the city size of reference")
plt.ylabel("Proportion")
plt.legend()
plt.savefig(dir_path+"/theoretical_Infected_vs_size_v{:d}_pI_strP_10KB.png".format(version))

#%%% Plot "Infected_vs_size_v{:d}_pI_strP_10KG.png"

PrI_avg = np.sum(PrI, axis =0)/N_p
summary_per_bin["pI_strP_10KG_emp_pi"] = PrI_avg

PrI_var = np.sum(PrI**2, axis =0)/N_p - PrI_avg**2
summary_per_bin["pI_strP_10KG_std_emp_pi"] = np.sqrt(PrI_var)

PrI_min = np.min(PrI, axis =0)
summary_per_bin["pI_strP_10KG_min_pi"] = PrI_min

PrI_max = np.max(PrI, axis =0)
summary_per_bin["pI_strP_10KG_max_pi"] = PrI_max


PrI_sort = np.sort(PrI, axis =0)

PrI_2s = PrI_avg + np.sqrt(PrI_var)
PrI_m2s = PrI_avg - np.sqrt(PrI_var)

plt.figure(3)
plt.clf()
plt.plot(MidBins, piRef_P, "ks--", label = "analytical")
plt.plot(MidBins, PrI_avg, "bD", label="simulated")
plt.plot(MidBins, PrI_2s, "^r:", label="std")
plt.plot(MidBins, PrI_m2s, "vr:")
plt.vlines(MidBins, \
          ymin = PrI_min,  ymax = PrI_max,\
         color= "purple", linestyle =":", label = "range")
plt.plot(MidBins, PrI_min, "mv:")
plt.plot(MidBins, PrI_max, "m^:")
plt.title("Empirical and theoretical infection probability,\n \
fluctuations between the runs, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pI, strP, 10KG".format(D_Name))
plt.ylim(-0.02, 1.02)

plt.savefig(dir_path+"/Infected_vs_size_v{:d}_pI_strP_10KG.png".format(version))

#%%% Plot "variation_infected_vs_size_v{:d}_pI_strP_10KG.png"

Is_I_avg = np.sum(Is_I, axis =0)/N_p
summary_per_10city["pO_strP_emp_pi"] = Is_I_avg

Is_I_var = np.sum(Is_I**2, axis =0)/N_p - Is_I_avg**2
summary_per_10city["pO_strP_std_emp_pi"] = np.sqrt(Is_I_var)

PropKI_avg = np.zeros(MidBins.size)
PropKI_var = np.zeros(MidBins.size)
PropKI_min = np.zeros(MidBins.size)
PropKI_max = np.zeros(MidBins.size)
for i in np.arange(MidBins.size):
    Is_bin = (np.log10(beta_x10)>=Bins_ref[i])& (np.log10(beta_x10)<Bins_ref[i+1])
    PropKI_avg[i] = np.mean(Is_I_avg[Is_bin])
    PropKI_var[i] = np.var(Is_I_avg[Is_bin])
    PropKI_min[i] = np.min(Is_I_avg[Is_bin])
    PropKI_max[i] = np.max(Is_I_avg[Is_bin])
summary_per_bin["pI_strP_10KG_empB_pi"] = PropKI_avg
summary_per_bin["pI_strP_10KG_stdB_emp_pi"] = np.sqrt(PropKI_var)
summary_per_bin["pI_strP_10KG_minB_pi"] = PropKI_min
summary_per_bin["pI_strP_10KG_maxB_pi"] = PropKI_max


PropKI_s = PropKI_avg + np.sqrt(PropKI_var)
PropKI_ms = PropKI_avg - np.sqrt(PropKI_var)


R2 = 1-np.var(Is_I_avg - pi)/np.var(Is_I_avg)

plt.figure(4)
plt.clf()
plt.plot(MidBins, piRef_P, "ks--", label = "analytical")
plt.plot(MidBins, PropKI_avg, "bD", label="simulated")
plt.plot(MidBins, PropKI_s, "^r:", label="std")
plt.plot(MidBins, PropKI_ms, "vr:")
plt.vlines(MidBins, \
          ymin = PropKI_min,  ymax = PropKI_max,\
         color= "purple", linestyle =":")
plt.plot(MidBins, PropKI_min, "mv:")
plt.plot(MidBins, PropKI_max, "m^:", label = "range")
plt.title("Infection probability,\n \
fluctuations within the bin, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pI, strP, 10KG".format(abbrev))
plt.ylim(-0.02, 1.02)


plt.savefig(dir_path+"/variation_infected_vs_size_v{:d}_pI_strP_10KG.png".format(version))


#%%% Plot "Error_I_vs_size_v{:d}_pI_strP_10KB.png"

N_it = 20
rho = 1- np.exp(- bKA_P - bKB_P)
pi_AP = np.ones(N_it)
pi_BP = np.ones(N_it)
for i in np.arange(N_it):
    pi_AP[i] = np.sum(bnuA*rho)
    pi_BP[i] = np.sum(bnuB*rho)
    rho = 1-np.exp(- bKA_P * pi_AP[i]  - bKB_P* pi_BP[i])
    n_it = n_it +1

plt.plot(np.arange(N_it-3), np.log10(np.abs(pi_AP[:-3]/pi_AP[-1] -1)), c="blue", marker="*", label = "pi_C")
plt.plot(np.arange(N_it-3), np.log10(np.abs(pi_BP[:-3]/pi_BP[-1] -1)), c="red", marker="+", label = "pi_D")


plt.xlabel("Iteration")
plt.ylabel("Log10 of the error")
plt.title("Error in the estimation of the proprotion of infected\n \
relative error for the two parameters, str P, 10KB")
plt.legend()
plt.savefig(dir_path+"/Error_I_vs_size_v{:d}_pI_strP_10KB.png".format(version))

#%%% Plot "theoretical_pOutbreak_vs_size_v{:d}_pI_strP_10KB.png"
err = 1
n_it = 0
eta = 1- np.exp(- KA_P - KB_P)
while (err > 5E-5) * (n_it < 500):
    eta2 = 1-np.exp(- KA_P * np.sum(nuA*eta) - KB_P* np.sum(nuB*eta))
    err = np.max(np.abs(1-eta/eta2))
    n_it = n_it +1
    eta = eta2
    
Sort = np.argsort(beta_x10)
RefV = Sort[Bins]
etaRef_P = (eta[RefV[1:]] + eta[RefV[:-1]])/2

plt.figure(5)
plt.clf()
plt.scatter(MidBins, etaRef_P, c="black", marker="+", label = "analytical relation")
plt.title("Theoretical probability of triggering an outbreak as a function , \n \
of the size of first infected, from {:s} data, strP, 10KB".format(D_Name))
plt.xlabel("Log10 of the city size of reference")
plt.ylabel("Proportion")
plt.legend()
plt.savefig(dir_path+"/theoretical_pOutbreak_vs_size_v{:d}_pI_strP_10KB.png".format(version))
    

    


# #%%

# Isort = np.argsort(beta_x10)
# PropOut = np.zeros(MidBins.size)
# for ir in np.arange(MidBins.size):
#     iA = Isort[Bins[ir]]
#     N_O = 0
#     #S_O = int(input("What is the size threshold for a successful outbreak?  > S_O = "))
#     S_O = 30
#     #S_O = int(input("How many runs for each initial city?  > Rc = "))
#     Rc = 1000   
#     for k in np.arange(Rc):
#         IA = np.zeros((10*NC), dtype = bool)
#         IA[iA] = True
#         Tf, IAf, Rf, Shf = InfectionI(1+TL_U_x10[0]*15, IA, KA_P, KB_P, nuA, nuB, TL_P_x10)
#         if np.sum(Rf) > S_O:
#             N_O =N_O +1
#     PropOut[ir] = N_O/Rc
    

# summary_per_bin["pI_strP_10KG_emp_eta"] = PropOut 
    
# plt.figure(4)
# plt.clf()
# plt.plot(MidBins, etaRef, "k+-", label = "analytical relation")
# plt.plot(np.log10(beta_x10[Isort[Bins[:-1]]]), PropOut, "rD", label = "from simulation runs of epidemic")
# plt.title("Empirical probability of triggering an outbreak as a function , \n \
# of the size of first infected, from {:s} data, str. P, K".format(D_Name))
# plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
# plt.ylabel("Proportion of outbreaks")
# plt.ylim(0, 1)
# plt.legend(title = "pI, strP, K")
# plt.savefig(dir_path+"/empirical_pOutbreak_vs_size_v{:d}_pI_strP_K.png".format(version))


# #%%

# n_it = 20
# eta = 1- np.exp(- KA_U - KB_U)
# eta_A_P = np.ones(n_it)
# eta_B_P = np.ones(n_it)
# for i in np.arange(n_it):
#     eta_A_P[i] = np.sum(nuA*eta)
#     eta_B_P[i] = np.sum(nuB*eta)
#     eta = 1-np.exp(- KA_U * eta_A_P[i]  - KB_U* eta_B_P[i])

# plt.plot(np.arange(n_it-1), np.log10(np.abs(eta_A_P[:-1]/eta_A_P[-1] -1)), c="blue", marker="*", label = "eta_A")
# plt.plot(np.arange(n_it-1), np.log10(np.abs(eta_B_P[:-1]/eta_B_P[-1] -1)), c="red", marker="+", label = "eta_B")


# plt.xlabel("Iteration")
# plt.ylabel("Log10 of the error")
# plt.title("Error in the estimation of the probability of Outbreak\n \
# relative error for the two parameters, str P, K")
# plt.legend()
# plt.savefig(dir_path+"/Error_probO_v{:d}_pI_strP_K.png".format(version))

#%% Indicators of centrality
sPABoPBA = np.sqrt(np.sum(beta_x10**(2*a))/np.sum(beta_x10**(2+2*b)))*np.sum(beta_x10**(1+b))/np.sum(beta_x10**(a))
hA_P = 1/2+ 1/2*sPABoPBA

hB_P = 1/2 + 1/(2*sPABoPBA)
h_P = (hA_P * KA_P + hB_P *KB_P)/RP_ref

plt.figure(15)
plt.clf()
plt.ylim(-3, 1)
plt.scatter(np.log10(beta_x10), np.log10(KA_P/RP_ref), c="blue", marker="+", alpha=0.2, label = "KA_P/R_0")
plt.scatter(np.log10(beta_x10), np.log10(KB_P/RP_ref), c="green", marker="+", alpha=0.2, label = "KB_P/R_0")
plt.scatter(np.log10(beta_x10), np.log10(h_P), c="black", marker="+", label = "eigenvector centrality")
plt.xlabel("City size ($\log_{10}$-scale)")
plt.ylabel("Centrality value  ($\log_{10}$-scale)")
plt.title("Eigenvector centrality value, \n\
data: {:s}, strP, 10KB".format(D_Name))
plt.legend()
plt.savefig(dir_path+"/eigenvector_centrality_v{:d}_pI_strP_10KB.png".format(version))

plt.figure(25)
plt.clf()
plt.scatter(np.log10(beta_x10), h_P, c="black", marker="+", label = "eigenvector value")
plt.xlabel("City size ($\log_{10}$-scale)")
plt.scatter(np.log10(beta_x10), KA_P/RP_ref, c="blue", marker="+", alpha=0.2, label = "KA_P/R_0")
plt.scatter(np.log10(beta_x10), KB_P/RP_ref, c="green", marker="+", alpha=0.2, label = "KB_P/R_0")
plt.ylabel("Centrality value")
plt.title("Eigenvector centrality value, \n\
data: {:s}, strP, 10KB".format(D_Name))
plt.legend()
plt.savefig(dir_path+"/eigenvector_centrality_noExp_v{:d}_pI_strP_10KB.png".format(version))

#%% summary_parameters and summary_per_bin saved
summary_parameters.to_csv("summary_parameters_v{:d}.csv".format(version))
summary_per_bin.to_csv("summary_per_bin_v{:d}.csv".format(version))
########################################################################
#%% R0 estimation

#%%% Generation-based estimation

Rt_GB = np.zeros((N_p, Nt))
It_GB = np.zeros((N_p, Nt))

PrI_GB = np.zeros((N_p, Counts_ref.size))

N_fl_GB = 0
for k in np.arange(N_p):
    Tf_GB, IAf_GB, Rf_GB, Shf_GB, N_it2_GB = InfectionR(1+t_o, N_I, KA_P, KB_P, nuA, nuB, TL_U_x10)
    #failure update
    N_fl_GB = N_fl_GB + N_it2_GB/N_p
    #update for the total number of infected 
    T1 = Tf_GB[Rf_GB]
    T1 = T1.reshape((1, np.size(T1)))
    Rt_GB[k] = np.sum(T1 < Gtb, axis = 1)
    #update for the number of infectious
    T2 = Tf_GB[Rf_GB] + TL_P_x10[Rf_GB]
    T2 = np.reshape(T2, (1, np.size(T2)))
    It_GB[k] = np.sum((T1 < Gtb)*(Gtb < T2), axis = 1)
    #proportion of infected cities as a function of their size
    counts, bins, bars = plt.hist(np.log10(beta_x10[Tf_GB==0]), bins = Sep)
    plt.clf()
    PrI_GB[k] = 1- counts/Counts_ref
    
    
print("Runs of simulations completed.")
print("Averaged number of failures observed : N_fl~{:.2e}".format(N_fl) )  


#%%% Selection of the data (pay attention to the conditioning on epidemics sufficiently large)
#PoI = float(input("Which threshold for the infectious population size with enough stability? \n >PoI = "))
PoI = 10
#N_o = int(input("Number of generations for the measurement:"))

#First, we need to make restrictions of It to the data that we are really interested in
#simulations discarded if the threshold PoI is not reached, the others are "Simulations that are Kept"
SK = (np.max(It_GB, axis = 1)>PoI)
NpK = np.sum(SK)
t_I=np.zeros(NpK, dtype = int)
#we need a further filtering for simulations that are kept N_o generations after t_I
for k in np.arange(NpK):
    t_I[k]= np.where(It_GB[SK][k] > PoI)[0][0]
It3 = np.zeros((NpK, N_o+2))
Z = np.arange(N_o+2)
for k in np.arange(NpK):
    It3[k, :] = It_GB[SK][k, t_I[k]+N_d*Z]

#%%%Plot R0_LSestimation_{:d}_v{:d}_pI_strP_10KG.png

eR0_m = np.zeros(N_o-2)
eR0_s = np.zeros(N_o-2)
eR0_f = np.zeros(N_o-2)
eR0_n = np.zeros(N_o-2)


for i in np.arange(N_o-2):
    SK4 = (It3[:, i+1]>0)
    NpK4 = np.sum(SK4)
    if NpK4 > N_p / 10:
        It4 = It3[SK4]
        lR0 = np.zeros(NpK4)
        X = np.arange(i+2)
        Y = np.log10(It4[:, :i+2])
        X1 = X.reshape(1, X.size)
        lR0 = (np.mean(X1*Y, axis = 1)-np.mean(X)*np.mean(Y, axis = 1))/ np.var(X)
        eR0 = 10**lR0
        eR0_m[i] = np.mean(eR0)
        eR0_s[i] = np.std(eR0)
        eR0_sort = np.sort(eR0)
        eR0_f[i] = eR0_sort[int(NpK4/20)]
        eR0_n[i] = eR0_sort[int(NpK4*19/20)]

summary_per_gen2["pO_strP_nbrI_{:d}_R0_10KG_emp".format(PoI)] = eR0_m
summary_per_gen2["pO_strP_nbrI_{:d}_R0_10KG_std".format(PoI)] = eR0_s
summary_per_gen2["pO_strP_nbrI_{:d}_R0_10KG_q5".format(PoI)] = eR0_f
summary_per_gen2["pO_strP_nbrI_{:d}_R0_10KG_q95".format(PoI)] = eR0_n
eR0_ms = eR0_m - eR0_s
eR0_ps = eR0_m + eR0_s


print("Expected R0:~{:.2e}".format(RP_ref))

plt.figure(6)
plt.clf()
plt.vlines(np.arange(2, N_o), \
          ymin = eR0_f,\
          ymax = eR0_n,\
         color= "purple", linestyle =":")
pl_FivePer, = plt.plot(np.arange(2, N_o), eR0_n, "m^:", label = "5%-95%")
plt.plot(np.arange(2, N_o), eR0_f, "mv:")
pl_Std, = plt.plot(np.arange(2, N_o), eR0_ps, "r^:", label="+std")
plt.plot(np.arange(2, N_o), eR0_ms, "rv:")
pl_Avg, = plt.plot(np.arange(2, N_o), eR0_m, "bD--", label="simulated")
pl_Exp, = plt.plot(np.arange(2, N_o), RP_ref*np.ones(N_o-2), c="black", label="expected")
plt.title("R0 estimation, \n\
reference at {:.0f}  cities currently infected, data: {:s} ".format(PoI, D_Name))
plt.xlabel("Number of generations in the time-interval")
plt.ylabel("Estimated R0")
plt.legend(handles = [pl_Exp, pl_Avg, pl_Std, pl_FivePer], title = "{:s}, pI, strP, 10KG".format(abbrev))
plt.ylim(-0.02*RP_ref, 1.3*RP_ref)
plt.savefig(dir_path+"/R0_LSestimation_{:d}_v{:d}_pI_strP_10KG.png".format(PoI, version))
    

#%%% Rescaled plot
plt.figure(7)
plt.clf()
plt.vlines(np.arange(2, N_o), \
          ymin = eR0_f/RP_ref,\
          ymax = eR0_n/RP_ref,\
         color= "purple", linestyle =":")
pl_FivePer, = plt.plot(np.arange(2, N_o), eR0_n/RP_ref, "m^:", label = "5%-95%")
plt.plot(np.arange(2, N_o), eR0_f/RP_ref, "mv:")
pl_Std, = plt.plot(np.arange(2, N_o), eR0_ps/RP_ref, "r^:", label="+std")
plt.plot(np.arange(2, N_o), eR0_ms/RP_ref, "rv:")
pl_Avg, = plt.plot(np.arange(2, N_o), eR0_m/RP_ref, "bD--", label="simulated")
pl_Exp, = plt.plot(np.arange(2, N_o), np.ones(N_o-2), c="black", label="expected")
plt.title("R0 estimation, \n\
reference at {:.0f}  cities currently infected, data: {:s} ".format(PoI, D_Name))
plt.xlabel("Number of generations in the time-interval")
plt.ylabel("Estimated R0 (compared to the expected value)")
plt.legend(handles = [pl_Exp, pl_Avg, pl_Std, pl_FivePer], title = "{:s}, pI, strP, 10KG".format(abbrev))
plt.ylim(-0.02, 1.3)
plt.savefig(dir_path+"/R0_LSestimation_RS_{:d}_v{:d}_pI_strP_10KG.png".format(PoI, version))
    

#%%% summary_parameters and summary_per_gen saved
summary_parameters.to_csv("summary_parameters_v{:d}.csv".format(version))
summary_per_gen2.to_csv("summary_per_gen2_v{:d}.csv".format(version))


#%% Incidence plots as a function of R0
#%%% Plot Final_incidence_ro_v{:d}_strP_10KG

Inf_cit = np.zeros((bR, NR))
Inf_ppl = np.zeros((bR, NR))
NP = np.sum(beta_x10)
for iR in np.arange(bR):
    for j in np.arange(NR):
        Tf2, IAf2, Rf2, Shf2, N_it2 = InfectionR(100, 1, (R0_var[iR]/RP_ref) *KA_P, (R0_var[iR]/RP_ref) *KB_P, nuA, nuB, np.ones(10*NC))
        #InfectionRM(100, 1, (R0_var[iR]/RP_ref) * MErP, nuA, np.ones(10*NC))
        Inf_cit[iR, j] = np.sum(Rf2)/(10*NC)
        Inf_ppl[iR, j] = np.sum(beta_x10[Rf2])/NP
summary_per_R0['pI_10KG_strP_Inf_cit'] = np.mean(Inf_cit, axis = 1)
summary_per_R0['pI_10KG_strP_Inf_ppl'] = np.mean(Inf_ppl, axis = 1)


plt.plot(R0_var, summary_per_R0['pI_10KG_strP_Inf_cit'], 'b*', label = "Cities under isolation")
plt.plot(R0_var, summary_per_R0['pI_10KG_strP_Inf_ppl'], 'r^', label = "People under isolation")
plt.title("Mean proportion of cities and people under isolation,\n \
data: {:s}, strP, 10KG".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("Mean proportion")
plt.legend(title = "{:s}, strP, 10KG".format(abbrev))
plt.ylim(0, 1)
plt.savefig(dir_path+"/Final_incidence_ro_v{:d}_strP_10KG.png".format(version))

#%%% Plot Final_incidence_thr_ro_v{:d}_strP_10KB

pi_AP= np.ones(Nr)
pi_BP= np.ones(Nr)
Inc_cit_P = np.zeros(bR)
Inc_pop_P = np.zeros(bR)
for i in np.arange(bR):
    err = 1
    n_it = 0
    pi = 1- np.exp(- R0_var[i] *(bKA_P0 + bKB_P0) )
    while (err > 5E-5) * (n_it < 500):
        pi_AP[i]= np.sum(bnuA_P*pi)
        pi_BP[i]= np.sum(bnuB_P*pi)
        pi2 = 1-np.exp(- R0_var[i] *( bKA_P0 *pi_AP[i] +bKB_P0* pi_BP[i]))
        err = np.max(np.abs(1-pi/pi2))
        n_it = n_it +1
        pi = pi2
    Inc_cit_P[i] = 1/(10*NC) * np.sum(pi)
    Inc_pop_P[i] = 1/np.sum(beta_x10) * np.sum(beta_x10*pi)

plt.plot(R0_var, Inc_cit_P, 'b*', label = "Cities under isolation")
plt.plot(R0_var, Inc_pop_P, 'r*', label = "People under isolation")
plt.title("Expected proportion of cities and people under isolation,\n \
data: {:s}, strP, 10KB".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("Mean proportion")
plt.legend(title = "{:s}, strP, 10KB".format(abbrev))
plt.ylim(0, 1)
plt.savefig(dir_path+"/Final_incidence_thr_ro_v{:d}_strP_10KB.png".format(version))

#%%% Plot Final_incidence_Nc_v{:d}_strP_10KG

eta_AP= np.ones(Nr)
eta_BP= np.ones(Nr)
Prob_Out = np.zeros(bR)
for i in np.arange(bR):
    err = 1
    n_it = 0
    eta = 1- np.exp(- R0_var[i] *(KA_P0 + KB_P0) )
    while (err > 5E-5) * (n_it < 500):
        eta_AP[i]= np.sum(nuA*eta)
        eta_BP[i]= np.sum(nuB*eta)
        eta2 = 1-np.exp(- R0_var[i] *( KA_P0 *eta_AP[i] + KB_P0* eta_BP[i]))
        err = np.max(np.abs(1-eta/eta2))
        n_it = n_it +1
        eta = eta2
    Prob_Out[i] = np.sum(eta*nuA)



plt.plot(R0_var, summary_per_R0['pI_strP_Inf_cit'], 'g*', label = "Cities under isolation, 10KG")
plt.plot(R0_var, summary_per_R0['pI_strP_Inf_ppl'], 'm*', label = "People under isolation, 10KG")
plt.plot(R0_var, Prob_Out * Inc_cit_P, 'b+-', label = "Cities under isolation, 10KB")
plt.plot(R0_var, Prob_Out * Inc_pop_P, 'r+-', label = "People under isolation, 10KB")

plt.title("Mean proportion of cities and people under isolation,\n \
data: {:s}, strP".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("Mean proportion")
plt.legend(loc='lower right', title = "{:s}, strP, 10KG".format(abbrev))
plt.ylim(0, 1)
plt.savefig(dir_path+"/Final_incidence_Nc_v{:d}_strP_10KG.png".format(version))

#%%% summary_per_R0 saved
summary_per_R0.to_csv("summary_per_R0_v{:d}.csv".format(version))


#%% Plot variation_empirical_pOutbreak_vs_size_v{:d}_pI_strP_10KG

Isort = np.argsort(beta)
Prop_Out = np.zeros(beta.size)
#S_O = int(input("What is the size threshold for a successful outbreak?  > S_O = "))
S_O = summary_parameters.loc[0, "pI_size_successful_outbreak"]
#R_c = int(input("How many runs for each initial city?  > Rc = "))
Rc = summary_parameters.loc[0, "pI_strU_eta_Nbr_runs"]
for i in np.arange(beta.size):
    Success = np.zeros((Rc), dtype = bool)
    for k in np.arange(Rc):
        IA = np.zeros((10*NC), dtype = bool)
        IA[10*i] = 1
        Tf, IAf, Rf, Shf = InfectionI(1+15*TL_U_x10[0], IA, KA_P, KB_P, nuA, nuB, TL_U_x10)
        Success[k] = ( np.sum(Rf) > S_O)
    Prop_Out[i] = np.sum(Success)/Rc

summary_per_10city["pI_strP_emp_eta_10KG"] = Prop_Out

Mean_pOut = np.zeros(MidBins.size)
Std_pOut = np.zeros(MidBins.size)
Min_pOut = np.zeros(MidBins.size)
Max_pOut = np.zeros(MidBins.size)
for ir in np.arange(MidBins.size):
    Is_bin = (np.log10(beta)>=Bins_ref[ir])& (np.log10(beta)<Bins_ref[ir+1])
    Mean_pOut[ir] = np.mean(Prop_Out[Is_bin])
    Std_pOut[ir] = np.std(Prop_Out[Is_bin])
    Min_pOut[ir] = np.min(Prop_Out[Is_bin])
    Max_pOut[ir] = np.max(Prop_Out[Is_bin])
#GX = summary_per_bin['Central_value_log_sc']
summary_per_bin["pI_strP_empB_eta_10KG"] = Mean_pOut
summary_per_bin["pI_strP_stdB_emp_eta_10KG"] = Std_pOut
summary_per_bin["pI_strP_minB_eta_10KG"] = Min_pOut
summary_per_bin["pI_strP_maxB_eta_10KG"] = Max_pOut

plt.figure(4)
plt.clf()
plt.vlines(MidBins, \
          ymin = Min_pOut,  ymax = Max_pOut,\
         color= "purple", linestyle =":")
plt.plot(MidBins, Min_pOut, "mv:")
plt.plot(MidBins, Max_pOut, "m^:", label = "range")
plt.plot(MidBins, Mean_pOut + Std_pOut, "^r:", label="std")
plt.plot(MidBins, Mean_pOut - Std_pOut, "vr:")
plt.plot(MidBins, Mean_pOut, "bD--", label = "simulated")
plt.plot(MidBins, etaRef_P, "ks-", label = "analytical")
plt.title("Outbreak probability, \n \
Fluctuations within the bin, data :{:s}, 10KG".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.ylim(0,1.02)
plt.legend(title = "{:s}, pI, strP, 10KG".format(abbrev))
plt.savefig(dir_path+"/variation_empirical_pOutbreak_vs_size_v{:d}_pI_strP_10KG.png".format(version))

summary_per_bin.to_csv("summary_per_bin_v{:d}.csv".format(version))


#%% End
