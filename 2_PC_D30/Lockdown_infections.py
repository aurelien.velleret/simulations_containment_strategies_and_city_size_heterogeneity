#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 13 15:32:47 2021

@author: user
"""


#%%
import numpy as np
import numpy.random as npr
import random as rd
import matplotlib.pyplot as plt
import time 
from mpl_toolkits.mplot3d import Axes3D
import scipy.sparse as sp

from Inf_run import Propa, Ife_U, InfectionI, InfectionR, PropaM, Ife_UM, InfectionIM, InfectionRM 

version = 2
D_Name = "Poland (D30+)"
abbrev = "PC, D30+"

#%%
import pandas as pd

print('Choice of Polish powiats as core data')
# #vector of empirical city size distribution
beta = pd.read_csv("beta_30_Pow.csv", index_col = 0)
beta = np.array(beta).flatten()
summary_per_city = pd.DataFrame(beta, columns = ['City_size'])
#All values are distinct and different from 0
print("The distribution of city size is now given by the array 'beta'. \n \
You may consider changing the working directory for the figures.")
NC = np.size(beta)
#Total graph data of the flux between long distance powiats
MCom = sp.load_npz("MPow_30.npz")

#%%
print("Check the figure for a sanity check that the power-law distribution is correct")
beta_s=np.sort(beta)
plt.clf()
plt.plot(np.log10(beta_s), np.log(1 - np.arange(NC)/NC), "xr")
plt.title("Distribution of city sizes\n\
from Polish powiats data")
plt.xlabel("log of ordered city sizes")
plt.ylabel("log of empirical repartition function")
plt.savefig("dist_sizes_v{:d}_Pow".format(version))
#%%

#a = input("Which value takes the parameter 'a' for the choice of the outwards infected city size?\n")
a = 1.95
#b = input("Which value takes the parameter 'b' for the choice of the inwards infected city size?\n")
b = -0.11
summary_parameters = pd.DataFrame([[a, b]], columns = ['a', 'b'])
summary_parameters['City_Nbr'] = NC
summary_parameters['Data_Name'] = D_Name
summary_parameters['abbrev'] = abbrev

#Bins for the proportion of infected cities as a function of their size
NL =20
beta_LS = np.sort(np.log10(beta))
Bins = np.linspace(0, beta.size-1, NL, dtype = int)
Bins = Bins[:-1]
#the last component are too variable for any inference, all cities then get infected
Sep = beta_LS[Bins]
Counts_ref, Bins_ref, bars = plt.hist(beta_LS, bins = Sep)
MidBins = (Bins_ref[:-1] + Bins_ref[1:])/2

summary_per_bin = pd.DataFrame(MidBins, columns = ['Central_value_log_sc'])
summary_per_bin['Central_value'] = 10**MidBins
summary_per_bin['City_Count'] = np.array(Counts_ref, dtype = int)
summary_per_bin['Lower_size'] = 10**(Bins_ref[:-1])
summary_per_bin['Upper_size'] = 10**(Bins_ref[1:])

#%%
#For the estimation of incidence as a function of R0
#bR = input("How many bins for the threshold values?")
bR = 50
summary_parameters['how many_threshold_value'] = bR
#NR = input("How many runs for each threshold value?")
NR = 300
summary_parameters['runs_per_threshold_value'] = NR
R0_var = np.linspace(0.9, 40, bR)

summary_per_R0 = pd.DataFrame(R0_var, columns = ['R0'])


#%%
#For the estimation of R0 in terms of generations
#Number of generations for the measurement
N_o = 10
#k_dec = int(input("computation initiated after k_dec %ge 2 generation:"))
#k_dec = 2
#we shall only look at multiples of the generation time
summary_per_gen = pd.DataFrame(np.arange(2, N_o), columns = ['Gen_after_thr'])


#we fix 1/1000=100/100000 as a reference
#p_V = input("Which reference value is to be taken for the parameter 'p_v' \n (reference fraction of the population infected for lockdown)")
p_v = 1/1000
TL_P = np.log(beta*p_v)/np.log(2)*14
#Note that there are 6 Gemeindeverband with less than 1000 inhabitants !
#
TL_P[TL_P<1] = 1
TL_U = np.log(np.mean(beta*p_v))/np.log(2)*14*np.ones(beta.size)

#%%
import os.path
if os.path.isfile("summary_parameters_v{:d}.csv".format(version)):
    summary_parameters = pd.read_csv("summary_parameters_v{:d}.csv".format(version), index_col = 0)
else:
    summary_parameters.to_csv("summary_parameters_v{:d}.csv".format(version))
    
if os.path.isfile("summary_per_city_v{:d}.csv".format(version)):
    summary_per_city = pd.read_csv("summary_per_city_v{:d}.csv".format(version), index_col = 0)
else:
    summary_per_city.to_csv("summary_per_city_v{:d}.csv".format(version))
#To avoid making a new sampling, we recall the previous sampling:
beta = np.array(summary_per_city['City_size'])

if os.path.isfile("summary_per_bin_v{:d}.csv".format(version)):
    summary_per_bin = pd.read_csv("summary_per_bin_v{:d}.csv".format(version), index_col = 0)
else:
    summary_per_bin.to_csv("summary_per_bin_v{:d}.csv".format(version))    
    
if os.path.isfile("summary_per_gen_v{:d}.csv".format(version)):
    summary_per_gen = pd.read_csv("summary_per_gen_v{:d}.csv".format(version), index_col = 0)
else:
    summary_per_gen.to_csv("summary_per_gen_v{:d}.csv".format(version))
    
if os.path.isfile("summary_per_R0_v{:d}.csv".format(version)):
    summary_per_R0 = pd.read_csv("summary_per_R0_v{:d}.csv".format(version), index_col = 0)
else:
    summary_per_R0.to_csv("summary_per_R0_v{:d}.csv".format(version))

#%%
summary_parameters.loc[0, "Upper_R_pI_strU"] = 20
summary_parameters.loc[0, "Upper_R_pI_strP"] = 30
summary_parameters.loc[0, "Upper_R_pO_strU"] = 3
summary_parameters.loc[0, "Upper_R_pO_strP"] = 45

#%%
print("Start of simulations:")
print("for variant U of the lockdown, check file 'variantU.py';")
print("for variant P of the lockdown, check file 'variantP.py'.")

