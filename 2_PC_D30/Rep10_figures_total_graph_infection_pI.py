# -*- coding: utf-8 -*-
"""
Created on Tue Feb 15 10:50:24 2022

@author: alter
"""

#%%
#%%
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

print("The simulation data is assumed to have already been collected under this version number\n \
under the stipulated version name. Proceed only if correct!")
version = int(input("What is the number of the version?  > version = "))
#version = 1
D_Name = input("How the data should be described in the titles?  > D_Name = ")
#D_Name = "France (10 rep.)"
abbrev = input("How the data should be abbreviated in the legends?  > abbrev = ")
#abbrev = "FC, D30+, x10"

print("The simulation data is assumed to be available for the (pI) choice of R0\n\
where a reference for the theoretical probability for a city to be infected is taken.\n\
cf threshold_through_pInfected.py for more details.")
#%%

summary_rep_parameters = pd.read_csv("summary_rep_parameters_v{:d}.csv".format(version), index_col = 0)
summary_rep_per_city= pd.read_csv("summary_rep_per_city_v{:d}.csv".format(version), index_col = 0)
summary_rep_per_bin= pd.read_csv("summary_rep_per_bin_v{:d}.csv".format(version), index_col = 0)
summary_rep_per_gen = pd.read_csv("summary_rep_per_gen_v{:d}.csv".format(version), index_col = 0)
summary_rep_per_R0 = pd.read_csv("summary_rep_per_R0_v{:d}.csv".format(version), index_col = 0)

beta_rep = np.array(summary_rep_per_city["City_size"])
NC = beta_rep.size
a = summary_rep_parameters.loc[0, 'a']
b = summary_rep_parameters.loc[0, 'b']

MidBins = summary_rep_per_bin['Central_value_log_sc']

summary_parameters = pd.read_csv("summary_parameters_v{:d}.csv".format(version), index_col = 0)
summary_per_city= pd.read_csv("summary_per_city_v{:d}.csv".format(version), index_col = 0)
summary_per_bin= pd.read_csv("summary_per_bin_v{:d}.csv".format(version), index_col = 0)
summary_per_gen = pd.read_csv("summary_per_gen_v{:d}.csv".format(version), index_col = 0)
summary_per_R0 = pd.read_csv("summary_per_R0_v{:d}.csv".format(version), index_col = 0)

beta = np.array(summary_per_city["City_size"])    

#%%
RU = 1/NC*(np.sum(beta_rep**(a+b)) + np.sqrt(np.sum(beta_rep**(2*a-1)) \
* np.sum(beta_rep**(1+2*b))))

RP = 1/NC*(np.sum(beta_rep**(a+b+1)) + np.sqrt(np.sum(beta_rep**(2*a)) \
* np.sum(beta_rep**(2+2*b))))
    
    
RU_ref = summary_rep_parameters.loc[0, "pI_strU_thr_R0"] 

RP_ref = summary_rep_parameters.loc[0, "pI_strP_thr_R0"]    

rhoE = summary_parameters.loc[0, 'pI_strU_exp_growth_rate']
#rhoE = np.log10(summary_parameters.loc[0, "pI_strU_thr_R0"] )/TL_U[0]
#rhoE_10 = np.log10(summary_rep_parameters.loc[0, "pI_strU_thr_R0"] )/TL_U[0]
##good agreement between these two predictions, cf (rhoE-rhoE_10)/rhoE_10
#
print("Expected growth rate~{:.2e} for power 10".format(rhoE))


#%%
pi_U = summary_rep_per_city["pI_strU_thr_pi"] 
piRef_U = summary_rep_per_bin["pI_strU_thr_pi"] 

plt.figure(5)
plt.clf()
plt.scatter(np.log10(beta_rep), pi_U, c="black", marker="+", label = "analytical relation")
plt.title("Theoretical infection probability, \n \
data: {:s}, strU, TG".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pI, strP, TG".format(abbrev))
plt.ylim(0, 1)
plt.savefig("theoretical_Infected_vs_size_v{:d}_pI_strU_TG_rep10.png".format(version))

#%%

Emp_pi_rep_avg = summary_rep_per_bin["pI_strU_emp_pi"] 
Emp_pi_rep_std = summary_rep_per_bin["pI_strU_std_emp_pi"]
Emp_pi_rep_min = summary_rep_per_bin["pI_strU_min_pi"] 
Emp_pi_rep_max = summary_rep_per_bin["pI_strU_max_pi"]

Emp_pi_rep_s = Emp_pi_rep_avg + Emp_pi_rep_std
Emp_pi_rep_ms = Emp_pi_rep_avg - Emp_pi_rep_std


plt.figure(3)
plt.clf()
plt.plot(MidBins, piRef_U, "ks--", label = "analytical")

alpha_IP = 0.8

plt.plot(MidBins, Emp_pi_rep_avg, "bD", label="empirical (x10)")
plt.plot(MidBins, Emp_pi_rep_s, "^r:", alpha = alpha_IP, label="std (x10)")
plt.plot(MidBins, Emp_pi_rep_ms, "vr:", alpha = alpha_IP)
plt.vlines(MidBins, \
          ymin = Emp_pi_rep_min,  ymax = Emp_pi_rep_max,\
         color= "m", alpha = alpha_IP, linestyle =":")
plt.plot(MidBins, Emp_pi_rep_min, "mv:", alpha = alpha_IP)
plt.plot(MidBins, Emp_pi_rep_max, "m^:", alpha = alpha_IP, label = "range (x10)")

plt.title("Empirical and theoretical infection probability,\n \
fluctuations between the runs, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pI, strU, TG".format(abbrev))
plt.ylim(-0.02, 1.02)

plt.savefig("Infected_vs_size_v{:d}_pI_strU_TG_rep10.png".format(version))
#%%

Emp_pi_avg = summary_per_bin["pI_strU_emp_pi"] 
Emp_pi_std = summary_per_bin["pI_strU_std_emp_pi"]
Emp_pi_min = summary_per_bin["pI_strU_min_pi"] 
Emp_pi_max = summary_per_bin["pI_strU_max_pi"]

Emp_pi_s = Emp_pi_avg + Emp_pi_std
Emp_pi_ms = Emp_pi_avg - Emp_pi_std

plt.figure(3)
plt.clf()
plt.plot(MidBins, piRef_U, "ks--", label = "analytical")

alpha_IP = 0.6

plt.plot(MidBins, Emp_pi_rep_avg, "gD", label="empirical (x10)")
plt.plot(MidBins, Emp_pi_rep_s, "^y:", alpha = alpha_IP, label="std (x10)")
plt.plot(MidBins, Emp_pi_rep_ms, "vy:", alpha = alpha_IP)
plt.vlines(MidBins, \
          ymin = Emp_pi_rep_ms,  ymax = Emp_pi_rep_s,\
          color= "y", alpha = alpha_IP, linestyle =":")
# plt.vlines(MidBins, \
#           ymin = Emp_pi_rep_min,  ymax = Emp_pi_rep_max,\
#           color= "c", alpha = alpha_IP, linestyle =":")
# plt.plot(MidBins, Emp_pi_rep_min, "cv:", alpha = alpha_IP)
# plt.plot(MidBins, Emp_pi_rep_max, "c^:", alpha = alpha_IP, label = "range (x10)")

plt.plot(MidBins, Emp_pi_avg, "bD", label="empirical")
plt.vlines(MidBins, \
          ymin = Emp_pi_ms,  ymax = Emp_pi_s,\
          color= "r", alpha = alpha_IP, linestyle =":")
plt.plot(MidBins, Emp_pi_s, "^r:", alpha = alpha_IP, label="std")
plt.plot(MidBins, Emp_pi_ms, "vr:", alpha = alpha_IP)
# plt.vlines(MidBins, \
#           ymin = Emp_pi_min,  ymax = Emp_pi_max,\
#          color= "purple", alpha = alpha_IP, linestyle =":")
# plt.plot(MidBins, Emp_pi_min, "mv:", alpha = alpha_IP)
# plt.plot(MidBins, Emp_pi_max, "m^:", alpha = alpha_IP, label = "range")

plt.title("Empirical and theoretical infection probability,\n \
fluctuations between the runs, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pI, strU, TG".format(abbrev))
plt.ylim(-0.02, 1.02)

plt.savefig("Infected_vs_size_v{:d}_pI_strU_TG_Crep10.png".format(version))

#%%

Is_I_rep_avg = summary_rep_per_city["pI_strU_emp_pi"] 

#Is_I_rep_var = np.sum(Is_I**2, axis =0)/N_p - Is_I_rep_avg**2
Is_I_rep_std = summary_rep_per_city["pI_strU_std_emp_pi"]

EmpB_pi_rep_avg = summary_rep_per_bin["pI_strU_empB_pi"]
EmpB_pi_rep_std = summary_rep_per_bin["pI_strU_stdB_emp_pi"]
EmpB_pi_rep_min = summary_rep_per_bin["pI_strU_minB_pi"]
EmpB_pi_rep_max = summary_rep_per_bin["pI_strU_maxB_pi"]

EmpB_pi_rep_s = EmpB_pi_rep_avg + EmpB_pi_rep_std
EmpB_pi_rep_ms = EmpB_pi_rep_avg - EmpB_pi_rep_std

R2 = summary_rep_parameters.loc[0, "pI_strU_infP_R2"]

plt.figure(4)
plt.clf()
plt.plot(MidBins, piRef_U, "ks--", label = "analytical")
plt.plot(MidBins, EmpB_pi_rep_avg, "bD", label="empirical (x10)")
plt.plot(MidBins, EmpB_pi_rep_s, "^r:", label="std (x10)")
plt.plot(MidBins, EmpB_pi_rep_ms, "vr:")
plt.vlines(MidBins, \
          ymin = EmpB_pi_rep_min,  ymax = EmpB_pi_rep_max,\
         color= "purple", linestyle =":")
plt.plot(MidBins, EmpB_pi_rep_min, "mv:")
plt.plot(MidBins, EmpB_pi_rep_max, "m^:", label = "range  (x10)")
plt.title("Empirical and theoretical infection probability,\n \
fluctuations within the bin, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pI, strU, TG\n\
R^2 = {:.2f}".format(abbrev, summary_rep_parameters.loc[0,"pI_strU_infP_R2"]))
plt.ylim(-0.02, 1.02)


plt.savefig("variation_infected_vs_size_v{:d}_pI_strU_TG_rep10.png".format(version))

#%%

Is_I_avg = summary_per_city["pI_strU_emp_pi"] 

#Is_I_var = np.sum(Is_I**2, axis =0)/N_p - Is_I_avg**2
Is_I_std = summary_per_city["pI_strU_std_emp_pi"]

EmpB_pi_avg = summary_per_bin["pI_strU_empB_pi"]
EmpB_pi_std = summary_per_bin["pI_strU_stdB_emp_pi"]
EmpB_pi_min = summary_per_bin["pI_strU_minB_pi"]
EmpB_pi_max = summary_per_bin["pI_strU_maxB_pi"]

EmpB_pi_s = EmpB_pi_avg + EmpB_pi_std
EmpB_pi_ms = EmpB_pi_avg - EmpB_pi_std
alpha_IP = 0.9

plt.figure(4)
plt.clf()
plt.plot(MidBins, piRef_U, "ks--", label = "analytical")
plt.plot(MidBins, EmpB_pi_rep_avg, "gD", label="empirical (x10)")
plt.plot(MidBins, EmpB_pi_rep_s, "^y:", alpha = alpha_IP, label="std (x10)")
plt.plot(MidBins, EmpB_pi_rep_ms, "vy:", alpha = alpha_IP)
plt.vlines(MidBins, \
          ymin = EmpB_pi_rep_ms,  ymax = EmpB_pi_rep_s,\
          color= "y", alpha = alpha_IP, linestyle =":")
# plt.vlines(MidBins, \
#           ymin = EmpB_pi_rep_min,  ymax = EmpB_pi_rep_max,\
#          color= "c", alpha = alpha_IP, linestyle =":")
# plt.plot(MidBins, EmpB_pi_rep_min, "cv:", alpha = alpha_IP)
# plt.plot(MidBins, EmpB_pi_rep_max, "c^:", alpha = alpha_IP, label = "range (x10)")

plt.plot(MidBins, EmpB_pi_avg, "bD", label="empirical")
plt.plot(MidBins, EmpB_pi_s, "^r:", alpha = alpha_IP, label="std")
plt.plot(MidBins, EmpB_pi_ms, "vr:", alpha = alpha_IP)
plt.vlines(MidBins, \
          ymin = EmpB_pi_ms,  ymax = EmpB_pi_s,\
          color= "r", alpha = alpha_IP, linestyle =":")
# plt.vlines(MidBins, \
#           ymin = EmpB_pi_min,  ymax = EmpB_pi_max,\
#          color= "purple", alpha = alpha_IP, linestyle =":")
# plt.plot(MidBins, EmpB_pi_min, "mv:", alpha = alpha_IP)
# plt.plot(MidBins, EmpB_pi_max, "m^:", alpha = alpha_IP, label = "range")


plt.title("Empirical and theoretical infection probability,\n \
fluctuations within the bin, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pI, strU, TG".format(abbrev))
plt.ylim(-0.02, 1.02)


plt.savefig("variation_infected_vs_size_v{:d}_pI_strU_TG_Crep10.png".format(version))



#%%
pi_P = summary_rep_per_city["pI_strP_thr_pi"]
piRef_P = summary_rep_per_bin["pI_strP_thr_pi"]

Emp_pi_rep_strP_avg = summary_rep_per_bin["pI_strP_emp_pi"]
Emp_pi_rep_strP_std = summary_rep_per_bin["pI_strP_std_emp_pi"]
Emp_pi_rep_strP_min = summary_rep_per_bin["pI_strP_min_pi"]
Emp_pi_rep_strP_max = summary_rep_per_bin["pI_strP_max_pi"]

Emp_pi_rep_strP_s = Emp_pi_rep_strP_avg + Emp_pi_rep_strP_std
Emp_pi_rep_strP_ms = Emp_pi_rep_strP_avg - Emp_pi_rep_strP_std

plt.figure(3)
plt.clf()
plt.plot(MidBins, piRef_P, "ks--", label = "analytical")
plt.plot(MidBins, Emp_pi_rep_strP_avg, "bD", label="empirical (x10)")
plt.plot(MidBins, Emp_pi_rep_strP_s, "^r:", label="std (x10)")
plt.plot(MidBins, Emp_pi_rep_strP_ms, "vr:")
plt.vlines(MidBins, \
          ymin = Emp_pi_rep_strP_min,  ymax = Emp_pi_rep_strP_max,\
         color= "purple", linestyle =":")
plt.plot(MidBins, Emp_pi_rep_strP_min, "mv:")
plt.plot(MidBins, Emp_pi_rep_strP_max, "m^:", label = "range (x10)")
plt.title("Empirical and theoretical infection probability,\n \
fluctuations between the runs, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pI, strP, TG".format(abbrev))
plt.ylim(0, 1.02)

plt.savefig("Infected_vs_size_v{:d}_pI_strP_TG_rep10.png".format(version))
#%%

Emp_pi_strP_avg = summary_per_bin["pI_strP_emp_pi"]
Emp_pi_strP_std = summary_per_bin["pI_strP_std_emp_pi"]
Emp_pi_strP_min = summary_per_bin["pI_strP_min_pi"]
Emp_pi_strP_max = summary_per_bin["pI_strP_max_pi"]

Emp_pi_strP_s = Emp_pi_strP_avg + Emp_pi_strP_std
Emp_pi_strP_ms = Emp_pi_strP_avg - Emp_pi_strP_std

alpha_IP = 0.9

plt.figure(4)
plt.clf()
plt.plot(MidBins, piRef_P, "ks--", label = "analytical")
plt.plot(MidBins, Emp_pi_rep_strP_avg, "gD", label="empirical (x10)")
plt.plot(MidBins, Emp_pi_rep_strP_s, "^y:", alpha = alpha_IP, label="std (x10)")
plt.plot(MidBins, Emp_pi_rep_strP_ms, "vy:", alpha = alpha_IP)
plt.vlines(MidBins, \
          ymin = Emp_pi_rep_strP_ms,  ymax = Emp_pi_rep_strP_s,\
          color= "y", alpha = alpha_IP, linestyle =":")
# plt.vlines(MidBins, \
#           ymin = Emp_pi_rep_min,  ymax = Emp_pi_rep_max,\
#          color= "c", alpha = alpha_IP, linestyle =":")
# plt.plot(MidBins, Emp_pi_rep_min, "cv:", alpha = alpha_IP)
# plt.plot(MidBins, Emp_pi_rep_max, "c^:", alpha = alpha_IP, label = "range (x10)")

plt.plot(MidBins, Emp_pi_strP_avg, "bD", label="empirical")
plt.plot(MidBins, Emp_pi_strP_s, "^r:", alpha = alpha_IP, label="std")
plt.plot(MidBins, Emp_pi_strP_ms, "vr:", alpha = alpha_IP)
plt.vlines(MidBins, \
          ymin = Emp_pi_strP_ms,  ymax = Emp_pi_strP_s,\
          color= "r", alpha = alpha_IP, linestyle =":")


plt.title("Empirical and theoretical infection probability,\n \
fluctuations between the runs, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pI, strP, TG".format(abbrev))
plt.ylim(0, 1.02)

plt.savefig("Infected_vs_size_v{:d}_pI_strP_TG_Crep10.png".format(version))

#%%
Is_IP_avg = summary_rep_per_city["pI_strP_emp_pi"]
Is_IP_std = summary_rep_per_city["pI_strP_std_emp_pi"]

EmpB_pi_rep_strP_avg = summary_rep_per_bin["pI_strP_empB_pi"] 
EmpB_pi_rep_strP_std = summary_rep_per_bin["pI_strP_stdB_emp_pi"]
EmpB_pi_rep_strP_min = summary_rep_per_bin["pI_strP_minB_pi"]
EmpB_pi_rep_strP_max = summary_rep_per_bin["pI_strP_maxB_pi"]

EmpB_pi_rep_strP_s = EmpB_pi_rep_strP_avg + EmpB_pi_rep_strP_std
EmpB_pi_rep_strP_ms = EmpB_pi_rep_strP_avg - EmpB_pi_rep_strP_std

R2 = summary_rep_parameters.loc[0, "pI_strP_infP_R2"]

plt.figure(4)
plt.clf()
plt.plot(MidBins, piRef_P, "ks--", label = "analytical")
plt.plot(MidBins, EmpB_pi_rep_strP_avg, "bD", label="empirical (x10)")
plt.plot(MidBins, EmpB_pi_rep_strP_s, "^r:", label="std (x10)")
plt.plot(MidBins, EmpB_pi_rep_strP_ms, "vr:")
plt.vlines(MidBins, \
          ymin = EmpB_pi_rep_strP_min,  ymax = EmpB_pi_rep_strP_max,\
         color= "purple", linestyle =":")
plt.plot(MidBins, EmpB_pi_rep_strP_min, "mv:")
plt.plot(MidBins, EmpB_pi_rep_strP_max, "m^:", label = "range (x10)")
plt.title("Empirical and theoretical infection probability,\n \
fluctuations within the bin, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pI, strP, TG\n\
R^2 = {:.2f}".format(abbrev, summary_rep_parameters.loc[0,"pI_strP_infP_R2"]))
plt.ylim(0, 1.02)


plt.savefig("variation_infected_vs_size_v{:d}_pI_strP_TG_rep10.png".format(version))

#%%
EmpB_pi_strP_avg = summary_per_bin["pI_strP_empB_pi"] 
EmpB_pi_strP_std = summary_per_bin["pI_strP_stdB_emp_pi"]
EmpB_pi_strP_min = summary_per_bin["pI_strP_minB_pi"]
EmpB_pi_strP_max = summary_per_bin["pI_strP_maxB_pi"]

EmpB_pi_strP_s = EmpB_pi_strP_avg + EmpB_pi_strP_std
EmpB_pi_strP_ms = EmpB_pi_strP_avg - EmpB_pi_strP_std


alpha_IP = 0.9

plt.figure(4)
plt.clf()
plt.plot(MidBins, piRef_P, "ks--", label = "analytical")
plt.plot(MidBins, EmpB_pi_rep_strP_avg, "gD", label="empirical (x10)")
plt.plot(MidBins, EmpB_pi_rep_strP_s, "^y:", alpha = alpha_IP, label="std (x10)")
plt.plot(MidBins, EmpB_pi_rep_strP_ms, "vy:", alpha = alpha_IP)
plt.vlines(MidBins, \
          ymin = EmpB_pi_rep_strP_ms,  ymax = EmpB_pi_rep_strP_s,\
          color= "y", alpha = alpha_IP, linestyle =":")
# plt.vlines(MidBins, \
#           ymin = EmpB_pi_rep_min,  ymax = EmpB_pi_rep_max,\
#          color= "c", alpha = alpha_IP, linestyle =":")
# plt.plot(MidBins, EmpB_pi_rep_min, "cv:", alpha = alpha_IP)
# plt.plot(MidBins, EmpB_pi_rep_max, "c^:", alpha = alpha_IP, label = "range (x10)")

plt.plot(MidBins, EmpB_pi_strP_avg, "bD", label="empirical")
plt.plot(MidBins, EmpB_pi_strP_s, "^r:", alpha = alpha_IP, label="std")
plt.plot(MidBins, EmpB_pi_strP_ms, "vr:", alpha = alpha_IP)
plt.vlines(MidBins, \
          ymin = EmpB_pi_strP_ms,  ymax = EmpB_pi_strP_s,\
          color= "r", alpha = alpha_IP, linestyle =":")
plt.title("Empirical and theoretical infection probability,\n \
fluctuations within the bin, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pI, strP, TG".format(abbrev))
plt.ylim(0, 1.02)


plt.savefig("variation_infected_vs_size_v{:d}_pI_strP_TG_Crep10.png".format(version))


#%%
eta_U = summary_rep_per_city["pI_strU_thr_eta"]
etaRef_U = summary_rep_per_bin["pI_strU_thr_eta"] 


plt.figure(5)
plt.clf()
plt.plot(np.log10(beta_rep), eta_U, "ks", markersize = 5, label = "analytical relation")
plt.title("Theoretical outbreak probability, \n \
data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pI, strU, KB".format(abbrev))
plt.savefig("theoretical_pOutbreak_vs_size_v{:d}_KB_rep10.png".format(version))


#%%

PropOut_TGU = summary_rep_per_bin["pI_strU_emp1_eta"]

plt.figure(4)
plt.clf()
plt.scatter(MidBins, etaRef_U, c="black", marker="+", label = "analytical relation")
plt.scatter(np.log10(summary_rep_per_bin['Lower_size']), PropOut_TGU, c="red", marker="+", label = "from simulation runs of epidemic")
plt.title("Empirical and theoretical outbreak probability, \n \
data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.ylim(0,1.02)
plt.legend(title = "{:s}, pI, strU, TG".format(abbrev))
plt.savefig("empirical_pOutbreak_vs_size_v{:d}_pI_strU_TG_rep10.png".format(version))

#It appears that the sizes of outbreaks are so small 
#that the threshold on the size makes a large impact on the observed probabilities


#%%
#
#S_O = int(input("What is the size threshold for a successful outbreak?  > S_O = "))
#S_O = 30.
S_O = summary_rep_parameters.loc[0, "pI_size_successful_outbreak"]
#R_c = int(input("How many runs for each initial city?  > Rc = "))
#Rc = 1000
Rc = summary_rep_parameters.loc[0, "pI_strU_eta_Nbr_runs"]

Prop_prOut_TGU = summary_rep_per_city["pI_strU_emp_eta"]

Mean_prOut_rep_TGU = summary_rep_per_bin["pI_strU_empB_eta"]
Std_prOut_rep_TGU = summary_rep_per_bin["pI_strU_stdB_emp_eta"] 
Min_prOut_rep_TGU = summary_rep_per_bin["pI_strU_minB_eta"]
Max_prOut_rep_TGU = summary_rep_per_bin["pI_strU_maxB_eta"]

plt.figure(4)
plt.clf()
plt.plot(MidBins, etaRef_U, "ks-", label = "analytical")
plt.plot(MidBins, Mean_prOut_rep_TGU, "bD--", label = "empirical (x10)")
plt.plot(MidBins, Mean_prOut_rep_TGU + Std_prOut_rep_TGU, "^r:", label="std (x10)")
plt.plot(MidBins, Mean_prOut_rep_TGU - Std_prOut_rep_TGU, "vr:")
plt.vlines(MidBins, \
          ymin = Min_prOut_rep_TGU,  ymax = Max_prOut_rep_TGU,\
         color= "purple", linestyle =":")
plt.plot(MidBins, Min_prOut_rep_TGU, "mv:")
plt.plot(MidBins, Max_prOut_rep_TGU, "m^:", label = "range (x10)")
plt.title("Empirical and theoretical outbreak probability, \n \
Fluctuations within the bin, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.ylim(0,1.02)
plt.legend(title = "{:s}, pI, strU, TG".format(abbrev))
plt.savefig("variation_empirical_prOutbreak_vs_size_v{:d}_pI_strU_TG_rep10.png".format(version))
#%%

Mean_prOut_TGU = summary_per_bin["pI_strU_empB_eta"]
Std_prOut_TGU = summary_per_bin["pI_strU_stdB_emp_eta"] 
Min_prOut_TGU = summary_per_bin["pI_strU_minB_eta"]
Max_prOut_TGU = summary_per_bin["pI_strU_maxB_eta"]

alpha_fig = 0.9
plt.figure(4)
plt.clf()
plt.plot(MidBins, etaRef_U, "ks-", label = "analytical")

plt.plot(MidBins, Mean_prOut_rep_TGU, "gD--", label = "empirical (x10)")
plt.plot(MidBins, Mean_prOut_rep_TGU + Std_prOut_rep_TGU, "^y:", alpha = alpha_fig, label="std (x10)")
plt.plot(MidBins, Mean_prOut_rep_TGU - Std_prOut_rep_TGU, "vy:", alpha = alpha_fig)
plt.vlines(MidBins, \
         ymin = Mean_prOut_rep_TGU - Std_prOut_rep_TGU,\
         ymax = Mean_prOut_rep_TGU + Std_prOut_rep_TGU,\
         color= "y", alpha = alpha_fig, linestyle =":")
    
plt.plot(MidBins, Mean_prOut_TGU, "bD--", label = "empirical")
plt.plot(MidBins, Mean_prOut_TGU + Std_prOut_TGU, "^r:", alpha = alpha_fig, label="std")
plt.plot(MidBins, Mean_prOut_TGU - Std_prOut_TGU, "vr:", alpha = alpha_fig)
plt.vlines(MidBins, \
         ymin = Mean_prOut_TGU - Std_prOut_TGU,\
         ymax = Mean_prOut_TGU + Std_prOut_TGU,\
         color= "r", alpha = alpha_fig, linestyle =":")

plt.title("Empirical and theoretical outbreak probability, \n \
Fluctuations within the bin, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.ylim(0,1.02)
plt.legend(title = "{:s}, pI, strU, TG".format(abbrev))
plt.savefig("variation_empirical_prOutbreak_vs_size_v{:d}_pI_strU_TG_Crep10.png".format(version))

#%%

PropOut_KGU = summary_rep_per_bin["pI_strU_empK_eta"]
    
plt.figure(4)
plt.clf()
plt.scatter(MidBins, etaRef_U, c="black", marker="+", label = "analytical relation")
plt.scatter(np.log10(summary_rep_per_bin['Lower_size']), PropOut_KGU, c="red", marker="+", label = "from simulation runs of epidemic")
plt.title("Empirical probability of triggering an outbreak as a function , \n \
of the size of first infected, data: {:s}, str. U, KG".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion of outbreaks")
plt.ylim(0,1)
plt.legend(title = "{:s}, pI, strU, KG".format(abbrev))
plt.savefig("empirical_pOutbreak_vs_size_v{:d}_pI_strU_K_rep10.png".format(version))


#%%
eta_P = summary_rep_per_city["pI_strP_thr_eta"]
etaRef_P = summary_rep_per_bin["pI_strP_thr_eta"]

plt.figure(5)
plt.clf()
plt.plot(np.log10(beta_rep), eta_P, "ks", markersize = 5, label = "analytical relation")
plt.title("Theoretical probability of triggering an outbreak as a function , \n \
of the size of first infected, data: {:s}, str (P), TG".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pI, strP, TG".format(abbrev))
plt.savefig("theoretical_pOutbreak_vs_size_v{:d}_pI_strP_TG_rep10.png".format(version))




#%%
#
#PropOut_TGP = summary_rep_per_city["pI_strP_emp_eta"]

Mean_prOut_rep_TGP = summary_rep_per_bin["pI_strP_empB_eta"] 
Std_prOut_rep_TGP = summary_rep_per_bin["pI_strP_stdB_emp_eta"]
Min_prOut_rep_TGP = summary_rep_per_bin["pI_strP_minB_eta"]
Max_prOut_rep_TGP = summary_rep_per_bin["pI_strP_maxB_eta"]

plt.figure(4)
plt.clf()
plt.plot(MidBins, etaRef_P, "ks-", label = "analytical")
plt.plot(MidBins, Mean_prOut_rep_TGP, "bD:", label = "empirical (x10)")
plt.plot(MidBins, Mean_prOut_rep_TGP + Std_prOut_rep_TGP, "^r:", label="std (x10)")
plt.plot(MidBins, Mean_prOut_rep_TGP - Std_prOut_rep_TGP, "vr:")
plt.vlines(MidBins, \
          ymin = Min_prOut_rep_TGP,  ymax = Max_prOut_rep_TGP,\
         color= "purple", linestyle =":")
plt.plot(MidBins, Min_prOut_rep_TGP, "mv:")
plt.plot(MidBins, Max_prOut_rep_TGP, "m^:", label = "range (x10)")
plt.title("Empirical and theoretical outbreak probability, \n \
Fluctuations within the bin, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.ylim(0,1.02)
plt.legend(title = "{:s}, pI, strP, TG".format(abbrev))
plt.savefig("variation_empirical_pOutbreak_vs_size_v{:d}_pI_strP_TG_rep10.png".format(version))

#%%
Mean_prOut_TGP = summary_per_bin["pI_strP_empB_eta"] 
Std_prOut_TGP = summary_per_bin["pI_strP_stdB_emp_eta"]
Min_prOut_TGP = summary_per_bin["pI_strP_minB_eta"]
Max_prOut_TGP = summary_per_bin["pI_strP_maxB_eta"]

plt.figure(4)
plt.clf()
plt.plot(MidBins, etaRef_P, "ks-", label = "analytical")

plt.plot(MidBins, Mean_prOut_rep_TGP, "gD--", label = "empirical (x10)")
plt.plot(MidBins, Mean_prOut_rep_TGP + Std_prOut_rep_TGP, "^y:", alpha = alpha_fig, label="std (x10)")
plt.plot(MidBins, Mean_prOut_rep_TGP - Std_prOut_rep_TGP, "vy:", alpha = alpha_fig)
plt.vlines(MidBins, \
         ymin = Mean_prOut_rep_TGP - Std_prOut_rep_TGP,\
         ymax = Mean_prOut_rep_TGP + Std_prOut_rep_TGP,\
         color= "y", alpha = alpha_fig, linestyle =":")
    
plt.plot(MidBins, Mean_prOut_TGP, "bD--", label = "empirical")
plt.plot(MidBins, Mean_prOut_TGP + Std_prOut_TGP, "^r:", alpha = alpha_fig, label="std")
plt.plot(MidBins, Mean_prOut_TGP - Std_prOut_TGP, "vr:", alpha = alpha_fig)
plt.vlines(MidBins, \
         ymin = Mean_prOut_TGP - Std_prOut_TGP,\
         ymax = Mean_prOut_TGP + Std_prOut_TGP,\
         color= "r", alpha = alpha_fig, linestyle =":")
    
plt.title("Empirical and theoretical outbreak probability, \n \
Fluctuations within the bin, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.ylim(0,1.02)
plt.legend(title = "{:s}, pI, strP, TG".format(abbrev))
plt.savefig("variation_empirical_pOutbreak_vs_size_v{:d}_pI_strP_TG_rep10.png".format(version))


#%%
#######################################################
#%%
#In this new version, least square estimation is exploited \
#instead of a simple interpolation between extremities

#PoT = float(input("Which threshold for the infected population size at quasi-equilibrium? \n >PoR = "))
#PoR = 30
PoR = summary_rep_parameters.loc[0, "pI_strU_TI_init_thres"]

TotE_avg = summary_rep_per_gen["pI_strU_TI_R0_emp"] 
TotE_std = summary_rep_per_gen["pI_strU_TI_R0_std"] 
N_o = TotE_avg-1
    
TotE_s = TotE_avg +TotE_std
TotE_ms = TotE_avg - TotE_std

rhoE = summary_rep_parameters.loc[0, "pI_strU_log10-R0_per_gen"]
print("Expected growth rate~{:.2e}".format(rhoE))

plt.figure(6)
plt.clf()
plt.plot(summary_rep_per_gen['Gen_after_thr'], TotE_avg, "bD--", label="empirical")
plt.plot(summary_rep_per_gen['Gen_after_thr'], \
   rhoE*np.ones(summary_rep_per_gen['Gen_after_thr'].size), c="black", label="expected")
plt.plot(summary_rep_per_gen['Gen_after_thr'], TotE_s, "r^:", label="+std")
plt.plot(summary_rep_per_gen['Gen_after_thr'], TotE_ms, "rv:", label="-std")
plt.vlines(summary_rep_per_gen['Gen_after_thr'], \
          ymin = summary_rep_per_gen["pI_strU_TI_R0_q5"],\
          ymax = summary_rep_per_gen["pI_strU_TI_R0_q95"],\
         color= "purple", linestyle =":")
plt.plot(summary_rep_per_gen['Gen_after_thr'], summary_rep_per_gen["pI_strU_TI_R0_q95"], "m^:")
plt.plot(summary_rep_per_gen['Gen_after_thr'], summary_rep_per_gen["pI_strU_TI_R0_q5"], "mv:", label = "5%-95%")
plt.title("Growth rate estimation, \n\
from the total number of infected, reference at {:.0f}, data: {:s} ".format(PoR, D_Name))
plt.xlabel("Number of generations in the time-interval")
plt.ylabel("Estimated growth rate")
plt.legend(title = "{:s}, pI, strU, TG".format(abbrev))
plt.ylim(-0.02*rhoE, 1.1*rhoE)
plt.savefig("growth_rate_R0_LSestimation_{:d}_v{:d}_pI_strU_TG_rep10.png".format(PoR, version))

#%%
#rescaled graph
plt.figure(7)
plt.clf()
plt.plot(summary_rep_per_gen['Gen_after_thr'], TotE_avg/rhoE, "bD--", label="empirical")
plt.plot(summary_rep_per_gen['Gen_after_thr'], \
         np.ones(summary_rep_per_gen['Gen_after_thr'].size), c="black", label="expected")
plt.plot(summary_rep_per_gen['Gen_after_thr'], TotE_s/rhoE, "r^:", label="+std")
plt.plot(summary_rep_per_gen['Gen_after_thr'], TotE_ms/rhoE, "rv:", label="-std")
plt.vlines(summary_rep_per_gen['Gen_after_thr'], \
          ymin = summary_rep_per_gen["pI_strU_TI_R0_q5"]/rhoE,\
          ymax = summary_rep_per_gen["pI_strU_TI_R0_q95"]/rhoE,\
         color= "purple", linestyle =":")
plt.plot(summary_rep_per_gen['Gen_after_thr'], summary_rep_per_gen["pI_strU_TI_R0_q95"]/rhoE, "m^:")
plt.plot(summary_rep_per_gen['Gen_after_thr'], summary_rep_per_gen["pI_strU_TI_R0_q5"]/rhoE, "mv:", label = "5%-95%")
plt.title("Growth rate estimation, \n\
from the total number of infected, reference at {:.0f}, data: {:s} ".format(PoR, D_Name))
plt.xlabel("Number of generations in the time-interval")
plt.ylabel("Estimated growth rate")
plt.legend(title = "{:s}, pI, strU, TG".format(abbrev))
plt.ylim(-0.02, 1.1)
plt.savefig("RS_growth_rate_TotE_estimation_{:d}_v{:d}_pI_strU_TG_rep10.png".format(PoR, version))


#%%
############################################################
#%%
Nr = 200
r_0test= np.linspace(1.01, 6, Nr)

KV0 = 1/RU

#for the backward dynamics
bKA_U0 = KV0 * beta_rep**(1+b)*np.sum(beta_rep**(a-1))/NC
bKB_U0 = KV0 * beta_rep**(a)*np.sum(beta_rep**(b))/NC

bnuA_U = beta_rep**(a-1)/np.sum(beta_rep**(a-1))
bnuB_U = beta_rep**(b)/np.sum(beta_rep**(b))

#%%
#Nr = 200
pi_AU= np.ones(Nr)
pi_BU= np.ones(Nr)
Inc_cit_U = np.zeros(Nr)
Inc_pop_U = np.zeros(Nr)
for i in np.arange(Nr):
    err = 1
    n_it = 0
    pi = 1- np.exp(- r_0test[i] *(bKA_U0 + bKB_U0) )
    while (err > 5E-5) * (n_it < 500):
        pi_AU[i]= np.sum(bnuA_U*pi)
        pi_BU[i]= np.sum(bnuB_U*pi)
        pi2 = 1-np.exp(- r_0test[i] *( bKA_U0 *pi_AU[i] +bKB_U0* pi_BU[i]))
        err = np.max(np.abs(1-pi/pi2))
        n_it = n_it +1
        pi = pi2
    Inc_cit_U[i] = 1/NC * np.sum(pi)
    Inc_pop_U[i] = 1/np.sum(beta_rep) * np.sum(beta_rep*pi)
    
#%%

Tr = 2/NC*np.sum(beta_rep**(1+a+b))
Det = (np.sum(beta_rep**(1+a+b))/NC)**2 - np.sum(beta_rep**(2*a))/NC * np.sum(beta_rep**(2+2*b))/NC
Delta = Tr**2 - 4*Det
RP = (Tr + np.sqrt(Delta))/2
pV0 = 1 / RP

bKA_P0 = pV0 * beta_rep**(1+b)*np.sum(beta_rep**a)/NC
bKB_P0 = pV0 * beta_rep**(a)*np.sum(beta_rep**(1+b))/NC

bnuA_P = beta_rep**(a)/np.sum(beta_rep**(a))
bnuB_P = beta_rep**(1+b)/np.sum(beta_rep**(1+b))
#%%
pi_AP= np.ones(Nr)
pi_BP= np.ones(Nr)
Inc_cit_P = np.zeros(Nr)
Inc_pop_P = np.zeros(Nr)
for i in np.arange(Nr):
    err = 1
    n_it = 0
    pi = 1- np.exp(- r_0test[i] *(bKA_P0 + bKB_P0) )
    while (err > 5E-5) * (n_it < 500):
        pi_AP[i]= np.sum(bnuA_P*pi)
        pi_BP[i]= np.sum(bnuB_P*pi)
        pi2 = 1-np.exp(- r_0test[i] *( bKA_P0 *pi_AP[i] +bKB_P0* pi_BP[i]))
        err = np.max(np.abs(1-pi/pi2))
        n_it = n_it +1
        pi = pi2
    Inc_cit_P[i] = 1/NC * np.sum(pi)
    Inc_pop_P[i] = 1/np.sum(beta_rep) * np.sum(beta_rep*pi)


#%%
plt.plot(r_0test, Inc_cit_U, 'r^', label = "str (U)")
plt.plot(r_0test, Inc_cit_P, 'b*', label = "str (P)")
plt.title("Final incidence as a function \n \
of the initial reproduction number, KB")
plt.xlabel("Initial reproduction number")
plt.ylabel("Mean proportion of infected cities")
plt.legend(title = "{:s}, pI, KB".format(abbrev))
plt.ylim(0, 1)
plt.savefig("final_inc_cit_ro_b_v{:d}_a{:.2f}_b{:.2f}_K_rep10.png".format(version, a, b))
#%%
plt.plot(r_0test, Inc_pop_U, 'r^', label = "str (U)")
plt.plot(r_0test, Inc_pop_P, 'b*', label = "str (P)")
plt.title("Final incidence of population as a function \n\
 of the initial reproduction number, KB")
plt.xlabel("Initial reproduction number")
plt.ylabel("Mean proportion of infected people")
plt.legend(title = "{:s}, pI, KB".format(abbrev))
plt.ylim(0, 1)
plt.savefig("final_inc_pop_ro_b_v{:d}_a{:.2f}_b{:.2f}_K_rep10.png".format(version, a, b))

#%%
plt.plot(Inc_pop_U, r_0test*KV0*Inc_cit_U*NC/(Inc_pop_U*np.sum(beta_rep)), 'r^', label = "str (U)")
plt.plot(Inc_pop_P, r_0test*pV0, 'b*', label = "str (P)")
plt.title("Scaled proportion of infected among quarantined \n \
as a function of the quarantined, KB")
plt.xlabel("Proportion of quarantined people")
plt.ylabel("Scaled proportion of infected among quarantined")
plt.legend(title = "{:s}, pI, KB".format(abbrev))
plt.xlim(0, np.max(Inc_pop_P))
plt.ylim(0, np.max(r_0test*pV0))
plt.savefig("final_inc_ratio_b_v{:d}_a{:.2f}_b{:.2f}_K_rep10.png".format(version, a, b))

#%%
plt.plot(r_0test*KV0*Inc_cit_U*NC, r_0test*KV0*Inc_cit_U*NC/(Inc_pop_U*np.sum(beta_rep)), 'r^', label = "str (U)")
plt.plot(r_0test*pV0*Inc_pop_P*np.sum(beta_rep), r_0test*pV0, 'b*', label = "str (P)")
plt.title("Scaled proportion of infected among quarantined \n \
as a function of the infected population, KB")
plt.xlabel("Scaled proportion of infected")
plt.ylabel("Scaled proportion of infected among quarantined")
plt.legend(title = "{:s}, pI, KB".format(abbrev))
plt.xlim(0, np.max(r_0test*pV0*Inc_pop_P*np.sum(beta_rep)))
plt.ylim(0, np.max(r_0test*pV0))
plt.savefig("final_inc_ratio2_b_v{:d}_a{:.2f}_b{:.2f}_K_rep10.png".format(version, a, b))

#%%
plt.plot(r_0test*KV0*Inc_cit_U*NC, Inc_pop_U, 'r^', label = "str (U)")
plt.plot(r_0test*pV0*Inc_pop_P*np.sum(beta_rep), Inc_pop_P, 'b*', label = "str (P)")
plt.title("Scaled proportion of infected among quarantined \n \
as a function of the infected population, KB")
plt.xlabel("Scaled proportion of infected, a={:.2f}, b= {:.2f}".format(a, b))
plt.ylabel("Scaled proportion of people quarantined")
plt.legend(title = "{:s}, pI, KB".format(abbrev))
Im= np.sum(Inc_pop_U < np.max(Inc_pop_P))
plt.xlim(0, r_0test[Im]*KV0*Inc_cit_U[Im]*NC)
plt.ylim(0, np.max(Inc_pop_P))
plt.savefig("final_quar_vs_infected_v{:d}_a{:.2f}_b{:.2f}_K_rep10.png".format(version, a, b))
#%%
#Nr = 200
pi_AU= np.ones(Nr)
pi_BU= np.ones(Nr)
Inc_cit_U = np.zeros(Nr)
Inc_pop_U = np.zeros(Nr)
for i in np.arange(Nr):
    err = 1
    n_it = 0
    pi = 1- np.exp(- r_0test[i] *bKA_U0)
    while (err > 5E-5) * (n_it < 500):
        pi_AU[i]= np.sum(bnuA_U*pi)
        pi_BU[i]= np.sum(bnuB_U*pi)
        pi2 = 1-np.exp(- r_0test[i] *( bKA_U0 *pi_AU[i] +bKB_U0* pi_BU[i]))
        err = np.max(np.abs(1-pi/pi2))
        n_it = n_it +1
        pi = pi2
    Inc_cit_U[i] = 1/NC * np.sum(pi)
    Inc_pop_U[i] = 1/np.sum(beta_rep) * np.sum(beta_rep*pi)
    
#%%

Tr = 2/NC*np.sum(beta_rep**(1+a+b))
Det = (np.sum(beta_rep**(1+a+b))/NC)**2 - np.sum(beta_rep**(2*a))/NC * np.sum(beta_rep**(2+2*b))/NC
Delta = Tr**2 - 4*Det
RP = (Tr + np.sqrt(Delta))/2
pV0 = 1 / RP

bKA_P0 = pV0 * beta_rep**(1+b)*np.sum(beta_rep**a)/NC
bKB_P0 = pV0 * beta_rep**(a)*np.sum(beta_rep**(1+b))/NC

bnuA_P = beta_rep**(a)/np.sum(beta_rep**(a))
bnuB_P = beta_rep**(1+b)/np.sum(beta_rep**(1+b))
#%%
pi_AP= np.ones(Nr)
pi_BP= np.ones(Nr)
Inc_cit_P = np.zeros(Nr)
Inc_pop_P = np.zeros(Nr)
for i in np.arange(Nr):
    err = 1
    n_it = 0
    pi = 1- np.exp(- r_0test[i] *(bKA_P0 + bKB_P0) )
    while (err > 5E-5) * (n_it < 500):
        pi_AP[i]= np.sum(bnuA_P*pi)
        pi_BP[i]= np.sum(bnuB_P*pi)
        pi2 = 1-np.exp(- r_0test[i] *( bKA_P0 *pi_AP[i] +bKB_P0* pi_BP[i]))
        err = np.max(np.abs(1-pi/pi2))
        n_it = n_it +1
        pi = pi2
    Inc_cit_P[i] = 1/NC * np.sum(pi)
    Inc_pop_P[i] = 1/np.sum(beta_rep) * np.sum(beta_rep*pi)


#%%
KA_U0 = KV0 * beta_rep**(b)*np.sum(beta_rep**a)/NC
KB_U0 = KV0 * beta_rep**(a-1)*np.sum(beta_rep**(1+b))/NC

KA_P0 = pV0 * beta_rep**(1+b)*np.sum(beta_rep**a)/NC
KB_P0 = pV0 * beta_rep**(a)*np.sum(beta_rep**(1+b))/NC

nuA = beta_rep**(a)/np.sum(beta_rep**(a))
nuB = beta_rep**(1+b)/np.sum(beta_rep**(1+b))

#%%
pi_AU= np.ones(Nr)
pi_BU= np.ones(Nr)
Poutbreak_cit_U = np.zeros(Nr)
Poutbreak_pop_U = np.zeros(Nr)
for i in np.arange(Nr):
    err = 1
    n_it = 0
    pi = 1- np.exp(- r_0test[i] *(KA_U0 + KB_U0) )
    while (err > 5E-5) * (n_it < 500):
        pi_AU[i]= np.sum(nuA*pi)
        pi_BU[i]= np.sum(nuB*pi)
        pi2 = 1-np.exp(- r_0test[i] *( KA_U0 *pi_AU[i] +KB_U0* pi_BU[i]))
        err = np.max(np.abs(1-pi/pi2))
        n_it = n_it +1
        pi = pi2
    Poutbreak_cit_U[i] = 1/NC * np.sum(pi)
    Poutbreak_pop_U[i] = 1/np.sum(beta_rep) * np.sum(beta_rep*pi)

#%%
pi_AP= np.ones(Nr)
pi_BP= np.ones(Nr)
Poutbreak_cit_P = np.zeros(Nr)
Poutbreak_pop_P = np.zeros(Nr)
for i in np.arange(Nr):
    err = 1
    n_it = 0
    pi = 1- np.exp(- r_0test[i] *(KA_P0 + KB_P0))
    while (err > 5E-5) * (n_it < 500):
        pi_AP[i]= np.sum(nuA*pi)
        pi_BP[i]= np.sum(nuB*pi)
        pi2 = 1-np.exp(- r_0test[i] *( KA_P0 *pi_AP[i] +KB_P0* pi_BP[i]))
        err = np.max(np.abs(1-pi/pi2))
        n_it = n_it +1
        pi = pi2
    Poutbreak_cit_P[i] = 1/NC * np.sum(pi)
    Poutbreak_pop_P[i] = 1/np.sum(beta_rep) * np.sum(beta_rep*pi)
    

#%%
plt.plot(r_0test, Poutbreak_cit_U, 'r^', label = "str (U)")
plt.plot(r_0test, Poutbreak_cit_P, 'b*', label = "str (P)")
plt.title("Theoretical outbreak probability\n \
from a uniformly chosen city, data: {:s}, pI, KB".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("outbreak probability")
plt.legend(title = "{:s}, pI, KB".format(abbrev))
plt.ylim(0, 1)
plt.savefig("final_Poutbreak_cit_ro_v{:d}_K_rep10.png".format(version))
#%%
plt.plot(r_0test, Poutbreak_pop_U, 'r^', label = "str (U)")
plt.plot(r_0test, Poutbreak_pop_P, 'b*', label = "str (P)")
plt.title("Theoretical outbreak probability\n \
from a uniformly chosen citizen, data: {:s}, pI, KB".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("outbreak probability")
plt.legend(title = "{:s}, pI, KB".format(abbrev))
plt.ylim(0, 1)
plt.savefig("final_Poutbreak_pop_ro_v{:d}_K_rep10.png".format(version))

#%%
R0_var = summary_rep_per_R0['R0']
bR = R0_var.size

plt.plot(R0_var, summary_rep_per_R0['pI_strP_Inf_cit'], 'b*', label = "Cities under isolation")
plt.plot(R0_var, summary_rep_per_R0['pI_strP_Inf_ppl'], 'r^', label = "People under isolation")
plt.title("Mean proportion of cities and people under isolation,\n \
data: {:s}, str (P), TG".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("Mean proportion")
plt.legend(title = "{:s}, strP, TG".format(abbrev))
plt.ylim(0, 1)
plt.savefig("Final_incidence_ro_v{:d}_strP_TG_rep10.png".format(version))

#%%
pi_AP= np.ones(Nr)
pi_BP= np.ones(Nr)
Inc_cit_P = np.zeros(bR)
Inc_pop_P = np.zeros(bR)
for i in np.arange(bR):
    err = 1
    n_it = 0
    pi = 1- np.exp(- R0_var[i] *(bKA_P0 + bKB_P0) )
    while (err > 5E-4) * (n_it < 500):
        pi_AP[i]= np.sum(bnuA_P*pi)
        pi_BP[i]= np.sum(bnuB_P*pi)
        pi2 = 1-np.exp(- R0_var[i] *( bKA_P0 *pi_AP[i] +bKB_P0* pi_BP[i]))
        err = np.max(np.abs(1-pi/pi2))
        n_it = n_it +1
        pi = pi2
    Inc_cit_P[i] = 1/NC * np.sum(pi)
    Inc_pop_P[i] = 1/np.sum(beta_rep) * np.sum(beta_rep*pi)




plt.plot(R0_var, Inc_cit_P, 'b*', label = "Cities under isolation")
plt.plot(R0_var, Inc_pop_P, 'r*', label = "People under isolation")
plt.title("Expected proportion of cities and people under isolation,\n \
data: {:s}, str (P), KB".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("Mean proportion")
plt.legend(title = "{:s}, strP, KB".format(abbrev))
plt.ylim(0, 1)
plt.savefig("Final_incidence_thr_ro_v{:d}_strP_K_rep10.png".format(version))
#%%
eta_AP= np.ones(Nr)
eta_BP= np.ones(Nr)
Prob_Out = np.zeros(bR)
for i in np.arange(bR):
    err = 1
    n_it = 0
    eta = 1- np.exp(- R0_var[i] *(KA_P0 + KB_P0) )
    while (err > 5E-5) * (n_it < 500):
        eta_AP[i]= np.sum(nuA*eta)
        eta_BP[i]= np.sum(nuB*eta)
        eta2 = 1-np.exp(- R0_var[i] *( KA_P0 *eta_AP[i] + KB_P0* eta_BP[i]))
        err = np.max(np.abs(1-eta/eta2))
        n_it = n_it +1
        eta = eta2
    Prob_Out[i] = np.sum(eta*nuA)



plt.plot(R0_var, summary_rep_per_R0['pI_strP_Inf_cit'], 'g*', label = "Cities under isolation, TG")
plt.plot(R0_var, summary_rep_per_R0['pI_strP_Inf_ppl'], 'm*', label = "People under isolation, TG")
plt.plot(R0_var, Prob_Out * Inc_cit_P, 'b+-', label = "Cities under isolation, KB")
plt.plot(R0_var, Prob_Out * Inc_pop_P, 'r+-', label = "People under isolation, KB")

plt.title("Mean proportion of cities and people under isolation,\n \
data: {:s}, str (P)".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("Mean proportion")
plt.legend(loc='upper left')
plt.ylim(0, 1)
plt.savefig("Final_incidence_Nc_v{:d}_strP_TG_rep10.png".format(version))

#%%
R0_var_2 = np.array(R0_var[R0_var > 2])
bR2 = R0_var_2.size

rho_AP= np.ones(Nr)
rho_BP= np.ones(Nr)
Inc_cit_P = np.zeros(bR2)
Inc_pop_P = np.zeros(bR2)
for i in np.arange(bR2):
    err = 1
    n_it = 0
    rho = 1- np.exp(- R0_var_2[i] *(bKA_P0 + bKB_P0) )
    while (err > 5E-5) * (n_it < 500):
        rho_AP[i]= np.sum(bnuA_P*rho)
        rho_BP[i]= np.sum(bnuB_P*rho)
        rho2 = 1-np.exp(- R0_var_2[i] *( bKA_P0 *rho_AP[i] +bKB_P0* rho_BP[i]))
        err = np.max(np.abs(1-rho/rho2))
        n_it = n_it +1
        rho = rho2
    Inc_cit_P[i] = 1/NC * np.sum(rho)
    Inc_pop_P[i] = 1/np.sum(beta_rep) * np.sum(beta_rep*rho)




plt.plot(R0_var_2, Inc_cit_P, 'b*', label = "Cities under isolation")
plt.plot(R0_var_2, Inc_pop_P, 'r*', label = "People under isolation")
plt.title("Expected proportion of cities and people under isolation,\n \
data: {:s}, str (P), KB, conditional on outbreak".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("Mean proportion")
plt.legend()
plt.ylim(0, 1)
plt.savefig("Final_incidence_thr_cO_v{:d}_pI_strP_K_rep10.png".format(version))
#%%

plt.plot(R0_var_2, summary_rep_per_R0.loc[R0_var > 2, 'pI_strP_cO_Inf_cit'], 'g*', label = "Cities under isolation, TG")
plt.plot(R0_var_2, summary_rep_per_R0.loc[R0_var > 2, 'pI_strP_cO_Inf_ppl'], 'm*', label = "People under isolation, TG")
plt.plot(R0_var_2, Inc_cit_P, 'b+-', label = "Cities under isolation, KB")
plt.plot(R0_var_2, Inc_pop_P, 'r+-', label = "People under isolation, KB")

plt.title("Mean proportion of cities and people under isolation,\n \
data: {:s}, str (P), conditional on outbreak".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("Mean proportion")
plt.legend(loc='upper left')
plt.ylim(0, 1)
plt.savefig("Final_incidence_cO_v{:d}_pI_strP_TG_rep10.png".format(version))

#%%
###########################################
#%%
