# -*- coding: utf-8 -*-
"""
Created on Sun Aug  7 21:09:11 2022

@author: alter
"""
#%%
from Rep10_Lockdown_infections import *
Is_collected = input("Has the data already been collected? (Y/N)\n\
>Is_collected = ")
if Is_collected == "Y":
    summary_rep_parameters = pd.read_csv("summary_rep_parameters_v{:d}.csv".format(version))
    summary_rep_per_city= pd.read_csv("summary_rep_per_city_v{:d}.csv".format(version))
    summary_rep_per_bin= pd.read_csv("summary_rep_per_bin_v{:d}.csv".format(version))
    summary_rep_per_gen= pd.read_csv("summary_rep_per_gen_v{:d}.csv".format(version))
    summary_rep_per_R0= pd.read_csv("summary_rep_per_R0_v{:d}.csv".format(version))


#%%
#evaluation of the threshold corresponding to a theoretical probability
# of getting infected at 1/2
#for the set of cities on which to base the reference
Ref_size = "1E5"
Lref = (beta_rep>10**(4.9))&(beta_rep<10**(5.1))
summary_rep_parameters["pI_ref_size"] = "1E5"

RU = 1/NC*(np.sum(beta_rep**(a+b)) + np.sqrt(np.sum(beta_rep**(2*a-1)) 
                                         * np.sum(beta_rep**(1+2*b))))
#threshold corresponding to R0 = 1 for the U strategy:
KV0 = 1/RU
KA_U0 = KV0 * beta_rep**(b)*np.sum(beta_rep**a)/NC
KB_U0 = KV0 * beta_rep**(a-1)*np.sum(beta_rep**(1+b))/NC


RP = 1/NC*(np.sum(beta_rep**(a+b+1)) + np.sqrt(np.sum(beta_rep**(2*a)) 
                                         * np.sum(beta_rep**(2+2*b))))
#threshold corresponding to R0 = 1 for the U strategy:
pV0 = 1/RP
KA_P0 = pV0 * beta_rep**(1+b)*np.sum(beta_rep**a)/NC
KB_P0 = pV0 * beta_rep**(a)*np.sum(beta_rep**(1+b))/NC

nuA = beta_rep**(a)/np.sum(beta_rep**(a))
nuB = beta_rep**(1+b)/np.sum(beta_rep**(1+b))

#for the backward dynamics
bKA_U0 = KV0 * beta_rep**(1+b)*np.sum(beta_rep**(a-1))/NC
bKB_U0 = KV0 * beta_rep**(a)*np.sum(beta_rep**(b))/NC

bnuA = beta_rep**(a-1)/np.sum(beta_rep**(a-1))
bnuB = beta_rep**(b)/np.sum(beta_rep**(b))

bKA_P0 = pV0 * beta_rep**(1+b)*np.sum(beta_rep**a)/NC
bKB_P0 = pV0 * beta_rep**(a)*np.sum(beta_rep**(1+b))/NC

bnuA_P = beta_rep**(a)/np.sum(beta_rep**(a))
bnuB_P = beta_rep**(1+b)/np.sum(beta_rep**(1+b))

#%%
Nr = 400
r_0test= np.linspace(1.01, summary_rep_parameters.loc[0, "Upper_R_pI_strU"], Nr)
eta_AU = np.ones(Nr)
eta_BU = np.ones(Nr)
pi_BU= np.ones(Nr)
rho_ref_U = np.zeros(Nr)
for i in np.arange(Nr):
    err = 1
    n_it = 0
    eta = 1- np.exp(- r_0test[i] *(bKA_U0 + bKB_U0) )
    while (err > 5E-5) * (n_it < 500):
        eta_AU[i]= np.sum(bnuA*eta)
        eta_BU[i]= np.sum(bnuB*eta)
        eta2 = 1-np.exp(- r_0test[i] *( bKA_U0 *eta_AU[i] +bKB_U0* eta_BU[i]))
        err = np.max(np.abs(1-eta/eta2))
        n_it = n_it +1
        eta = eta2
    rho_ref_U[i] = np.sum(eta[Lref])/np.sum(Lref)

RU_ref= r_0test[np.sum(rho_ref_U<0.5)-1]
KA_U = RU_ref/RU * beta_rep**(b)*np.sum(beta_rep**a)/NC
KB_U = RU_ref/RU * beta_rep**(a-1)*np.sum(beta_rep**(1+b))/NC
bKA_U = RU_ref/RU * beta_rep**(1+b)*np.sum(beta_rep**(a-1))/NC
bKB_U = RU_ref/RU * beta_rep**(a)*np.sum(beta_rep**(b))/NC

print("equivalent R_0^U~{:.3e}".format(RU_ref))

summary_rep_parameters["pI_strU_thr_R0"] = RU_ref
summary_rep_per_city["pI_strU_KA"] = KA_U
summary_rep_per_city["pI_strU_KB"] = KB_U
summary_rep_per_city["pI_strU_bKA"] = bKA_U
summary_rep_per_city["pI_strU_bKB"] = bKB_U

#%%
plt.clf()
plt.plot(r_0test, rho_ref_U, 'b*', label = "str (U)")
plt.title("Theoretical probability of being part of an outbreak\n under str (U) for different reproduction numbers")
plt.xlabel("Initial reproduction number")
plt.ylabel("Theoretical probability of a city of size size {:s}\n \
to get infected".format(Ref_size))
plt.legend(title = "Rep10 of {:s}".format(abbrev))
plt.ylim(0, 1)
plt.savefig("final_infected_vs_R0_ref_U_v{:d}.png".format(version))
#%%
r_0Ptest = np.linspace(1.01, summary_rep_parameters.loc[0,"Upper_R_pI_strP"], Nr)
eta_AP= np.ones(Nr)
eta_BP= np.ones(Nr)
rho_ref_P = np.zeros(Nr)
for i in np.arange(Nr):
    err = 1
    n_it = 0
    eta = 1- np.exp(- r_0Ptest[i] *(bKA_P0 + bKB_P0) )
    while (err > 5E-5) * (n_it < 500):
        eta_AP[i]= np.sum(bnuA_P*eta)
        eta_BP[i]= np.sum(bnuB_P*eta)
        eta2 = 1-np.exp(- r_0Ptest[i] *( bKA_P0 *eta_AP[i] +bKB_P0* eta_BP[i]))
        err = np.max(np.abs(1-eta/eta2))
        n_it = n_it +1
        eta = eta2
    rho_ref_P[i] = np.sum(eta[Lref])/np.sum(Lref)


RP_ref= r_0Ptest[np.sum(rho_ref_P<0.5)]
KA_P = RP_ref/RP * beta_rep**(1+b)*np.sum(beta_rep**a)/NC
KB_P = RP_ref/RP * beta_rep**(a)*np.sum(beta_rep**(1+b))/NC
bKA_P = RP_ref/RP * beta_rep**(1+b)*np.sum(beta_rep**a)/NC
bKB_P = RP_ref/RP * beta_rep**(a)*np.sum(beta_rep**(1+b))/NC
print("equivalent R_0^P~{:.3e}".format(RP_ref))

summary_rep_parameters["pI_strP_thr_R0"] = RP_ref
summary_rep_per_city["pI_strP_KA"] = KA_P
summary_rep_per_city["pI_strP_KB"] = KB_P
summary_rep_per_city["pI_strP_bKA"] = bKA_P
summary_rep_per_city["pI_strP_bKB"] = bKB_P

summary_rep_per_city.to_csv("summary_rep_per_city_v{:d}.csv".format(version))
summary_rep_parameters.to_csv("summary_rep_parameters_v{:d}.csv".format(version))
#%%
plt.clf()
plt.plot(r_0Ptest, rho_ref_P, 'r*', label = "str (P)")
plt.title("Theoretical probability of being part of an outbreak\n under str (P) for different reproduction numbers")
plt.xlabel("Initial reproduction number")
plt.ylabel("Theoretical probability of a city of size {:s}\n \
to get infected".format(Ref_size))
plt.legend(title = "Rep10 of {:s}".format(abbrev))
plt.ylim(0, 1)
plt.savefig("final_Pinfected_ref_P_v{:d}.png".format(version))
#%%