# -*- coding: utf-8 -*-
"""
Created on Sun Aug  7 21:09:11 2022

@author: alter
"""
#%%
from Lockdown_infections import *

#%% evaluation of the threshold corresponding to a theoretical probability
# of triggering an outbreak at 1/2
#for the set of cities on which to base the reference
Ref_size = 5.0
Lref = (beta>10**(4.9))&(beta<10**(5.1))
summary_parameters["pI_ref_size"] = "1E5"


RU = 1/NC*(np.sum(beta**(a+b)) + np.sqrt(np.sum(beta**(2*a-1)) 
                                         * np.sum(beta**(1+2*b))))
#threshold corresponding to R0 = 1 for the U strategy:
KV0 = 1/RU
KA_U0 = KV0 * beta**(b)*np.sum(beta**a)/NC
KB_U0 = KV0 * beta**(a-1)*np.sum(beta**(1+b))/NC


RP = 1/NC*(np.sum(beta**(a+b+1)) + np.sqrt(np.sum(beta**(2*a)) 
                                         * np.sum(beta**(2+2*b))))
#threshold corresponding to R0 = 1 for the U strategy:
pV0 = 1/RP
KA_P0 = pV0 * beta**(1+b)*np.sum(beta**a)/NC
KB_P0 = pV0 * beta**(a)*np.sum(beta**(1+b))/NC

nuA = beta**(a)/np.sum(beta**(a))
nuB = beta**(1+b)/np.sum(beta**(1+b))

#for the backward dynamics
bKA_U0 = KV0 * beta**(1+b)*np.sum(beta**(a-1))/NC
bKB_U0 = KV0 * beta**(a)*np.sum(beta**(b))/NC

bnuA_U = beta**(a-1)/np.sum(beta**(a-1))
bnuB_U = beta**(b)/np.sum(beta**(b))
#in previous codes, strU was not mentioned
bnuA = bnuA_U
bnuB = bnuB_U

bKA_P0 = pV0 * beta**(1+b)*np.sum(beta**a)/NC
bKB_P0 = pV0 * beta**(a)*np.sum(beta**(1+b))/NC

bnuA_P = beta**(a)/np.sum(beta**(a))
bnuB_P = beta**(1+b)/np.sum(beta**(1+b))

#%%
Nr = 200
r_0test= np.linspace(1.01, summary_parameters.loc[0, "Upper_R_pO_strU"], Nr)
eta_AU = np.ones(Nr)
eta_BU = np.ones(Nr)
eta_ref_U = np.zeros(Nr)
for i in np.arange(Nr):
    err = 1
    n_it = 0
    eta = 1- np.exp(- r_0test[i] *(KA_U0 + KB_U0) )
    while (err > 5E-5) * (n_it < 500):
        eta_AU[i]= np.sum(nuA*eta)
        eta_BU[i]= np.sum(nuB*eta)
        eta2 = 1-np.exp(- r_0test[i] *(KA_U0 *eta_AU[i] +KB_U0* eta_BU[i]))
        err = np.max(np.abs(1-eta/eta2))
        n_it = n_it +1
        eta = eta2
    eta_ref_U[i] = np.sum(eta[Lref])/np.sum(Lref)

RU_ref= r_0test[np.sum(eta_ref_U<0.5)-1]
KA_U = RU_ref/RU * beta**(b)*np.sum(beta**a)/NC
KB_U = RU_ref/RU * beta**(a-1)*np.sum(beta**(1+b))/NC
bKA_U = RU_ref/RU * beta**(1+b)*np.sum(beta**(a-1))/NC
bKB_U = RU_ref/RU * beta**(a)*np.sum(beta**(b))/NC

print("equivalent R_0^U~{:.3e}".format(RU_ref))
summary_parameters["pO_strU_thr_R0"] = RU_ref
summary_per_city["pO_strU_KA"] = KA_U
summary_per_city["pO_strU_KB"] = KB_U
summary_per_city["pO_strU_bKA"] = bKA_U
summary_per_city["pO_strU_bKB"] = bKB_U
#%%
plt.clf()
plt.plot(r_0test, eta_ref_U, 'b*', label = "str (U)")
plt.title("Theoretical probability of a unit triggering an outbreak\n under str (U) for different reproduction numbers")
plt.xlabel("Initial reproduction number")
plt.ylabel("Theoretical probability of a city of size 10^({:.1f})\n \
to trigger an infection".format(5.0))
plt.legend()
plt.ylim(0, 1)
plt.savefig("final_outbreak_vs_R0_ref_U_v{:d}.png".format(version))
#%%
r_0Ptest = np.linspace(1.01, summary_parameters.loc[0, "Upper_R_pO_strP"], Nr)
eta_AP= np.ones(Nr)
eta_BP= np.ones(Nr)
eta_ref_P = np.zeros(Nr)
for i in np.arange(Nr):
    err = 1
    n_it = 0
    eta = 1- np.exp(- r_0Ptest[i] *(KA_P0 + KB_P0) )
    while (err > 5E-5) * (n_it < 500):
        eta_AP[i]= np.sum(nuA*eta)
        eta_BP[i]= np.sum(nuB*eta)
        eta2 = 1-np.exp(- r_0Ptest[i] *(KA_P0 *eta_AP[i] +KB_P0* eta_BP[i]))
        err = np.max(np.abs(1-eta/eta2))
        n_it = n_it +1
        eta = eta2
    eta_ref_P[i] = np.sum(eta[Lref])/np.sum(Lref)


RP_ref= r_0Ptest[np.sum(eta_ref_P<0.5)-1]
KA_P = RP_ref/RP * beta**(1+b)*np.sum(beta**a)/NC
KB_P = RP_ref/RP * beta**(a)*np.sum(beta**(1+b))/NC
bKA_P = RP_ref/RP * beta**(1+b)*np.sum(beta**a)/NC
bKB_P = RP_ref/RP * beta**(a)*np.sum(beta**(1+b))/NC
print("equivalent R_0^P~{:.3e}".format(RP_ref))
summary_parameters["pO_strP_thr_R0"] = RP_ref
summary_per_city["pO_strP_KA"] = KA_P
summary_per_city["pO_strP_KB"] = KB_P
summary_per_city["pO_strP_bKA"] = bKA_P
summary_per_city["pO_strP_bKB"] = bKB_P
#%%
plt.clf()
plt.plot(r_0Ptest, eta_ref_P, 'r*', label = "str (P)")
plt.title("Theoretical probability of a unit triggering an outbreak\n under str (P) for different reproduction numbers")
plt.xlabel("Initial reproduction number")
plt.ylabel("Theoretical probability of a city of 10^({:.1f})\n \
to trigger an infection".format(Ref_size))
plt.legend()
plt.ylim(0, 1)
plt.savefig("final_outbreak_vs_R0_ref_P_v{:d}.png".format(version))
#%%
summary_parameters.to_csv("summary_parameters_v{:d}.csv".format(version))
summary_per_city.to_csv("summary_per_city_v{:d}.csv".format(version))
#%%
