# -*- coding: utf-8 -*-
"""
Created on Tue Feb 15 10:50:24 2022

@author: alter
"""
#%% Initialisation

#%%% Import 
#Analysis for variant U of the model
from Lockdown_infections import *

#from threshold_through_pInfected, deduce the values of R0 for the reference infection
from threshold_through_pInfected import *

import os.path
dir_n = 'TG_PI'
if not os.path.isdir(dir_n): os.makedirs(dir_n)
dir_path = os.path.join(os.getcwd(),dir_n)
plt.rcParams["savefig.directory"] = dir_path

#%%% Test run str U

beta1 = beta.reshape((NC, 1))
beta2 = beta.reshape((1, NC))
#sum of contributions to the kernel, the diagonal being removed
Sb = np.sum(beta1**(1+b)*beta2**a)-np.sum(beta**(1+b+a))
KM = RU_ref/(RU * NC) * Sb / MCom.sum() 
# KV = RU_ref/RU
# KM = KV/MCom.sum() * Sb / NC
print("k_M.K_vee~{:.3e}".format(KM))
print("equivalent R_0~{:.3e}".format(RU_ref))

MERi = MCom
MERe = MCom.transpose()
MER = KM*(MERi + MERe)
for j in np.arange(NC):
    MER[j] = MER[j]/beta[j]
MEr = MER.toarray()

Tf2, IAf2, Rf2, Shf2, N_it2 = InfectionRM(10000, 30, MEr, nuA, TL_U)
print("Number of failed attempts =", N_it2-1)

#%%% Test run str P

beta1 = beta.reshape((NC, 1))
beta2 = beta.reshape((1, NC))
pM = RP_ref/(RP * NC) * Sb / MCom.sum() 
# KV = RU_ref/RU
# KM = KV/MCom.sum() * Sb / NC
print("p_M.K_vee~{:.3e}".format(pM))
print("equivalent R_0~{:.3e}".format(RP_ref))
        

MERi = MCom
MERe = MCom.transpose()
MER = pM*(MERi + MERe)
MErP = MER.toarray()

Tf2, IAf2, Rf2, Shf2, N_it2 = InfectionRM(10000, 30, MErP, nuA, TL_P)
print("Number of failed attempts =", N_it2-1)

#%% Infection and outbreak probabilities

#%%% Parameters to be fixed
#t_o = TL_U[0]*float(input("Up to how many reference generations does the epidemic spread?  > t_o/TL_U = "))
t_o = TL_U[0]*100

#reference time adjusted just a bit higher than a fraction of the generation time for the "U" model
#N_d = int(input("Which precision in the time-discretisation (Dt = TL_U/N_d) for aggregation?  > N_d = "))
N_d = 5
t_R = TL_U[0]*(1/N_d + 1e-9)

#N_p = int(input("How many runs do you wish to perform?  > N_p = "))
N_p = 1000  #200 suggested
summary_parameters["pI_Nbr_runs"] = N_p
#N_I = int(input("What is the size threshold under which simulations are restarted?  > N_I = "))
N_I = 5
summary_parameters["pI_min_Size_restart"] = N_I

Gt = np.arange(1+1e-8, 1+t_o, t_R)
Nt = Gt.size
Gtb = Gt.reshape((Nt, 1))
Rt = np.zeros((N_p, Nt))
It = np.zeros((N_p, Nt))


PrI = np.zeros((N_p, Counts_ref.size))
Is_I = np.zeros((N_p, beta.size), dtype = bool)
#%%% Computation of PrI
N_fl = 0
for k in np.arange(N_p):
    Tf, IAf, Rf, Shf, N_it2 = InfectionRM(1+t_o, N_I, MEr, nuA, TL_U)
    #failure update
    N_fl = N_fl + N_it2/N_p
    #Is_I : who has been infected eventually?
    Is_I[k] = (Tf!= 0)
    #update for the total number of infected 
    T1 = Tf[Rf]
    T1 = T1.reshape((1, np.size(T1)))
    Rt[k] = np.sum(T1 < Gtb, axis = 1)
    #update for the number of infectious
    T2 = Tf[Rf] + TL_U[Rf]
    T2 = np.reshape(T2, (1, np.size(T2)))
    It[k] = np.sum((T1 < Gtb)*(Gtb < T2), axis = 1)
    #proportion of infected cities as a function of their size
    counts, bins, bars = plt.hist(np.log10(beta[Tf==0]), bins = Sep)
    plt.clf()
    PrI[k] = 1- counts/Counts_ref
    
    
print("Runs of simulations completed.")
print("Averaged number of failures observed : N_fl~{:.2e}".format(N_fl-1) )  
summary_parameters['pI_strU_N_fl'] = N_fl-1

#%%% Computation of PrIP
#Now for the case of the P strategy
RtP = np.zeros((N_p, Nt))
ItP = np.zeros((N_p, Nt))
PrIP = np.zeros((N_p, Counts_ref.size))
Is_IP = np.zeros((N_p, beta.size), dtype = bool)
N_flP = 0
for k in np.arange(N_p):
    TfP, IAfP, RfP, ShfP, N_itP2 = InfectionRM(1+t_o, N_I, MErP, nuA, TL_P)
    #failure update
    N_flP = N_flP + N_itP2/N_p
    #Is_IP : who has been infected eventually?
    Is_IP[k] = (TfP!= 0)
    #update for the total number of infected 
    T1P = TfP[RfP]
    T1P = T1P.reshape((1, np.size(T1P)))
    RtP[k] = np.sum(T1P < Gtb, axis = 1)
    #update for the number of infectious
    T2P = TfP[RfP] + TL_P[RfP]
    T2P = np.reshape(T2P, (1, np.size(T2P)))
    ItP[k] = np.sum((T1P < Gtb)*(Gtb < T2P), axis = 1)
    #proportion of infected cities as a function of their size
    counts, bins, bars = plt.hist(np.log10(beta[TfP==0]), bins = Sep)
    plt.clf()
    PrIP[k] = 1- counts/Counts_ref
    
    
print("Runs of simulations completed.")
print("Averaged number of failures observed : N_fl~{:.2e}".format(N_flP-1) )  
summary_parameters['pI_strP_N_fl'] = N_flP-1

#%%% Plots for infected

#%%%% Plot number_infected_perG_{:d}_v{:d}_pI_strU_TG
print("Results from the last run")
t_f = np.max(Tf[Rf])
Gt2 = Gt[Gt < t_f]
Rtf = Rt[-1, Gt < t_f]

plt.figure(1)
plt.clf()
plt.plot(Gt2[1:], np.log10(Rtf[1:]))
rhoE = np.log10(RU_ref)/TL_U[0]
print("Expected growth rate~{:.2e} for power 10".format(rhoE))
summary_parameters['pI_strU_exp_growth_rate'] = rhoE

t_I = np.sum((Rtf < 20))
Lin = np.log10(Rtf[t_I]) + rhoE*(Gt2-Gt2[t_I])
t_M = np.sum((Lin < np.log10(Rtf[-1])))
t_m = np.sum(Lin < 0)
plt.plot(Gt2[t_m:t_M], Lin[t_m:t_M], c ="purple")
plt.xlabel("Number of generations (since the start of infection)")
plt.ylabel("Total number of infected ({:s} scale)".format(r'$log_{10}$'))
plt.title("Progression of the number of infected\n \
data: {:s}".format(D_Name))
plt.legend(title = "{:s}, pI, strU, TG".format(abbrev))
plt.savefig(dir_path+"/slope_adjustment_{:d}_v{:d}_TG.png".format(20, version))

plt.figure(2)
plt.clf()
plt.plot(Gt2[1:]/TL_U[0], Rtf[1:])
plt.plot(Gt2[t_m:t_M]/TL_U[0], 10**(Lin[t_m:t_M]), c ="purple")
plt.xlabel("Number of generations (since the start of infection)")
plt.ylabel("Total number of infected")
plt.title("Progression of the number of infected\n \
data: {:s}".format(D_Name))
plt.legend(title = "{:s}, pI, strU, TG".format(abbrev))
plt.savefig(dir_path+"/number_infected_perG_{:d}_v{:d}_pI_strU_TG.png".format(20, version))

#%%%% Plot number_infected_perG_{:d}_v{:d}_pI_strP_TG
print("Results from the last run")
t_f = np.max(TfP[RfP])
Gt2 = Gt[Gt < t_f]
RtfP = RtP[-1, Gt < t_f]

plt.figure(1)
plt.clf()
plt.plot(Gt2[1:], np.log10(RtfP[1:]))
plt.xlabel("Number of generations (since the start of infection)")
plt.ylabel("Total number of infected ({:s} scale)".format(r'$log_{10}$'))
plt.title("Progression of the number of infected\n \
data: {:s}".format(D_Name))
plt.legend(title = "{:s}, pI, strP, TG".format(abbrev))
plt.savefig(dir_path+"/slope_adjustment_{:d}_v{:d}_TG_pI_strP.png".format(20, version))

plt.figure(2)
plt.clf()
plt.plot(Gt2[1:]/TL_P[0], RtfP[1:])
plt.xlabel("Number of generations")
plt.ylabel("Total number of infected")
plt.title("Progression of the number of infected\n \
data: {:s}".format(D_Name))
plt.legend(title = "{:s}, pI, strU, TG".format(abbrev))
plt.savefig(dir_path+"/number_infected_perG_{:d}_v{:d}_pI_strP_TG.png".format(20, version))

#%%%% Computation of pi_U and piRef_U
err = 1
n_it = 0
pi_U = 1- np.exp(- bKA_U - bKB_U)
while (err > 5E-5) * (n_it < 500):
    pi2 = 1-np.exp(- bKA_U * np.sum(bnuA_U*pi_U) - bKB_U* np.sum(bnuB_U*pi_U))
    err = np.max(np.abs(1-pi_U/pi2))
    n_it = n_it +1
    pi_U = pi2
    
Sort = np.argsort(beta)
RefV = Sort[Bins]
piRef_U = (pi_U[RefV[1:]] + pi_U[RefV[:-1]])/2
summary_per_city["pI_strU_thr_pi"] = pi_U
summary_per_bin["pI_strU_thr_pi"] = piRef_U

#%%%% Plot theoretical_Infected_vs_size_v{:d}_pI_strU_TG

plt.figure(5)
plt.clf()
plt.scatter(np.log10(beta), pi_U, c="black", marker="+", label = "analytical")
plt.title("Theoretical infection probability, \n \
data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pI, strU, KB".format(abbrev))
plt.ylim(0, 1)
plt.savefig(dir_path+"/theoretical_Infected_vs_size_v{:d}_pI_strU_TG.png".format(version))
summary_per_city["pI_strU_thr_pi"] = pi_U
summary_per_bin["pI_strU_thr_pi"] = piRef_U

#%%%% Plot Error_I_vs_size_v{:d}_U_KB

n_it = 0
pi = 1- np.exp(- bKA_U - bKB_U)
pi_AU = np.ones(15)
pi_BU = np.ones(15)
for i in np.arange(15):
    pi_AU[i] = np.sum(bnuA_U*pi)
    pi_BU[i] = np.sum(bnuB_U*pi)
    pi = 1-np.exp(- bKA_U * pi_AU[i]  - bKB_U* pi_BU[i])
    n_it = n_it +1

plt.plot(np.arange(14), np.log10(np.abs(pi_AU[:-1]/pi_AU[-1] -1)), c="blue", marker="*", label = "pi_C")
plt.plot(np.arange(14), np.log10(np.abs(pi_BU[:-1]/pi_BU[-1] -1)), c="red", marker="+", label = "pi_D")



plt.xlabel("Iteration")
plt.ylabel("Discrepancy  ({:s} scale)".format(r'$log_{10}$'))
plt.title("Infection probability: error in the estimation\n \
data: {:s}".format(D_Name))
plt.legend(title = "{:s}, pI, strU, KB".format(abbrev))
plt.savefig(dir_path+"/Error_I_vs_size_v{:d}_U_KB.png".format(version))


#%%%% Plot Infected_vs_size_v{:d}_pI_strU_TG

PrI_avg = np.sum(PrI, axis =0)/N_p
summary_per_bin["pI_strU_emp_pi"] = PrI_avg

PrI_var = np.sum(PrI**2, axis =0)/N_p - PrI_avg**2
summary_per_bin["pI_strU_std_emp_pi"] = np.sqrt(PrI_var)

PrI_min = np.min(PrI, axis =0)
summary_per_bin["pI_strU_min_pi"] = PrI_min

PrI_max = np.max(PrI, axis =0)
summary_per_bin["pI_strU_max_pi"] = PrI_max


PrI_sort = np.sort(PrI, axis =0)

PrI_s = PrI_avg + np.sqrt(PrI_var)
PrI_ms = PrI_avg - np.sqrt(PrI_var)

plt.figure(3)
plt.clf()
plt.plot(MidBins, piRef_U, "ks--", label = "analytical")
plt.plot(MidBins, PrI_avg, "bD", label="simulated")
plt.plot(MidBins, PrI_s, "^r:", label="std")
plt.plot(MidBins, PrI_ms, "vr:")
plt.vlines(MidBins, \
          ymin = PrI_min,  ymax = PrI_max,\
         color= "purple", linestyle =":")
plt.plot(MidBins, PrI_min, "mv:")
plt.plot(MidBins, PrI_max, "m^:", label = "range")
plt.title("Empirical and theoretical infection probability,\n \
fluctuations between the runs, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pI, strU, TG".format(abbrev))
plt.ylim(-0.02, 1.02)

plt.savefig(dir_path+"/Infected_vs_size_v{:d}_pI_strU_TG.png".format(version))

#%%%% Plot variation_infected_vs_size_v{:d}_pI_strU_TG

Is_I_avg = np.sum(Is_I, axis =0)/N_p
summary_per_city["pI_strU_emp_pi"] = Is_I_avg

Is_I_var = np.sum(Is_I**2, axis =0)/N_p - Is_I_avg**2
summary_per_city["pI_strU_std_emp_pi"] = np.sqrt(Is_I_var)

Prop2I_avg = np.zeros(MidBins.size)
Prop2I_var = np.zeros(MidBins.size)
Prop2I_min = np.zeros(MidBins.size)
Prop2I_max = np.zeros(MidBins.size)
for i in np.arange(MidBins.size):
    Is_bin = (np.log10(beta)>=Bins_ref[i])& (np.log10(beta)<Bins_ref[i+1])
    Prop2I_avg[i] = np.mean(Is_I_avg[Is_bin])
    Prop2I_var[i] = np.var(Is_I_avg[Is_bin])
    Prop2I_min[i] = np.min(Is_I_avg[Is_bin])
    Prop2I_max[i] = np.max(Is_I_avg[Is_bin])
summary_per_bin["pI_strU_empB_pi"] = Prop2I_avg
summary_per_bin["pI_strU_stdB_emp_pi"] = np.sqrt(Prop2I_var)
summary_per_bin["pI_strU_minB_pi"] = Prop2I_min
summary_per_bin["pI_strU_maxB_pi"] = Prop2I_max


Prop2I_s = Prop2I_avg + np.sqrt(Prop2I_var)
Prop2I_ms = Prop2I_avg - np.sqrt(Prop2I_var)


R2 = 1-np.var(Is_I_avg - pi)/np.var(Is_I_avg)
summary_parameters["pI_strU_infP_R2"] = R2

plt.figure(4)
plt.clf()
plt.plot(MidBins, piRef_U, "ks--", label = "analytical")
plt.plot(MidBins, Prop2I_avg, "bD", label="simulated")
plt.plot(MidBins, Prop2I_s, "^r:", label="std")
plt.plot(MidBins, Prop2I_ms, "vr:")
plt.vlines(MidBins, \
          ymin = Prop2I_min,  ymax = Prop2I_max,\
         color= "purple", linestyle =":")
plt.plot(MidBins, Prop2I_min, "mv:")
plt.plot(MidBins, Prop2I_max, "m^:", label = "range")
plt.title("Empirical and theoretical infection probability,\n \
fluctuations within the bin, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pI, strU, TG\n\
R2 = {:.2f}".format(abbrev, summary_parameters.loc[0, "pI_strU_infP_R2"]))
plt.ylim(-0.02, 1.02)


plt.savefig(dir_path+"/variation_infected_vs_size_v{:d}_pI_strU_TG.png".format(version))


#%%%% Computation of pi_P and piRef_P
err = 1
n_it = 0
pi_P = 1- np.exp(- bKA_U - bKB_U)
while (err > 5E-5) * (n_it < 500):
    pi2 = 1-np.exp(- bKA_P * np.sum(bnuA_P*pi_P) - bKB_P* np.sum(bnuB_P*pi_P))
    err = np.max(np.abs(1-pi_P/pi2))
    n_it = n_it +1
    pi_P = pi2
    
Sort = np.argsort(beta)
RefV = Sort[Bins]
piRef_P = (pi[RefV[1:]] + pi[RefV[:-1]])/2
summary_per_city["pI_strP_thr_pi"] = pi_P
summary_per_bin["pI_strP_thr_pi"] = piRef_P

#%%%% Plot Infected_vs_size_v{:d}_pI_strP_TG

PrIP_avg = np.sum(PrIP, axis =0)/N_p
summary_per_bin["pI_strP_emp_pi"] = PrIP_avg

PrIP_var = np.sum(PrIP**2, axis =0)/N_p - PrIP_avg**2
summary_per_bin["pI_strP_std_emp_pi"] = np.sqrt(PrIP_var)

PrIP_min = np.min(PrIP, axis =0)
summary_per_bin["pI_strP_min_pi"] = PrIP_min

PrIP_max = np.max(PrIP, axis =0)
summary_per_bin["pI_strP_max_pi"] = PrIP_max


PrIP_sort = np.sort(PrIP, axis =0)

PrIP_2s = PrIP_avg + np.sqrt(PrIP_var)
PrIP_m2s = PrIP_avg - np.sqrt(PrIP_var)

plt.figure(3)
plt.clf()
plt.plot(MidBins, piRef_P, "ks--", label = "analytical")
plt.plot(MidBins, PrIP_avg, "bD", label="simulated")
plt.plot(MidBins, PrIP_2s, "^r:", label="std")
plt.plot(MidBins, PrIP_m2s, "vr:")
plt.vlines(MidBins, \
          ymin = PrIP_min,  ymax = PrIP_max,\
         color= "purple", linestyle =":")
plt.plot(MidBins, PrIP_min, "mv:")
plt.plot(MidBins, PrIP_max, "m^:", label = "range")
plt.title("Empirical and theoretical infection probability,\n \
fluctuations between the runs, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pI, strP, TG".format(abbrev))
plt.ylim(0, 1.02)

plt.savefig(dir_path+"/Infected_vs_size_v{:d}_pI_strP_TG.png".format(version))

#%%%% Plot variation_infected_vs_size_v{:d}_pI_strP_TG

Is_IP_avg = np.sum(Is_IP, axis =0)/N_p
summary_per_city["pI_strP_emp_pi"] = Is_IP_avg

Is_IP_var = np.sum(Is_IP**2, axis =0)/N_p - Is_IP_avg**2
summary_per_city["pI_strP_std_emp_pi"] = np.sqrt(Is_IP_var)

Prop2IP_avg = np.zeros(MidBins.size)
Prop2IP_var = np.zeros(MidBins.size)
Prop2IP_min = np.zeros(MidBins.size)
Prop2IP_max = np.zeros(MidBins.size)
for i in np.arange(MidBins.size):
    Is_bin = (np.log10(beta)>=Bins_ref[i])& (np.log10(beta)<Bins_ref[i+1])
    Prop2IP_avg[i] = np.mean(Is_IP_avg[Is_bin])
    Prop2IP_var[i] = np.var(Is_IP_avg[Is_bin])
    Prop2IP_min[i] = np.min(Is_IP_avg[Is_bin])
    Prop2IP_max[i] = np.max(Is_IP_avg[Is_bin])
summary_per_bin["pI_strP_empB_pi"] = Prop2IP_avg
summary_per_bin["pI_strP_stdB_emp_pi"] = np.sqrt(Prop2IP_var)
summary_per_bin["pI_strP_minB_pi"] = Prop2IP_min
summary_per_bin["pI_strP_maxB_pi"] = Prop2IP_max


Prop2IP_s = Prop2IP_avg + np.sqrt(Prop2IP_var)
Prop2IP_ms = Prop2IP_avg - np.sqrt(Prop2IP_var)


R2 = 1-np.var(Is_IP_avg - pi)/np.var(Is_IP_avg)
summary_parameters["pI_strP_infP_R2"] = R2

plt.figure(4)
plt.clf()
plt.plot(MidBins, piRef_P, "ks--", label = "analytical")
plt.plot(MidBins, Prop2IP_avg, "bD", label="simulated")
plt.plot(MidBins, Prop2IP_s, "^r:", label="std")
plt.plot(MidBins, Prop2IP_ms, "vr:")
plt.vlines(MidBins, \
          ymin = Prop2IP_min,  ymax = Prop2IP_max,\
         color= "purple", linestyle =":")
plt.plot(MidBins, Prop2IP_min, "mv:")
plt.plot(MidBins, Prop2IP_max, "m^:", label = "range")
plt.title("Empirical and theoretical infection probability,\n \
fluctuations within the bin, data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pI, strP, TG\n\
R^2 = {:.2f}".format(abbrev, summary_parameters.loc[0, "pI_strP_infP_R2"]))
plt.ylim(0, 1.02)


plt.savefig(dir_path+"/variation_infected_vs_size_v{:d}_pI_strP_TG.png".format(version))

#%%% Plots for PrOutbreak

#%%%% Plot theoretical_PrOutbreak_vs_size_v{:d}_TG

err = 1
n_it = 0
eta_U = 1- np.exp(- KA_U - KB_U)
while (err > 5E-5) * (n_it < 500):
    eta_U2 = 1-np.exp(- KA_U * np.sum(nuA*eta_U) - KB_U* np.sum(nuB*eta_U))
    err = np.max(np.abs(1-eta_U/eta_U2))
    n_it = n_it +1
    eta_U = eta_U2
    
Sort = np.argsort(beta)
RefV = Sort[Bins]
etaRef_U = (eta_U[RefV[1:]] + eta_U[RefV[:-1]])/2
summary_per_city["pI_strU_thr_eta"] = eta_U
summary_per_bin["pI_strU_thr_eta"] = etaRef_U


plt.figure(5)
plt.clf()
plt.plot(np.log10(beta), eta_U, "ks", markersize = 5, label = "analytical")
plt.title("Theoretical outbreak probability, \n \
data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pI, strU, KB".format(abbrev))
plt.savefig(dir_path+"/theoretical_PrOutbreak_vs_size_v{:d}_KB.png".format(version))


#%%%% Plot Error_probO_v{:d}_strU_KB

Err = 1
n_it = 0
eta = 1- np.exp(- KA_U - KB_U)
eta_AU = np.ones(15)
eta_BU = np.ones(15)
for i in np.arange(15):
    eta_AU[i] = np.sum(nuA*eta)
    eta_BU[i] = np.sum(nuB*eta)
    eta = 1-np.exp(- KA_U * eta_AU[i]  - KB_U* eta_BU[i])
    n_it = n_it +1

plt.plot(np.arange(14), np.log10(np.abs(eta_AU[:-1]/eta_AU[-1] -1)), c="blue", marker="*", label = "eta_A")
plt.plot(np.arange(14), np.log10(np.abs(eta_BU[:-1]/eta_BU[-1] -1)), c="red", marker="+", label = "eta_B")


plt.xlabel("Iteration")
plt.ylabel("Discrepancy ($\log_{10}$-scale)")
plt.title("Outbreak probability: error in the estimation\n \
data: {:s}".format(D_Name))
plt.legend(title = "{:s}, pI, strU, KB".format(abbrev))
plt.savefig(dir_path+"/Error_probO_v{:d}_strU_KB.png".format(version))

#%%%% Plot empirical_prOutbreak_vs_size_v{:d}_pI_strU_TG

Isort = np.argsort(beta)
PrOut = np.zeros(Bins.size-1)
for ir in np.arange(Bins.size -1):
    iA = Isort[np.arange(Bins[ir], Bins[ir+1])]
    #N_O = 0
    #S_O = int(input("What is the size threshold for a successful outbreak?  > S_O = "))
    S_O = 30.
    #R_c = int(input("How many runs for each initial city?  > Rc = "))
    Rc = 300
    Success = np.zeros((Rc), dtype = bool)
    for k in np.arange(Rc):
        IA = np.zeros((NC), dtype = float)
        IA[iA] = 1
        IA = IA/np.sum(IA)
        Tf, IAf, Rf, Shf, Ni = InfectionRM(1+15*TL_U[0], 1, MEr, IA, TL_U)
        Success[k] = ( np.sum(Rf) > S_O)
    #PrOut[ir] = N_O/Rc
    PrOut[ir] = np.sum(Success)/Rc
    
summary_per_bin["pI_strU_emp1_eta"] = PrOut

plt.figure(4)
plt.clf()
plt.scatter(MidBins, etaRef_U, c="black", marker="+", label = "analytical")
plt.scatter(np.log10(beta[Isort[Bins]][:-1]), PrOut, c="red", marker="+", label = "simulated")
plt.title("Empirical and theoretical outbreak probability, \n \
data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.ylim(0,1.02)
plt.legend(title = "{:s}, pI, strU, TG".format(abbrev))
plt.savefig(dir_path+"/empirical_prOutbreak_vs_size_v{:d}_pI_strU_TG.png".format(version))

#It appears that the sizes of outbreaks are so small 
#that the threshold on the size makes a large impact on the observed probabilities


#%%%% Plot variation_empirical_prOutbreak_vs_size_v{:d}_pI_strU_TG

Isort = np.argsort(beta)
PrOut = np.zeros(beta.size)
#S_O = int(input("What is the size threshold for a successful outbreak?  > S_O = "))
S_O = 30.
summary_parameters["pI_size_successful_outbreak"] = S_O
#R_c = int(input("How many runs for each initial city?  > Rc = "))
Rc = 300
summary_parameters["pI_strU_eta_Nbr_runs"] = Rc
for i in np.arange(beta.size):
    Success = np.zeros((Rc), dtype = bool)
    for k in np.arange(Rc):
        IA = np.zeros((NC), dtype = float)
        IA[i] = 1
        Tf, IAf, Rf, Shf, Ni = InfectionRM(1+20*TL_U[0], 1, MEr, IA, TL_U)
        Success[k] = ( np.sum(Rf) > S_O)
    PrOut[i] = np.sum(Success)/Rc

summary_per_city["pI_strU_emp_eta"] = PrOut

Mean_prOut = np.zeros(MidBins.size)
Std_prOut = np.zeros(MidBins.size)
Min_prOut = np.zeros(MidBins.size)
Max_prOut = np.zeros(MidBins.size)
for ir in np.arange(MidBins.size):
    Is_bin = (np.log10(beta)>=Bins_ref[ir])& (np.log10(beta)<Bins_ref[ir+1])
    Mean_prOut[ir] = np.mean(PrOut[Is_bin])
    Std_prOut[ir] = np.std(PrOut[Is_bin])
    Min_prOut[ir] = np.min(PrOut[Is_bin])
    Max_prOut[ir] = np.max(PrOut[Is_bin])
#GX = summary_per_bin['Central_value_log_sc']
summary_per_bin["pI_strU_empB_eta"] = Mean_prOut
summary_per_bin["pI_strU_stdB_emp_eta"] = Std_prOut
summary_per_bin["pI_strU_minB_eta"] = Min_prOut
summary_per_bin["pI_strU_maxB_eta"] = Max_prOut

plt.figure(4)
plt.clf()
plt.plot(MidBins, etaRef_U, "ks-", label = "analytical")
plt.plot(MidBins, Mean_prOut, "bD--", label = "simulated")
plt.plot(MidBins, Mean_prOut + Std_prOut, "^r:", label="std")
plt.plot(MidBins, Mean_prOut - Std_prOut, "vr:")
plt.vlines(MidBins, \
          ymin = Min_prOut,  ymax = Max_prOut,\
         color= "purple", linestyle =":")
plt.plot(MidBins, Min_prOut, "mv:")
plt.plot(MidBins, Max_prOut, "m^:", label = "range")
plt.title("Empirical and theoretical outbreak probability, \n \
Fluctuations within the bin, data :{:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.ylim(0,1.02)
plt.legend(title = "{:s}, pI, strU, TG".format(abbrev))
plt.savefig(dir_path+"/variation_empirical_prOutbreak_vs_size_v{:d}_pI_strU_TG.png".format(version))

#It appears that the sizes of outbreaks are so small 
#that the threshold on the size makes a large impact on the observed probabilities

#%%%% Plot empirical_prOutbreak_vs_size_v{:d}_pI_strU_KG

Isort = np.argsort(beta)
PrOut = np.zeros(Bins.size-1)
for ir in np.arange(Bins.size-1):
    iA = Isort[Bins[ir]]
    #N_O = 0
    #S_O = int(input("What is the size threshold for a successful outbreak?  > S_O = "))
    S_O = 30.
    #R_c = int(input("How many runs for each initial city?  > Rc = "))
    Rc = 300 
    Success = np.zeros((Rc), dtype = bool)
    for k in np.arange(Rc):
        IA = np.zeros((NC), dtype = bool)
        IA[iA] = True
        Tf, IAf, Rf, Shf = InfectionI(1+30*TL_U[0], IA, KA_U, KB_U, nuA, nuB, TL_U)
        Success[k] = ( np.sum(Rf) > S_O)
    #PrOut[ir] = N_O/Rc
    PrOut[ir] = np.sum(Success)/Rc
summary_per_bin["pI_strU_empK_eta"] = PrOut    
    
plt.figure(4)
plt.clf()
plt.scatter(MidBins, etaRef_U, c="black", marker="+", label = "analytical")
plt.scatter(np.log10(beta[Isort[Bins[:-1]]]), PrOut, c="red", marker="+", label = "simulated")
plt.title("Empirical probability of triggering an outbreak as a function , \n \
of the size of first infected, data:{:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion of outbreaks")
plt.ylim(0,1)
plt.legend(title = "{:s}, pI, strU, KG".format(abbrev))
plt.savefig(dir_path+"/empirical_prOutbreak_vs_size_v{:d}_pI_strU_KG.png".format(version))


#%%%% Plot theoretical_prOutbreak_vs_size_v{:d}_pI_strP_TG
err = 1
n_it = 0
eta_P = 1- np.exp(- KA_P - KB_P)
while (err > 5E-5) * (n_it < 500):
    eta_P2 = 1-np.exp(- KA_P * np.sum(nuA*eta_P) - KB_P* np.sum(nuB*eta_P))
    err = np.max(np.abs(1-eta_P/eta_P2))
    n_it = n_it +1
    eta_P = eta_P2
    
Sort = np.argsort(beta)
RefV = Sort[Bins]
etaRef_P = (eta_P[RefV[1:]] + eta_P[RefV[:-1]])/2
#problem with the name of eta! corrected in the git file, v0
summary_per_city["pI_strP_thr_eta"] = eta_U
summary_per_bin["pI_strP_thr_eta"] = etaRef_P

plt.figure(5)
plt.clf()
plt.plot(np.log10(beta), eta_P, "ks", markersize = 5, label = "analytical")
plt.title("Theoretical outbreak probability, \n \
data: {:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.legend(title = "{:s}, pI, strP, TG".format(abbrev))
plt.savefig(dir_path+"/theoretical_prOutbreak_vs_size_v{:d}_pI_strP_TG.png".format(version))




#%%%% Plot variation_empirical_prOutbreak_vs_size_v{:d}_pI_strP_TG
#
Isort = np.argsort(beta)
PrOut = np.zeros(beta.size)
#S_O = int(input("What is the size threshold for a successful outbreak?  > S_O = "))
S_O = 30.
#R_c = int(input("How many runs for each initial city?  > Rc = "))
Rc = 300
for i in np.arange(beta.size):
    Success = np.zeros((Rc), dtype = bool)
    for k in np.arange(Rc):
        IA = np.zeros((NC), dtype = float)
        IA[i] = 1
        Tf, IAf, Rf, Shf, Ni = InfectionRM(1+20*TL_U[0], 1, MErP, IA, TL_U)
        Success[k] = ( np.sum(Rf) > S_O)
    PrOut[i] = np.sum(Success)/Rc

summary_per_city["pI_strP_emp_eta"] = PrOut

Mean_prOut = np.zeros(MidBins.size)
Std_prOut = np.zeros(MidBins.size)
Min_prOut = np.zeros(MidBins.size)
Max_prOut = np.zeros(MidBins.size)
for ir in np.arange(MidBins.size):
    Is_bin = (np.log10(beta)>=Bins_ref[ir])& (np.log10(beta)<Bins_ref[ir+1])
    Mean_prOut[ir] = np.mean(PrOut[Is_bin])
    Std_prOut[ir] = np.std(PrOut[Is_bin])
    Min_prOut[ir] = np.min(PrOut[Is_bin])
    Max_prOut[ir] = np.max(PrOut[Is_bin])
#GX = summary_per_bin['Central_value_log_sc']
summary_per_bin["pI_strP_empB_eta"] = Mean_prOut
summary_per_bin["pI_strP_stdB_emp_eta"] = Std_prOut
summary_per_bin["pI_strP_minB_eta"] = Min_prOut
summary_per_bin["pI_strP_maxB_eta"] = Max_prOut

plt.figure(4)
plt.clf()
plt.plot(MidBins, etaRef_P, "ks-", label = "analytical")
plt.plot(MidBins, Mean_prOut, "bD:", label = "simulated")
plt.plot(MidBins, Mean_prOut + Std_prOut, "^r:", label="std")
plt.plot(MidBins, Mean_prOut - Std_prOut, "vr:")
plt.vlines(MidBins, \
          ymin = Min_prOut,  ymax = Max_prOut,\
         color= "purple", linestyle =":")
plt.plot(MidBins, Min_prOut, "mv:")
plt.plot(MidBins, Max_prOut, "m^:", label = "range")
plt.title("Empirical and theoretical outbreak probability, \n \
Fluctuations within the bin, data :{:s}".format(D_Name))
plt.xlabel("City Size ({:s} scale)".format(r'$log_{10}$'))
plt.ylabel("Proportion")
plt.ylim(0,1.02)
plt.legend(title = "{:s}, pI, strP, TG".format(abbrev))
plt.savefig(dir_path+"/variation_empirical_prOutbreak_vs_size_v{:d}_pI_strP_TG.png".format(version))


#%%% Save bin caracterisation

summary_parameters.to_csv("summary_parameters_v{:d}.csv".format(version))
summary_per_bin.to_csv("summary_per_bin_v{:d}.csv".format(version))
summary_per_city.to_csv("summary_per_city_v{:d}.csv".format(version))


#%% R0 estimation
#%%% Str U: Selection of the data (pay attention to the conditioning on epidemics sufficiently large)
#PoI = float(input("Which threshold for the infectious population size with enough stability? \n >PoI = "))
PoI = 10
#N_o = int(input("Number of generations for the measurement:"))

#First, we need to make restrictions of It to the data that we are really interested in
#simulations discarded if the threshold PoI is not reached, the others are "Simulations that are Kept"
SK = (np.max(It, axis = 1)>PoI)
NpK = np.sum(SK)
t_I=np.zeros(NpK, dtype = int)
#we need a further filtering for simulations that are kept N_o generations after t_I
for k in np.arange(NpK):
    t_I[k]= np.where(It[SK][k] > PoI)[0][0]
It3 = np.zeros((NpK, N_o+2))
Z = np.arange(N_o+2)
for k in np.arange(NpK):
    It3[k, :] = It[SK][k, t_I[k]+N_d*Z]

#%%%Plot R0_LSestimation_{:d}_v{:d}_pI_strU_TG.png

eR0_m = np.zeros(N_o-2)
eR0_s = np.zeros(N_o-2)
eR0_f = np.zeros(N_o-2)
eR0_n = np.zeros(N_o-2)


for i in np.arange(N_o-2):
    SK4 = (It3[:, i+1]>0)
    NpK4 = np.sum(SK4)
    if NpK4 > N_p / 10:
        It4 = It3[SK4]
        lR0 = np.zeros(NpK4)
        X = np.arange(i+2)
        Y = np.log10(It4[:, :i+2])
        X1 = X.reshape(1, X.size)
        lR0 = (np.mean(X1*Y, axis = 1)-np.mean(X)*np.mean(Y, axis = 1))/ np.var(X)
        eR0 = 10**lR0
        eR0_m[i] = np.mean(eR0)
        eR0_s[i] = np.std(eR0)
        eR0_sort = np.sort(eR0)
        eR0_f[i] = eR0_sort[int(NpK4/20)]
        eR0_n[i] = eR0_sort[int(NpK4*19/20)]

summary_per_gen["pI_strU_nbrI_{:d}_R0_TG_emp".format(PoI)] = eR0_m
summary_per_gen["pI_strU_nbrI_{:d}_R0_TG_std".format(PoI)] = eR0_s
summary_per_gen["pI_strU_nbrI_{:d}_R0_TG_q5".format(PoI)] = eR0_f
summary_per_gen["pI_strU_nbrI_{:d}_R0_TG_q95".format(PoI)] = eR0_n
eR0_ms = eR0_m - eR0_s
eR0_ps = eR0_m + eR0_s


print("Expected R0:~{:.2e}".format(RU_ref))

plt.figure(6)
plt.clf()
plt.vlines(np.arange(2, N_o), \
          ymin = eR0_f,\
          ymax = eR0_n,\
         color= "purple", linestyle =":")
pl_FivePer, = plt.plot(np.arange(2, N_o), eR0_n, "m^:", label = "5%-95%")
plt.plot(np.arange(2, N_o), eR0_f, "mv:")
pl_Std, = plt.plot(np.arange(2, N_o), eR0_ps, "r^:", label="+std")
plt.plot(np.arange(2, N_o), eR0_ms, "rv:")
pl_Avg, = plt.plot(np.arange(2, N_o), eR0_m, "bD--", label="simulated")
pl_Exp, = plt.plot(np.arange(2, N_o), RU_ref*np.ones(N_o-2), c="black", label="expected")
plt.title("R0 estimation, \n\
reference at {:.0f}  cities currently infected, data: {:s} ".format(PoI, D_Name))
plt.xlabel("Number of generations in the time-interval")
plt.ylabel("Estimated R0")
plt.legend(handles = [pl_Exp, pl_Avg, pl_Std, pl_FivePer], title = "{:s}, pI, strU, TG".format(abbrev))
plt.ylim(-0.02*RU_ref, 1.3*RU_ref)
plt.savefig(dir_path+"/R0_LSestimation_{:d}_v{:d}_pI_strU_TG.png".format(PoI, version))
    

#%%% Rescaled plot
plt.figure(7)
plt.clf()
plt.vlines(np.arange(2, N_o), \
          ymin = eR0_f/RU_ref,\
          ymax = eR0_n/RU_ref,\
         color= "purple", linestyle =":")
pl_FivePer, = plt.plot(np.arange(2, N_o), eR0_n/RU_ref, "m^:", label = "5%-95%")
plt.plot(np.arange(2, N_o), eR0_f/RU_ref, "mv:")
pl_Std, = plt.plot(np.arange(2, N_o), eR0_ps/RU_ref, "r^:", label="+std")
plt.plot(np.arange(2, N_o), eR0_ms/RU_ref, "rv:")
pl_Avg, = plt.plot(np.arange(2, N_o), eR0_m/RU_ref, "bD--", label="simulated")
pl_Exp, = plt.plot(np.arange(2, N_o), np.ones(N_o-2), c="black", label="expected")
plt.title("R0 estimation, \n\
reference at {:.0f}  cities currently infected, data: {:s} ".format(PoI, D_Name))
plt.xlabel("Number of generations in the time-interval")
plt.ylabel("Estimated R0 (compared to the expected value)")
plt.legend(handles = [pl_Exp, pl_Avg, pl_Std, pl_FivePer], title = "{:s}, pI, strU, TG".format(abbrev))
plt.ylim(-0.02, 1.3)
plt.savefig(dir_path+"/R0_LSestimation_RS_{:d}_v{:d}_pI_strU_TG.png".format(PoI, version))
    

#%%% summary_parameters and summary_per_gen saved
summary_parameters.to_csv("summary_parameters_v{:d}.csv".format(version))
summary_per_gen.to_csv("summary_per_gen_v{:d}.csv".format(version))

#%%% Str P: Generation based data
Rt_GB = np.zeros((N_p, Nt))
It_GB = np.zeros((N_p, Nt))
PrI_GB = np.zeros((N_p, Counts_ref.size))
Is_I_GB = np.zeros((N_p, beta.size), dtype = bool)
N_fl_GB = 0
for k in np.arange(N_p):
    Tf_GB, IAf_GB, Rf_GB, Shf_GB, N_it_GB2 = InfectionRM(1+t_o, N_I, MErP, nuA, TL_U)
    #failure update
    N_fl_GB = N_fl_GB + N_it_GB2/N_p
    #Is_I_GB : who has been infected eventually?
    Is_I_GB[k] = (Tf_GB!= 0)
    #update for the total number of infected 
    T1_GB = Tf_GB[Rf_GB]
    T1_GB = T1_GB.reshape((1, np.size(T1_GB)))
    Rt_GB[k] = np.sum(T1_GB < Gtb, axis = 1)
    #update for the number of infectious
    T2_GB = Tf_GB[Rf_GB] + TL_U[Rf_GB]
    T2_GB = np.reshape(T2_GB, (1, np.size(T2_GB)))
    It_GB[k] = np.sum((T1_GB < Gtb)*(Gtb < T2_GB), axis = 1)
    #proportion of infected cities as a function of their size
    counts, bins, bars = plt.hist(np.log10(beta[Tf_GB==0]), bins = Sep)
    plt.clf()
    PrI_GB[k] = 1- counts/Counts_ref
        
    
#%%% Selection of the data (pay attention to the conditioning on epidemics sufficiently large)
#PoI = float(input("Which threshold for the infectious population size with enough stability? \n >PoI = "))
PoI = 10
#N_o = int(input("Number of generations for the measurement:"))

#First, we need to make restrictions of It_GB to the data that we are really interested in
#simulations discarded if the threshold PoI is not reached, the others are "Simulations that are Kept"
SK_GB = (np.max(It_GB, axis = 1)>PoI)
NpK_GB = np.sum(SK_GB)
t_I_GB=np.zeros(NpK_GB, dtype = int)
#we need a further filtering for simulations that are kept N_o generations after t_I_GB
for k in np.arange(NpK_GB):
    t_I_GB[k]= np.where(It_GB[SK_GB][k] > PoI)[0][0]
It_GB3 = np.zeros((NpK_GB, N_o+2))
Z = np.arange(N_o+2)
for k in np.arange(NpK_GB):
    It_GB3[k, :] = It_GB[SK_GB][k, t_I_GB[k]+N_d*Z]

#%%%Plot R0_LSestimation_{:d}_v{:d}_pI_strP_TG.png

eR0_m = np.zeros(N_o-2)
eR0_s = np.zeros(N_o-2)
eR0_f = np.zeros(N_o-2)
eR0_n = np.zeros(N_o-2)


for i in np.arange(N_o-2):
    SK_GB4 = (It_GB3[:, i+1]>0)
    NpK_GB4 = np.sum(SK_GB4)
    if NpK_GB4 > N_p / 10:
        It_GB4 = It_GB3[SK_GB4]
        lR0 = np.zeros(NpK_GB4)
        X = np.arange(i+2)
        Y = np.log10(It_GB4[:, :i+2])
        X1 = X.reshape(1, X.size)
        lR0 = (np.mean(X1*Y, axis = 1)-np.mean(X)*np.mean(Y, axis = 1))/ np.var(X)
        eR0 = 10**lR0
        eR0_m[i] = np.mean(eR0)
        eR0_s[i] = np.std(eR0)
        eR0_sort = np.sort(eR0)
        eR0_f[i] = eR0_sort[int(NpK_GB4/20)]
        eR0_n[i] = eR0_sort[int(NpK_GB4*19/20)]

summary_per_gen["pI_strP_nbrI_{:d}_R0_TG_emp".format(PoI)] = eR0_m
summary_per_gen["pI_strP_nbrI_{:d}_R0_TG_std".format(PoI)] = eR0_s
summary_per_gen["pI_strP_nbrI_{:d}_R0_TG_q5".format(PoI)] = eR0_f
summary_per_gen["pI_strP_nbrI_{:d}_R0_TG_q95".format(PoI)] = eR0_n
eR0_ms = eR0_m - eR0_s
eR0_ps = eR0_m + eR0_s


print("Expected R0:~{:.2e}".format(RP_ref))

plt.figure(6)
plt.clf()
plt.vlines(np.arange(2, N_o), \
          ymin = eR0_f,\
          ymax = eR0_n,\
         color= "purple", linestyle =":")
pl_FivePer, = plt.plot(np.arange(2, N_o), eR0_n, "m^:", label = "5%-95%")
plt.plot(np.arange(2, N_o), eR0_f, "mv:")
pl_Std, = plt.plot(np.arange(2, N_o), eR0_ps, "r^:", label="+std")
plt.plot(np.arange(2, N_o), eR0_ms, "rv:")
pl_Avg, = plt.plot(np.arange(2, N_o), eR0_m, "bD--", label="simulated")
pl_Exp, = plt.plot(np.arange(2, N_o), RP_ref*np.ones(N_o-2), c="black", label="expected")
plt.title("R0 estimation, \n\
reference at {:.0f}  cities currently infected, data: {:s} ".format(PoI, D_Name))
plt.xlabel("Number of generations in the time-interval")
plt.ylabel("Estimated R0")
plt.legend(handles = [pl_Exp, pl_Avg, pl_Std, pl_FivePer], title = "{:s}, pI, strP, TG".format(abbrev))
plt.ylim(-0.02*RP_ref, 1.3*RP_ref)
plt.savefig(dir_path+"/R0_LSestimation_{:d}_v{:d}_pI_strP_TG.png".format(PoI, version))
    

#%%% Rescaled plot
plt.figure(7)
plt.clf()
plt.vlines(np.arange(2, N_o), \
          ymin = eR0_f/RP_ref,\
          ymax = eR0_n/RP_ref,\
         color= "purple", linestyle =":")
pl_FivePer, = plt.plot(np.arange(2, N_o), eR0_n/RP_ref, "m^:", label = "5%-95%")
plt.plot(np.arange(2, N_o), eR0_f/RP_ref, "mv:")
pl_Std, = plt.plot(np.arange(2, N_o), eR0_ps/RP_ref, "r^:", label="+std")
plt.plot(np.arange(2, N_o), eR0_ms/RP_ref, "rv:")
pl_Avg, = plt.plot(np.arange(2, N_o), eR0_m/RP_ref, "bD--", label="simulated")
pl_Exp, = plt.plot(np.arange(2, N_o), np.ones(N_o-2), c="black", label="expected")
plt.title("R0 estimation, \n\
reference at {:.0f}  cities currently infected, data: {:s} ".format(PoI, D_Name))
plt.xlabel("Number of generations in the time-interval")
plt.ylabel("Estimated R0 (compared to the expected value)")
plt.legend(handles = [pl_Exp, pl_Avg, pl_Std, pl_FivePer], title = "{:s}, pI, strP, TG".format(abbrev))
plt.ylim(-0.02, 1.3)
plt.savefig(dir_path+"/R0_LSestimation_RS_{:d}_v{:d}_pI_strP_TG.png".format(PoI, version))
    

#%%% summary_parameters and summary_per_gen saved
summary_parameters.to_csv("summary_parameters_v{:d}.csv".format(version))
summary_per_gen.to_csv("summary_per_gen_v{:d}.csv".format(version))


#%% Incidence as a function of R0

#%%% Plot Final_incidence_rO_v{:d}_strP_TG

Inf_cit = np.zeros((bR, NR))
Inf_ppl = np.zeros((bR, NR))
NP = np.sum(beta)
for iR in np.arange(bR):
    for j in np.arange(NR):
        Tf2, IAf2, Rf2, Shf2, N_it2 = InfectionRM(100, 1, (R0_var[iR]/RP_ref) * MErP, nuA, np.ones(NC))
        Inf_cit[iR, j] = np.sum(Rf2)/NC
        Inf_ppl[iR, j] = np.sum(beta[Rf2])/NP
summary_per_R0['pI_strP_TG_Inf_cit'] = np.mean(Inf_cit, axis = 1)
summary_per_R0['pI_strP_TG_Inf_ppl'] = np.mean(Inf_ppl, axis = 1)


plt.plot(R0_var, summary_per_R0['pI_strP_TG_Inf_cit'], 'b*', label = "Cities under isolation")
plt.plot(R0_var, summary_per_R0['pI_strP_TG_Inf_ppl'], 'r*', label = "People under isolation")
plt.title("Proportion of cities and people under isolation,\n \
data:{:s}".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("Proportion of infected entities")
plt.legend(title = "{:s}, strP, TG".format(abbrev))
plt.ylim(0, 1)
plt.savefig(dir_path+"/Final_incidence_rO_v{:d}_strP_TG.png".format(version))

#%%% Plot Final_incidence_cO_v{:d}_strP_TG

R0_var_2 = R0_var[R0_var > 2]
bR2 = R0_var_2.size
Inf_cit = np.zeros((bR2, NR))
Inf_ppl = np.zeros((bR2, NR))
NP = np.sum(beta)
for iR in np.arange(bR2):
    for j in np.arange(NR):
        Tf2, IAf2, Rf2, Shf2, N_it2 = InfectionRM(100, 20, (R0_var_2[iR]/RP_ref) * MErP, nuA, np.ones(NC))
        Inf_cit[iR, j] = np.sum(Rf2)/NC
        Inf_ppl[iR, j] = np.sum(beta[Rf2])/NP
summary_per_R0['pI_strP_TG_cO_Inf_cit'] = -1 
summary_per_R0.loc[R0_var > 2, 'pI_strP_TG_cO_Inf_cit'] = np.mean(Inf_cit, axis = 1)
summary_per_R0['pI_strP_TG_cO_Inf_ppl'] = -1
summary_per_R0.loc[R0_var > 2, 'pI_strP_TG_cO_Inf_ppl'] = np.mean(Inf_ppl, axis = 1)


plt.plot(R0_var_2, summary_per_R0.loc[R0_var > 2, 'pI_strP_TG_cO_Inf_cit'], 'b*', label = "Cities under isolation")
plt.plot(R0_var_2, summary_per_R0.loc[R0_var > 2, 'pI_strP_TG_cO_Inf_ppl'], 'r*', label = "People under isolation")
plt.title("Proportion of cities and people under isolation,\n \
data:{:s}".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("Proportion of infected entities")
plt.legend(title = "{:s}, strP, TG".format(abbrev))
plt.ylim(0, 1)
plt.savefig(dir_path+"/Final_incidence_cO_v{:d}_strP_TG.png".format(version))

#%%% Plot Final_incidence_ro_v{:d}_strU_TG

Inf_cit = np.zeros((bR, NR))
Inf_ppl = np.zeros((bR, NR))
NP = np.sum(beta)
for iR in np.arange(bR):
    for j in np.arange(NR):
        Tf2, IAf2, Rf2, Shf2, N_it2 = InfectionRM(100, 1, (R0_var[iR]/RU_ref) * MEr, nuA, np.ones(NC))
        Inf_cit[iR, j] = np.sum(Rf2)/NC
        Inf_ppl[iR, j] = np.sum(beta[Rf2])/NP
summary_per_R0['pI_strU_TG_Inf_cit'] = np.mean(Inf_cit, axis = 1)
summary_per_R0['pI_strU_TG_Inf_ppl'] = np.mean(Inf_ppl, axis = 1)


plt.plot(R0_var, summary_per_R0['pI_strU_TG_Inf_cit'], 'b*', label = "Cities under isolation")
plt.plot(R0_var, summary_per_R0['pI_strU_TG_Inf_ppl'], 'r*', label = "People under isolation")
plt.title("Proportion of cities and people under isolation,\n \
data:{:s}".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("Proportion of infected entities")
plt.legend(title = "{:s}, strU, TG".format(abbrev))
plt.ylim(0, 1)
plt.savefig(dir_path+"/Final_incidence_rO_v{:d}_strU_TG.png".format(version))

#%%% Plot Final_incidence_ro_v{:d}_pI_strU_TG

R0_var_2 = np.array(R0_var[R0_var > 2])
bR2 = R0_var_2.size
Inf_cit = np.zeros((bR2, NR))
Inf_ppl = np.zeros((bR2, NR))
NP = np.sum(beta)
for iR in np.arange(bR2):
    for j in np.arange(NR):
        Tf2, IAf2, Rf2, Shf2, N_it2 = InfectionRM(100, 20, (R0_var_2[iR]/RU_ref) * MEr, nuA, np.ones(NC))
        Inf_cit[iR, j] = np.sum(Rf2)/NC
        Inf_ppl[iR, j] = np.sum(beta[Rf2])/NP
summary_per_R0['pI_strU_TG_cO_Inf_cit'] = -1 
summary_per_R0.loc[R0_var > 2, 'pI_strU_TG_cO_Inf_cit'] = np.mean(Inf_cit, axis = 1)
summary_per_R0['pI_strU_TG_cO_Inf_ppl'] = -1
summary_per_R0.loc[R0_var > 2, 'pI_strU_TG_cO_Inf_ppl'] = np.mean(Inf_ppl, axis = 1)


plt.plot(R0_var_2, summary_per_R0.loc[R0_var > 2, 'pI_strU_TG_cO_Inf_cit'], 'b*', label = "Cities under isolation")
plt.plot(R0_var_2, summary_per_R0.loc[R0_var > 2, 'pI_strU_TG_cO_Inf_ppl'], 'r*', label = "People under isolation")
plt.title("Proportion of cities and people under isolation,\n \
Conditional on an outbreak event,\
data:{:s}".format(D_Name))
plt.xlabel("Initial reproduction number")
plt.ylabel("Proportion of infected entities")
plt.legend(title = "{:s}, strU, TG".format(abbrev))
plt.ylim(0, 1)
plt.savefig(dir_path+"/Final_incidence_cO_v{:d}_pI_strU_TG.png".format(version))


#%%% Save Incidence vs R0
summary_parameters.to_csv("summary_parameters_v{:d}.csv".format(version))
summary_per_R0.to_csv("summary_per_R0_v{:d}.csv".format(version))

#%% End
