#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 10 11:05:21 2023

@author: avelleret
"""

#%% Import
import numpy as np
import numpy.random as npr
import random as rd
import matplotlib.pyplot as plt
import pandas as pd
import scipy.sparse as sp
import time


#%% Data_extraction D30

summary_per_FC = pd.read_csv('summary_per_Aire_Attr_py_v55.csv')
log_beta = np.log10(summary_per_FC["PAA"])

D_Name = "FC, D30+"
Dist = 30
Mat_flux_D_AA = pd.read_csv('Mat_flux_D30_AA_py_P.csv', sep = ",", index_col=0)
F_Orig_D = Mat_flux_D_AA.sum(axis = 1)
F_Desti_D = Mat_flux_D_AA.sum(axis = 0)


#%% a estimation (adjusted for any distance)

from scipy import stats
slopeA, interceptA, rA, pA, std_errA \
    = stats.linregress(log_beta, np.log10(F_Desti_D))
def myfuncA(x):
  return slopeA * x + interceptA

mymodelA = list(map(myfuncA, log_beta))


plt.plot(log_beta,  np.log10(F_Desti_D), 'b+', alpha = 0.4)
plt.plot(log_beta, mymodelA, 'r', label = "fitted slope: {:.2f}".format(slopeA))
# plt.plot(log_beta, np.mean(log_influx) + log_beta - np.mean(log_beta), 'k--',\
#          label = "test: {:.2f}".format(1.))
plt.title("Estimation of a={:.2f} from influx data".format(slopeA))
plt.xlabel("Population size (${:s}$-scale)".format(r'\log_{10}'))
plt.ylabel("Influx (${:s}$-scale)".format(r'\log_{10}'))
plt.legend(title = "{:s}, R2 = {:.2f}".format(D_Name, rA))
plt.savefig("aEstimate_D{:d}_France.png".format(Dist))
plt.show() 
print("a = {:.2f}".format(slopeA))


#%% b estimation (adjusted for any distance)


from scipy import stats
slopeB, interceptB, rB, pB, std_errB \
    = stats.linregress(log_beta, np.log10(F_Orig_D))
def myfuncB(x):
  return slopeB * x + interceptB

mymodelB = list(map(myfuncB, log_beta))


plt.plot(log_beta, np.log10(F_Orig_D), 'b+', alpha = 0.4)
plt.plot(log_beta, mymodelB, 'r', label = "fitted slope: {:.2f}".format(slopeB))
# plt.plot(log_beta, np.mean(log_outflux) + log_beta - np.mean(log_beta), 'k--',\
#          label = "test: {:.2f}".format(1.))
plt.title("Estimation of b={:.2f} from outflux data".format(slopeB-1))
plt.xlabel("Population size (${:s}$-scale)".format(r'\log_{10}'))
plt.ylabel("Outflux (${:s}$-scale)".format(r'\log_{10}'))
plt.legend(title = "{:s}, R2 = {:.2f}".format(D_Name, rB))
plt.savefig("bEstimate_D{:d}_France.png".format(Dist))
plt.show() 
print("b = {:.2f}".format(slopeB - 1))

#%% Data_extraction D1+

summary_per_FC = pd.read_csv('summary_per_Aire_Attr_py_v55.csv')
log_beta = np.log10(summary_per_FC["PAA"])

D_Name = "FC, D1+"
Dist = 0
Mat_flux_D_AA = pd.read_csv('Mat_flux_D0_AA_py_P.csv', sep = ",", index_col=0)
F_Orig_D = Mat_flux_D_AA.sum(axis = 1)
F_Desti_D = Mat_flux_D_AA.sum(axis = 0)



#%% Data_extraction D50

summary_per_FC = pd.read_csv('summary_per_Aire_Attr_py_v55.csv')
log_beta = np.log10(summary_per_FC["PAA"])

D_Name = "FC, D50+"
Dist = 50
Mat_flux_D_AA = pd.read_csv('Mat_flux_D50_AA_py_P.csv', sep = ",", index_col=0)
F_Orig_D = Mat_flux_D_AA.sum(axis = 1)
F_Desti_D = Mat_flux_D_AA.sum(axis = 0)



#%% End 