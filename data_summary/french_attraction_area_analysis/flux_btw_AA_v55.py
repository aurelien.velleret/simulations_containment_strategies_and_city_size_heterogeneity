# -*- coding: utf-8 -*-
"""
Created on Sun Jan 23 23:53:21 2022

@author: alter
"""

#%%
import numpy as np
import numpy.random as npr
import random as rd
import matplotlib.pyplot as plt
import pandas as pd
import scipy.sparse as sp
import time
#%%

Aires_Attr_E = pd.read_csv("communes_per_aires_attraction_v55.csv", sep = ",", \
header=0, dtype= {'CCom' : str, 'CAA' : str})
# #pd.read_csv("Aires_Attr_Etr.csv", sep = ";", header=0)
Com_flux = pd.read_csv("commune_origin_destination_matrix_work_mobility_with_distance_v2.csv", \
sep = ",", header=0, dtype= {'Origin' : str, 'Destination' : str})

#some empty lines have to be removed
Com_flux = Com_flux[~Com_flux["Origin"].isnull()]
Com_flux["Origin"] = Com_flux["Origin"].astype(int)
Com_flux["Destination"] = Com_flux["Destination"].astype(int)
    
#Aires_Attr_E.columns = np.array(["CCom", "LCom", "CAA", "LAA", "CatCom", "CDep", "CReg"])
#We remove the rows with CAA entries that are not integer (foreign)
CAA = Aires_Attr_E["CAA"]
CaaInt= CAA.str.isdigit()
Aires_Attr_E = Aires_Attr_E[CaaInt]
#We remove the rows with CAA entries that are 0 (outside of attraction)
CAA =CAA[CaaInt].astype(int)
Aires_Attr_E = Aires_Attr_E[CAA>0]
#Among others, Corsica shall be removed ! 256 communes with a code that is not numeric!
CCom = Aires_Attr_E["CCom"].astype(str)
CComInt= CCom.str.isdigit()
Aires_Attr_E = Aires_Attr_E[CComInt]
Aires_Attr_E["CCom"] = Aires_Attr_E["CCom"].astype(int)
Aires_Attr_E["CAA"] = Aires_Attr_E["CAA"].astype(int)


#%%

#population sizes derived from the dataset provided by Piotr
communes, iCom = np.unique(Com_flux[["Origin"]], return_index = True)
Com_POP = Com_flux.iloc[iCom][["Origin", "Source[Population]"]]
Com_POP.rename(columns = {'Origin': 'CCom', 'Source[Population]':'Population'}, inplace = True)
#The values exactly correspond to the expected ones for population sizes of major FUA!!

#%%
#prevent SettingWithCopyWarning message from appearing
pd.options.mode.chained_assignment = None


summary_per_Aire_Attr= pd.DataFrame(pd.unique(Aires_Attr_E['CAA']), columns= ["CAA"])
summary_per_Aire_Attr['LAA'] = Aires_Attr_E['LAA'][0]
summary_per_Aire_Attr['PAA'] = 0
# summary_per_Aire_Attr['Ex_Com'] = 0
# summary_per_Aire_Attr['In_Com'] = 0

pAA = summary_per_Aire_Attr['PAA']
laa= summary_per_Aire_Attr['LAA']
# ExC= summary_per_Aire_Attr['Ex_Com']
# InC =summary_per_Aire_Attr['In_Com']
CAA_F = summary_per_Aire_Attr['CAA']
CAA_E = Aires_Attr_E['CAA'].astype(int)
for ind in np.arange(pAA.size):
  CCAi = np.asarray(CAA_E==CAA_F[ind])
  lAA = Aires_Attr_E.loc[CCAi, 'LAA']
  laa[ind] = lAA[lAA.index[0]]
  COM_AAi = Aires_Attr_E.loc[CCAi, 'CCom'].astype(int)
  for j in COM_AAi:
      pAA[ind] += np.sum(Com_POP.loc[Com_POP['CCom'] == j, 'Population'])
      # for k in np.where(Com_flux['Origin']==j)[0]:
      #     if not(Com_flux.loc[k,'Destination'] in COM_AAi):
      #        ExC[ind]+= Com_flux.loc[k, 'TravellerCount']
      # for k in np.where(Com_flux['Destination']==j)[0]:
      #     if not(Com_flux.loc[k,'Origin'] in COM_AAi):
      #        InC[ind]+= Com_flux.loc[k, 'TravellerCount']
  summary_per_Aire_Attr.to_csv('summary_per_Aire_Attr_py_v55.csv')

#%%

laa = summary_per_Aire_Attr['LAA']
  #read.csv("Mat_flux_AA.csv", sep = ",", header = TRUE)
COM_AA= pd.DataFrame(0, index = Aires_Attr_E['CCom'].astype("int64"), columns = CAA_F, dtype = bool)
#<- (matrix(0, length(lAA), length(Aires_Attr_E$CAA)) ==1)
Flux_AAO = pd.DataFrame(0, index =  Com_flux['Source[Commune]'], columns = CAA_F, dtype = bool)
Flux_AAD = pd.DataFrame(0, index =  Com_flux['Target[Commune]'], columns = CAA_F, dtype = bool)
#CAA_F<-as.numeric(as.character(summary_per_Aire_Attr$CAA))
CAA_F.index = CAA_F
Aires_Attr_E.index = COM_AA.index
for i_O in CAA_F:
    COMAAi = np.asarray(CAA_E==CAA_F[i_O])
    COM_AA[i_O] = COMAAi
    LCi = COM_AA.index[COM_AA[i_O]]
    for j in LCi:
        Flux_AAO.loc[Flux_AAO.index == j, i_O] = True
        Flux_AAD.loc[Flux_AAD.index == j, i_O] = True
#COM_AA.to_csv('COM_AA.csv')
Flux_AAO.index = Com_flux.index
Flux_AAD.index = Com_flux.index
    # for kO in Com_flux['LIBGEO']:
    #     Flux_AAO.loc[kO, i_O]= (kO in LCi)
    # for kD in Com_flux['L_DCLT']:
    #     Flux_AAD.loc[kD, i_O]= (kD  in LCi)


#%%

Timing = pd.Series(0, index = CAA_F)
Mat_flux_AA = pd.DataFrame(0, index = CAA_F, columns = CAA_F)
for i_O in CAA_F:
    start_time = time.process_time() 
    for i_D in CAA_F:
        Mat_flux_AA.loc[i_O, i_D] = np.sum(Com_flux.loc[Flux_AAO[i_O]&Flux_AAD[i_D], 'TravellerCount'])
    Timing[i_O] = time.process_time()  - start_time
Mat_flux_AA.to_csv('Mat_flux_AA_py_v55.csv')

#%%
ext = pd.Series(0, index = laa, dtype = bool)
for l in laa:
    ext[l] = ("fran" in l)
ext.index = laa.index
Border = laa[ext]

#%%
Mat_flux_AA = pd.read_csv('Mat_flux_D50_AA_py_P.csv', sep = ",", index_col=0)
for i in np.arange(Mat_flux_AA.shape[0]):
    Mat_flux_AA.iloc[i, i] = 0
F_Orig = Mat_flux_AA.sum(axis = 1)
F_Desti = Mat_flux_AA.sum(axis = 0)
#summary_per_Aire_Attr.index = F_Desti.index
plt.plot(np.log10(summary_per_Aire_Attr['PAA']), np.log10(F_Orig), 'r+')

#%%
PAA = summary_per_Aire_Attr['PAA']
Dmin = 0
#%%
from scipy import stats
slopeB, interceptB, rB, pB, std_errB \
    = stats.linregress(np.log10(PAA), np.log10(F_Orig))
def myfuncB(x):
  return slopeB * x + interceptB

mymodelB = list(map(myfuncB, np.log10(PAA)))

plt.scatter(np.log10(PAA), np.log10(F_Orig))
plt.plot(np.log10(PAA), mymodelB, 'r')
plt.title("Estimation of b={:.2f} from French Areas of Attraction\n \
          with no restriction on distance" 
              .format(slopeB - 1))
plt.xlabel("Population size (log scale)")
plt.ylabel("Travels from the source (log scale)")
#plt.savefig("bEstimate_D{:d}.png".format(int(Dmin/1e3)))
plt.show() 
print("b = {:.2f}".format(slopeB - 1))
#%%
slopeA, interceptA, rA, pA, std_errA \
    = stats.linregress(np.log10(PAA), np.log10(F_Desti))
def myfuncA(x):
  return slopeA * x + interceptA

mymodelA = list(map(myfuncA, np.log10(PAA)))

plt.scatter(np.log10(PAA), np.log10(F_Desti))
plt.plot(np.log10(PAA), mymodelA, 'r')
plt.title("Estimation of a={:.2f} from French Areas of Attraction\n \
          with no restriction on distance".format(slopeA))
plt.xlabel("Population size (log scale)")
plt.ylabel("Travels into the source (log scale)")
#plt.savefig("aEstimate_D{:d}.png".format(int(Dmin/1e3))) 
plt.show() 
print("a = {:.2f}".format(slopeA))
#%%

#With the spatial restriction
Dmin = 4e4
Com_flux_D = Com_flux[Com_flux["CentroidDistance [m]"]>Dmin]

#%%
#prevent SettingWithCopyWarning message from appearing
pd.options.mode.chained_assignment = None


flux_D_Aires_Attr= pd.DataFrame(pd.unique(Aires_Attr_E['CAA']), columns= ["CAA"])
flux_D_Aires_Attr = flux_D_Aires_Attr[flux_D_Aires_Attr['CAA'] !=0]
sortCAA = np.argsort(flux_D_Aires_Attr["CAA"].astype(int))
flux_D_Aires_Attr= flux_D_Aires_Attr.iloc[sortCAA]
flux_D_Aires_Attr.index = np.arange(flux_D_Aires_Attr.index.size)

flux_D_Aires_Attr['LAA'] = Aires_Attr_E['LAA'].iloc[0]
flux_D_Aires_Attr['PAA'] = 0
flux_D_Aires_Attr['Ex_Com'] = 0
flux_D_Aires_Attr['In_Com'] = 0
pAA = flux_D_Aires_Attr['PAA']
laa= flux_D_Aires_Attr['LAA']
ExC= flux_D_Aires_Attr['Ex_Com']
InC =flux_D_Aires_Attr['In_Com']

CAA_F = flux_D_Aires_Attr['CAA'].astype(int)
#CAA = CAA.astype(int)
CAA_E = Aires_Attr_E['CAA'].astype(int)

for ind in np.arange(pAA.size):
  CCAi = np.asarray(CAA_E==CAA_F[ind])
  lAA = Aires_Attr_E.loc[CCAi, 'LAA']
  laa[ind] = lAA[lAA.index[0]]
  COM_AAi = Aires_Attr_E.loc[CCAi, 'CCom'].astype("int64")
  for j in COM_AAi:
      pAA[ind] += np.sum(Com_POP.loc[Com_POP['CCom'] == j, 'Population'])
      for k in np.where(Com_flux_D['Source[Commune]']==j)[0]:
          if not(Com_flux_D.iloc[k,2] in COM_AAi):
             ExC[ind]+= Com_flux_D.iloc[k, 3]
      for k in np.where(Com_flux_D['Target[Commune]']==j)[0]:
          if not(Com_flux_D.iloc[k,1] in COM_AAi):
             InC[ind]+= Com_flux_D.iloc[k, 3]
  flux_D_Aires_Attr.to_csv('flux_D{:d}_Aires_Attr_py_P.csv'.format(int(Dmin/1e3)) )
  
#%%

laa = flux_D_Aires_Attr['LAA']

CAA_F = flux_D_Aires_Attr['CAA'].astype(int)
  #read.csv("Mat_flux_AA.csv", sep = ",", header = TRUE)
COM_AA= pd.DataFrame(0, index = Aires_Attr_E['CCom'].astype("int64"), columns = CAA_F, dtype = bool)
#<- (matrix(0, length(lAA), length(Aires_Attr_E$CAA)) ==1)
flux_D_AAO = pd.DataFrame(0, index =  Com_flux_D['Source[Commune]'], columns = CAA_F, dtype = bool)
flux_D_AAD = pd.DataFrame(0, index =  Com_flux_D['Target[Commune]'], columns = CAA_F, dtype = bool)
#CAA<-as.numeric(as.character(flux_D_Aires_Attr$CAA))


#CAA.index = CAA
Aires_Attr_E.index = COM_AA.index
for i_O in CAA_F:
    COM_AA[i_O] = np.asarray(CAA_E==i_O)
    LCi = COM_AA.index[COM_AA[i_O]]
    for j in LCi:
        flux_D_AAO.loc[flux_D_AAO.index == j, i_O] = True
        flux_D_AAD.loc[flux_D_AAD.index == j, i_O] = True
#COM_AA.to_csv('COM_AA.csv')
flux_D_AAO.index = Com_flux_D.index
flux_D_AAD.index = Com_flux_D.index
    # for kO in Com_flux_D['LIBGEO']:
    #     flux_D_AAO.loc[kO, i_O]= (kO in LCi)
    # for kD in Com_flux_D['L_DCLT']:
    #     flux_D_AAD.loc[kD, i_O]= (kD  in LCi)


#%%

Timing = pd.Series(0, index = CAA_F)
Mat_flux_D_AA = pd.DataFrame(0, index = CAA_F, columns = CAA_F)
for i_O in CAA_F:
    start_time = time.process_time() 
    for i_D in CAA_F:
        if (i_D!=i_O):
            Mat_flux_D_AA.loc[i_O, i_D] = np.sum(Com_flux_D.loc[flux_D_AAO[i_O]&flux_D_AAD[i_D], 'TravellerCount'])
    Timing[i_O] = time.process_time()  - start_time
Mat_flux_D_AA.to_csv('Mat_flux_D{:d}_AA_py_P.csv'.format(int(Dmin/1e3)))


# #%%

# Timing = pd.Series(0, index = CAA_F)
# Mat_flux_D2_AA = pd.DataFrame(0, index = CAA_F, columns = CAA_F)
# for i_O in np.arange(CAA_F.size):
#     start_time = time.process_time() 
#     for i_D in np.arange(CAA_F.size):
#         if (i_D!=i_O):
#             Mat_flux_D2_AA.iloc[i_O, i_D] = np.sum(Com_flux_D.loc[flux_D_AAO.iloc[:,i_O]&flux_D_AAD.iloc[:,i_D], 'TravellerCount'])
#     Timing[i_O] = time.process_time()  - start_time
# #%%
# #Dmin = 0
# #Mat_flux_D_AA= pd.read_csv('Mat_flux_D{:d}_AA_py_P.csv'.format(int(Dmin/1e3)), index_col = 0)
# #Mat_flux_D_AA= pd.read_csv('Mat_flux_AA_py_PNT.csv', index_col = 0)
# summary_per_Aire_Attr2 = pd.read_csv("summary_per_Aire_Attr_py1.csv", index_col = 0)

# summary_per_Aire_Attr2.index = summary_per_Aire_Attr2["CAA"]
# CAA_F = np.array(Mat_flux_D_AA.index)
# iCAA = pd.Series(0, index = summary_per_Aire_Attr2.index, dtype = bool)
# for j in summary_per_Aire_Attr2.index:
#     iCAA[j] = (j in CAA_F)
# PAA = summary_per_Aire_Attr2[iCAA]["PAA"]
# CAA2 = summary_per_Aire_Attr2[iCAA]["CAA"]
# iCAA2 = pd.Series(0, index = Mat_flux_D_AA.index, dtype = bool)
# for j in Mat_flux_D_AA.index:
#     iCAA2[j] = (j in CAA2)
# Mat_flux_D_AA2 = Mat_flux_D_AA.loc[iCAA2, np.array(iCAA2)]

#%%
Mat_flux_D_AA = pd.read_csv('Mat_flux_D50_AA_py_P.csv', sep = ",", index_col=0)
flux_D_Aires_Attr = pd.read_csv('flux_D50_Aires_Attr_py_P.csv')
F_Orig_D = Mat_flux_D_AA.sum(axis = 1)
F_Desti_D = Mat_flux_D_AA.sum(axis = 0)
Degree_Orig_D =(Mat_flux_D_AA >0).sum(axis = 1)
Degree_Desti_D =(Mat_flux_D_AA >0).sum(axis = 0)


#%%
plt.plot(F_Orig_D, Degree_Orig_D, '+r')

#%%
plt.plot(F_Desti_D, Degree_Desti_D, '+r')


#%%
plt.plot(np.log10(PAA)+3, np.log10(Degree_Orig_D), '+r', label = "Outdegree")
plt.plot(np.log10(PAA)+3, np.log10(Degree_Desti_D), '+b', label = "Indegree")
plt.title("Degrees between French Areas of Attraction\n as a function of population sizes (log-log)")
plt.xlabel("Population size")
plt.ylabel("In- and outdegree")
plt.legend()

#%%

PAA = flux_D_Aires_Attr['PAA']
PAA.index = flux_D_Aires_Attr['CAA'].astype("int64")
PAA_O = PAA[F_Orig_D > 0]
F_Orig_DO  = F_Orig_D[F_Orig_D > 0]
from scipy import stats
slopeB, interceptB, rB, pB, std_errB \
    = stats.linregress(np.log10(PAA_O)+3, np.log10(F_Orig_DO))
def myfuncB(x):
  return slopeB * x + interceptB

mymodelB = list(map(myfuncB, np.log10(PAA_O)+3))

plt.scatter(np.log10(PAA_O)+3, np.log10(F_Orig_DO))
plt.plot(np.log10(PAA_O)+3, mymodelB, 'r')
plt.title("Estimation of b={:.2f} from French Areas of Attraction\n \
          with a restriction on distance being larger than {:d}km" 
              .format(slopeB - 1, int(Dmin/1e3)))
plt.xlabel("Population size (log scale)")
plt.ylabel("Travels from the source (log scale)")
plt.savefig("bEstimate_D{:d}.png".format(int(Dmin/1e3)))
plt.show() 
print("b = {:.2f}".format(slopeB - 1))

#%%
PAA_O = PAA[F_Desti_D > 0]
F_Desti_DO  = F_Desti_D[F_Desti_D > 0]

slopeA, interceptA, rA, pA, std_errA \
    = stats.linregress(np.log10(PAA_O)+3, np.log10(F_Desti_DO))
def myfuncA(x):
  return slopeA * x + interceptA

mymodelA = list(map(myfuncA, np.log10(PAA_O)+3))

plt.scatter(np.log10(PAA_O)+3, np.log10(F_Desti_DO))
plt.plot(np.log10(PAA_O)+3, mymodelA, 'r')
plt.title("Estimation of a={:.2f} from French Areas of Attraction\n \
          with a restriction on distance being larger than {:d}km" 
              .format(slopeA, int(Dmin/1e3)))
plt.xlabel("Population size (log scale)")
plt.ylabel("Travels into the source (log scale)")
plt.savefig("aEstimate_D{:d}.png".format(int(Dmin/1e3))) 
plt.show() 
print("a = {:.2f}".format(slopeA))


#%%