#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  2 12:06:32 2024

@author: avelleret
"""
#%% Import

import powerlaw
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


summary_per_city = pd.read_csv('summary_per_Aire_Attr_py_v55.csv', sep=',')
beta = summary_per_city['PAA']

results = powerlaw.Fit(beta, xmin_distance = "V")
print(results.power_law.alpha)
print(results.power_law.xmin)
#R, p = results.distribution_compare('power_law', 'lognormal')

#%%
fig1 = results.plot_ccdf(linestyle = "None", marker = "P", color = "b", markersize=10)
results.power_law.plot_ccdf(ax=fig1, color="r", linestyle="--", linewidth=3, label = "powerlaw")
# resultsLN = powerlaw.Fit(beta[1:], xmin = 10**(5.8))
results.lognormal.plot_ccdf(ax=fig1, color="g", linestyle="--", linewidth=3, label = "lognormal")
#lognormal is not producing satisfying results unless Paris is removed
plt.xlabel = r"City size ($\log_{10}$-scale)"
plt.ylabel = r"CCDF ($\log_{10}$-scale)"
plt.ylim(1/700, 1.3)
plt.legend()
plt.xlabel = r"City size ($\log_{10}$-scale)"
plt.ylabel = r"CCDF ($\log_{10}$-scale)"
plt.title("City size distribution of France\n\
estimated: $\phi = {:.2f}$".format(results.power_law.alpha))
plt.savefig("tail_comparison_France.png")

#%%
Z2_Z12 = np.mean(beta**2)/np.mean(beta)**2
print("ratio of the second moment as compared to the squared first moment:{:.2f}".format(Z2_Z12))