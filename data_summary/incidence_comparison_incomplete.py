# -*- coding: utf-8 -*-
"""
Created on Tue Jan 25 20:57:00 2022

@author: alter
"""

#%%
import numpy as np
import numpy.random as npr
import matplotlib.pyplot as plt
import pandas as pd
#Analysis of the criterion of efficiency in the theoretical setting,
#depending on a and b
from Lockdown_infections import beta, NC

#With powiats distribution
# beta = pd.read_csv("beta_30_Pl_AA.csv", index_col = 0)
# beta = np.array(beta).flatten()
# NC = beta.size

# print('Choice of French Urban areas as reference for synthetic data')
#NC =input("How many cities are to be considered?\n")
NC = 1000
#Npop = input("What is the total population size?\n")
Npop = 6e7
#phi =input("What is the power-exponent in this size distribution?\n")
phi = 2.
# beta = npr.lognormal(-4.5, 3.3, size = 100*NC)
# beta = 1000 * beta[beta>10]
# beta = beta[:NC]
beta = 1 /npr.uniform(size = NC)**(1/(phi-1))
beta = Npop * beta/np.sum(beta)

pd.DataFrame(beta).to_csv("beta_simulated_PL2.csv")

#%%

beta = np.array(pd.read_csv("beta_simulated_PL2.csv"))
NC = beta.size
Nr = 400
r_0test = np.linspace(1.01, 15, Nr)
r_0test = r_0test.reshape((1, Nr, 1, 1))

Na = 40
a = np.linspace(0.5, 2.5, Na)
a = a.reshape((1, 1, Na, 1))
Nb = 20
b = np.linspace(-0.5, 0.5, Nb)
b = b.reshape((1, 1, 1, Nb))

beta_d = np.reshape(beta, (NC, 1, 1, 1))

RU = 1/NC*(np.sum(beta_d**(a+b), axis =0) + np.sqrt(np.sum(beta_d**(2*a-1),axis =0) \
                                         * np.sum(beta_d**(1+2*b),axis =0)))
KV0 = np.reshape(1/RU, (1, 1, Na, Nb))


#for the backward dynamics
bKA_U = KV0 * beta_d**(1+b)*np.sum(beta_d**(a-1), axis = 0).reshape((1, 1, Na, 1))/NC
bKB_U = KV0 * beta_d**(a)*np.sum(beta_d**(b), axis = 0).reshape((1, 1, 1, Nb))/NC

bnuA = beta_d**(a-1)/np.sum(beta_d**(a-1), axis = 0).reshape((1, 1, Na, 1))
bnuB = beta_d**(b)/np.sum(beta_d**(b), axis = 0).reshape((1, 1, 1, Nb))

#%%
RP = 1/NC*(np.sum(beta_d**(a+b+1), axis = 0) + np.sqrt(np.sum(beta_d**(2*a), axis = 0)\
                                         * np.sum(beta_d**(2+2*b), axis =0)))
pV0 = np.reshape(1/RP, (1, 1, Na, Nb))

bKA_P = pV0 * beta_d**(1+b)*np.sum(beta_d**a, axis = 0).reshape((1, 1, Na, 1))/NC
bKB_P = pV0 * beta_d**(a)*np.sum(beta_d**(1+b), axis = 0).reshape((1, 1, 1, Nb))/NC

bnuA_P = beta_d**(a)/np.sum(beta_d**(a), axis = 0).reshape((1, 1, Na, 1))
bnuB_P = beta_d**(1+b)/np.sum(beta_d**(1+b), axis = 0).reshape((1, 1, 1, Nb))

#%%
Inc_cit_U = np.zeros((Nr, Na, Nb))
Inc_pop_U = np.zeros((Nr, Na, Nb))
rho_U = 1- np.exp(- r_0test *(bKA_U + bKB_U) )
for n_it in np.arange(10):
    rho_U = 1-np.exp(- r_0test *( bKA_U *np.sum(bnuA*rho_U, axis = 0)\
                                +bKB_U* np.sum(bnuB*rho_U, axis = 0)))
Inc_cit_U = 1/NC * np.sum(rho_U, axis = 0)
Inc_pop_U = 1/np.sum(beta) * np.sum(beta_d*rho_U, axis = 0)
    
Inf_pop_U = np.sum(r_0test*KV0, axis = 0)*Inc_cit_U*NC
    
#%%
Inc_cit_P = np.zeros((Nr, Na, Nb))
Inc_pop_P = np.zeros((Nr, Na, Nb))
rho_P = 1- np.exp(- r_0test *(bKA_P + bKB_P) )
for n_it in np.arange(10):
    rho_P = 1-np.exp(- r_0test *( bKA_P *np.sum(bnuA_P*rho_P, axis = 0)\
                                +bKB_P* np.sum(bnuB_P*rho_P, axis = 0)))
Inc_cit_P = 1/NC * np.sum(rho_P, axis = 0)
Inc_pop_P = 1/np.sum(beta) * np.sum(beta_d*rho_P, axis = 0)
    
Inf_pop_P = np.sum(r_0test*pV0, axis = 0)*Inc_pop_P*np.sum(beta)


# #%%
# #reference given by a threshold at 1E-4 for R0 = 1
# Inf_pop_U_1 =  np.reshape(Inf_pop_U[0], (1, Na, Nb))
# Inf_pop_U_r = Inf_pop_U * 10**(-4)/Inf_pop_U_1 

# Inf_pop_P_1 =  np.reshape(Inf_pop_P[0], (1, Na, Nb))
# Inf_pop_P_r =  Inf_pop_P * 10**(-4)/Inf_pop_P_1 
#%%
#Q_star fitted for a good range in the values of ind_RU_star
Q_star = 0.3
ind_RU_star = np.sum((Inc_pop_U<Q_star), axis = 0, dtype = int)
ind_RU_star[ind_RU_star == Nr] = Nr-1
ind_RP_star = np.sum((Inc_pop_P<Q_star), axis = 0, dtype = int)
ind_RP_star[ind_RP_star == Nr] = Nr-1

#%%
Inc_pop_U_star = Inc_pop_U[ind_RU_star]
Inc_pop_P_star = Inc_pop_P[ind_RP_star]

#%%
Inf_U_star = np.zeros((Na, Nb))
Inf_P_star = np.zeros((Na, Nb))
for ind_a in np.arange(Na):
    for ind_b in np.arange(Nb):
        Inf_U_star[ind_a, ind_b] = Inf_pop_U[ind_RU_star[ind_a, ind_b], ind_a, ind_b]
        Inf_P_star[ind_a, ind_b] = Inf_pop_P[ind_RP_star[ind_a, ind_b], ind_a, ind_b]

#%%
Visu =np.log10(Inf_U_star/Inf_P_star)

GX, GY = np.meshgrid(a.flatten(), b.flatten(), indexing = 'ij')
plt.figure(1)
plt.clf()
plt.contourf(GX, GY, np.log10(Inf_U_star/Inf_P_star), linestyles = 'solid',\
            levels = np.linspace(-1, 1, 29), cmap=plt.colormaps["winter"] )                    
plt.title("Which strategy is most efficient (log-ratio of str (U) over (P))\n\
at a given value of isolated individuals?")
plt.xlabel("value of $a$")
plt.ylabel("value of $b$")
plt.colorbar() 
#plt.legend("Polish data")
#plt.savefig("most_efficient_strategy_v") 






#%%