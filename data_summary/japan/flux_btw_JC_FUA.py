# -*- coding: utf-8 -*-
"""
Created on Sun Jan 23 23:53:21 2022

@author: alter
"""

#%% *** Init
import numpy as np
import numpy.random as npr
import random as rd
import matplotlib.pyplot as plt
import pandas as pd
import time 
from mpl_toolkits.mplot3d import Axes3D
import scipy.sparse as sp
from scipy import stats

#version = 59
D_Name = "Japanese units"

#%% *** Extraction of the data

print('Choice of {:s} as core data'.format(D_Name))
Flux = pd.read_csv('japan_census_flows_with_population_and_distances_per_level2_bis.csv', \
sep = ";", decimal = ",", index_col =0)
Flux = Flux.loc[Flux.loc[:, "TravellerCount"]> 0]
del Flux["SourceNameJp"]
del Flux["TargetNameJp"]


#%% Correcting the population sizes per municipalities
Flux = pd.read_csv('japan_flows_with_Cpopulation_and_distances_per_level2.csv', \
sep = ",", decimal = ".", index_col =0)


Japan_Pop = pd.read_csv("2015_population_per_municipality_WF.csv",\
sep = ",", index_col =0)
Flux["Source_2015_pop"] = 0
Flux["Target_2015_pop"] = 0

Missed_Smun = {}
Missed_Tmun = {}
for i in Flux.index:
    Source_where = np.where(Japan_Pop["Municipalities"]\
        == Flux.loc[i, "SourceNameEn"])[0]
    if Source_where.size != 1:
        Missed_Smun[i] = Flux.loc[i, "SourceNameEn"]
    else:
        Flux.loc[i, "Source_2015_pop"] = Japan_Pop.iloc[Source_where[0], 2]
    Target_where = np.where(Japan_Pop["Municipalities"]\
        == Flux.loc[i, "TargetNameEn"])[0]
    if Target_where.size != 1:
        Missed_Tmun[i] = Flux.loc[i, "TargetNameEn"]
    else:
        Flux.loc[i, "Target_2015_pop"] = Japan_Pop.iloc[Target_where[0], 2]
Flux.to_csv('japan_flows_with_Cpopulation_and_distances_per_level2.csv')

#%% Analysis of discrepancies in naming (none after correcting the data)

Set_Missed_Smun = set(Missed_Smun.values())  
Set_Missed_Tmun = set(Missed_Tmun.values())  
Uncertain_source_population = 0 
Missed_pop = pd.DataFrame(Set_Missed_Smun, columns = ["NameEn"])
Missed_pop["Population_sum"] = 0
Missed_pop["Mun_sum"] = 0
for j in np.arange(Missed_pop.shape[0]):
    Name_j = (Japan_Pop["Municipalities"] == Missed_pop.iloc[j, 0])
    Missed_pop.loc[j, "Population_sum"] = np.sum(Japan_Pop.loc[Name_j, "Population"])
    Missed_pop.loc[j, "Mun_sum"] = np.sum(Name_j) 
Uncertain_source_population= np.sum(Missed_pop["Population_sum"])

Flux= pd.read_csv('japan_flows_with_Cpopulation_and_distances_per_level2.csv', index_col =0)
#%% *** Restriction on 30km distance

Flux_30 = Flux.loc[Flux.loc[:, "Distance"]> 30]
com, iCom = np.unique(Flux_30[["Source"]], return_index = True)
Com_Name, iCom2 = np.unique(Flux_30[["SourceNameEn"]], return_index = True)
#Hopefully, the same list for the different size restrictions

#Population sizes deduced from the Source_Population column
beta_30 = Flux.loc[Flux.index[iCom], "Source_2015_pop"]
beta_30 = np.copy(beta_30)
NC = np.size(beta_30)

print("The distribution of city size is now given by the array 'beta_30'. \n \
You may consider changing the working directory for the figures.")
beta_30.tofile('beta_japan_D30.csv', sep=',')


#%% * Listing of municipalities per FUA

OECD_FUA = pd.read_csv("Japanese_OECD_municipality_per_FUA_v2.csv", \
sep = ",", index_col =0)

dict_FUA = {}
OECD_mun = OECD_FUA["Mun_Name"]
OECD_FUAn = OECD_FUA["FUA"]
Flux_30.loc[:, "Source_FUA"] = np.copy(Flux_30.loc[:, "SourceNameEn"])
Flux_30.loc[:, "Target_FUA"] = np.copy(Flux_30.loc[:,"TargetNameEn"])

N_id_S = 0
N_id_D = 0
list_FUA = np.unique(OECD_FUAn)
is_FUA_found = np.zeros(list_FUA.size, dtype = bool)

for ind_FUA in np.arange(list_FUA.size):
    i = list_FUA[ind_FUA]
    dict_FUA[i] = set(OECD_mun[OECD_FUAn == i])
    for j in Flux_30.index:
        if Flux_30.loc[j, "SourceNameEn"] in dict_FUA[i]:
                Flux_30.loc[j, "Source_FUA"] = i
                N_id_S+= 1
                is_FUA_found[ind_FUA]= True
        if Flux_30.loc[j, "TargetNameEn"] in dict_FUA[i]:        
                Flux_30.loc[j, "Target_FUA"] = i
                N_id_D+= 1
                is_FUA_found[ind_FUA]= True
#1647 lines are identified out of 22170 for the Source FUA

Source_FUA, i_SFUA = np.unique(Flux_30["Source_FUA"], return_index = True)
Target_FUA, i_DFUA = np.unique(Flux_30["Target_FUA"], return_index = True)
print("Number of FUA identified :", np.sum(is_FUA_found))

Flux_30.to_csv("flux_btw_mun_units_with_FUA.csv")

#%% * Identification of the unreported OECD units

OECD_FUA.index = np.arange(OECD_FUA.index.size)
Set_SN = set(Flux_30["SourceNameEn"])
Set_DN = set(Flux_30["TargetNameEn"])
OECD_FUA["S_reported"] = False
OECD_FUA["D_reported"] = False
for j in OECD_FUA.index:
    OECD_FUA.loc[j, "S_reported"] = (OECD_mun[j] in Set_SN)
    OECD_FUA.loc[j, "D_reported"] = (OECD_mun[j] in Set_DN)

Unreported = (~OECD_FUA["S_reported"])&(~OECD_FUA["D_reported"])
UR_FUA = OECD_FUA[Unreported]

#%% * Computed Population Sizes of the FUA, D30
Flux_30 = pd.read_csv("flux_btw_mun_units_with_FUA.csv", index_col = 0)
set_listed_FUA = set(Flux_30["Source_FUA"]).intersection(set(Flux_30["Target_FUA"]))

Flux_30["Is_listed"] = np.zeros(Flux_30.index.size, dtype = bool)
for j in Flux_30.index:
    Flux_30.loc[j, "Is_listed"] = (Flux_30.loc[j, "Source_FUA"] in set_listed_FUA)\
&(Flux_30.loc[j, "Target_FUA"]in set_listed_FUA)
Flux_30L = Flux_30.loc[Flux_30["Is_listed"], ['Source', 'Target', 'TravellerCount', 'Source_2015_pop',\
        'Target_2015_pop', 'Distance', 'SourceNameEn', 'TargetNameEn',\
        'Source_FUA', 'Target_FUA']]
    

Mun_Name, iMun = np.unique(Flux_30L[["SourceNameEn"]], return_index = True)
listed_Mun = pd.DataFrame(Mun_Name, columns = ["Mun_Name"])
listed_Mun.index = Flux_30L.index[iMun]
listed_Mun["iMun"] = iMun
listed_Mun["index_Mun"] = Flux_30L.index[iMun]
listed_Mun["Population"] = Flux_30L.loc[listed_Mun["index_Mun"], 'Source_2015_pop']
listed_Mun["FUA"] = Flux_30L.loc[listed_Mun["index_Mun"], 'Source_FUA']

FUA_pop = listed_Mun.groupby(['FUA'])["Population"].sum()
FUA_pop = FUA_pop.to_frame()
FUA_pop = FUA_pop.sort_values(by = ['Population'], ascending = False)
FUA_pop["FUA"] = FUA_pop.index
FUA_pop["outflux"] = Flux_30L.groupby(['Source_FUA'])["TravellerCount"].sum()
FUA_pop["influx"] = Flux_30L.groupby(['Target_FUA'])["TravellerCount"].sum()


FUA_pop.to_csv('summary_per_Jap_FUA_D30.csv')


#%% ** Tail distribution analysis, D30
FUA_pop = pd.read_csv("summary_per_Jap_FUA_D30.csv", index_col = 0)

print("Check the figure for a sanity check that the power-law distribution is correct")
beta_s=np.sort(FUA_pop["Population"])
NC = beta_s.size
plt.plot(np.log10(beta_s), np.log(1 - np.arange(NC)/NC), "xr")
plt.title("{:s} city size distribution".format(D_Name))
plt.xlabel(r"Population size ($\log_{10}$-scale)")
plt.ylabel(r"1 - repartition function ($\log_{10}$-scale)")
plt.savefig("dist_sizes_FCom.png")

#%% ** Esimation of a, from Jpn, D30, by FUA

D_Name = "Jpn FUA, D30"

log_beta = np.log10(FUA_pop["Population"])
log_influx = np.log10(FUA_pop["influx"])
slopeA, interceptA, rA, pA, std_errA \
    = stats.linregress(log_beta, log_influx)
def myfuncA(x):
  return slopeA * x + interceptA

mymodelA = list(map(myfuncA, log_beta))

plt.plot(log_beta, log_influx, 'g+', alpha = 0.4)
plt.plot(log_beta, mymodelA, 'r', label = "fitted slope: {:.2f}".format(slopeA))
# plt.plot(log_beta, np.mean(log_influx) + log_beta - np.mean(log_beta), 'k--',\
#          label = "test: {:.2f}".format(1.))
plt.title("Estimation of a={:.2f} from influx data".format(slopeA))
plt.xlabel("Population size (${:s}$-scale)".format(r'\log_{10}'))
plt.ylabel("Influx (${:s}$-scale)".format(r'\log_{10}'))
plt.legend(title = "{:s}, R2 = {:.2f}".format(D_Name, rA))
plt.savefig("aEstimate_FUA_D{:d}_Japan.png".format(30))
plt.show() 
print("a = {:.2f}".format(slopeA))

#%% ** Esimation of b, from Jpn, D30, by FUA

D_Name = "Jpn FUA_D30"

log_beta = np.log10(FUA_pop["Population"])
log_outflux = np.log10(FUA_pop["outflux"])
slopeB, interceptB, rB, pB, std_errB \
    = stats.linregress(log_beta, log_outflux)
def myfuncB(x):
  return slopeB * x + interceptB

mymodelB = list(map(myfuncB, log_beta))

plt.plot(log_beta, log_outflux, 'g+', alpha = 0.4)
plt.plot(log_beta, mymodelB, 'r', label = "fitted slope: {:.2f}".format(slopeB))
# plt.plot(log_beta, np.mean(log_outflux) + log_beta - np.mean(log_beta), 'k--',\
#          label = "test: {:.2f}".format(1.))
plt.title("Estimation of b={:.2f} from outflux data".format(slopeB-1))
plt.xlabel("Population size (${:s}$-scale)".format(r'\log_{10}'))
plt.ylabel("Outflux (${:s}$-scale)".format(r'\log_{10}'))
plt.legend(title = "{:s}, R2 = {:.2f}".format(D_Name, rB))
plt.savefig("bEstimate_FUA_D{:d}_Japan.png".format(30))
plt.show() 
print("b = {:.2f}".format(slopeB-1))

#%% * Set of interaction links, D30

flux_btw_FUA = Flux_30L.groupby(['Source_FUA', 'Target_FUA'])["TravellerCount"].sum()
flux_btw_FUA.to_csv('flux_btw_Jap_FUA_D30.csv')

#%% ** Matrix of interactions, D30
flux_btw_FUA_df = pd.read_csv('flux_btw_Jap_FUA_D30.csv')

#Flux between units in sparse matrix form
Jp_FUA = np.array(FUA_pop["FUA.1"])
#it is not excluded that the target is not in the source list,
#then we simply forget about this connection...
M_Jp_FUA = sp.lil_matrix(np.zeros((NC, NC)))
for i in np.arange(flux_btw_FUA_df.shape[0]):
    Source = flux_btw_FUA_df.iloc[i, 0]
    Target = flux_btw_FUA_df.iloc[i, 1]
    if np.sum(Jp_FUA == Target)>0:
        iO = np.where(Jp_FUA == Source)[0][0]
        iD = np.where(Jp_FUA == Target)[0][0]
        M_Jp_FUA[iO, iD] = flux_btw_FUA_df.iloc[i, 2]
M_Jp_FUA = M_Jp_FUA.tocsc()

sp.save_npz("M_Jp_FUA_D30.npz", M_Jp_FUA)
    
#%% *** Similar study with restriction on 50km distance
    

Flux_50 = Flux.loc[Flux.loc[:, "Distance"]> 50]
com, iCom = np.unique(Flux_50[["Source"]], return_index = True)
Com_Name, iCom2 = np.unique(Flux_50[["SourceNameEn"]], return_index = True)
#Hopefully, the same list for the different size restrictions

#Population sizes deduced from the Source_Population column
beta_50 = Flux_50.iloc[iCom, 4]
beta_50 = np.copy(beta_50)
NC = np.size(beta_50)

print("The distribution of city size is now given by the array 'beta_50'. \n \
You may consider changing the working directory for the figures.")
beta_50.tofile('beta_japan_D50.csv', sep=',')

#%% * Listing of municipalities per FUA, D50

OECD_FUA = pd.read_csv("Japanese_OECD_municipality_per_FUA_v2.csv", \
sep = ",", index_col =0)

dict_FUA = {}
OECD_mun = OECD_FUA["Mun_Name"]
OECD_FUAn = OECD_FUA["FUA"]
Flux_50.loc[:, "Source_FUA"] = np.copy(Flux_50.loc[:, "SourceNameEn"])
Flux_50.loc[:, "Target_FUA"] = np.copy(Flux_50.loc[:,"TargetNameEn"])

N_id_S = 0
N_id_D = 0
list_FUA = np.unique(OECD_FUAn)
is_FUA_found = np.zeros(list_FUA.size, dtype = bool)

for ind_FUA in np.arange(list_FUA.size):
    i = list_FUA[ind_FUA]
    dict_FUA[i] = set(OECD_mun[OECD_FUAn == i])
    for j in Flux_50.index:
        if Flux_50.loc[j, "SourceNameEn"] in dict_FUA[i]:
                Flux_50.loc[j, "Source_FUA"] = i
                N_id_S+= 1
                is_FUA_found[ind_FUA]= True
        if Flux_50.loc[j, "TargetNameEn"] in dict_FUA[i]:        
                Flux_50.loc[j, "Target_FUA"] = i
                N_id_D+= 1
                is_FUA_found[ind_FUA]= True
#1647 lines are identified out of 22170 for the Source FUA

Source_FUA, i_SFUA = np.unique(Flux_50["Source_FUA"], return_index = True)
Target_FUA, i_DFUA = np.unique(Flux_50["Target_FUA"], return_index = True)
print("Number of FUA identified :", np.sum(is_FUA_found))

Flux_50.to_csv("flux_btw_mun_units_with_FUA_D50.csv")

#%% * Computed Population Sizes of the FUA, D50

set_listed_FUA = set(Flux_50["Source_FUA"]).intersection(set(Flux_50["Target_FUA"]))

Flux_50["Is_listed"] = np.zeros(Flux_50.index.size, dtype = bool)
for j in Flux_50.index:
    Flux_50.loc[j, "Is_listed"] = (Flux_50.loc[j, "Source_FUA"] in set_listed_FUA)\
&(Flux_50.loc[j, "Target_FUA"]in set_listed_FUA)
Flux_50L = Flux_50.loc[Flux_50["Is_listed"], ['Source', 'Target', 'TravellerCount', 'Source_2015_pop',\
        'Target_2015_pop', 'Distance', 'SourceNameEn', 'TargetNameEn',\
        'Source_FUA', 'Target_FUA']]
    

Mun_Name, iMun = np.unique(Flux_50L[["SourceNameEn"]], return_index = True)
listed_Mun = pd.DataFrame(Mun_Name, columns = ["Mun_Name"])
listed_Mun.index = Flux_50L.index[iMun]
listed_Mun["iMun"] = iMun
listed_Mun["index_Mun"] = Flux_50L.index[iMun]
listed_Mun["Population"] = Flux_50L.loc[listed_Mun["index_Mun"], 'Source_2015_pop']
listed_Mun["FUA"] = Flux_50L.loc[listed_Mun["index_Mun"], 'Source_FUA']

FUA_pop = listed_Mun.groupby(['FUA'])["Population"].sum()
FUA_pop = FUA_pop.to_frame()
FUA_pop = FUA_pop.sort_values(by = ['Population'], ascending = False)
FUA_pop["FUA"] = FUA_pop.index
FUA_pop["outflux"] = Flux_50L.groupby(['Source_FUA'])["TravellerCount"].sum()
FUA_pop["influx"] = Flux_50L.groupby(['Target_FUA'])["TravellerCount"].sum()


FUA_pop.to_csv('summary_per_Jap_FUA_D50.csv')


#%% ** Tail distribution analysis, D50
FUA_pop = pd.read_csv("summary_per_Jap_FUA_D50.csv", index_col = 0)

print("Check the figure for a sanity check that the power-law distribution is correct")
beta_s=np.sort(FUA_pop["Population"])
NC = beta_s.size
plt.plot(np.log10(beta_s), np.log(1 - np.arange(NC)/NC), "xr")
plt.title("{:s} city size distribution".format(D_Name))
plt.xlabel(r"Population size ($\log_{10}$-scale)")
plt.ylabel(r"1 - repartition function ($\log_{10}$-scale)")
plt.savefig("dist_sizes_JFUA_D50.png")

#%% ** Esimation of a, from Jpn, D50, by FUA

D_Name = "Jpn FUA, D50"

log_beta = np.log10(FUA_pop["Population"])
log_influx = np.log10(FUA_pop["influx"])
slopeA, interceptA, rA, pA, std_errA \
    = stats.linregress(log_beta, log_influx)
def myfuncA(x):
  return slopeA * x + interceptA

mymodelA = list(map(myfuncA, log_beta))

plt.plot(log_beta, log_influx, 'b+', alpha = 0.4)
plt.plot(log_beta, mymodelA, 'r', label = "fitted: {:.2f}".format(slopeA))
plt.plot(log_beta, np.mean(log_influx) + log_beta - np.mean(log_beta), 'k--',\
         label = "test: {:.2f}".format(1.))
plt.title("Estimation of a={:.2f} from influx".format(slopeA))
plt.xlabel("Population size (${:s}$-scale)".format(r'\log_{10}'))
plt.ylabel("Influx (${:s}$-scale)".format(r'\log_{10}'))
plt.legend(title = "{:s}, R2 = {:.2f}".format(D_Name, rA))
plt.savefig("aEstimate_FUA_D{:d}.png".format(50))
plt.show() 
print("a = {:.2f}".format(slopeA))

#%% ** Esimation of b, from Jpn, D50, by FUA

D_Name = "Jpn FUA_D50"

log_beta = np.log10(FUA_pop["Population"])
log_outflux = np.log10(FUA_pop["outflux"])
slopeB, interceptB, rB, pB, std_errB \
    = stats.linregress(log_beta, log_outflux)
def myfuncB(x):
  return slopeB * x + interceptB

mymodelB = list(map(myfuncB, log_beta))

plt.plot(log_beta, log_outflux, 'b+', alpha = 0.4)
plt.plot(log_beta, mymodelB, 'r', label = "fitted: {:.2f}".format(slopeB))
plt.plot(log_beta, np.mean(log_outflux) + log_beta - np.mean(log_beta), 'k--',\
         label = "test: {:.2f}".format(1.))
plt.title("Estimation of b={:.2f} from outflux".format(slopeB-1))
plt.xlabel("Population size (${:s}$-scale)".format(r'\log_{10}'))
plt.ylabel("Outflux (${:s}$-scale)".format(r'\log_{10}'))
plt.legend(title = "{:s}, R2 = {:.2f}".format(D_Name, rB))
plt.savefig("bEstimate_FUA_D{:d}.png".format(50))
plt.show() 
print("b = {:.2f}".format(slopeB-1))

#%% * Set of interaction links, D50

flux_btw_FUA = Flux_50L.groupby(['Source_FUA', 'Target_FUA'])["TravellerCount"].sum()
flux_btw_FUA.to_csv('flux_btw_Jap_FUA_D50.csv')

#%% ** Matrix of interactions, D50
flux_btw_FUA_df = pd.read_csv('flux_btw_Jap_FUA_D50.csv')

#Flux between units in sparse matrix form
Jp_FUA = np.array(FUA_pop["FUA.1"])
#it is not excluded that the target is not in the source list,
#then we simply forget about this connection...
M_Jp_FUA = sp.lil_matrix(np.zeros((NC, NC)))
for i in np.arange(flux_btw_FUA_df.shape[0]):
    Source = flux_btw_FUA_df.iloc[i, 0]
    Target = flux_btw_FUA_df.iloc[i, 1]
    if np.sum(Jp_FUA == Target)>0:
        iO = np.where(Jp_FUA == Source)[0][0]
        iD = np.where(Jp_FUA == Target)[0][0]
        M_Jp_FUA[iO, iD] = flux_btw_FUA_df.iloc[i, 2]
M_Jp_FUA = M_Jp_FUA.tocsc()

sp.save_npz("M_Jp_FUA_D50.npz", M_Jp_FUA)
    
#%% *** Similar study without any restriction on distance
    

Flux_00 = Flux.loc[Flux.loc[:, "Distance"]> 00]
com, iCom = np.unique(Flux_00[["Source"]], return_index = True)
Com_Name, iCom2 = np.unique(Flux_00[["SourceNameEn"]], return_index = True)
#Hopefully, the same list for the different size restrictions

#Population sizes deduced from the Source_Population column
beta_00 = Flux_00.iloc[iCom, 4]
beta_00 = np.copy(beta_00)
NC = np.size(beta_00)

print("The distribution of city size is now given by the array 'beta_00'. \n \
You may consider changing the working directory for the figures.")
beta_00.tofile('beta_japan_D00.csv', sep=',')

#%% * Listing of municipalities per FUA, D00

OECD_FUA = pd.read_csv("Japanese_OECD_municipality_per_FUA_v2.csv", \
sep = ",", index_col =0)

dict_FUA = {}
OECD_mun = OECD_FUA["Mun_Name"]
OECD_FUAn = OECD_FUA["FUA"]
Flux_00.loc[:, "Source_FUA"] = np.copy(Flux_00.loc[:, "SourceNameEn"])
Flux_00.loc[:, "Target_FUA"] = np.copy(Flux_00.loc[:,"TargetNameEn"])

N_id_S = 0
N_id_D = 0
list_FUA = np.unique(OECD_FUAn)
is_FUA_found = np.zeros(list_FUA.size, dtype = bool)

for ind_FUA in np.arange(list_FUA.size):
    i = list_FUA[ind_FUA]
    dict_FUA[i] = set(OECD_mun[OECD_FUAn == i])
    for j in Flux_00.index:
        if Flux_00.loc[j, "SourceNameEn"] in dict_FUA[i]:
                Flux_00.loc[j, "Source_FUA"] = i
                N_id_S+= 1
                is_FUA_found[ind_FUA]= True
        if Flux_00.loc[j, "TargetNameEn"] in dict_FUA[i]:        
                Flux_00.loc[j, "Target_FUA"] = i
                N_id_D+= 1
                is_FUA_found[ind_FUA]= True
#1647 lines are identified out of 22170 for the Source FUA

Source_FUA, i_SFUA = np.unique(Flux_00["Source_FUA"], return_index = True)
Target_FUA, i_DFUA = np.unique(Flux_00["Target_FUA"], return_index = True)
print("Number of FUA identified :", np.sum(is_FUA_found))

Flux_00.to_csv("flux_btw_mun_units_with_FUA_D00.csv")

#%% * Computed Population Sizes of the FUA, D00

set_listed_FUA = set(Flux_00["Source_FUA"]).intersection(set(Flux_00["Target_FUA"]))

Flux_00["Is_listed"] = np.zeros(Flux_00.index.size, dtype = bool)
for j in Flux_00.index:
    Flux_00.loc[j, "Is_listed"] = (Flux_00.loc[j, "Source_FUA"] in set_listed_FUA)\
&(Flux_00.loc[j, "Target_FUA"]in set_listed_FUA)
Flux_00L = Flux_00.loc[Flux_00["Is_listed"], ['Source', 'Target', 'TravellerCount', 'Source_2015_pop',\
        'Target_2015_pop', 'Distance', 'SourceNameEn', 'TargetNameEn',\
        'Source_FUA', 'Target_FUA']]
    

Mun_Name, iMun = np.unique(Flux_00L[["SourceNameEn"]], return_index = True)
listed_Mun = pd.DataFrame(Mun_Name, columns = ["Mun_Name"])
listed_Mun.index = Flux_00L.index[iMun]
listed_Mun["iMun"] = iMun
listed_Mun["index_Mun"] = Flux_00L.index[iMun]
listed_Mun["Population"] = Flux_00L.loc[listed_Mun["index_Mun"], 'Source_2015_pop']
listed_Mun["FUA"] = Flux_00L.loc[listed_Mun["index_Mun"], 'Source_FUA']

FUA_pop = listed_Mun.groupby(['FUA'])["Population"].sum()
FUA_pop = FUA_pop.to_frame()
FUA_pop = FUA_pop.sort_values(by = ['Population'], ascending = False)
FUA_pop["FUA"] = FUA_pop.index
FUA_pop["outflux"] = Flux_00L.groupby(['Source_FUA'])["TravellerCount"].sum()
FUA_pop["influx"] = Flux_00L.groupby(['Target_FUA'])["TravellerCount"].sum()

FUA_pop.to_csv('summary_per_Jap_FUA_D00.csv')


#%% ** Tail distribution analysis, D00
FUA_pop = pd.read_csv("summary_per_Jap_FUA_D00.csv", index_col = 0)

print("Check the figure for a sanity check that the power-law distribution is correct")
beta_s=np.sort(FUA_pop["Population"])
NC = beta_s.size
plt.plot(np.log10(beta_s), np.log(1 - np.arange(NC)/NC), "xr")
plt.title("{:s} city size distribution".format(D_Name))
plt.xlabel(r"Population size ($\log_{10}$-scale)")
plt.ylabel(r"1 - repartition function ($\log_{10}$-scale)")
plt.savefig("dist_sizes_JFUA_D00.png")

#%% ** Esimation of a, from Jpn, D00, by FUA

D_Name = "Jpn FUA, D00"

log_beta = np.log10(FUA_pop["Population"])
log_influx = np.log10(FUA_pop["influx"])
slopeA, interceptA, rA, pA, std_errA \
    = stats.linregress(log_beta, log_influx)
def myfuncA(x):
  return slopeA * x + interceptA

mymodelA = list(map(myfuncA, log_beta))

plt.plot(log_beta, log_influx, 'b+', alpha = 0.4)
plt.plot(log_beta, mymodelA, 'r', label = "fitted: {:.2f}".format(slopeA))
plt.plot(log_beta, np.mean(log_influx) + log_beta - np.mean(log_beta), 'k--',\
         label = "test: {:.2f}".format(1.))
plt.title("Estimation of a={:.2f} from influx".format(slopeA))
plt.xlabel("Population size (${:s}$-scale)".format(r'\log_{10}'))
plt.ylabel("Influx (${:s}$-scale)".format(r'\log_{10}'))
plt.legend(title = "{:s}, R2 = {:.2f}".format(D_Name, rA))
plt.savefig("aEstimate_FUA_D{:d}.png".format(00))
plt.show() 
print("a = {:.2f}".format(slopeA))

#%% ** Esimation of b, from Jpn, D00, by FUA

D_Name = "Jpn FUA_D00"

log_beta = np.log10(FUA_pop["Population"])
log_outflux = np.log10(FUA_pop["outflux"])
slopeB, interceptB, rB, pB, std_errB \
    = stats.linregress(log_beta, log_outflux)
def myfuncB(x):
  return slopeB * x + interceptB

mymodelB = list(map(myfuncB, log_beta))

plt.plot(log_beta, log_outflux, 'b+', alpha = 0.4)
plt.plot(log_beta, mymodelB, 'r', label = "fitted: {:.2f}".format(slopeB))
plt.plot(log_beta, np.mean(log_outflux) + log_beta - np.mean(log_beta), 'k--',\
         label = "test: {:.2f}".format(1.))
plt.title("Estimation of b={:.2f} from outflux".format(slopeB-1))
plt.xlabel("Population size (${:s}$-scale)".format(r'\log_{10}'))
plt.ylabel("Outflux (${:s}$-scale)".format(r'\log_{10}'))
plt.legend(title = "{:s}, R2 = {:.2f}".format(D_Name, rB))
plt.savefig("bEstimate_FUA_D{:d}.png".format(00))
plt.show() 
print("b = {:.2f}".format(slopeB-1))

#%% * Set of interaction links, D00

flux_btw_FUA = Flux_00L.groupby(['Source_FUA', 'Target_FUA'])["TravellerCount"].sum()
flux_btw_FUA.to_csv('flux_btw_Jap_FUA_D00.csv')

#%% ** Matrix of interactions, D00
flux_btw_FUA_df = pd.read_csv('flux_btw_Jap_FUA_D00.csv')

#Flux between units in sparse matrix form
Jp_FUA = np.array(FUA_pop["FUA.1"])
#it is not excluded that the target is not in the source list,
#then we simply forget about this connection...
M_Jp_FUA = sp.lil_matrix(np.zeros((NC, NC)))
for i in np.arange(flux_btw_FUA_df.shape[0]):
    Source = flux_btw_FUA_df.iloc[i, 0]
    Target = flux_btw_FUA_df.iloc[i, 1]
    if np.sum(Jp_FUA == Target)>0:
        iO = np.where(Jp_FUA == Source)[0][0]
        iD = np.where(Jp_FUA == Target)[0][0]
        M_Jp_FUA[iO, iD] = flux_btw_FUA_df.iloc[i, 2]
M_Jp_FUA = M_Jp_FUA.tocsc()

sp.save_npz("M_Jp_FUA_D00.npz", M_Jp_FUA)
   
#%% End 
