# -*- coding: utf-8 -*-
"""
Created on Sun Jan 23 23:53:21 2022

@author: alter
"""

#%% Import
import numpy as np
import numpy.random as npr
import random as rd
import matplotlib.pyplot as plt
import pandas as pd
import time 
from mpl_toolkits.mplot3d import Axes3D
import scipy.sparse as sp
from scipy import stats

D_Name = "PC, D30+"
#%% 

Com_flux = pd.read_csv("pl_attraction_areas.csv", sep = ",")
#Restriction on the distance
Com_flux_50 = Com_flux.loc[Com_flux.loc[:, "Distance"]> 50]
Com_flux_50.to_csv("Com_flux_50.csv")

Com_flux_30 = Com_flux.loc[Com_flux.loc[:, "Distance"]> 30]

Com_flux_20 = Com_flux.loc[Com_flux.loc[:, "Distance"]> 20]

#Identification of the powiets
powiets, iPow = np.unique(Com_flux_50[["From(Powiat)"]], return_index = True)
#Hopefully, the same list for the different size restrictions

#Population sizes deduced from the Source_Population column
beta = Com_flux_50.iloc[iPow, 3]
NC = np.size(beta)
print("{:d} units are referenced".format(NC)) #349


#%%
(Com_flux.iloc[iPow, 3]).to_csv("beta_D0+.csv")
F_Orig = np.zeros(NC)
F_Desti =  np.zeros(NC)
for i in np.arange(NC):
    U_or = (Com_flux.loc[:,"From(Powiat)"] == powiets[i])
    F_Orig[i] = np.sum(Com_flux[U_or].iloc[:, 5])
    U_des = (Com_flux.loc[:,"To(Powiat)"] == powiets[i])
    F_Desti[i] = np.sum(Com_flux[U_des].iloc[:, 5])
    
#Flux between powiets in sparse matrix form
MPow = sp.lil_matrix(np.zeros((NC, NC)))
for i in np.arange(Com_flux.shape[0]):
    Origin = Com_flux.iloc[i, 1]
    iO = np.where(powiets == Origin)[0][0]
    Desti = Com_flux.iloc[i, 2]
    iD = np.where(powiets == Desti)[0][0]
    MPow[iO, iD] = Com_flux.iloc[i, 5]
MPow = MPow.tocsc()

sp.save_npz("MPow_0.npz", MPow)
#%%

print("Check the figure for a sanity check that the power-law distribution is correct")
beta_s=np.sort(beta)
plt.plot(np.log10(beta_s), np.log(1 - np.arange(NC)/NC), "xr")
plt.title("Distribution of city sizes\n\
          from Polish powiets data")
plt.xlabel("log of ordered city sizes")
plt.ylabel("log of empirical repartition function")
plt.savefig("dist_sizes_Pol")

#%%
import poweRlaw
results = powerlaw.Fit(beta)
print(results.power_law.alpha)
print(results.power_law.xmin)
R, p = results.distribution_compare('power_law', 'lognormal')

#%%
F_Orig = np.zeros(NC)
F_Desti =  np.zeros(NC)
for i in np.arange(NC):
    U_or = (Com_flux_50.loc[:,"From(Powiat)"] == powiets[i])
    F_Orig[i] = np.sum(Com_flux_50[U_or].iloc[:, 5])
    U_des = (Com_flux_50.loc[:,"To(Powiat)"] == powiets[i])
    F_Desti[i] = np.sum(Com_flux_50[U_des].iloc[:, 5])
    
#For 49 units, there is no entry from people beyond 50km,

#%%
Trunc = (F_Desti>0)
beta_50 = beta[Trunc]
F_Orig_50 = F_Orig[Trunc]
F_Desti_50 = F_Desti[Trunc]

#Flux between powiets in sparse matrix form
MPow = sp.lil_matrix(np.zeros((NC, NC)))
for i in np.arange(Com_flux_50.shape[0]):
    Origin = Com_flux_50.iloc[i, 1]
    iO = np.where(powiets == Origin)[0][0]
    Desti = Com_flux_50.iloc[i, 2]
    iD = np.where(powiets == Desti)[0][0]
    MPow[iO, iD] = Com_flux_50.iloc[i, 5]
MPow = MPow[Trunc][:, Trunc]
MPow = MPow.tocsc()

sp.save_npz("MPow_50.npz", MPow)
#%%

plt.hist2d(np.log10(F_Desti_50), np.log10(F_Orig_50), bins =15, density = True)
plt.xlabel("log10 incomming")
plt.ylabel("log10 outgoing")
plt.set_cmap('Reds')
plt.colorbar()
plt.title("Correspondance between incomming\n\
and outgoing commuting levels, from Powiets, 50km")
plt.savefig("outgoing_vs_incomming_Pol.png")

#%%
plt.hist2d(np.log10(beta_50), np.log10(F_Orig_50), bins =15, density = True)
plt.xlabel("log10 Population size")
plt.ylabel("log10 outgoing")
plt.set_cmap('Reds')
plt.colorbar()
plt.title("Correspondance between population size\n\
and outgoing commuting levels, from Powiets, 50km")
plt.savefig("Pop_vs_outgoing_Pol.png")

#%%
plt.hist2d(np.log10(beta_50), np.log10(F_Desti_50), bins =15, density = True)
plt.xlabel("log10 Population size")
plt.ylabel("log10 incomming")
plt.set_cmap('Reds')
plt.colorbar()
plt.title("Correspondance between population sizes\n\
and incomming commuting levels, from Powiets, 50km")
plt.savefig("Pop_vs_incomming_Pol.png")

#%% b Estimation D50+
from scipy import stats
slopeB, interceptB, rB, pB, std_errB \
    = stats.linregress(np.log10(beta_50), np.log10(F_Orig_50))
def myfuncB(x):
  return slopeB * x + interceptB

mymodelB = list(map(myfuncB, np.log10(beta_50)))

plt.scatter(np.log10(beta_50), np.log10(F_Orig_50))
plt.plot(np.log10(beta_50), mymodelB, 'r')
plt.title("Estimation of b={:.2f} from commuting between Polish powiets\n \
          with a restriction on 50km distance" 
              .format(slopeB - 1))
plt.xlabel("Population size (log scale)")
plt.ylabel("Travels from the source (log scale)")
plt.savefig("bEstimate_D{:d}.png".format(50))
plt.show() 
print("b = {:.2f}".format(slopeB - 1))

#%% a estimation D50+
from scipy import stats
slopeA, interceptA, rA, pA, std_errA \
    = stats.linregress(np.log10(beta_50), np.log10(F_Desti_50))
def myfuncA(x):
  return slopeA * x + interceptA

mymodelA = list(map(myfuncA, np.log10(beta_50)))

plt.scatter(np.log10(beta_50), np.log10(F_Desti_50))
plt.plot(np.log10(beta_50), mymodelA, 'r')
plt.title("Estimation of a={:.2f} from commuting between Polish powiets\n \
          with a restriction on 50km distance" 
              .format(slopeA))
plt.xlabel("Population size (log scale)")
plt.ylabel("Travels from the source (log scale)")
plt.savefig("aEstimate_D{:d}.png".format(50))
plt.show() 
print("a = {:.2f}".format(slopeA))

#%%
#Robust against a change of distance?
#Test with 30km distance
F_Orig = np.zeros(NC)
F_Desti =  np.zeros(NC)
for i in np.arange(NC):
    U_or = (Com_flux_30.loc[:,"From(Powiat)"] == powiets[i])
    F_Orig[i] = np.sum(Com_flux_30[U_or].iloc[:, 5])
    U_des = (Com_flux_30.loc[:,"To(Powiat)"] == powiets[i])
    F_Desti[i] = np.sum(Com_flux_30[U_des].iloc[:, 5])
    

#%%
Trunc = (F_Desti>0)
beta_30 = beta[Trunc]
F_Orig_30 = F_Orig[Trunc]
F_Desti_30 = F_Desti[Trunc]
#%%

plt.hist2d(np.log10(F_Desti_30), np.log10(F_Orig_30), bins =20, density = True)
plt.xlabel("log10 incomming")
plt.ylabel("log10 outgoing")
plt.set_cmap('Reds')
plt.colorbar()
plt.title("Correspondance between incomming\n\
and outgoing commuting levels, from Powiets, 30km")
plt.savefig("outgoing_vs_incomming_Pol.png")

#%%
plt.hist2d(np.log10(beta_30), np.log10(F_Orig_30), bins =20, density = True)
plt.xlabel("log10 Population size")
plt.ylabel("log10 outgoing")
plt.set_cmap('Reds')
plt.colorbar()
plt.title("Correspondance between population size\n\
and outgoing commuting levels, from Powiets, 30km")
plt.savefig("Pop_vs_outgoing_Pol.png")

#%%
plt.hist2d(np.log10(beta_30), np.log10(F_Desti_30), bins =20, density = True)
plt.xlabel("log10 Population size")
plt.ylabel("log10 incomming")
plt.set_cmap('Reds')
plt.colorbar()
plt.title("Correspondance between population sizes\n\
and incomming commuting levels, from Powiets, 30km")
plt.savefig("Pop_vs_incomming_Pol.png")

#%% b estimate D30+
log_beta = np.log10(beta_30)

from scipy import stats
slopeB, interceptB, rB, pB, std_errB \
    = stats.linregress(log_beta, np.log10(F_Orig_30))
def myfuncB(x):
  return slopeB * x + interceptB

mymodelB = list(map(myfuncB, np.log10(beta_30)))


plt.plot(log_beta, np.log10(F_Orig_30), 'r+', alpha = 0.6)
plt.plot(log_beta, mymodelB, 'r', label = "fitted slope: {:.2f}".format(slopeB))
# plt.plot(log_beta, np.mean(log_outflux) + log_beta - np.mean(log_beta), 'k--',\
#          label = "test: {:.2f}".format(1.))
plt.title("Estimation of b={:.2f} from outflux data".format(slopeB-1))
plt.xlabel("Population size (${:s}$-scale)".format(r'\log_{10}'))
plt.ylabel("Outflux (${:s}$-scale)".format(r'\log_{10}'))
plt.legend(title = "{:s}, R2 = {:.2f}".format(D_Name, rB))
plt.savefig("bEstimate_D{:d}_Poland.png".format(30))
plt.show() 
print("b = {:.2f}".format(slopeB - 1))

#%% a estimate D30+
from scipy import stats
slopeA, interceptA, rA, pA, std_errA \
    = stats.linregress(np.log10(beta_30), np.log10(F_Desti_30))
def myfuncA(x):
  return slopeA * x + interceptA

mymodelA = list(map(myfuncA, np.log10(beta_30)))


plt.plot(log_beta,  np.log10(F_Desti_30), 'r+', alpha = 0.6)
plt.plot(log_beta, mymodelA, 'r', label = "fitted slope: {:.2f}".format(slopeA))
# plt.plot(log_beta, np.mean(log_influx) + log_beta - np.mean(log_beta), 'k--',\
#          label = "test: {:.2f}".format(1.))
plt.title("Estimation of a={:.2f} from influx data".format(slopeA))
plt.xlabel("Population size (${:s}$-scale)".format(r'\log_{10}'))
plt.ylabel("Influx (${:s}$-scale)".format(r'\log_{10}'))
plt.legend(title = "{:s}, R2 = {:.2f}".format(D_Name, rA))
plt.savefig("aEstimate_D{:d}_Poland.png".format(30))
plt.show() 
print("a = {:.2f}".format(slopeA))


#%%
#Robust against a change of distance?
#Test with 20km distance
F_Orig = np.zeros(NC)
F_Desti =  np.zeros(NC)
for i in np.arange(NC):
    U_or = (Com_flux_20.loc[:,"From(Powiat)"] == powiets[i])
    F_Orig[i] = np.sum(Com_flux_20[U_or].iloc[:, 5])
    U_des = (Com_flux_20.loc[:,"To(Powiat)"] == powiets[i])
    F_Desti[i] = np.sum(Com_flux_20[U_des].iloc[:, 5])
    

#%%
Trunc = (F_Desti>0)
beta_20 = beta[Trunc]
F_Orig_20 = F_Orig[Trunc]
F_Desti_20 = F_Desti[Trunc]
#%%

plt.hist2d(np.log10(F_Desti_20), np.log10(F_Orig_20), bins =20, density = True)
plt.xlabel("log10 incomming")
plt.ylabel("log10 outgoing")
plt.set_cmap('Reds')
plt.colorbar()
plt.title("Correspondance between incomming\n\
and outgoing commuting levels, from Powiets, 20km")
plt.savefig("outgoing_vs_incomming_Pol.png")

#%%
plt.hist2d(np.log10(beta_20), np.log10(F_Orig_20), bins =20, density = True)
plt.xlabel("log10 Population size")
plt.ylabel("log10 outgoing")
plt.set_cmap('Reds')
plt.colorbar()
plt.title("Correspondance between population size\n\
and outgoing commuting levels, from Powiets, 20km")
plt.savefig("Pop_vs_outgoing_Pol.png")

#%%
plt.hist2d(np.log10(beta_20), np.log10(F_Desti_20), bins =20, density = True)
plt.xlabel("log10 Population size")
plt.ylabel("log10 incomming")
plt.set_cmap('Reds')
plt.colorbar()
plt.title("Correspondance between population sizes\n\
and incomming commuting levels, from Powiets, 20km")
plt.savefig("Pop_vs_incomming_Pol.png")
#%%

from scipy import stats
slopeB, interceptB, rB, pB, std_errB \
    = stats.linregress(np.log10(beta_20), np.log10(F_Orig_20))
def myfuncB(x):
  return slopeB * x + interceptB

mymodelB = list(map(myfuncB, np.log10(beta_20)))

plt.scatter(np.log10(beta_20), np.log10(F_Orig_20))
plt.plot(np.log10(beta_20), mymodelB, 'r')
plt.title("Estimation of b={:.2f} from commuting between Polish powiets\n \
          with a restriction on 20km distance" 
              .format(slopeB - 1))
plt.xlabel("Population size (log scale)")
plt.ylabel("Travels from the source (log scale)")
plt.savefig("bEstimate_D{:d}.png".format(20))
plt.show() 
print("b = {:.2f}".format(slopeB - 1))

#%%
from scipy import stats
slopeA, interceptA, rA, pA, std_errA \
    = stats.linregress(np.log10(beta_20), np.log10(F_Desti_20))
def myfuncA(x):
  return slopeA * x + interceptA

mymodelA = list(map(myfuncA, np.log10(beta_20)))

plt.scatter(np.log10(beta_20), np.log10(F_Desti_20))
plt.plot(np.log10(beta_20), mymodelA, 'r')
plt.title("Estimation of a={:.2f} from commuting between Polish powiets\n \
          with a restriction on 20km distance" 
              .format(slopeA))
plt.xlabel("Population size (log scale)")
plt.ylabel("Travels from the source (log scale)")
plt.savefig("aEstimate_D{:d}.png".format(20))
plt.show() 
print("a = {:.2f}".format(slopeA))

#%%
beta_50.to_csv('beta_50_Pow.csv')
beta_30.to_csv('beta_30_Pow.csv')
beta_20.to_csv('beta_20_Pow.csv')

#%%
#Behavior of a and b with the distance restriction
Com_flux = pd.read_csv("pl_attraction_areas.csv", sep = ",")
#Identification of the powiets
powiets, iPow = np.unique(Com_flux[["From(Powiat)"]], return_index = True)
#Hopefully, the same list for the different size restrictions
#Population sizes deduced from the Source_Population column
beta = Com_flux.iloc[iPow, 3]
NC = np.size(beta)

GD = np.arange(0, 90, 3)
Par_Pow = np.zeros((GD.size, 4))
Par_Pow = pd.DataFrame(Par_Pow, columns = ['a', 'b', 'std_a', 'std_b'])
for k in np.arange(GD.size):
    D = GD[k]
    #Restriction on the distance
    Com_flux_D = Com_flux.loc[Com_flux.loc[:, "Distance"]> D]
    Powiat_D = pd.DataFrame(beta)
    Powiat_D[["F_Orig"]] = 0
    Powiat_D[["F_Desti"]] = 0
    Name_D = np.array(Com_flux.iloc[iPow, 1])
    Powiat_D.loc[:, "Name"] = Name_D
    for i in np.arange(NC):
        U_or = (Com_flux_D.loc[:,"From(Powiat)"] == powiets[i])
        #F_orig
        Powiat_D.iloc[i, 1] = np.sum(Com_flux_D[U_or].iloc[:, 5])
        U_des = (Com_flux_D.loc[:,"To(Powiat)"] == powiets[i])
        #F_desti
        Powiat_D.iloc[i, 2] = np.sum(Com_flux_D[U_des].iloc[:, 5])
    Trunc = (Powiat_D.loc[:,"F_Orig"]>0)&(Powiat_D.loc[:, "F_Desti"]>0)
    Powiat_D = Powiat_D[Trunc]
    Powiat_D.to_csv("Powiat_{:d}.csv".format(D))
    beta_D = Powiat_D.iloc[:, 0]
    F_Orig_D = Powiat_D.iloc[:, 1]
    F_Desti_D = Powiat_D.iloc[:, 2]
    slopeA, interceptA, rA, pA, std_errA \
        = stats.linregress(np.log10(beta_D), np.log10(F_Desti_D))
    slopeB, interceptB, rB, pB, std_errB \
        = stats.linregress(np.log10(beta_D), np.log10(F_Orig_D))
    Par_Pow.loc[k, :] = np.array([slopeA, (slopeB-1), std_errA, std_errB])        
            
    def myfuncB(x):
      return slopeB * x + interceptB

    mymodelB = list(map(myfuncB, np.log10(beta_D)))

    plt.scatter(np.log10(beta_D), np.log10(F_Orig_D))
    plt.plot(np.log10(beta_D), mymodelB, 'r')
    plt.title("Estimation of b={:.2f} from commuting between Polish powiets\n \
              with a restriction on {:d}km distance" 
                  .format(slopeB - 1, D))
    plt.xlabel("Population size (log scale)")
    plt.ylabel("Travels from the source (log scale)")
    plt.savefig("bEstimate_D{:d}.png".format(D))
    plt.show() 
    print("For D_min = {:d}km, b = {:.2f}".format(D, slopeB - 1))

    def myfuncA(x):
      return slopeA * x + interceptA

    mymodelA = list(map(myfuncA, np.log10(beta_D)))

    plt.scatter(np.log10(beta_D), np.log10(F_Desti_D))
    plt.plot(np.log10(beta_D), mymodelA, 'r')
    plt.title("Estimation of a={:.2f} from commuting between Polish powiets\n \
              with a restriction on {:d}km distance" 
                  .format(slopeA, D))
    plt.xlabel("Population size (log scale)")
    plt.ylabel("Travels from the source (log scale)")
    plt.savefig("aEstimate_D{:d}.png".format(D))
    plt.show() 
    print("For D_min = {:d}km, a = {:.2f}".format(D, slopeA))

plt.plot(GD, Par_Pow.loc[:,"a"], "r+:", label = "slope estimate of a")
plt.vlines(GD, \
          ymin = Par_Pow.loc[:, "a"]-Par_Pow.loc[:,"std_a"]/np.mean(beta_50),\
        ymax = Par_Pow.loc[:,"a"]+Par_Pow.loc[:,"std_a"]/np.mean(beta_50),\
        colors = "purple", label = "slope@std/meanX")
plt.title("Estimation of a for varying distances of restrictions")
plt.xlabel("Distance on which the restriction is taken")
plt.ylabel("Value of a")
plt.legend()
plt.savefig("a_varying_Dmin.png")
plt.show()

plt.plot(GD, Par_Pow.loc[:,"b"], "r+:", label = "slope estimate of b")
plt.vlines(GD, \
          ymin = Par_Pow.loc[:,"b"]-Par_Pow.loc[:,"std_b"]/np.mean(beta_50),\
        ymax = Par_Pow.loc[:,"b"]+Par_Pow.loc[:,"std_b"]/np.mean(beta_50),\
        colors = "purple", label = "slope@std/meanX")
plt.title("Estimation of b for varying distances of restrictions")
plt.xlabel("Distance on which the restriction is taken")
plt.ylabel("Value of b")
plt.legend()
plt.savefig("b_varying_Dmin.png")

#%%
plt.plot(GD, Par_Pow.loc[:,"a"], "r+:", label = "slope estimate of a")
plt.vlines(GD, \
          ymin = Par_Pow.loc[:, "a"]-Par_Pow.loc[:,"std_a"]/np.mean(np.log10(beta_50)),\
        ymax = Par_Pow.loc[:,"a"]+Par_Pow.loc[:,"std_a"]/np.mean(np.log10(beta_50)),\
        colors = "purple", label = "slope@std/meanX")
plt.title("Estimation of a for varying distances of restrictions")
plt.xlabel("Distance on which the restriction is taken")
plt.ylabel("Value of a")
plt.legend()
plt.savefig("a_varying_Dmin_var.png")
plt.show()

plt.plot(GD, Par_Pow.loc[:,"b"], "r+:", label = "slope estimate of b")
plt.vlines(GD, \
          ymin = Par_Pow.loc[:,"b"]-Par_Pow.loc[:,"std_b"]/np.mean(np.log10(beta_50)),\
        ymax = Par_Pow.loc[:,"b"]+Par_Pow.loc[:,"std_b"]/np.mean(np.log10(beta_50)),\
        colors = "purple", label = "slope@std/meanX")
plt.title("Estimation of b for varying distances of restrictions")
plt.xlabel("Distance on which the restriction is taken")
plt.ylabel("Value of b")
plt.legend()
plt.savefig("b_varying_Dmin_var.png")
#%%