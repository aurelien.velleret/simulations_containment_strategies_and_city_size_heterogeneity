#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  2 12:06:32 2024

@author: avelleret
"""
#%% Import

import powerlaw
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

beta = pd.read_csv("beta_30_Pl_AA.csv", index_col = 0)
beta = np.array(beta).flatten()

results = powerlaw.Fit(beta, xmin_distance = "V")
print(results.power_law.alpha)
print(results.power_law.xmin)
#R, p = results.distribution_compare('power_law', 'lognormal')

#%%
fig1 = results.plot_ccdf(linestyle = "None", marker = "P", color = "r", markersize=10)
results.power_law.plot_ccdf(ax=fig1, color="r", linestyle="--", linewidth=3, label = "powerlaw")
# resultsLN = powerlaw.Fit(beta[beta<10**6])
results.lognormal.plot_ccdf(ax=fig1, color="g", linestyle="--", linewidth=3, label = "lognormal")
plt.xlabel = r"City size ($\log_{10}$-scale)"
plt.ylabel = r"CCDF ($\log_{10}$-scale)"
plt.legend()
plt.xlabel = r"City size ($\log_{10}$-scale)"
plt.ylabel = r"CCDF ($\log_{10}$-scale)"
plt.title("City size distribution of Poland\n\
estimated: $\phi = {:.2f}$".format(results.power_law.alpha))
plt.savefig("tail_comparison_Poland.png")

#%%
Z2_Z12 = np.mean(beta**2)/np.mean(beta)**2
print("ratio of the second moment as compared to the squared first moment:{:.2f}".format(Z2_Z12))
